<?php

namespace Tests\Feature;

use App\Domain\AcfValueExtractor;
use App\Domain\ContentFieldsStore;
use App\Domain\DynamicContentSheet;
use Illuminate\Support\Collection;
use Tests\TestCase;

/**
 * @author Cyrill Tekord
 */
class ContentFieldsStoreTest extends TestCase {

	public function testCfs() {
		$m1 = collect();
		$m2 = collect();

		$m1->put('vote', '555');
		$m1->put('vote_old', '111');

		$m2->put('vote', '777');

		$store = new ContentFieldsStore();
		$store->merge($m1);
		$store->merge($m2);

		$this->assertEquals('777', $store->get('vote'));
		$this->assertEquals('111', $store->get('vote_old'));
	}

	public function testDynamicContentSheetFilter() {
		$valueExtractor = new AcfValueExtractor();

		$valueExtractor->registerFilterRules([
			'promoBox_*' => function (Collection $collection, $key, $value) {
				return $collection->get('promoBox_enabled') == 1;
			},
			'chunk_*' => function (Collection $collection, $key, $value) {
				return $collection->get('chunk_title') !== null;
			},
			'likbez_0*' => function (Collection $collection, $key, $value) {
				return true;
			},
			'likbez_1*' => function (Collection $collection, $key, $value) {
				return true;
			},
		]);

		$valueExtractor->setGroupPrefixesFromFilter();

		$sheet = new DynamicContentSheet(new ContentFieldsStore(), $valueExtractor);

		$c1 = collect([
			'vote' => '10',
			'visitors' => '120',
			'promoBox_enabled' => true,
			'promoBox_url' => 'http://example.com',
			'promoBox_tabs_0_title' => 'Tab 1',
			'chunk_title' => 'This is a test!',
			'chunk_description' => 'Lorem ipsum',
			'chunk_kind' => 'Cat',
			'likbez_0_items_0_title' => 'Title 1',
			'likbez_0_items_0_text' => 'Lorem ipsum 1',
			'likbez_0_items_1_title' => 'Title 2',
			'likbez_0_items_1_text' => 'Lorem ipsum 2',
		]);

		$c2 = collect([
			'vote' => '5',
			'promoBox_enabled' => false,
			'promoBox_url' => 'localhost',
			'promoBox_tabs_0_title' => 'Tab X',
			'promoBox_tabs_1_title' => 'Tab Y',
			'greetingsText' => 'Welcome!',
			'chunk_title' => 'This is a test!',
			// Нет поля 'chunk_description'
			'chunk_kind' => 'Cat',
			'likbez_0_items_0_title' => 'C1 LIKBEZ Title',
			'likbez_0_items_0_text' => 'C1 LIKBEZ Text',
		]);

		$sheet->mergeFields([$c1, $c2]);

		$this->assertEquals($sheet->getCfs()->get('vote'), '5');
		$this->assertEquals($sheet->getCfs()->get('visitors'), '120');
		$this->assertEquals($sheet->getCfs()->get('greetingsText'), 'Welcome!');
		$this->assertEquals($sheet->getCfs()->get('promoBox_url'), 'http://example.com');
		$this->assertEquals($sheet->getCfs()->get('promoBox_tabs_0_title'), 'Tab 1');
		$this->assertNull($sheet->getCfs()->get('chunk_description'));
	}

	public function testDynamicContentSheetMapper() {
		$valueExtractor = new AcfValueExtractor();

		$valueExtractor->registerMappingRules([
			'someFlag' => function ($key, $value) {
				return is_bool($value) ? 'YES' : 'no';
			},
			'vote' => function ($key, $value) {
				return $value * 100;
			}
		]);

		$sheet = new DynamicContentSheet(new ContentFieldsStore(), $valueExtractor);

		$c1 = collect([
			'vote' => '10',
			'visitors' => '120',
			'someFlag' => true,
		]);

		$sheet->mergeFields([$c1]);

		$this->assertEquals($sheet->getCfs()->get('vote'), 1000);
		$this->assertEquals($sheet->getCfs()->get('visitors'), '120');
		$this->assertEquals($sheet->getCfs()->get('someFlag'), 'YES');
	}
}
