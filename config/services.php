<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, SparkPost and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'google_analytics' => [
		'tag_id' => env('GOOGLE_ANALYTICS_TAG_ID'),
		'tag_manager_id' => env('GOOGLE_TAG_MANAGER_ID'),
	],

	'yandex' => [
		'metrika_counter_id' => env('YANDEX_METRIKA_COUNTER_ID'),
		'site_search_id' => env('YANDEX_SITE_SEARCH_ID'),
	],

	'mango' => [
		'id' => env('MANGO_ID'),
	],

	'vk' => [
		'pixel_id' => env('VK_PIXEL_ID')
	],

	'facebook' => [
		'pixel_id' => env('FACEBOOK_PIXEL_ID')
	],

	'envybox' => [
		'id' => env('ENVYBOX_ID'),
	],

	'smsint' => [
		'login' => env('SMSINT_LOGIN'),
		'password' => env('SMSINT_PASSWORD'),
		'sender' => env('SMSINT_SENDER'),
	]
];
