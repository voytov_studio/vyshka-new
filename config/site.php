<?php

return [
	'header_logo_uri' => 'images/logo.svg',
	'header_logo_html_attributes' => [
		'width' => '182',
		'height' => '31'
	],
	'footer_logo_uri' => 'images/logo-footer.svg',
	'company_name' => 'ВЫСШАЯ ИНСТАНЦИЯ',
	'company_type' => 'юридический консорциум',
	'slogan' => 'Специализированный<br/>юридический консорциум №1',
	'copyright' => 'ООО «Юридический консорциум ВЫСШАЯ ИНСТАНЦИЯ»',

	'street' => 'ул. Шпалерная 36',
	'street_short' => 'ул. Шпалерная 36',
	'metro_station' => 'Чернышевская',

	'show_business_credentials' => true,
	'inn' => '7814679200',
	'kpp' => '781401001',
	'ogrn' => '1177847038203',

	'ot_kogo' => '«Высшей Инстанции»',
];
