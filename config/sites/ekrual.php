<?php

return [
	'app.settings.contact_email' => 'sos@accrual-law.ru',
	'app.settings.contacts_via_sms.message' => 'ЮК ЭКРУАЛ. Тел: 8 (812) 648-28-81. E-mail: sos@accrual-law.ru. ул.Шпалерная 36, БЦ Goldex 2 этаж, офис 206. Метро Чернышевская',

	'app.settings.phones' => [
		'main' => [
			'display' => '8 (812) 648-28-81',
			'value' => '+78126482881',
		],
	],

	'site' => [
		'title' => 'ЭКРУАЛ',
		'header_logo_uri' => 'images/accrual-logo.svg',
		'header_logo_html_attributes' => [
			'width' => '90',
			'height' => '31',
		],
		'footer_logo_uri' => 'images/accrual-logo-footer.svg',
		'company_name' => 'ЭКРУАЛ',
		'company_type' => 'юридическая коллегия',
		'slogan' => 'юридическая<br/>коллегия',
		'copyright' => 'ООО «Юридическая коллегия ЭКРУАЛ»',

		'street' => 'Лиговский проспект 21, БЦ',
		'street_short' => 'Лиговский пр. 21, БЦ',
		'metro_station' => 'Площадь Восстания',

		'show_business_credentials' => false,

		'ot_kogo' => '«ЭКРУАЛ»',
	],
];
