<?php

return [
	'site_id' => env('SITE_ID'),

	'configuration_file' => __DIR__ . '/sites/' . env('SITE_ID') . '.php',

	'wordpress_integration' => [
		'key' => env('WORDPRESS_INTEGRATION_KEY'),
		'cipher' => 'AES-256-CBC',

		'edit_post_url_template' => '/wordpress/wp-admin/post.php?post={{pageId}}&action=edit'
	],

	'crawler' => [
		'control_panel_url' => env('APP_CRAWLER_CONTROL_PANEL_URL')
	],

	'enable_widgets' => env('APP_ENABLE_WIDGETS', true),

	'api_base_url' => env('APP_API_BASE_URL'),

	'base_content_url' => env('APP_BASE_CONTENT_URL'),

	'postDataCachePrefix' => 'posts',

	'content_sources' => [
		'default_data_page' => 8725,

		'menu' => 10640,
		'main' => 29,
		'cases' => 17608,
		'mass_media' => 15795,
		'certificates' => 18,
		'reviews' => 15554,

		'service_prices' => [
			1  => 13533,
			2  => 13563,
			3  => 13323,
			4  => 13573,
			5  => 18808,
			6  => 13578,
			7  => 13580,
			8  => 13582,
			9  => 13584,
			10 => 13586,
			11 => 13588,
			12 => 13590,
			13 => 13592,
			14 => 13594,
			15 => 13596,
			16 => 13958, // Общая
			17 => 14857,
			21 => 19125,
		]
	],

	'root_blog_category_id' => 67,

	'phones' => [
		'federal' => [
			'display' => '8 (800) 500-66-96',
			'value' => '+78005006696'
		],
		'main' => [
			'display' => '8 (812) 748-23-61',
			'value' => '+78127482361'
		],
	],

	'contact_email' => 'sos@7482361.ru',

	'contacts_via_sms' => [
		'message' => 'ЮЦ ВЫСШАЯ ИНСТАНЦИЯ. Тел: 8 (812) 748-23-61. E-mail: sos@7482361.ru. ул.Шпалерная 36, БЦ Goldex 2 этаж, офис 206. Метро Чернышевская'
	],

	'social_links' => [
		'youtube' => 'https://www.youtube.com/channel/UCmOMH98FklXrK7TwUVMkQlA/videos',
		'instagram' => 'https://www.instagram.com/jur.nirvana/',
		'vk' => 'https://vk.com/vysshaja_instancija',
		'facebook' => null,
		'whatsapp' => 'https://api.whatsapp.com/send?phone=+79117156696',
		'telegram' => 'tg://resolve?domain=Urcontrol_bot'
	],

	'general' => [
		'developer_name' => 'Developer Name',
		'developer_url' => '//example.com',
		'copyright_holder' => 'Copyright Holder'
	]
];
