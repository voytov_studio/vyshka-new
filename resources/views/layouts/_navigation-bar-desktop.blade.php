<!-- Desktop Navigation Bar -->
<div class="top-nav-wrap">
    <div class="main-drop">
        <ul class="top-nav">
            <li class="top-nav__item">
                <a href="javascript:" class="top-nav__link has-drop">
					<span>Услуги</span>
				</a>
				@include("chunks._navbar-drop-specializations")
            </li>
            <li class="top-nav__item">
                <a href="{!! url('contacts') !!}" class="top-nav__link">
                    <span>Контакты</span>
                </a>
            </li>
        </ul>
        <div class="top-nav-overlay js-top-nav-close"></div>
    </div>
</div>
<!-- /Desktop Navigation Bar -->
