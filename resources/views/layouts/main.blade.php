<?php

$config = [
    'app_name' => config('app.name'),
    'locale' => $locale = app()->getLocale(),
    'api_base_url' => config('app.settings.api_base_url'),
    'api_content_loader_url' => route('api.contentLoader'),
    'yandex_metrika_counter_id' => config('services.yandex.metrika_counter_id'),
];

$title = $__env->yieldContent('title');

if (!empty($title))
    $title .= ' — ';

$title .= config('app.name');

?>

@include("layouts._external")

<!DOCTYPE html>
<html lang="{!! $locale !!}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{!! $title !!}</title>

    <link href="images/favicon.png" rel="shortcut icon" type="image/x-icon" />

    <link rel="stylesheet" href="{!! mix('/build/vendor.css') !!}">
    <link rel="stylesheet" href="{!! mix('/build/theme.css') !!}">
    <link rel="stylesheet" href="{!! mix('/build/main.css') !!}">

    @stack('head')
</head>
<body>

@stack('body_begin')

<div class="page">
    <div class="wrapper">
        @include("layouts._main-header")

        @yield('content')

        @include("chunks._main-contacts")
    </div>

    <!-- Maintenance -->
    @admin
        @include("maintenance.panel")
    @endadmin

    @include("layouts._main-footer")
</div>

@include("layouts._widgets")

{{-- Global configuration object --}}
<script>window.config = @json($config);</script>

<script src="{!! mix('/build/manifest.js') !!}"></script>
<script src="{!! mix('/build/vendor.js') !!}"></script>
<script src="{!! mix('/build/app.js') !!}"></script>

<script src="{!! mix('/build/theme.js') !!}"></script>

@stack('scripts')

</body>
</html>
