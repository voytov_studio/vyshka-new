<?php

/**
 * @var \App\Data\SpecializationMenuItemDto[]|\Illuminate\Support\Collection $specializationsMenuItems
 */

$siteDataProvider = app(\App\Domain\SiteDataProvider::class);

?>
<div class="c-footer-specialization specialization">
    <div class="container">
        <h2>Мы также специализируемся</h2>

        <div class="specialization__header row d-sm-none d-md-flex">
            <div class="col-12 col-md">
                <div class="sort-search">
                    <form action="#">
                        <input type="text" class="text js-search-service-input" placeholder="Начните набирать название...">
                    </form>
                </div>
            </div>
            <div class="col-12 col-md-4 col-lg-3">
                <div class="keywords-search">
                    <div class="keywords-search__row">
                        @include("_shared._yandex-search-form")
                    </div>
                </div>
            </div>
        </div>

        <ul class="specialization-list">
            @foreach($specializationsMenuItems as $index => $i)
                <li>
                    @component('chunks._specialization-link', ['route' => $i->url])
                        <span class="icon {{ $i->icon }}"></span>
                        <div class="text-box">
                            <span class="text">{{ $i->name }}</span>
                            <span class="number">({{ $i->counter }})</span>
                        </div>
                    @endcomponent
                </li>
            @endforeach
        </ul>
    </div>
</div>

<!-- Footer -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-5">
                <a href="javascript:" class="footer-logo">
                    <span class="footer-logo__icon">
                        @if(config('site.footer_logo_uri'))
                            {!! html()->img()->attributes(array_merge([
                                'class' => 'lazy',
                                'data-src' => config('site.footer_logo_uri'),
                                'alt' => config('site.company_name'),
                            ], config('site.header_logo_html_attributes', []))) !!}
                        @else
                            {!! config('site.title') !!}
                        @endif
                    </span>
                </a>
                <span class="copy">&copy; {{ date('Y') }} {!! config('site.copyright') !!}</span>

                @if(config('site.show_business_credentials'))
                    <ul class="company-copy-info">
                        <li>ИНН {!! config('site.inn') !!}</li>
                        <li>КПП {!! config('site.kpp') !!}</li>
                        <li>ОГРН {!! config('site.ogrn') !!}</li>
                    </ul>
                @endif
            </div>
            <div class="col-12 col-md-7">
                <a href="#privacy-policy-modal" class="privacy fancybox">Политика конфиденциальности</a>
                <p>
                    Все права защищены. Использование материалов возможно только с письменного разрешения администрации.
                    Цены, указанные на сайте, не являются публичной офертой.
                </p>
            </div>
        </div>
    </div>
</footer>
<!-- Footer end -->

<div style="display:none;" content="js-mango-number" id="contacts-to-clipboard">{!! $siteDataProvider->getPhoneEntry('main')->display !!}</div>

<!-- Message Box -->
<div class="thank_form" id="user-alert-box">
    <div class="thank_form_text">...</div>
    <div class="rating-info__popup_close js-reviews-close thank_close"></div>
</div>

<div class="over_all"></div>
<!-- /Message Box -->

@include("layouts._modals")
