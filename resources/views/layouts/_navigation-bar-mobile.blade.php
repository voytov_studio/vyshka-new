<?php

/**
 * @var \App\Data\SpecializationMenuItemDto[]|\Illuminate\Support\Collection $specializationsMenuItems
 */

$siteDataProvider = app(\App\Domain\SiteDataProvider::class);

?>
<div class="mobile-nav-wrap">
    <a href="javascript:" class="main-nav-opener"><span></span></a>
    <div class="mobile-nav-drop">
        <ul class="mobile-nav">
            <li class="mobile-nav__item"><a href="javascript:" class="mobile-nav__link" data-main-nav="1">Услуги</a></li>

            @if(false)
            <li class="mobile-nav__item"><a href="#mass-modal" class="mobile-nav__link fancybox">Мы в СМИ</a></li>
            @endif

            <li class="mobile-nav__item"><a href="{!! route('site.contacts') !!}" class="mobile-nav__link">Контакты</a></li>
        </ul>

        @include("chunks._contacts-compact")
    </div>

    <div class="js-mobile-navigation-drop mobile-nav-first" data-navigation-id="1">
        <a href="javascript:" class="btn-back">Услуги</a>
        <ul class="mobile-nav-first__list">
            @foreach ($specializationsMenuItems as $c)
                <li>
                    @component('chunks._specialization-link', ['route' => $c->url])
                        <span class="title">
                            {!! str_replace('право', '<em>право</em>', $c->name) !!}
                        </span>
                    @endcomponent
                </li>
            @endforeach
        </ul>
        <div class="mobile-nav-footer">
            <div class="contact-info">
                <span class="contact-info__question">Не нашли нужную услугу?</span>
                <p>Оставьте номер телефона.<br/>Наш специалист проконсультирует вас бесплатно!</p>
            </div>
            <div class="callback-form">
                @include("_shared._call-me-form-variant-5")
            </div>
        </div>
    </div>
    <div class="js-mobile-navigation-drop mobile-nav-first" data-navigation-id="2">
        <a href="javascript:" class="btn-back">Услуги</a>
        <ul class="mobile-nav-first__list">
            @foreach ($servicesMenuItems as $index => $c)
                <li>
                    @component('chunks._service-link', ['id' => $c['name']])
                        <span class="title">
                            {!! str_replace('право', '<em>право</em>', $c['name']) !!}
                        </span>
                    @endcomponent
                </li>
            @endforeach
        </ul>
        <div class="mobile-nav-footer">
            <div class="contact-info">
                <span class="contact-info__question">Не нашли нужную услугу?</span>
                <p>Оставьте номер телефона.<br/>Наш специалист проконсультирует вас бесплатно!</p>
            </div>
            <div class="callback-form">
                @include("_shared._call-me-form-variant-5")
            </div>
        </div>
    </div>
</div>
