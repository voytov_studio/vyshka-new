<?php

$config = [
	'appName' => config('app.name'),
	'locale' => $locale = app()->getLocale()
];

$title = $__env->yieldContent('title');

if ($title !== null)
	$title .= ' — ';

$title .= config('app.name');

?>
<!DOCTYPE html>
<html lang="{!! $locale !!}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{!! $title !!}</title>

    <link rel="stylesheet" href="{!! mix('/build/vendor.css') !!}">
    <link rel="stylesheet" href="{!! mix('/build/theme.css') !!}">
    <link rel="stylesheet" href="{!! mix('/build/main.css') !!}">

    @stack('head')
</head>
<body class="@yield('body_class')">

@stack('body_begin')

<div id="app">
    <div class="site-body-wrapper">
        <div class="mb-5"></div>

        @yield('content')
    </div>
</div>

{{-- Global configuration object --}}
<script>window.config = @json($config);</script>

<script src="{!! mix('/build/manifest.js') !!}"></script>
<script src="{!! mix('/build/vendor.js') !!}"></script>
<script src="{!! mix('/build/app.js') !!}"></script>

<script src="{!! mix('/build/theme.js') !!}"></script>

@stack('scripts')

</body>
</html>
