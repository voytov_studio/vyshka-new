<?php

$cfs = DynamicContentSheetFacade::getCfs();

$buttonTitle = $cfs->isMissingOrEmpty('stickyHeader_callbackButtonText')
    ? 'Перезвоните'
    : $cfs->get('stickyHeader_callbackButtonText');

?>
<div class="fixed-bar">
    <div class="container">
        <div class="fixed-menu-holder mr-3">
            <a href="javascript:" class="fixed-menu-opener"><span>Меню</span></a>
        </div>

        <div class="fixed-bar-form d-flex justify-content-center">

            <div class="fixed-bar-form__row">
                <span class="fixed-bar-form__question d-none d-sm-block">
                    {{ ContentFieldsStore::get('stickyHeader_callback', 'Остались вопросы или приняли решение?') }}
                </span>
                <span class="fixed-bar-form__question d-block d-sm-none">ПЕРЕЗВОНИТЬ?</span>

                <!-- FORM -->
                <form method="post" class="c-feedback-form js-feedback-form mr-3"
                      data-form-submission-id="top_callback_form"
                      data-parsley-validate>
                    <div class="input-group">
                        <input name="phone" type="text"
                               class="contact-info__text-field c-phone-input form-control phone-mask js-feedback-form__phone-input" placeholder="Телефон"
                               data-parsley-required data-parsley-phone>

                        <div class="input-group-append d-md-none">
                            <button class="btn js-feedback-form__submit text-uppercase btn-warning font-small p-1" type="button">
                                ПЕРЕЗВОНИТЕ
                            </button>
                        </div>

                        <div class="input-group-append d-none d-md-flex">
                            <button class="btn js-feedback-form__submit text-uppercase btn-warning" type="button">
                                {{ $buttonTitle }}
                            </button>
                        </div>
                    </div>
                </form>
                <!-- /FORM -->

                <a href="tel:{{ $siteDataProvider->getPhoneEntry('main')->value }}" class="fixed-bar-form__phone js-mango-number">
                    {{ $siteDataProvider->getPhoneEntry('main')->display }}
                </a>

                <span class="fixed-bar-form__schedule"><span>24</span>/7</span>
            </div>
        </div>
    </div>
</div>
