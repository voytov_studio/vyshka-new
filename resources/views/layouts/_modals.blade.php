<?php

$chunkFactory = app(\App\Domain\ContentChunkFactory::class);
$siteDataProvider = app(\App\Domain\SiteDataProvider::class);

?>
<!-- Modals -->

@php
	$chunk = $chunkFactory->make('company_owner_modal');
	$chunk->postAggregate = ContentPageContextFacade::getCurrentPostAggregate();;

	echo $chunk->render();
@endphp

<div class="modal-window modal-window_small" id="meeting-modal">
    <div class="help-form">
        <h2>Записаться на встречу</h2>
        <p>Оставьте свой номер телефона и консультант перезвонит Вам и уточнит время встречи</p>

        <div class="text-wrap">
            @component('_shared._call-me-form-variant-4', ['wrapperClass' => '', 'buttonLabel' => 'ЗАПИСАТЬСЯ'])
                @slot('promo')
                    <div class="help-box__stick">
                        <span class="help-box__stick__label">На {!! \Carbon\Carbon::now()->format('d.m') !!} осталось</span>
                        <span class="help-box__stick__amount">3 бесплатные консультации</span>
                    </div>
                @endslot
            @endcomponent
        </div>
    </div>
</div>

<div class="modal-window modal-window_small" id="callback-modal">
    <div class="help-form">
        <h2>Заказать звонок</h2>
        <p>Оставьте свой номер телефона и мы перезвоним Вам в удобное время</p>
        <ul class="main-office__tabs js-tabs">
            <li><a href="#tab-help-9" class="active">Сейчас</a></li>
            <li><a href="#tab-help-10">На время</a></li>
        </ul>
        <div class="tab-content" id="tab-help-9">
            @include("_shared._call-me-form-variant-3")
        </div>
        <div class="tab-content" id="tab-help-10">
            @include("_shared._call-me-form-with-time")
        </div>
    </div>
</div>

@if(false)
    <div class="modal-window modal-window_small" id="connect-modal">
        <div class="help-form">
            <h2>Связаться с сотрудником</h2>
            <p>Оставьте свой номер телефона и мы перезвоним Вам в удобное время</p>
            <ul class="main-office__tabs js-tabs">
                <li><a href="#tab-help-11" class="active">Сейчас</a></li>
                <li><a href="#tab-help-12">На время</a></li>
            </ul>
            <div class="tab-content" id="tab-help-11">
                <form action="#" data-parsley-validate>
                    <div class="text-wrap">
                        <input type="text" class="text-field phone-mask" placeholder="Ваш телефон" data-parsley-required>
                    </div>
                    <input type="submit" class="submit" value="Перезвоните мне">
                </form>
            </div>
            <div class="tab-content" id="tab-help-12">
                <form action="#" data-parsley-validate>
                    <div class="text-wrap">
                        <input type="text" class="text-field phone-mask" placeholder="Ваш телефон" data-parsley-required>
                    </div>
                    <div class="time-row row">
                        <div class="col-6">
                            <input type="text" class="text-field text-date" placeholder="Дата звонка" id="date2" data-parsley-required>
                        </div>
                        <div class="col-6">
                            <input type="text" class="text-field text-time only-time" placeholder="Время" id="time2">
                        </div>
                    </div>
                    <input type="submit" class="submit" value="Перезвоните мне">
                </form>
            </div>
            <div class="data-check font-tiny">
                <input type="checkbox" class="checkbox" checked="checked" id="check02111">
                <label for="check02111">Согласен на обработку персональных данных</label>
            </div>
        </div>
    </div>
@endif

<div class="modal-window modal-window_small" id="get-sale-modal">
    <div class="help-form">
        <h2>Получить скидку</h2>

        <p>Оставьте свой номер телефона и консультант перезвонит Вам с информацией о скидке</p>

        @component('_shared._call-me-form-variant-4', ['buttonLabel' => 'Получить скидку'])
            @slot('promo')
                <div class="help-box__stick">
                    <span class="help-box__stick__label">На {!! \Carbon\Carbon::now()->format('d.m') !!} осталось</span>
                    <span class="help-box__stick__amount">3 скидки</span>
                </div>
            @endslot
        @endcomponent
    </div>
</div>

<div class="modal-window" id="result-modal">
    <div class="result-info">
        <h2>Результат по делу №</h2>
        <p></p>
    </div>
</div>

<div class="modal-window" id="route-modal">
    <div class="route-info">
        <h2>Как добраться</h2>
        <span class="route-info__title">
                <span class="route-info__icon">
                    <img src="images/icon-user.png" alt="">
                </span>
                <span class="route-info__text">Пешком</span>
            </span>
        <p>Наш офис расположен в 7 минутах ходьбы от станции метро Чернышевская.</p>
        <p>Чтобы до нас добраться, Вам достаточно выйдя из метро повернуть направо на пр. Чернышевского и насладившись
            видами старого Петербурга пройти 500 метров до поворота на ул. Шпалерная. Поворачивать на Шпалерную улицу не
            нужно. Переходите пр. Чернышевского и заходите в угловой дом пр Чернышевского 1 и ул. Шпалерной 36.
            Поднимаетесь на второй этаж, наш офис номер 206.</p><br/>
        <p>На территории Бизнес центра работает пропускной режим, по этому возьмите с собой любой документ,
            удостоверяющий личность (паспорт, водительские права и пр.).</p>
        <span class="route-info__title">
                <span class="route-info__icon">
                    <img src="images/icon-car.png" alt="">
                </span>
                <span class="route-info__text">На машине</span>
            </span>
        <p>Наш офис расположен на перекрёстке улицы Шпалерная и пр. Чернышевского. Вы можете доехать до нас по
            Воскресенской набережной, либо по пр. Чернышевского (если едите из центра города). Обратите внимание, на ул.
            Шпалерной парковка запрещена.</p>
        <p>Вход в Бизнес центр расположен в угловом доме пр Чернышевского 1 и ул. Шпалерной 36. Поднимаетесь на второй
            этаж, наш офис номер 206. </p>
        <p>На территории Бизнес центра работает пропускной режим, по этому возьмите с собой любой документ,
            удостоверяющий личность (паспорт, водительские права и пр.).</p>
    </div>
</div>

<div class="modal-window modal-window_small" id="sms-modal">
    <div class="help-form">
        <h2>Получить контакты по СМС</h2>
        <p>Оставьте свой номер телефона и мы пришлём вам наши контакты по смс</p>

        @include("_shared._send-me-contacts-via-sms")
    </div>
</div>

<div class="modal-window modal-window_small" id="email-modal">
    <div class="help-form">
        <h2>Отправить сообщение</h2>

        @include("_shared._call-me-form-variant-3")
    </div>
</div>

<div class="modal-window modal-window_success" id="modal-success2">
    <div class="success-messages">
        <h2>Благодарим Вас за заявку</h2>
        <p>Мы свяжемся с Вами в ближайшее время</p>
    </div>
</div>

<div class="modal-window modal-window_success" id="modal-success">
    <div class="success-messages">
        <h2>Отправка прошла успешно!</h2>
        <p>Ваша заявка была отправлена!<br/>Мы вышлем Вам СМС <b>в течение 10 секунд</b>!</p>
    </div>
</div>

<div class="modal-window c-mass-media-modal" id="mass-modal">
    <div class="mass-media__header">
        <span class="mass-media__header-title">Мы в СМИ</span>
    </div>
    <div class="mass-media__tabs-wrap">
        @include("_shared._lazy-content-loader", ['contentId' => 'mass_media_modal'])
    </div>
</div>

<div class="modal-window" id="privacy-policy-modal">
	<h3>Политика конфиденциальности</h3>

	<article>
		<p>
			Оставляя данные на сайте, Вы соглашаетесь с нашей Политикой конфиденциальности и защиты информации
		</p>
		<h4>Защита данных</h4>
		<p>
			Администрация сайта «{!! config('site.company_name') !!}» (далее - Сайт) не может передать или раскрыть информацию,
			предоставленную пользователем (далее Пользователь) при заполнении и отправке полей форм Сайта и
			использовании функций сайта третьим лицам, кроме случаев, описанных законодательством страны, на
			территории которой пользователь ведет свою деятельность.</p>
		<h4>Получение персональной информации</h4>
		<p>
			Для коммуникации с представителями Сайта пользователь обязан внести некоторую персональную информацию в
			соответствующие поля формы.</p>
		<h4>Использование персональной информации</h4>
		<p>
			Сайт использует личную информацию Пользователя для обслуживания и для улучшения качества предоставляемых
			услуг. Сайт прилагает все усилия для сбережения в сохранности личных данных Пользователя. Личная
			информация может быть раскрыта в случаях, описанных законодательством, либо когда администрация сочтет
			подобные действия необходимыми для соблюдения юридической процедуры, судебного распоряжения или
			легального процесса необходимого для работы Пользователя с Сайтом. В других случаях, ни при каких
			условиях, информация, которую Пользователь передает Сайту, не будет раскрыта третьим лицам.</p>
		<h4>Коммуникация</h4>
		<p>
			После того, как Пользователь оставил данные, он получает сообщение, содержащее контактные данные
			компании, представленной на Сайте.</p>
		<h4>Ссылки</h4>
		<p>
			На сайте могут содержаться ссылки на другие сайты. Сайт не несет ответственности за содержание, качество
			и политику безопасности этих сайтов. Данное заявление о конфиденциальности относится только к
			информации, размещенной непосредственно на сайте.</p>
		<h4>Безопасность</h4>
		<p>
			Сайт обеспечивает безопасность и конфиденциальность контактных данных Пользователя от
			несанкционированного доступа.</p>
		<h4>Уведомления об изменениях</h4>
		<p>
			Сайт оставляет за собой право вносить изменения в Политику конфиденциальности без дополнительных
			уведомлений. Нововведения вступают в силу с момента их опубликования. Пользователи могут отслеживать
			изменения в Политике конфиденциальности самостоятельно.</p>
	</article>
</div>

@php
    $externalReviews = app()->make('data.reviews');
@endphp
<div class="modal-window modal-window_small" id="reviews-modal">
    <h3>Отзывы</h3>

    <article>
        <ol>
            @foreach($externalReviews as $r)
                <li>
                    <a href="{{ $r['link'] }}" target="_blank" rel="nofollow noopener noreferrer">
                        {{ $r['text'] }}
                    </a>
                </li>
            @endforeach
        </ol>
    </article>
</div>

<div class="modal-window modal-window_small" id="send-checklist-modal">
    <div class="help-form">
        <h2>Получить чек-лист</h2>

        @include("_shared._call-me-form-variant-3", ['buttonLabel' => 'Получить чек-лист'])
    </div>
</div>
