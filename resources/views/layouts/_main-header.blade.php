<?php

/**
 * @var SiteDataProvider $siteDataProvider
 */

use App\Domain\SiteDataProvider;

$externalReviews = app()->make('data.reviews');

?>
@include("layouts._navigation-bar-fixed")

<header class="с-site-header header">
    <div class="top-line">
        <div class="container d-flex d-md-block justify-content-center">
            <div class="logo">
                <span class="logo__text">
                    <a href="{!! config('app.url') !!}">
                        @if(config('site.header_logo_uri'))
                            {!! html()->img()->attributes(array_merge([
                                'class' => 'lazy img_fluid',
                                'data-src' => config('site.header_logo_uri'),
                                'alt' => config('site.company_name'),
                            ], config('site.header_logo_html_attributes', []))) !!}
                        @else
                            {!! config('site.title') !!}
                        @endif
                    </a>
                </span>
            </div>
            <div class="slogan">{!! config('site.slogan') !!}</div>

            @include('layouts._navigation-bar-mobile')
            @include('layouts._navigation-bar-desktop')

            <a href="tel:{{ $siteDataProvider->getPhoneEntry('main')->value }}" class="btn-call js-mango-number">
                {{ $siteDataProvider->getPhoneEntry('main')->display }}
            </a>
        </div>
    </div>
    <div class="top-info">
        <div class="c-top-navigation-bar top-info__holder">
            <div class="container">
                <div class="d-block d-md-none text-center mb-sm-3">
                    @if(is_ekrual())
                        <img class="lazy ml-2 mr-2" data-src="images/icon-phone04.png">

                        <a href="tel:{!! $siteDataProvider->getPhoneEntry('main')->value !!}" class="phone top-info__tel js-mango-number">
                            {!! $siteDataProvider->getPhoneEntry('main')->display !!}
                        </a>
                    @else
                        @if (false)
                            <a href="tel:{!! $siteDataProvider->getPhoneEntry('federal')->value !!}" class="phone top-info__tel d-block d-sm-inline-block">
                                {!! $siteDataProvider->getPhoneEntry('federal')->display !!}
                            </a>
                        @endif

                        <img class="lazy ml-2 mr-2" data-src="images/icon-phone04.png">

                        <a href="tel:{!! $siteDataProvider->getPhoneEntry('main')->value !!}" class="phone top-info__tel js-mango-number">
                            {!! $siteDataProvider->getPhoneEntry('main')->display !!}
                        </a>
                    @endif
                </div>

                <div class="top-row">
                    <div class="top-row__left">
                        <div class="c-top-navigation-bar__item rating-info">
                            <div class="rating-info__score"><span>9.8</span> / 10</div>
                            <div class="rating-info__text">
                                Рейтинг
                                <div class="rating-info__popup-wrap">
                                    <a href="javascript:" class="rating-info__popup-opener js-reviews-opener">по отзывам</a>
                                    <div class="rating-info__popup">
                                        <span class="rating-info__popup-label">Почитайте отзывы о нас:</span>
                                        <ul class="rating-info__popup-list">
                                            @foreach($externalReviews as $r)
                                                <li>
                                                    <a href="{{ $r['link'] }}" target="_blank" rel="nofollow noopener noreferrer">
                                                        {{ $r['text'] }}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <span class="d-none d-md-in">на {!! \Carbon\Carbon::now()->format('d.m.Y') !!}</span>
                            </div>
                            <div class="rating-info__overlay js-reviews-close"></div>
                        </div>

                        @if(false)
                            <div class="c-top-navigation-bar__item lawyer-online">
                                <span class="lawyer-online__time">24/7</span><span class="lawyer-online__text">Юристы онлайн: <span class="lawyer-online__status online">доступны</span></span>
                            </div>
                        @endif

                        @if(false)
                            <div class="c-top-navigation-bar__item mass-media">
                                <a href="#mass-modal" class="mass-media__opener fancybox">
                                <span class="mass-media__icon">
                                    <img class="lazy" data-src="images/icon-media.png" alt="">
                                </span>
                                    <span class="mass-media__text">Мы в СМИ</span>
                                </a>
                            </div>
                        @endif

                        @if(false)
                            <div class="c-top-navigation-bar__item mass-media">
                                <a href="{!! config('app.settings.social_links.youtube') !!}" class="video-blog"
                                   target="_blank" rel="noopener nofollow noreferrer">
                                    <span class="video-blog__icon">
                                        <img class="lazy" data-src="images/icon-blog.png">
                                    </span>
                                    <span class="video-blog__text">Видеоблог</span>
                                </a>
                            </div>
                        @endif
                    </div>
                    <div class="top-row__right">
                        <div class="d-none d-sm-inline-flex">
                            @include("chunks._contacts-full")

                            @if(is_ekrual())
                                <a href="tel:{!! $siteDataProvider->getPhoneEntry('main')->value !!}" class="top-info__phone d-none d-md-block mr-3">
                                    {!! $siteDataProvider->getPhoneEntry('main')->display !!}
                                </a>
                            @else
                                <a href="tel:{!! $siteDataProvider->getPhoneEntry('federal')->value !!}" class="top-info__phone d-none d-md-block mr-3">
                                    {!! $siteDataProvider->getPhoneEntry('federal')->display !!}
                                </a>
                                <a href="tel:{!! $siteDataProvider->getPhoneEntry('main')->value !!}" class="top-info__phone d-none d-md-block js-mango-number">
                                    {!! $siteDataProvider->getPhoneEntry('main')->display !!}
                                </a>
                            @endif
                        </div>

                        @if(is_ekrual())
                            {{-- Ничего не выводим --}}
                        @else
                            <div class="top-info__rf-rating d-none">
                                <span class="top-info__rf-rating-number">2</span>
                                <span class="top-info__rf-rating-text">место в рейтинге РФ 2017 <a href="https://300.pravo.ru/archive/#2017">Pravo.ru</a></span>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
