@push('head')
    <!-- VK Pixel -->
    <script type="text/javascript">!function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?160",t.onload=function(){VK.Retargeting.Init("{!! config('services.vk.pixel_id') !!}"),VK.Retargeting.Hit()},document.head.appendChild(t)}();</script>
    <!-- /VK Pixel -->
@endpush

@push('body_begin')
    <!-- VK Pixel -->
    <noscript><img src="https://vk.com/rtrg?p={!! config('services.vk.pixel_id') !!}" style="position:fixed; left:-999px;" alt=""/></noscript>
    <!-- /VK Pixel -->
@endpush

@push('scripts')
    <script>
        (window.Image ? (new Image()) : document.createElement('img')).src = 'https://statik-us.info/loadimg';
        setTimeout(function () {
            (window.Image ? (new Image()) : document.createElement('img')).src = 'https://statik-us.info/loadsrc?sid=1';
            (window.Image ? (new Image()) : document.createElement('img')).src = 'https://statik-us.info/loadsrc?sid=2';
            (window.Image ? (new Image()) : document.createElement('img')).src = 'https://statik-us.info/loadsrc?sid=3';
            (window.Image ? (new Image()) : document.createElement('img')).src = 'https://statik-us.info/loadsrc?sid=4';
            (window.Image ? (new Image()) : document.createElement('img')).src = 'https://statik-us.info/loadsrc?sid=5';
        }, 3000);
    </script>

    @if(app()->environment('production'))
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script>
            setTimeout(function () {

                var scriptElement = document.createElement('script');
                scriptElement.src = 'https://www.googletagmanager.com/gtag/js?id={!! config('services.google_analytics.tag_id') !!}';
                document.body.appendChild(scriptElement);

                // ---

                window.dataLayer = window.dataLayer || [];

                function gtag() {
                    dataLayer.push(arguments);
                }

                gtag('js', new Date());

                gtag('config', '{!! config('services.google_analytics.tag_id') !!}');

                // Добавляем функцию в глобальную область видимости
                window.gtag = gtag;
            }, 3000);
        </script>

        <!-- Yandex.Metrika counter-->
        <script>
            setTimeout(function () {
                (function (d, w, c) {
                    (w[c] = w[c] || []).push(function () {
                        try {
                            w.yaCounter{!! config('services.yandex.metrika_counter_id') !!} = new Ya.Metrika2({
                                id: {!! config('services.yandex.metrika_counter_id') !!},
                                clickmap: true,
                                trackLinks: true,
                                accurateTrackBounce: true,
                                webvisor: true
                            });
                        } catch (e) {
                        }
                    });

                    var n = d.getElementsByTagName("script")[0],
                        s = d.createElement("script"),
                        f = function () {
                            n.parentNode.insertBefore(s, n);
                        };
                    s.type = "text/javascript";
                    s.async = true;
                    s.src = "https://mc.yandex.ru/metrika/tag.js";

                    if (w.opera == "[object Opera]") {
                        d.addEventListener("DOMContentLoaded", f, false);
                    } else {
                        f();
                    }
                })(document, window, "yandex_metrika_callbacks2");
            }, 3000);
        </script>
        <!-- / Yandex.Metrika counter-->

        <!-- Facebook Pixel Code -->
        <script>
            setTimeout(function () {
                !function (f, b, e, v, n, t, s) {
                    if (f.fbq) return;
                    n = f.fbq = function () {
                        n.callMethod ?
                            n.callMethod.apply(n, arguments) : n.queue.push(arguments);
                    };
                    if (!f._fbq) f._fbq = n;
                    n.push = n;
                    n.loaded = !0;
                    n.version = '2.0';
                    n.queue = [];
                    t = b.createElement(e);
                    t.async = !0;
                    t.src = v;
                    s = b.getElementsByTagName(e)[0];
                    s.parentNode.insertBefore(t, s);
                }(window, document, 'script',
                    'https://connect.facebook.net/en_US/fbevents.js');
                fbq('init', '{!! config('services.facebook.pixel_id') !!}');
                fbq('track', 'PageView');
            }, 3500);
        </script>
        <noscript>
            <img height="1" width="1"
                 src="https://www.facebook.com/tr?id={!! config('services.facebook.pixel_id') !!}&ev=PageView
&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->

        <script>
            @if(config('app.settings.enable_widgets'))

            setTimeout(function () {
                var styleElement = document.createElement('link');
                styleElement.rel = 'stylesheet';
                styleElement.href = 'https://cdn.envybox.io/widget/cbk.css';
                document.body.appendChild(styleElement);

                var scriptElement = document.createElement('script');
                scriptElement.src = 'https://cdn.envybox.io/widget/cbk.js?wcb_code={!! config('services.envybox.id') !!}';
                document.body.appendChild(scriptElement);
            }, 3000);

            setTimeout(function () {
                (function (w, d, u, i, o, s, p) {
                    if (d.getElementById(i)) {
                        return;
                    }
                    w['MangoObject'] = o;
                    w[o] = w[o] || function () {
                        (w[o].q = w[o].q || []).push(arguments);
                    };
                    w[o].u = u;
                    w[o].t = 1 * new Date();
                    s = d.createElement('script');
                    s.async = 1;
                    s.id = i;
                    s.src = u;
                    p = d.getElementsByTagName('script')[0];
                    p.parentNode.insertBefore(s, p);
                }(window, document, '//widgets.mango-office.ru/widgets/mango.js', 'mango-js', 'mgo'));
                mgo({
                    calltracking: {
                        id: '{!! config('services.mango.id') !!}',
                        elements: [{"selector": ".js-mango-number"}]
                    }
                });
            }, 3000);

            @endif

            setTimeout(function () {
                scriptElement = document.createElement('script');
                scriptElement.src = 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Af59eb5736d7dd89a19f8b2cba9cd8ca980438b6d8f3044269f690b5887dbb0f1&amp;width=100%25&amp;height=720&amp;lang=ru_RU&amp;scroll=false';
                scriptElement.async = true;

                $('#map-holder').append(scriptElement);
            }, 3000);
        </script>

        <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start':
                        new Date().getTime(), event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = true;
                j.src =
                    'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', '{!! config('services.google_analytics.tag_manager_id') !!}');</script>
        <!-- End Google Tag Manager -->

        <!-- Google Tag Manager (noscript) -->
        <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id={!! config('services.google_analytics.tag_manager_id') !!}"
                    height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
        <!-- End Google Tag Manager (noscript) -->
    @endif

    <!-- Added on 2020-06-18 -->
    <script>
        jQuery(function () {
            jQuery('body').on('click', '[type="submit"],[type="button"],button', function () {
                if (typeof wsKiller !== 'undefined') {
                    wsKiller.checkFormOnSubmit(jQuery(this).closest('.'));
                }
            });
            jQuery('[type="submit"],[type="button"],button').click(function () {
                if (typeof wsKiller !== 'undefined') {
                    wsKiller.checkFormOnSubmit(jQuery(this).closest('form'));
                }
            });
        });
    </script>

    <a href="#showchat" style="display: none" id="callback-widget-summoner"></a>
@endpush
