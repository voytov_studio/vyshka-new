@if(config('app.settings.enable_widgets'))
    <div class="connect d-none d-lg-block">
        <button class="connect__button">
            <span class="connect__button-text">Вопрос юристу Online</span><span class="connect__button-icon"></span>
        </button>
        <ul class="connect__list">
            <li class="connect__title">Выберите способ связи:</li>
            <li class="connect__item">
                <a class="connect__item-link" href="javascript:" onclick="window.app.widgets.openChatWidget(); return false;">
                    Chat
                </a>
            </li>
            <li class="connect__item">
                <a class="connect__item-link" href="{!! config('app.settings.social_links.vk') !!}" target="_blank" rel="noopener nofollow noreferrer">Vkontakte</a>
            </li>
            <li class="connect__item">
                <a class="connect__item-link" href="{!! config('app.settings.social_links.telegram') !!}">Telegram</a>
            </li>
            <li class="connect__item">
                <a class="connect__item-link" href="{!! config('app.settings.social_links.whatsapp') !!}" target="_blank" rel="noopener nofollow noreferrer">WhatsApp</a>
            </li>
        </ul>
    </div>
@endif
