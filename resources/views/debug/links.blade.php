@extends('layouts.main')

@section('content')
    <main class="container">
        @include("chunks._link-list")
    </main>
@endsection
