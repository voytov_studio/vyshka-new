<?php

$currentPostAggregate = ContentPageContextFacade::getCurrentPostAggregate();

$currentPageId = $currentPostAggregate !== null
    ? $currentPostAggregate->model->getKey()
    : null;

$isPage = $currentPageId !== null;

if ($isPage) {
    $editPageUrl = route('servicePanels.pages.edit', [
        'pageId' => $currentPageId
    ]);
}

?>
<div class="c-service-panel-widget">
    <div class="container d-flex">
        @if($isPage)
            <div class="c-service-panel-widget__block-wrapper">
                <a href="{!! $editPageUrl !!}"
                   class="btn btn-warning">
                    Редактировать страницу
                </a>
            </div>

            <div class="c-service-panel-widget__block-wrapper">
                <a href="{!! route('servicePanels.pages.invalidateCache', ['uri' => \Illuminate\Support\Facades\Request::path()]) !!}"
                   class="btn btn-warning">
                    Очистить кеш страницы
                </a>
            </div>
        @endif

        <div class="c-service-panel-widget__block-wrapper">
            <a href="{!! route('servicePanels.pages.clearCache') !!}"
               class="btn btn-warning" onclick="return confirm('Вы уверены, что хотите очистить весь кеш страниц?')">
                Очистить ВЕСЬ кеш
            </a>
        </div>

        <div class="c-service-panel-widget__block-wrapper">
            <a href="{!! route('servicePanels.crawler') !!}"
               class="btn btn-warning">
                Кроулер
            </a>
        </div>

        <div class="c-service-panel-widget__block-wrapper d-flex align-items-center ml-2">
            <div class="static-text text-secondary">
                {!! \Illuminate\Support\Facades\Session::get('servicePanels.message') !!}
            </div>
        </div>
    </div>
</div>
