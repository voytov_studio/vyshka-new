<?php

/**
 * @var string $url
 */

?>
@extends('layouts.plain')

@section('body_class', 'c-maintenance')

@section('content')
    <div class="d-flex justify-content-center">
        <iframe scrolling="yes" style="width: 100%; height: 100vh"
                src="{!! $url !!}">
        </iframe>
    </div>
@endsection
