<div class="helper-contacts">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-6">
                <div class="helper-contacts__text-box">
                    <div class="helper-contacts__image-hand"></div>
                    <h3>Получите наши контакты по СМС</h3>
                    <p>Оставьте свой номер телефона и получите наши контакты через 10 секунд</p>
                    <div class="contact-info small">
                        <div class="contact-info__row">
                            <div class="contact-info__form">

                                @include("_shared._send-me-contacts-via-sms")

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6">
                <div class="helper-contacts__text-box">
                    <div class="helper-contacts__image-girl"></div>
                    <h3><b>Остались вопросы?<br>Мы на них ответим!</b></h3>
                    <p>Наш профессиональный юрист свяжется с вами и ответит на них в течении 1 минуты</p>
                    <div class="contact-info">
                        <div class="contact-info__row">
                            <div class="contact-info__form">
                                <form action="#" data-parsley-validate>
                                    <div class="contact-info__form-row">
                                        <div class="contact-info__text-wrap">
                                            <input type="text" class="contact-info__text-field phone-mask" name="cons_phone17" placeholder="Ваш телефон" data-parsley-required>
                                        </div>
                                        <input type="submit" class="submit cons_send17" value="Перезвоните мне">
                                    </div>
                                    <div class="data-check font-tiny">
                                        <input type="checkbox" class="checkbox" name="cons_agree17" checked="checked" id="check303">
                                        <label for="check303">Согласен на обработку персональных данных</label>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
