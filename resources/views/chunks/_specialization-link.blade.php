<a href="javascript:;"
   data-fancybox data-type="ajax" data-src="{{ isset($route) ? $route : route('site.showPageLinksBySpecialization', ['id' => $id]) }}">
    {{ $slot }}
</a>
