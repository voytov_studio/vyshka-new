<?php

/**
 * @var \Illuminate\Support\Collection $data
 */

?>
<ul class="guarantee-list">
	@foreach ($data as $index => $item)
		<li class="guarantee-list__item-{!! $index + 1 !!}">
			<div class="guarantee-list__icon">
				<img class="lazy" data-src="images/icon-guarantee0{!! $index + 1 !!}.png" alt="">
			</div>
			<div class="guarantee-list__text">
				<span class="guarantee-list__title">
					{!! Arr::get($item, 'head') !!}
				</span>

                {!! Arr::get($item, 'text') !!}
			</div>
		</li>
	@endforeach
</ul>
