<?php

/**
 * @var ContentFieldsStore $cfs
 * @var \Illuminate\Support\Collection $tvItems
 * @var \Illuminate\Support\Collection $pressItems
 * @var \Illuminate\Support\Collection $attachments
 */

?>
<div class="mass-section">
    <div class="container">
        <div class="section-title h_mb25 section-title_left">
            <h2><span>Мы в СМИ</span></h2>
        </div>
        <div class="mass-section__header">
            <div class="how-it-works__tabs-wrap">
                <ul class="how-it-works__tabs js-tabs">
                    <li><a href="#mass-tab10" class="active"><span>Телевидение</span></a></li>
                    <li><a href="#mass-tab11"><span>Пресса</span></a></li>
                </ul>
            </div>
            <div class="btn-holder">
                <a href="#email-modal" class="btn-yellow small fancybox">ПРЕДСТАВИТЕЛЯМ СМИ</a>
            </div>
        </div>
        <div class="mass-media__tabs-wrap">
            <div class="tab-content" id="mass-tab10">
                <div class="slider-wrap">
                    <div class="container">
                        <div class="mass-slider2 owl-carousel">
                            @foreach ($tvItems as $item)
                                <div class="item">
                                    <div class="tele-item">
                                        <div class="tele-item__video" style="background:url(//img.youtube.com/vi/{!! $item['video'] !!}/hqdefault.jpg) no-repeat center center;">
                                            <a href="https://www.youtube.com/embed/{!! $item['video'] !!}" class="btn-play" data-fancybox></a>
                                        </div>
                                        <a href="javascript:" class="tele-item__title">
                                            {{ $item['header'] }}
                                        </a>
                                        <div class="tele-item__date">
                                            {{ $item['date'] }}
                                        </div>

                                        <div>
                                            <p>{!! $item['text'] !!}</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <div class="text-center text-uppercase">
                            <a href="{!! config('app.settings.social_links.youtube') !!}" target="_blank" rel="noopener nofollow noreferrer">
                                Посмотреть все
                            </a>
                        </div>
                    </div>
                    <a href="javascript:" class="btn-slider-prev mass-prev2"></a>
                    <a href="javascript:" class="btn-slider-next mass-next2"></a>
                </div>
            </div>
            <div class="tab-content" id="mass-tab11">
                <div class="slider-wrap">
                    <div class="container">
                        <div class="mass-slider3 owl-carousel">
                            @foreach ($pressItems as $item)
                                @php
                                    $imageUri = data_get($attachments, $item['source'] . '.thumbnail_url');
                                @endphp
                                <div class="item">
                                    <div class="tele-item">
                                        <div class="tele-item__icon">
                                            @if ($imageUri)
                                                <img class="lazy" data-src="{!! $imageUri !!}" alt="">
                                            @endif
                                        </div>
                                        <a href="{!! $item['link'] !!}" class="tele-item__title">
                                            {{ $item['header'] }}
                                        </a>
                                        <div class="tele-item__date">
                                            {{ $item['date'] }}
                                        </div>
                                        <p>
                                            {!! $item['text'] !!}
                                        </p>

                                        <a href="{{ $item['link'] }}" class="to-source">Смотреть в источнике</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <div class="text-center text-uppercase">
                            <a href="{!! url('/site/press') !!}" rel="noopener nofollow noreferrer">
                                Показать все публикации
                            </a>
                        </div>
                    </div>
                    <a href="javascript:" class="btn-slider-prev mass-prev3"></a>
                    <a href="javascript:" class="btn-slider-next mass-next3"></a>
                </div>
            </div>
        </div>
    </div>
</div>
