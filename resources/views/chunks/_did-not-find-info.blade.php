<span class="contact-info__question">Не нашли интересующей информации?</span>

<p>
    Мы более 10 лет занимаемся помощью в банковских делах, и имеем огромный кладезь информации по
    всем проблемам в этой сфере. <b>Мы Вам точно поможем</b>:
</p>

<div class="row justify-content-center">
    <div class="col-10 col-sm-8">
        @include("_shared._call-me-form-variant-2")
    </div>

    <div class="col-10 col-sm-4">
        <div class="help-box__phone-holder">
            <div class="help-box__phone-icon">
                <img class="lazy" data-src="images/icon-phone02.png" alt="">
            </div>

            @include("_shared.call-to-us", ['skipPhones' => ['primary']])
        </div>
    </div>
</div>
