<?php

$siteDataProvider = app(\App\Domain\SiteDataProvider::class);

?>
<div class="main-contacts">
    <div class="container">
        <div class="section-title section-title_left">
            @if(!is_ekrual())
                <a href="{!! url('/files/business-card.png') !!}" class="download-card" download>Скачать визитку</a>
            @endif

            <h2><span>Контакты</span></h2>
        </div>
        <div class="main-contacts__row row first">
            <div class="col-12 col-sm-3 mobile-col">
                <span class="main-contacts__row-label">Режим работы:</span>
            </div>
            <div class="col-12 col-sm-3 mobile-col">
                <span class="main-contacts__col-title">Будние дни</span>
                <span class="main-contacts__col-text">
                    {{ DynamicContentSheetFacade::getCfs()->get('workSchedule_text') }}
                </span>
            </div>
            <div class="col-12 col-sm-3 mobile-col">
                <span class="main-contacts__col-title">В субботу</span>
                <span class="main-contacts__col-text">{{ DynamicContentSheetFacade::getCfs()->get('workSchedule_saturday') }}</span>
            </div>
            <div class="col-12 col-sm-3">
                <span class="main-contacts__col-title">В воскресенье</span>
                <span>{{ DynamicContentSheetFacade::getCfs()->get('workSchedule_sunday') }}</span>
            </div>
        </div>
        <hr>
        <div class="main-contacts__row row">
            <div class="col-12 col-sm-3 mobile-col">

                <a href="tel:{{ $siteDataProvider->getPhoneEntry('main')->value }}" class="main-contacts__phone js-mango-number">
                    {{ $siteDataProvider->getPhoneEntry('main')->display }}
                </a>

                <span class="main-contacts__label">Звоните, или оставьте контакты:</span>
                <div class="links-wrap">
                    <a href="#callback-modal" class="main-contacts__modal-link fancybox">Заказать звонок</a>
                </div>
            </div>
            <div class="col-12 col-sm-6 mobile-col">
                <address class="main-contacts__main-address">Санкт-Петербург, улица Шпалерная д. 36</address>
                <span class="main-contacts__station-address">Площадь Восстания, БЦ Goldex, 2 этаж, офис 206</span>
                <div class="links-wrap">
                    <a href="#route-modal" class="main-contacts__modal-link fancybox">Как добраться?</a>
                    <a href="#meeting-modal" class="main-contacts__modal-link fancybox">Записаться на встречу</a>
                </div>
            </div>
            <div class="col-12 col-sm-3">
                <span class="main-contacts__email">
                    Эл. почта:
                    <a href="mailto:{{ config('app.settings.contact_email') }}">
                        {{ config('app.settings.contact_email') }}
                    </a>
                </span>
                <span class="social-label">Пишите в чаты:</span>
                <ul class="social-list">
                    <li>
                        <a href="javascript:" onclick="window.app.widgets.openChatWidget(); return false;">
                            <img class="lazy" data-src="images/icon-wechat.svg" alt="" width="20" height="20">
                        </a>
                    </li>
                    <li>
                        <a href="{!! config('app.settings.social_links.whatsapp') !!}" target="_blank" rel="noopener nofollow noreferrer">
                            <img class="lazy" data-src="images/icon-whatsapp.svg" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="{!! config('app.settings.social_links.telegram') !!}" target="_blank" rel="noopener nofollow noreferrer">
                            <img class="lazy" data-src="images/icon-telegram.svg" alt="">
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        @if(!is_ekrual())
        <hr>

        <div class="main-contacts__row row align-items-center">
            <div class="col-12 col-sm-3 mobile-col">
                <span class="main-contacts__row-label">Интересное о нас в социальных сетях:</span>
            </div>
            <div class="col-12 col-sm">
                <ul class="social-links">
                    <li>
                        <a href="{!! config('app.settings.social_links.youtube') !!}" target="_blank" rel="noopener nofollow noreferrer">
                            <span class="social-links__icon">
                                <img class="lazy" data-src="images/icon-youtube.svg" alt="">
                            </span>
                            <span class="social-links__text">Канал на Youtube</span>
                        </a>
                    </li>
                    <li>
                        <a href="{!! config('app.settings.social_links.instagram') !!}" target="_blank" rel="noopener nofollow noreferrer">
                            <span class="social-links__icon">
                                <img class="lazy" data-src="images/icon-instagram.svg" alt="">
                            </span>
                            <span class="social-links__text">Инстаграмм-канал</span>
                        </a>
                    </li>
                    <li>
                        <a href="{!! config('app.settings.social_links.vk') !!}" target="_blank" rel="noopener nofollow noreferrer">
                            <span class="social-links__icon">
                                <img class="lazy" data-src="images/icon-vk.svg" alt="">
                            </span>
                            <span class="social-links__text">Группа в VK</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        @endif

        <div class="map-holder" id="map-holder"></div>
    </div>
</div>
