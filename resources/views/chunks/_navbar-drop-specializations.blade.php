<?php

/**
 * @var \App\Data\SpecializationMenuItemDto[]|\Illuminate\Support\Collection $specializationsMenuItems
 */

$menuItems = app('data.menu.specializations');

?>
<div class="top-nav-drop top-nav-drop--specializations">
    <div class="first-level-holder">
        <ul class="first-level">
            @foreach ($specializationsMenuItems as $index => $c)
                <li>
                    <a href="javascript:" class="@if ($index == 0) active @endif">
                        <span class="title" data-id="menu-specialization-item-{!! $index !!}">
                            {!! str_replace('право', '<em>право</em>', $c->name) !!}
                        </span>
                        <span class="number">({{ $c->counter }})</span>
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
    <div class="second-level-holder">
        <div class="second-level-services">
            <div class="drop-header">
                <span class="drop-header__title">
                    <span class="drop-header__title-icon">
                        <img class="lazy" data-src="images/icon-service.png" alt="">
                    </span>
                    <span class="drop-header__title-text popup_service_title">Специализации</span>
                </span>
                @if (false)
                    <div class="sort-box">
                        <span class="sort-box__label">по</span>
                        <ul class="sort-box__list">
                            <li><a href="javascript:" class="active">алфавиту</a></li>
                            <li><a href="javascript:">популярности</a></li>
                            <li><a href="javascript:">теме</a></li>
                        </ul>
                    </div>
                @endif
                <div class="sort-search">
                    <input type="text" class="text js-search-service-input" placeholder="Начните набирать название...">
                </div>
            </div>

            @foreach ($menuItems as $index => $c)
                <div class="popup_service_list pretty-links-decorator" data-id="menu-specialization-item-{!! $index !!}">
                    <ul class="drop-list drop-list_two">
                        @php
                            $items = Arr::get($c, 'items.popular');
                        @endphp
                        @foreach ($items as $i)
                            <li>
                                <a href="{{ $i['link'] }}">{{ $i['name'] }}</a>
                            </li>
                        @endforeach
                        @php
                            $freeItems = Arr::get($c, 'items.free', []);
                        @endphp
                        @if(count($freeItems) > 0)
                            <li>
                                <span class="font-weight-bold">Бесплатные услуги</span>

                                <ul class="mt-3 px-0">
                                    @foreach ($freeItems as $i)
                                        <li>
                                            <a href="{{ $i['link'] }}">{{ $i['name'] }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            @endforeach

            <div class="keywords-search d-block d-md-none">
                <span class="keywords-search__label">Поиск по ключевым словам:</span>
                <div class="keywords-search__row">
                    @include("_shared._yandex-search-form")
                </div>
            </div>
        </div>

        {{-- !!! --}}
        <div class="second-level-specialists pretty-links-decorator">
            <div class="drop-header">
                <div class="drop-header__title">
                    <span class="drop-header__title-icon">
                        <img class="lazy" data-src="images/icon-spec.png" alt="">
                    </span>
                    <span class="drop-header__title-text">Специалисты</span>
                </div>
            </div>
            <div>
                @foreach ($menuItems as $index => $c)
                    <ul class="drop-list js-specialization-menu__specialist-chunk" style="display: none"
                        data-id="menu-specialization-item-{!! $index !!}">
                        @php
                            $items = Arr::get($c, 'items.specialists_1', []) + Arr::get($c, 'items.specialists_2', []);
                        @endphp
                        @foreach ($items as $k => $i)
                            @if(isset($i['items']))
                                @php
                                    $subItemId = 'specialization-sublist-item-' . $index . '_' . $k;
                                    $subItems = $i['items'];
                                @endphp
                                <li class="c-service-navigation-item-dropdown">
                                    <a href="javascript:"
                                       data-toggle="collapse" data-target=".c-service-navigation-item__sub-list.collapse[data-id='{{ $subItemId }}']">
                                        {{ $i['name'] }}&nbsp;<span>&triangledown;</span>
                                    </a>
                                    <ul class="mt-3 c-service-navigation-item__sub-list collapse" data-id="{{ $subItemId }}">
                                        @foreach($subItems as $j)
                                            <li>
                                                <a href="{{ $j['link'] }}">{{ $j['name'] }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @else
                                <li>
                                    <a href="{{ $i['link'] }}">{{ $i['name'] }}</a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                @endforeach
            </div>
        </div>
        {{-- !!! --}}
    </div>

    <div class="drop-footer">
        <div class="row">
            <div class="col-12 col-md-4 d-none d-md-block">
                <div class="keywords-search">
                    <span class="keywords-search__label">Поиск по ключевым словам:</span>
                    <div class="keywords-search__row">
                        @include("_shared._yandex-search-form")
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <div class="contact-info">
                    <span class="contact-info__question">Не нашли нужную услугу?</span>
                    <p>Оставьте номер телефона.<br/>Наш специалист проконсультирует вас бесплатно!</p>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <div class="callback-form">
                    <span class="callback-form__label">Бесплатная консультация:</span>

                    @include("_shared._call-me-form-variant-5")
                </div>
            </div>
        </div>
    </div>
</div>
