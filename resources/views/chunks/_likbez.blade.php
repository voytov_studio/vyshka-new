<?php

$disableScript = $disableScript ?? false;
$idPrefix = $idPrefix ?? 'js-likbez-block__';
$blockClass = $blockClass ?? null;
$titleClass = $titleClass ?? null;
$bottomChunk = $bottomChunk ?? null;

$blockId = uniqid($idPrefix);
$containerElementId = $blockId . '__container';
$sideBarElementId = $blockId . '__sidebar';
$innerWrapperElementId = $blockId . '__inner-wrapper';
$contentWrapperElementId = $blockId . '__content-wrapper';

?>
<div class="c-likbez-block risks {{ $blockClass }}">
    <div class="container">
        @if(isset($title))
            <div class="risks__header {{ $titleClass }}">
                <h2>
                    {{ $title }}
                </h2>
            </div>
        @endif
        <div class="risks__row {!! $containerElementId !!}">
            <div class="risks__sidebar {!! $sideBarElementId !!}" style="position: relative;">
                <span class="risks__sidebar-title">Навигация по разделу:</span>

                <div class="{!! $innerWrapperElementId !!}">
                    <div class="sticky-bar">
                        <ul class="c-likbez-block__navigation risks__questions-list">
                            {!! $navigation !!}
                        </ul>
                    </div>
                </div>

                @include('_shared._text-size-switcher')
            </div>
            <div class="risks__content likbez-content {!! $contentWrapperElementId !!}">
                <div class="risks__content-holder">
                    <div class="risks__content-frame">
                        {!! $content !!}
                    </div>
                </div>

                @if(isset($contentFooter))
                    {!! $contentFooter !!}
                @endif
            </div>
        </div>
        <ul class="risks-accordion d-block d-sm-none">
            {!! $mobileChunk !!}
        </ul>

        {!! $bottomChunk !!}
    </div>
</div>

@unless($disableScript)
    @push('scripts')
        <script>
            $(document).ready(function () {
                document.likbezStickyMenus = document.sidebars || [];

                var instance = new window.StickySidebar('.{!! $sideBarElementId !!}', {
                    topSpacing: 100,
                    bottomSpacing: 80,
                    containerSelector: '.{!! $containerElementId !!}',
                    innerWrapperSelector: '.{!! $innerWrapperElementId !!}'
                });

                document.likbezStickyMenus.push(instance);

                window.Gumshoe('.{!! $innerWrapperElementId !!} a', {
                    offset: 100,
                    nestedClass: 'active-parent'
                });

                window.app.EventBus.$on('LAZY_CONTENT_MANAGER.CHUNK_LOADED', function (data) {
                    console.debug('LAZY_CONTENT_MANAGER.CHUNK_LOADED', "{!! $blockId !!}", data);

                    instance.updateSticky();
                });
            });
        </script>
    @endpush
@endunless
