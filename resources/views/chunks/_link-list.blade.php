<ol class="c-link-list list-group list-group-flush">
	@foreach($links as $i)
		<li class="list-group-item">
			<a href="{{ $i['link'] }}">{{ $i['title'] }}</a>
		</li>
	@endforeach
</ol>
