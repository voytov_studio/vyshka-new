<a href="javascript:;"
   data-fancybox data-type="ajax" data-src="{{ route('site.showPageLinksByService', ['id' => $id]) }}">
    {{ $slot }}
</a>
