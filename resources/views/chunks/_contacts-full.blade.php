<?php

$siteDataProvider = app(\App\Domain\SiteDataProvider::class);

?>
<div class="offices-box">
    <a href="javascript:" class="offices-box__opener js-office-opener"><span>Офисы в СПб, ЛО</span></a>
    <div class="offices-box__popup">
        <div class="offices-box__content">
            <div class="offices-box__offices-content">
                <span class="offices-box__content-title">Отделения</span>
                <ul class="offices-list popup_offices-list active" data-id="moscow">
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Комендантский проспект</b>, ул. Гаккелевская, 21.
                            БЦ Балтийский Деловой Центр (Ресо)
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Ладожская</b>, Пл. Карла Фаберже д. 8. БЦ Русские
                            Самоцветы
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Петроградская</b>, ул. Профессора Попова 37 лит
                            Щ. БЦ Сенатор
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Площадь Александра Невского</b>, ул. Херсонская
                            д. 12-14. БЦ Renaissance Pravda
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Площадь Ленина</b>, Пискарёвский просп., 2. БЦ
                            Бенуа
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Пролетарская</b>, пр. Обуховской обороны 112,
                            литера И, 3. БЦ Вант
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Сенная Площадь</b>, наб. р. Мойки 58, литера А.
                            БЦ Мариинский
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Технологический Институт</b>, 10-я
                            Красноармейская, 22. БЦ Kellermann Center
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Черная Речка</b>, Аптекарская наб., д. 20. БЦ
                            Авеню
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Площадь Восстания</b>, Лиговский проспект 21. БЦ
                            Сенатор
                        </div>
                    </li>
                </ul>
                <ul class="offices-list popup_offices-list" data-id="ek">
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Комендантский проспект</b>, ул. Гаккелевская,
                            21. БЦ Балтийский Деловой Центр (Ресо)
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Ладожская1</b>, Пл. Карла Фаберже д. 8. БЦ
                            Русские Самоцветы
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Петроградская</b>, ул. Профессора Попова 37 лит
                            Щ. БЦ Сенатор
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Площадь Александра Невского</b>, ул. Херсонская
                            д. 12-14. БЦ Renaissance Pravda
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Площадь Ленина</b>, Пискарёвский просп., 2. БЦ
                            Бенуа
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Пролетарская</b>, пр. Обуховской обороны 112,
                            литера И, 3. БЦ Вант
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Сенная Площадь</b>, наб. р. Мойки 58, литера А.
                            БЦ Мариинский
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Технологический Институт</b>, 10-я
                            Красноармейская, 22. БЦ Kellermann Center
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Черная Речка</b>, Аптекарская наб., д. 20. БЦ
                            Авеню
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Площадь Восстания</b>, Лиговский проспект 21. БЦ
                            Сенатор
                        </div>
                    </li>
                </ul>
                <ul class="offices-list popup_offices-list" data-id="ufa">
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Комендантский проспект</b>, ул. Гаккелевская, 21.
                            БЦ Балтийский Деловой Центр (Ресо)
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Ладожская</b>, Пл. Карла Фаберже д. 8. БЦ Русские
                            Самоцветы
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Петроградская</b>, ул. Профессора Попова 37 лит
                            Щ. БЦ Сенатор
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Площадь Александра Невского</b>, ул. Херсонская
                            д. 12-14. БЦ Renaissance Pravda
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Площадь Ленина</b>, Пискарёвский просп., 2. БЦ
                            Бенуа
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Пролетарская</b>, пр. Обуховской обороны 112,
                            литера И, 3. БЦ Вант
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Сенная Площадь</b>, наб. р. Мойки 58, литера А.
                            БЦ Мариинский
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Технологический Институт</b>, 10-я
                            Красноармейская, 22. БЦ Kellermann Center
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Черная Речка</b>, Аптекарская наб., д. 20. БЦ
                            Авеню
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Площадь Восстания</b>, Лиговский проспект 21. БЦ
                            Сенатор
                        </div>
                    </li>
                </ul>
                <ul class="offices-list popup_offices-list" data-id="kr">
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Комендантский проспект</b>, ул. Гаккелевская, 21.
                            БЦ Балтийский Деловой Центр (Ресо)
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Ладожская</b>, Пл. Карла Фаберже д. 8. БЦ Русские
                            Самоцветы
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Петроградская</b>, ул. Профессора Попова 37 лит
                            Щ. БЦ Сенатор
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Площадь Александра Невского</b>, ул. Херсонская
                            д. 12-14. БЦ Renaissance Pravda
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Площадь Ленина</b>, Пискарёвский просп., 2. БЦ
                            Бенуа
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Пролетарская</b>, пр. Обуховской обороны 112,
                            литера И, 3. БЦ Вант
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Сенная Площадь</b>, наб. р. Мойки 58, литера А.
                            БЦ Мариинский
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Технологический Институт</b>, 10-я
                            Красноармейская, 22. БЦ Kellermann Center
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Черная Речка</b>, Аптекарская наб., д. 20. БЦ
                            Авеню
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Площадь Восстания</b>, Лиговский проспект 21. БЦ
                            Сенатор
                        </div>
                    </li>
                </ul>
                <ul class="offices-list popup_offices-list" data-id="kazan">
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Комендантский проспект</b>, ул. Гаккелевская, 21.
                            БЦ Балтийский Деловой Центр (Ресо)
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Ладожская</b>, Пл. Карла Фаберже д. 8. БЦ Русские
                            Самоцветы
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img class="lazy" data-src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Петроградская</b>, ул. Профессора Попова 37 лит
                            Щ. БЦ Сенатор
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Площадь Александра Невского</b>, ул. Херсонская
                            д. 12-14. БЦ Renaissance Pravda
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Площадь Ленина</b>, Пискарёвский просп., 2. БЦ
                            Бенуа
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Пролетарская</b>, пр. Обуховской обороны 112,
                            литера И, 3. БЦ Вант
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Сенная Площадь</b>, наб. р. Мойки 58, литера А.
                            БЦ Мариинский
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Технологический Институт</b>, 10-я
                            Красноармейская, 22. БЦ Kellermann Center
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Черная Речка</b>, Аптекарская наб., д. 20. БЦ
                            Авеню
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Площадь Восстания</b>, Лиговский проспект 21. БЦ
                            Сенатор
                        </div>
                    </li>
                </ul>
                <ul class="offices-list popup_offices-list" data-id="nov">
                    <li>
                        <div class="offices-list__icon">
                            <img src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Комендантский проспект</b>, ул. Гаккелевская, 21.
                            БЦ Балтийский Деловой Центр (Ресо)
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Ладожская</b>, Пл. Карла Фаберже д. 8. БЦ Русские
                            Самоцветы
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Петроградская</b>, ул. Профессора Попова 37 лит
                            Щ. БЦ Сенатор
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Площадь Александра Невского</b>, ул. Херсонская
                            д. 12-14. БЦ Renaissance Pravda
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Площадь Ленина</b>, Пискарёвский просп., 2. БЦ
                            Бенуа
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Пролетарская</b>, пр. Обуховской обороны 112,
                            литера И, 3. БЦ Вант
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Сенная Площадь</b>, наб. р. Мойки 58, литера А.
                            БЦ Мариинский
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Технологический Институт</b>, 10-я
                            Красноармейская, 22. БЦ Kellermann Center
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Черная Речка</b>, Аптекарская наб., д. 20. БЦ
                            Авеню
                        </div>
                    </li>
                    <li>
                        <div class="offices-list__icon">
                            <img src="images/icon-underground.png" alt="" width="15" height="11">
                        </div>
                        <div class="offices-list__text"><b>Площадь Восстания</b>, Лиговский проспект 21. БЦ
                            Сенатор
                        </div>
                    </li>
                </ul>

            </div>
            <div class="offices-box__offices-zones">
				{{--
                <span class="offices-box__content-title">Зоны интересов</span>
                <ul class="zones-list">
                    <li><span data-id="moscow">г. Москва</span></li>
                    <li><span data-id="ek">г. Екатеринбург</span></li>
                    <li><span data-id="ufa">г. Уфа</span></li>
                    <li><span data-id="kr">г. Краснодар</span></li>
                    <li><span data-id="kazan">г. Казань</span></li>
                    <li><span data-id="nov">г. Нижний Новгород</span></li>
                </ul>
                --}}
            </div>
        </div>

        <div class="offices-box__sidebar">
            <div class="main-office">
                <span class="main-office__sub-title">Центральный филиал в<br>Санкт-Петербурге</span>
                <address class="main-office__address">{{ config('site.street_short') }}</address>
                <a href="javascript:" class="offices-box__content-opener js-sub-office-opener"><span>Отделения</span></a>
                <div class="main-office__schedule">
                    <span class="main-office__schedule-label">Режим работы офиса:</span>
                    <span class="main-office__schedule-time">Пн. - Пт.: {{ DynamicContentSheetFacade::getCfs()->get('workSchedule_text') }}</span>
                    <span class="main-office__schedule-time">Суббота: {{ DynamicContentSheetFacade::getCfs()->get('workSchedule_saturday') }}</span>
                </div>
                <div class="main-office__schedule">
                    <span class="main-office__schedule-label">Телефон:</span>
                    <span class="main-office__schedule-time">
                        <a href="tel:{!! $siteDataProvider->getPhoneEntry('main')->value !!}" class="phone">
                            <b>{!! $siteDataProvider->getPhoneEntry('main')->display !!}</b>
                        </a>
                    </span>
                </div>

                <span class="main-office__schedule-ordering">Заключение договоров:<span>24/7</span></span>
                <hr>
                <p>
                    Вы можете <a href="javascript:" class="copy_contact">скопировать наши контакты в буфер обмена</a>.
                </p>
                <div class="copy_message"></div>
            </div>
        </div>
        <a href="javascript:" class="offices-box__popup-close js-sub-office-close"><span>Свернуть</span></a>
    </div>
    <div class="offices-box__overlay js-office-close"></div>
</div>
