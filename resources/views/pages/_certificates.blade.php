<?php

/**
 * @var ContentFieldsStore $cfs
 */

$text = $cfs->getBlock('certificates_text');
$photoIds = $cfs->getCollection('certificates_items')->pluck('photo');

$photoUrls = array_map(function ($id) {
    $originalUrl = wp_get_image_by_post_id($id);
    $thumbnailUrl = wp_get_image_thumbnail_by_post_id($id, 'medium');

    // Миниатюра берётся из базы (если есть), или из специальной папки
    if ($thumbnailUrl === null) {
        $pathInfo = pathinfo($originalUrl);

        $thumbnailUrl = url('/storage/uploads/certificates/' . Arr::get($pathInfo, 'filename') . '.preview.' . $pathInfo['extension']);
    }

    return [
        'original' => $originalUrl,
        'thumbnail' => $thumbnailUrl,
    ];
}, $photoIds->all());

?>
<div class="sertificates">
	<div class="container">
		<div class="section-title">
			<h2>
				<span>Сертификаты и дипломы</span>
			</h2>

			{!! $text !!}
		</div>
		<div class="slider-wrap">
			<div class="container">
				<div class="sert-slider owl-carousel">
					@foreach ($photoUrls as $i)
						<div class="item">
							<div class="sert-item">
								<a href="{!! $i['original'] !!}" data-fancybox="sert">
									<img class="lazy" data-src="{!! $i['thumbnail'] !!}" alt="">
								</a>
							</div>
						</div>
					@endforeach
				</div>
			</div>
			<a href="javascript:" class="btn-slider-prev sert-prev"></a>
			<a href="javascript:" class="btn-slider-next sert-next"></a>
		</div>
	</div>
</div>
