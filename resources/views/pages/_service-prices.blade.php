<?php

/**
 * @var \Illuminate\Support\Collection $data
 */

$siteDataProvider = app(\App\Domain\SiteDataProvider::class);

$block = collect(data_get($data, '0.items-1.0.list', []));
$videoSectionFields = $cfs->getCollection('servprice_video');

$rawVideoUrl = $videoSectionFields->get('url');
$ytId = extract_yt_id($rawVideoUrl);

$videoUrl = make_youtube_embedded_url($rawVideoUrl);
$thumbnailUrl = make_youtube_default_thumbnail_url($ytId);

?>
<div class="services">
    <div class="container">
        <div class="services__header">
            <h3><span>Услуги и цены</span></h3>
            <div class="services__header-row">
                <div class="services__header-text">
                    <p>Мы позаботились о наших клиентах и собрали полный перечень всех услуг от профильных
                        юристов! Обратите внимание на эксклюзивные предложения {!! config('site.ot_kogo') !!}!</p>
                </div>
                <div class="services__header-video">
                    <div class="services__header-video-text">
                        <span class="title">{{ $videoSectionFields->get('title', 'Узнайте о услугах за 1 минуту') }}</span>
                        <span class="sub-title">{{ $videoSectionFields->get('subtitle', 'Рассказывает владелец') }}</span>
                    </div>
                    <div class="services__header-video-holder">
                        <div class="services__header-video-poster">
                            <img class="lazy" data-src="images/img03.jpg" alt="">
                        </div>
                        <a href="{{ $videoUrl }}" class="btn-play" data-fancybox></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="services__row">
            <div class="services__aside">
                <span class="risks__sidebar-title">Вид услуг:</span>
                <ul class="category-list js-tabs">
                    @php
                        $titles = $block->pluck('serv');
                    @endphp

                    @foreach ($titles as $index => $title)
                        <li>
                            <a href="#service-price-tab-{!! $index !!}" class="js-scroll-to-element @if ($index == 0) active @endif"
                               data-scroll-to="#service-price-tab-anchor">
                                <span>
                                    {{ $title }}
                                </span>
                            </a>
                        </li>
                    @endforeach

                    {{--
                    <li class="category-list-hidden">
                        <a href="#service-tab10">
                            <span>Выделение доли в натуре3</span>
                        </a>
                    </li>

                    <li>
                        <a href="javascript:" class="all js-category-list-opener">
                            <span>Все услуги</span>
                        </a>
                    </li>
                    --}}
                </ul>
            </div>
            <div class="services__content">
                <div class="services__content-header">
                    <div class="services__help-line">
                        <span class="pointer-left">Выберите раздел слева</span>
                        <span class="pointer-bottom">Нажмите на заголовок, чтобы узнать подробнее или заказать услугу</span>
                    </div>
                </div>

                <div id="service-price-tab-anchor"></div>

                @foreach ($block as $index => $items)
                    @php
                        $services = collect($items)->get('sublist');
                    @endphp
                    <div class="tab-content" id="service-price-tab-{!! $index !!}">
                        <ul class="info-list accordion">
                            @foreach ($services as $serviceIndex => $s)
                                @php
                                    $price = data_get($s, 'price');

                                    $price = str_replace('<i class="fa fa-rub"></i>', '<span class="price-value">i</span>', $price);

                                    $incase = collect(data_get($s, 'incase', []))->pluck('incase_item');
                                    $outcase = collect(data_get($s, 'outcase', []))->pluck('outcase_item');
                                @endphp
                                <li class="info-list__item @if ($serviceIndex == 0) active @endif">
                                    <a href="javascript:" class="info-list__opener">
                                        <div class="info-list__opener-text">
                                            <span class="info-list__opener-title">{{ data_get($s, 'title') }}</span>
                                            <span class="info-list__opener-sub-title">{!! data_get($s, 'visible') !!}</span>
                                        </div>
                                        <span class="info-list__opener-price">{!! $price !!}</span>
                                    </a>
                                    <div class="info-list__slide">
                                        <div class="info-list__slide-holder">
                                            <p>{!! nl2p(data_get($s, 'invisible')) !!}</p>
                                            <div class="info-list__slide-row row">
                                                @if(Arr::get($incase, '0', '') != '')
                                                    <div class="col-12 col-sm-6">
                                                        <h3>Что входит в услугу:</h3>
                                                        <ul class="tick-list tick-list_bordered">
                                                            @foreach($incase as $i)
                                                                <li>{{ $i }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif
                                                @if(Arr::get($outcase, '0', '') != '')
                                                    <div class="col-12 col-sm-6">
                                                        <h3>Что не входит в услугу:</h3>
                                                        <ul class="tick-list tick-list_not">
                                                            @foreach($outcase as $i)
                                                                <li>{{ $i }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="contact-info">
                                                <span class="contact-info__heading">Оставьте заявку на эту услугу</span>
                                                <div class="contact-info__row">
                                                    <div class="contact-info__form">
                                                        <form method="post" class="c-feedback-form js-feedback-form" data-parsley-validate>
                                                            <div class="contact-info__form-row">
                                                                <div class="contact-info__text-wrap">
                                                                    <input type="text" class="js-feedback-form__phone-input contact-info__text-field phone-mask " placeholder="Ваш телефон"
                                                                           data-parsley-required data-parsley-phone>
                                                                </div>
                                                                <input type="submit" class="js-feedback-form__submit submit" value="Перезвоните мне">
                                                            </div>
                                                            <div class="data-check font-tiny">
                                                                <input type="checkbox" class="checkbox" name="accept" checked="checked" id="check16" data-parsley-required>
                                                                <label for="check16">Согласен на обработку персональных данных</label>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
