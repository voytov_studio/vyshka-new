<?php

/**
 * @var ContentFieldsStore $cfs
 */

$videoUrl = $cfs->get('guarantees_video_url');
$listItems = $cfs->getCollection('guarantees_blocks_list')->pluck('item');

?>
<div class="guarantee-section">
    <div class="container">
        <h2 class="d-block d-sm-none">Наши гарантии</h2>
        <div class="guarantee-section__row">
            <div class="guarantee-section__video">
                <div class="guarantee-section__video-holder">
                    <div class="guarantee-section__video-poster">
                        <img src="//img.youtube.com/vi/ydh9aIuSgDM/hqdefault.jpg" alt="">
                    </div>
                    <a href="{{ $videoUrl }}" class="btn-play" data-fancybox></a>
                </div>
                <div class="guarantee-section__video-text">
                    <span class="guarantee-section__video-title">О гарантиях <span>за 1 минуту</span> </span>
                    <p>
                        {!! $cfs->get('guarantees_video_text') !!}
                    </p>
                </div>
            </div>
            <div class="guarantee-section__text-box">
                <h2 class="d-none d-sm-block"><span>Гарантии</span></h2>

                @include("chunks._guarantees", ['data' => $cfs->getCollection('guarantees_items')])

                <div class="d-none d-md-block">
                    <span class="payment-guarantee-label">{!! $cfs->get('guarantees_blocks_head') !!}</span>

                    @if($listItems->isNotEmpty())
                    <ul class="payment-guarantee-list">
                        @foreach($listItems as $item)
                            <li>{{ $item }}</li>
                        @endforeach
                    </ul>
                    @endif
                </div>
            </div>
        </div>
        <div class="d-block d-md-none h_center">
            <span class="payment-guarantee-label">{!! $cfs->get('guarantees_blocks_head') !!}</span>

            @if($listItems->isNotEmpty())
                <ul class="payment-guarantee-list h_center">
                    @foreach($listItems as $item)
                        <li>{{ $item }}</li>
                    @endforeach
                </ul>
            @endif
        </div>
    </div>
</div>
