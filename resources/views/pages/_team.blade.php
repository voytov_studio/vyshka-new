<?php

/**
 * @var ContentFieldsStore $cfs
 */

?>
<div class="team js-team-section">
    <div class="container">
        <div class="section-title">
            <h2><span>Кому Вы доверяете решение своего вопроса</span></h2>
        </div>

        @if (false)
        <div class="row justify-content-center">
            <div class="col-8 tele-item">
                <div class="tele-item__video" style="background:url(//img.youtube.com/vi/EFfbGIASiiE/hqdefault.jpg) no-repeat center center;">
                    <a href="https://www.youtube.com/embed/EFfbGIASiiE" class="btn-play" data-fancybox></a>
                </div>
            </div>
        </div>
        @endif

        <?php

        $tabCount = $cfs->get('specialists_people', 0);

        ?>
        <div class="centered-tabs">
            <div class="how-it-works__tabs-wrap">
                <ul class="how-it-works__tabs js-tabs">
                    @foreach (range(0, $tabCount - 1) as $tabIndex)
                        <li>
                            <a href="#team-tab-{!! $tabIndex + 1 !!}" class="@if ($tabIndex == 0) active @endif">
                                <span>
                                    {!! $cfs->getBlock('specialists_people.' . $tabIndex . '.tab-title') !!}
                                </span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="tab-content-wrap">
            @foreach (range(0, $tabCount - 1) as $tabIndex)
                <div class="tab-content" id="team-tab-{!! $tabIndex + 1 !!}">
                    <div class="slider-wrap">
                        <div class="container">
                            <div class="team-slider owl-carousel">
                                <?php

                                $itemCount = (int)$cfs->get('specialists_people.' . $tabIndex . '.items');

                                ?>
                                @foreach (range(0, $itemCount - 1) as $itemIndex)
                                    <?php
                                    $baseContentBlockKey = 'specialists_people.' . $tabIndex . '.items.' . $itemIndex;
                                    ?>
                                    <div class="item">
                                        <div class="team-item">
                                            <div class="team-item__image">
                                                <img class="lazy" data-src="{!! wp_get_image_by_post_id($cfs->get($baseContentBlockKey . '.photo')) !!}" alt="">
                                            </div>
                                            <span class="team-item__name">
                                                {!! $cfs->getBlock($baseContentBlockKey . '.name') !!}
                                            </span>
                                            <span class="team-item__position">
                                                {!! $cfs->getBlock($baseContentBlockKey . '.job') !!}
                                            </span>

                                            {!! $cfs->getTextBlock($baseContentBlockKey . '.text') !!}

                                            <a href="#connect-modal" class="team-item__contact-person fancybox">
                                                Связаться с сотрудником
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <a href="javascript:" class="btn-slider-prev team-prev"></a>
                        <a href="javascript:" class="btn-slider-next team-next"></a>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="team-video">
            <div class="team-video__holder">
                <div class="team-video__poster">
                    <img class="lazy" data-src="images/img13.jpg" alt="">
                    <a href="{{ $cfs->get('specialists_video') }}" class="btn-play" data-fancybox></a>
                </div>
            </div>
            <div class="team-video__text-box">
                <span class="team-video__text-title">Все факты о нашей команде за 3 минуты видео</span>
                <span class="team-video__text">Узнайте почему у наc Очень крутая и влиятельная юридическая команда</span>
            </div>
        </div>
    </div>
</div>
