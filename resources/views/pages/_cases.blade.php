<?php

/**
 * @var \Illuminate\Support\Collection $items
 * @var \Illuminate\Support\Collection $attachments
 */

?>
<div class="cases">
    <div class="container">
        <div class="section-title">
            <h2><span>Примеры из практики</span></h2>
        </div>
        <div class="slider-wrap">
            <div class="container">
                <div class="cases-slider owl-carousel">
                    @foreach ($items->all() as $i)
                        <?php
                        $isEnabled = (bool)Arr::get($i, 'enabled', true);

                        if ($isEnabled === false)
                            continue;

                        $docs = Arr::get($i, 'dec_docs', []);
                        $hasDocs = !empty($docs);

                        if ($hasDocs) {
                            $docs = collect($docs)
                                ->filter(function ($item) {
                                    return !empty($item);
                                })
                                ->pluck('dec_docs_file')
                                ->map(function ($item) use ($attachments) {
                                    return Arr::get($attachments, $item);
                                });
                        }
                        ?>
                        <div class="item">
                            <div class="case-item">
                                <div class="case-item__header">
                                    <h3>{{ Arr::get($i, 'dec_title', 'Судебное решение') }}</h3>
                                    <span class="case-item__date">от {{ $i['dec_date'] }}</span>
                                </div>
                                <dl>
                                    <dt>Номер дела:</dt>
                                    <dd>№{{ Arr::get($i, 'dec_number') }}</dd>
                                    <dt>Результат:</dt>
                                    <dd>
                                        {{ Arr::get($i, 'dec_result') }}
                                    </dd>
                                </dl>

                                @if($hasDocs)
                                    <div class="row">
                                        @foreach($docs as $index => $document)
                                            <div class="col-3 case-item__attachments">
                                                @if(isset($document['thumbnail_url']))
                                                    <a href="{!! $document['url'] !!}" class="fancybox">
                                                        <img data-src="{!! $document['thumbnail_url'] !!}" class="lazy case-item__image-preview">
                                                    </a>
                                                @else
                                                    <a href="{{ $document['url'] }}" class="text-primary" target="_blank">
                                                        Открыть файл [{{ $document['name'] }}]
                                                    </a>
                                                @endif
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                                <div class="case-item__stick green">
                                    <span class="case-item__stick-text">
                                        {{ Arr::get($i, 'dec_cat') }}
                                    </span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <a href="javascript:" class="btn-slider-prev cases-prev"></a>
            <a href="javascript:" class="btn-slider-next cases-next"></a>
        </div>
    </div>
</div>
