<?php

/**
 * @var \App\Domain\WordPressPostAggregate $postAggregate
 */

$returnUrl = $returnUrl ?? route('blog.index');

Debugbar::info('Post ID: ' . $postAggregate->model->getKey());

$siteDataProvider = app(\App\Domain\SiteDataProvider::class);
$chunkFactory = app(\App\Domain\ContentChunkFactory::class);

$cfs = DynamicContentSheetFacade::getCfs();

?>
@extends('layouts.main')

@section('title', $cfs->get('title', $postAggregate->getSeoTitle()))

@push('head')
    <meta name="keywords" content="{!! $cfs->get('keywords') !!}">
    <meta name="description" content="{!! $cfs->get('description') !!}">
    <meta name="robots" content="{!! $cfs->get('robots') !!}">
@endpush

@section('content')
    <main data-page-id="{!! $postAggregate->model->getKey() !!}">

        @include("pages._promo-box")

        @php
            $chunk = $chunkFactory->make('need_to_know');
            $chunk->postAggregate = ContentPageContextFacade::getCurrentPostAggregate();

            echo $chunk->render();
        @endphp

        @include("pages._guarantees")

        @include("_shared._lazy-content-loader", ['contentId' => 'service_prices'])
        @include("_shared._lazy-content-loader", ['contentId' => 'prices'])

        <div class="helper">
            <div class="container">
                <div class="contact-info">
                    <span class="contact-info__question">
                        {!! ContentFieldsStore::getBlock('svetlana_title') !!}
                    </span>
                    <p>
                        {!! ContentFieldsStore::getBlock('svetlana_text') !!}
                    </p>

                    <div class="row justify-content-center">
                        <div class="col-10 col-sm-8 mb-4">
                            @include("_shared._call-me-form-variant-2")
                        </div>
                    </div>

                    <div class="help-box__phone-holder">
                        <div class="help-box__phone-icon">
                            <img class="lazy" data-src="images/icon-phone02.png" alt="">
                        </div>

                        @include("_shared.call-to-us", ['skipPhones' => ['primary']])
                    </div>
                    <div class="contact-info__image">
                        <img class="lazy" data-src="images/img04.png" alt="" width="232" height="404">
                    </div>
                </div>
            </div>
        </div>

        @php
            $chunk = $chunkFactory->make('risks');
            $chunk->postAggregate = ContentPageContextFacade::getCurrentPostAggregate();

            echo $chunk->render();
        @endphp

        @include("_shared._lazy-content-loader", ['contentId' => 'how_it_works'])

        <div class="contacts-section">
            <div class="container">
                <div class="section-title">
                    <h2><span>Как с нами связаться?</span></h2>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12 col-sm-6 col-md mobile-col">
                        <div class="contact-item">
                            <div class="contact-item__icon">
                                <img class="lazy" data-src="images/icon-contact01.svg" alt="" width="45" height="45">
                            </div>

                            <a href="tel:{{ $siteDataProvider->getPhoneEntry('main')->value }}" class="phone js-mango-number">
                                {{ $siteDataProvider->getPhoneEntry('main')->display }}
                            </a>

                            <p>
                                {!! $cfs->getBlock('contactText_phone', 'Звоните нам по телефону, мы доступны 24/7') !!}
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md mobile-col">
                        <div class="contact-item">
                            <div class="contact-item__icon">
                                <img class="lazy" data-src="images/icon-contact02.svg" alt="" width="48" height="48">
                            </div>
                            <a href="#callback-modal" class="callback fancybox"><span>Заказать звонок</span></a>
                            <p>
                                {!! $cfs->getBlock('contactText_callback', 'Можем перезвонить сами в удобное для Вас время') !!}
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md mobile-col">
                        <div class="contact-item">
                            <div class="contact-item__icon">
                                <img class="lazy" data-src="images/icon-contact03.svg" alt="" width="48" height="42">
                            </div>
                            <ul class="social-list">
                                <li>
                                    <a href="javascript:" onclick="window.app.widgets.openChatWidget(); return false;">
                                        <img class="lazy" data-src="images/icon-wechat.svg" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="{!! config('app.settings.social_links.whatsapp') !!}" target="_blank" rel="noopener nofollow noreferrer">
                                        <img class="lazy" data-src="images/icon-whatsapp.svg" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="{!! config('app.settings.social_links.telegram') !!}" target="_blank" rel="noopener nofollow noreferrer">
                                        <img class="lazy" data-src="images/icon-telegram.svg" alt="">
                                    </a>
                                </li>
                            </ul>
                            <p>
                                {!! $cfs->getBlock('contactText_messenger', 'Или пишите в чатах, онлайн 24/7') !!}
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md mobile-col">
                        <div class="contact-item">
                            <div class="contact-item__icon">
                                <img class="lazy" data-src="images/icon-contact04.svg" alt="" width="48" height="48">
                            </div>
                            <a href="mailto:{{ config('app.settings.contact_email') }}" class="email">
                                <span>{{ config('app.settings.contact_email') }}</span>
                            </a>
                            <p>
                                {!! $cfs->getBlock('contactText_email', 'Всегда ответим Вам по почте') !!}
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md mobile-col">
                        <div class="contact-item">
                            <div class="contact-item__icon">
                                <img class="lazy" data-src="images/icon-contact05.svg" alt="" width="48" height="45">
                            </div>
                            <a href="#meeting-modal" class="meeting fancybox"><span>Записаться на встречу</span></a>
                            <p>
                                {!! $cfs->getBlock('contactText_meet', 'Личная встреча для более подробной консультации') !!}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include("_shared._lazy-content-loader", ['contentId' => 'consultation'])

        @php
            $chunk = $chunkFactory->make('we_work_with');
            $chunk->postAggregate = ContentPageContextFacade::getCurrentPostAggregate();

            echo $chunk->render();
        @endphp


        @if($cfs->isNotMissingOrEmpty('tatianaInner_enabled'))
        <div class="helper-green c-tatiana-chunk">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm helper-green__person">
                        <div class="d-none d-md-block position-relative">
                            <div class="about-person">
                                <b>
                                    {!! $cfs->get('tatianaInner_title') !!}
                                </b>

                                {!! $cfs->get('tatianaInner_text') !!}
                            </div>
                            <div class="about-person__girl"></div>
                        </div>
                        <div class="help-box__phone-holder">
                            <div class="help-box__phone-icon">
                                <img class="lazy" data-src="images/icon-phone05.png" alt="">
                            </div>

                            @include("_shared.call-to-us", ['skipPhones' => ['primary']])
                        </div>
                    </div>
                    <div class="col-12 col-sm-7 helper-green__form">
                        <div class="contact-info">
                            <div class="contact-info__row">
                                <div class="contact-info__form">
                                    {{-- FORM --}}
                                    <form method="post" class="js-feedback-form" data-parsley-validate>
                                        <div class="contact-info__form-row">
                                            <span class="contact-info__question">Помогу в решении Вашей проблемы</span>
                                            <p>Оставьте свой номер телефона и я свяжусь с вами в течение 1 минуты и
                                                отвечу на все ваши вопросы</p>
                                            <div class="contact-info__text-wrap">
                                                <input type="text" class="js-feedback-form__phone-input contact-info__text-field phone-mask" name="cons_phone13"
                                                       placeholder="Ваш телефон" data-parsley-required data-parsley-phone>
                                            </div>
                                            <input type="button" class="js-feedback-form__submit submit cons_send13" value="Перезвоните мне">
                                        </div>
                                        <div class="data-check font-tiny">
                                            <input type="checkbox" class="checkbox" name="accept" checked="checked" id="check999" data-parsley-required>
                                            <label for="check999">Согласен на обработку персональных данных</label>
                                        </div>
                                    </form>
                                    {{-- /FORM --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif

        @php
            $chunk = $chunkFactory->make('case_development');
            $chunk->postAggregate = ContentPageContextFacade::getCurrentPostAggregate();;

            echo $chunk->render();
        @endphp

        @php
            $chunk = $chunkFactory->make('faq');
            $chunk->postAggregate = ContentPageContextFacade::getCurrentPostAggregate();

            echo $chunk->render();
        @endphp

        @php
            $chunk = $chunkFactory->make('problems');

            echo $chunk->render();
        @endphp

        @php
            $block = app(\App\Domain\Content\TermsOfSolution::class);
            $block->postAggregate = $postAggregate;

            echo $block->render();
        @endphp

        @include("_shared._lazy-content-loader", ['contentId' => 'team'])
        @include("_shared._lazy-content-loader", ['contentId' => 'cases'])
        @include("_shared._lazy-content-loader", ['contentId' => 'certificates'])
        @include("_shared._lazy-content-loader", ['contentId' => 'mass_media'])
        @include("_shared._lazy-content-loader", ['contentId' => 'reviews'])
    </main>
@endsection

@push('scripts')
    <script>
        window.currentPageId = {!! $postAggregate->model->getKey() !!};
    </script>
@endpush
