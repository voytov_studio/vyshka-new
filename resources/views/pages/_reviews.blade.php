<?php

/**
 * @var \App\Domain\ContentFieldsStore $cfs
 */

$siteDataProvider = app(\App\Domain\SiteDataProvider::class);

$paperReviews = $cfs->getArray('reviews_paper');
$webReviews = $cfs->getArray('reviews_web');
$textReviews = $cfs->getArray('reviews_text');
$videoReviews = $cfs->getArray('reviews_video');

$tabDefinitions = (object)[
    'text' => (object)[
        'label' => 'Текстовые',
        'reference' => '#reviews-tab-01',
        'content' => $textReviews,
        'disabled' => $cfs->isMissingOrEmpty('reviews_text'),
    ],
    'paper' => (object)[
        'label' => 'Бумажные',
        'reference' => '#reviews-tab-02',
        'content' => $paperReviews,
        'disabled' => $cfs->isMissingOrEmpty('reviews_paper'),
    ],
    'web' => (object)[
        'label' => 'В интернете',
        'reference' => '#reviews-tab-03',
        'content' => $webReviews,
        'disabled' => $cfs->isMissingOrEmpty('reviews_web'),
    ],
    'video' => (object)[
        'label' => 'Видео',
        'reference' => '#reviews-tab-04',
        'content' => $videoReviews,
        'disabled' => $cfs->isMissingOrEmpty('reviews_video'),
    ],
];

$activeTabId = 'text';

?>
<div class="reviews-section">
    <div class="container">
        <div class="section-title">
            <h2><span>Отзывы клиентов</span></h2>
        </div>
        <div class="centered-tabs">
            <div class="how-it-works__tabs-wrap">
                <ul class="how-it-works__tabs js-tabs">
                    @foreach ($tabDefinitions as $index => $tab)
                        @php
                            if ($tab->disabled)
                                continue;
                        @endphp
                        <li>
                            <a href="#reviews-tab-{!! $tab->reference !!}" class="@if ($index == $activeTabId) active @endif">
                                <span>{{ $tab->label }}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="tab-content-wrap">
            @unless($tabDefinitions->text->disabled)
            <div class="tab-content" id="reviews-tab-01">
                <div class="slider-wrap">
                    <div class="container">
                        <div class="reviews-slider-1 owl-carousel">
                            @foreach ($textReviews as $index => $i)
                                @php
                                    $thumbnailUserPhoto = wp_get_image_thumbnail_by_post_id(Arr::get($i, 'userPhoto'));

                                    $caseFile = Arr::get($i, 'case_file');

                                    if ($caseFile !== null)
                                        $caseFile = wp_get_uri_by_post_id($caseFile);

                                    $caseNumber = Arr::get($i, 'case_number');
                                    $caseResult = nl2p(Arr::get($i, 'case_result'));
                                    $modalId = 'text-review-modal-' . $index;
                                @endphp
                                <div class="item">
                                    <div class="review-item">
                                        <div class="review-item__text-box review-item__text-box_type">
                                            <div class="review-item__person-row">
                                                <div class="review-item__person-image small">
                                                    <img class="lazy" data-src="{{ $thumbnailUserPhoto }}" alt="" width="80" height="80">
                                                </div>
                                                <div class="review-item__person-text">
                                                    <span class="review-item__person-name">{{ Arr::get($i, 'name') }}</span>
                                                    <span class="review-item__person-date">{{ Arr::get($i, 'date') }}</span>
                                                </div>
                                                <div class="review-item__wrap h_pt20">
                                                    @if($caseNumber)
                                                        <a href="{!! $caseFile !!}" class="review-item__case-number" download>
                                                            Дело №{!! $caseNumber !!}
                                                        </a>
                                                        <a href="#{!! $modalId !!}" class="review-item__result-link fancybox">
                                                            Результат
                                                        </a>
                                                    @endif
                                                </div>

                                                {!! nl2p(Arr::get($i, 'text')) !!}
                                            </div>
                                        </div>
                                    </div>

                                    @push('review-modals')
                                        @include("content-chunks._reviews-result-modal", [
                                            'modalId' => $modalId,
                                            'caseNumber' => $caseNumber,
                                            'caseResult' => $caseResult,
                                        ])
                                    @endpush
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <a href="javascript:" class="btn-slider-prev reviews-prev1"></a>
                    <a href="javascript:" class="btn-slider-next reviews-next1"></a>
                </div>
            </div>
            @endunless

            @unless($tabDefinitions->paper->disabled)
            <div class="tab-content" id="reviews-tab-02">
                <div class="slider-wrap">
                    <div class="container">
                        <div class="reviews-slider-2 owl-carousel">
                            @foreach ($paperReviews as $index => $i)
                                @php
                                    $originalImageUrl = wp_get_image_by_post_id(Arr::get($i, 'photo'));
                                    $thumbnailImageUrl = wp_get_image_thumbnail_by_post_id(Arr::get($i, 'photo'));

                                    $thumbnailUserPhoto = wp_get_image_thumbnail_by_post_id(Arr::get($i, 'userPhoto'));

                                    $caseFile = Arr::get($i, 'case_file');

                                    if ($caseFile !== null)
                                        $caseFile = wp_get_uri_by_post_id($caseFile);

                                    $caseNumber = Arr::get($i, 'case_number');
                                    $caseResult = nl2p(Arr::get($i, 'case_result'));
                                    $modalId = 'paper-review-modal-' . $index;
                                @endphp
                                <div class="item">
                                    <div class="review-item">
                                        <div class="review-item__image bg-image">
                                            <a href="{{ $originalImageUrl }}" class="review-item__image-link bg-image" data-fancybox>
                                                <img src="{{ $thumbnailImageUrl }}" alt="">
                                            </a>
                                        </div>
                                        <div class="review-item__text-box review-item__text-box_type">
                                            <div class="review-item__person-row">
                                                <div class="review-item__person-image small">
                                                    <img class="lazy" data-src="{{ $thumbnailUserPhoto }}" alt="" width="80" height="80">
                                                </div>
                                                <div class="review-item__person-text">
                                                    <span class="review-item__person-name">{{ Arr::get($i, 'name') }}</span>
                                                    <span class="review-item__person-date">{{ Arr::get($i, 'date') }}</span>
                                                </div>
                                                <div class="review-item__wrap h_pt20">
                                                    @if($caseNumber)
                                                        <a href="{!! $caseFile !!}" class="review-item__case-number" download>
                                                            Дело №{!! $caseNumber !!}
                                                        </a>
                                                        <a href="#{!! $modalId !!}" class="review-item__result-link fancybox">
                                                            Результат
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    @push('review-modals')
                                        @include("content-chunks._reviews-result-modal", [
                                            'modalId' => $modalId,
                                            'caseNumber' => $caseNumber,
                                            'caseResult' => $caseResult,
                                        ])
                                    @endpush
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <a href="javascript:" class="btn-slider-prev reviews-prev2"></a>
                    <a href="javascript:" class="btn-slider-next reviews-next2"></a>
                </div>
            </div>
            @endunless

            @unless($tabDefinitions->web->disabled)
            <div class="tab-content" id="reviews-tab-03">
                <div class="slider-wrap">
                    <div class="container">
                        <div class="reviews-slider-3 owl-carousel">
                            @foreach($webReviews as $index => $i)
                                <div class="item">
                                    <div class="review-item">
                                        <div class="review-item__text-box">
                                            <div class="review-item__icon">
                                                <img class="lazy" data-src="{{ Arr::get($i, 'imageUrl') }}" alt="" style="height: 60px">
                                            </div>
                                            <div class="other-text-box">
                                                <span class="other-title">
                                                    {{ Arr::get($i, 'title') }}
                                                </span>

                                                {!! Arr::get($i, 'text') !!}

                                                <a href="{{ Arr::get($i, 'link') }}" class="read-more">
                                                    Читать отзывы на
                                                    <span class="font-weight-bold">{{ Arr::get($i, 'siteName') }}</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <a href="javascript:" class="btn-slider-prev reviews-prev3"></a>
                    <a href="javascript:" class="btn-slider-next reviews-next3"></a>
                </div>
            </div>
            @endunless

            @unless($tabDefinitions->video->disabled)
            <div class="tab-content" id="reviews-tab-04">
                <div class="slider-wrap">
                    <div class="container">
                        <div class="reviews-slider-4 owl-carousel">
                            @foreach($videoReviews as $index => $i)
                                @php
                                    $ytId = extract_yt_id(acf_extract_url_from_link(Arr::get($i, 'movie')));

                                    $caseFile = Arr::get($i, 'case_file');

                                    if ($caseFile !== null)
                                        $caseFile = wp_get_uri_by_post_id($caseFile);

                                    $caseNumber = Arr::get($i, 'case_number');
                                    $caseResult = nl2p(Arr::get($i, 'case_result'));
                                    $modalId = 'video-review-modal-' . $index;
                                @endphp
                                <div class="item" id="video-review-item-{!! $index !!}">
                                    <div class="review-item">
                                        <div class="review-item__video bg-image">
                                            <a href="https://www.youtube.com/embed/{!! $ytId !!}" class="review-item__image-link bg-image" data-fancybox>
                                                <img src="{!! make_youtube_default_thumbnail_url($ytId) !!}" alt="">
                                            </a>
                                        </div>
                                        <div class="review-item__text-box review-item__text-box_type">
                                            <div class="review-item__person-row">
                                                <div class="review-item__person-text">
                                                    <span class="review-item__person-name">{!! Arr::get($i, 'name') !!}</span>
                                                    <span class="review-item__person-date">{!! Arr::get($i, 'date') !!}</span>
                                                </div>
                                                <div class="review-item__wrap h_pt20">
                                                    @if($caseNumber)
                                                        <a href="{!! $caseFile !!}" class="review-item__case-number" download>Дело
                                                            №{!! $caseNumber !!}</a>
                                                        <a href="#{!! $modalId !!}" class="review-item__result-link fancybox">
                                                            Результат
                                                        </a>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    @push('review-modals')
                                        @include("content-chunks._reviews-result-modal", [
                                            'modalId' => $modalId,
                                            'caseNumber' => $caseNumber,
                                            'caseResult' => $caseResult,
                                        ])
                                    @endpush
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <a href="javascript:" class="btn-slider-prev reviews-prev4"></a>
                    <a href="javascript:" class="btn-slider-next reviews-next4"></a>
                </div>
            </div>
            @endunless

        </div>

        @stack('review-modals')
    </div>
</div>
