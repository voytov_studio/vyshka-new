<?php

/**
 * @var \App\Domain\ContentFieldsStore $cfs
 */

$commonTitle = $cfs->get('prices_common_title');

$itemCount = $cfs->get('prices');
$items = collect($cfs->getArray('prices'))->slice(0, $itemCount);

$specialOfferEndsOn = \Carbon\Carbon::now()->addDays(4);

?>
<div class="price-tables">
    <div class="container">
        <div class="price-tables__header">
            <h2>
                <span class="bordered-line">
                    Услуги
                    @if ($commonTitle)
                        {{ $commonTitle }}
                    @else
                        профильного юриста
                    @endif
                </span>

                <span class="title-sale-text"> для Вас со скидкой</span>
                <span class="sale-date">
                    Спецпредложение до <?= $specialOfferEndsOn->format('d.m.Y') ?>
                </span>
            </h2>
        </div>
        <div class="price-tables__row row">
            @foreach ($items as $index => $p)
                @php
                    $p = collect($p);

                    $services = collect($p->get('prices-services'))
                        ->pluck('prices-service');

                    $currentPrice = (double)$p['prices-offer-price'];
                    $originalPrice = (double)$p['prices-sum'];

                    if ($originalPrice == 0) {
                        $discountPercentage = null;
                    }
                    else {
                        $discountPercentage = 100 - number_format($currentPrice * 100 / $originalPrice, 0);
                    }
                @endphp
                @if (($index + 1) == $items->count())
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="price-item price-item_filled">
                            <div class="price-item__header">
                                <div class="price-item__sub-title" style="margin-bottom: 1rem">{{ $p['prices-offer-title'] }}</div>
                                @if(isset($p['prices-title']))
                                    <div class="price-item__title">{{ $p['prices-title'] }}</div>
                                @endif
                                <div class="price-item__old-price">
                                    от {!! number_format($originalPrice, 0, '.', ' ') !!}
                                    <span class="price-value">i</span>
                                </div>
                                <div class="price-item__price">
                                    от <b>{!! number_format($currentPrice, 0, '.', ' ') !!}
                                        <span class="price-value">p</span></b>
                                </div>
                                @if ($discountPercentage)
                                    <div class="price-item__stick">-{!! $discountPercentage !!}%</div>
                                @endif
                            </div>
                            <div class="price-item__content">
                                <ul class="price-item__list">
                                    @foreach ($services as $serviceName)
                                        <li>{{ $serviceName }}</li>
                                    @endforeach
                                </ul>
                                <a href="#get-sale-modal" class="btn-yellow fancybox">Получить скидку</a>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="price-item">
                            <div class="price-item__header">
                                @if(isset($p['prices-title']))
                                    <div class="price-item__title">{{ $p['prices-title'] }}</div>
                                @endif
                                @if ($p['prices-offer'])
                                    <div class="price-item__old-price">
                                        от {!! number_format($originalPrice, 0, '.', ' ') !!}
                                        <span class="price-value">i</span>
                                    </div>
                                    <div class="price-item__price"> от
                                        <b>{!! number_format($currentPrice, 0, '.', ' ') !!}
                                            <span class="price-value">p</span></b>
                                    </div>
                                @endif
                            </div>
                            <div class="price-item__content">
                                <ul class="price-item__list">
                                    @foreach ($services as $serviceName)
                                        <li>{{ $serviceName }}</li>
                                    @endforeach
                                </ul>
                                <a href="#get-sale-modal" class="btn-yellow fancybox">Получить скидку</a>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
        <div class="price-tables__note">
            <div class="price-tables__note-icon">
                <img class="lazy" data-src="images/icon-wallet.png" alt="" width="40" height="38">
            </div>
            <div class="price-tables__note-text">
                <span class="price-tables__note-sub-title">На все акции действует</span>
                <span class="price-tables__note-title">рассрочка до 12 месяцев</span>
            </div>
        </div>
    </div>
</div>
