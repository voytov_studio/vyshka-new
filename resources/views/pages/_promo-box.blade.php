<?php

$siteDataProvider = app(\App\Domain\SiteDataProvider::class);
$cfs = DynamicContentSheetFacade::getCfs();

$offerSection = $cfs->getCollection('new_offer_tabs');

$tabNames = $offerSection->pluck('offer_tab_name');
$titles = $offerSection->pluck('offer_title');

if (!empty($titles)) {
    $titles = collect($titles)->filter(function ($value) {
        return !empty($value);
    })->slice(1, 3);
}

?>
<div class="promo-box">
    <div class="promo-box__slide slide-1">
        <div class="container">
            <div class="d-none d-lg-block">
                <div class="promo-box-dots">
                    @foreach ($tabNames as $index => $text)
                        <a href="javascript:" class="owl-dot @if ($index == 0) active @endif">
                            <span>{{ $text }}</span>
                        </a>
                    @endforeach
                </div>

                <div class="promo-slider owl-carousel">
                    @foreach ($offerSection as $index => $item)
                        <div class="item">
                            <div class="promo-slider__content">
                                <h1>
                                    {{ Arr::get($item, 'offer_title') }}
                                </h1>
                                <p>
                                    {{ Arr::get($item, 'offer_subtitle_txt') }}<br/>
                                </p>
                                <p class="font-weight-bold">
                                    {{ Arr::get($item, 'offer_subtitle_idea') }}
                                </p>
                            </div>
                        </div>
                    @endforeach
                </div>

                @if(is_ekrual())
                    {{-- Ничего не выводим --}}
                @else
                    <div class="country-rating">
                        <div class="country-rating__row">
                            <div class="country-rating__score">2 место</div>
                            <div class="country-rating__text drop-shadow--light">
                                <span class="country-rating__label">в рейтинге юридических<br/>фирм в России 2017</span>
                                <a href="https://300.pravo.ru/archive/#2017" class="country-rating__link" target="_blank" rel="noopener nofollow noreferrer">Pravo.ru</a>
                            </div>
                        </div>
                    </div>
                @endif
            </div>

            <div class="d-lg-none">
                @php
                    $offerItem = \Illuminate\Support\Arr::get($offerSection, 0);
                @endphp

                <div class="c-mobile-jumbotron">
                    <div class="c-mobile-jumbotron__content">
                        <h1>
                            {{ Arr::get($offerItem, 'offer_title') }}
                        </h1>
                        <p>
                            {{ Arr::get($offerItem, 'offer_subtitle_txt') }}<br/>
                        </p>
                        <p class="font-weight-bold">
                            {{ Arr::get($offerItem, 'offer_subtitle_idea') }}
                        </p>
                    </div>
                </div>
            </div>

            <div class="promo-slider__sidebar">
                <div class="help-box">
                    <div class="d-lg-none">
                        <div class="help-form">
                            @include("_shared._call-me-form-variant-3", [
                                'buttonLabel' => DynamicContentSheetFacade::getCfs()->get('offer-button')
                            ])
                        </div>
                    </div>

                    <div class="help-box__title">Консультация<br>бесплатно
                        <span class="help-box__title-price">5000 <span class="price-value">i</span></span>
                    </div>
                    <div class="help-box__stick d-none d-lg-block">
                        <span class="help-box__stick__label"><span>На текущий<br/>момент </span>осталось</span>
                        <span class="help-box__stick__amount">5 мест</span>
                    </div>
                    @unless(empty($titles))
                        <ul class="tick-list">
                            @foreach($titles as $t)
                                <li>{{ $t }}</li>
                            @endforeach
                        </ul>
                    @endunless

                    <div class="d-none d-lg-block">
                        <ul class="main-office__tabs js-tabs">
                            <li><a href="#tab-help-1" class="active">Сейчас</a></li>
                            <li><a href="#tab-help-2">На время</a></li>
                        </ul>
                        <div class="help-form">
                            <div class="tab-content" id="tab-help-1">

                                @include("_shared._call-me-form-variant-3", [
                                    'buttonLabel' => DynamicContentSheetFacade::getCfs()->get('offer-button')
                                ])

                            </div>
                            <div class="tab-content" id="tab-help-2">

                                @include("_shared._call-me-form-with-time")

                            </div>
                        </div>
                    </div>

                    <div class="help-box__footer d-none d-sm-block">
                        <div class="help-box__phone-holder">
                            <div class="help-box__phone-icon">
                                <img class="lazy" data-src="images/icon-phone02.png" alt="">
                            </div>

                            @include("_shared.call-to-us", ['skipPhones' => ['primary']])
                        </div>
                        <div class="help-box__sms"><span class="label">или получите наши контакты</span>
                            <a href="#sms-modal" class="fancybox">по СМС</a>
                            <span class="help-box__sms-stick">NEW</span>
                        </div>
                    </div>
                </div>
            </div>

            @if(is_ekrual())
                {{-- Не выводим --}}
            @else
                <div class="promo-box__video x-device--desktop d-none d-sm-flex">
                    <div class="promo-box__video-text">
                        <div class="promo-box__text-wrapper">
                            <span class="promo-box__video-title">Посмотрите минутное видео</span>
                            <div class="mb-4">
                                и узнайте, как правильно выбрать юридическую фирму для Ваших целей, сэкономить и не
                                рисковать.
                            </div>

                            <div class="promo-box__video-holder d-block d-sm-none">
                                <div class="promo-box__video-poster">
                                    <div class="promo-box__video-poster-image-wrapper">
                                        <img class="lazy" data-src="images/img07.jpg" alt="">
                                    </div>
                                </div>
                                <a href="{{ DynamicContentSheetFacade::getCfs()->get('promoBox_video') }}" class="btn-play" data-fancybox></a>
                            </div>

                            <span class="promo-box__author"><span>Владелец компании</span> <a href="#person-modal" class="fancybox">Шевельков И. В.</a></span>
                            <a href="{!! config('app.settings.social_links.youtube') !!}" class="more-video d-inline-block d-sm-none">Еще
                                видео</a>
                        </div>
                    </div>
                    <div class="promo-box__video-holder d-none d-sm-block">
                        <div class="promo-box__video-poster">
                            <img class="lazy" data-src="images/bg-video.jpg" alt=""></div>
                        <a href="{{ DynamicContentSheetFacade::getCfs()->get('promoBox_video') }}" class="btn-play" data-fancybox></a>
                    </div>
                </div>

                @if(false)
                <div class="promo-box__video x-device--mobile d-flex d-sm-none">
                    <div>
                        <span class="promo-box__video-title">Посмотрите минутное видео</span>

                        <p class="drop-shadow--dark drop-shadow--dark--only-desktop">
                            и узнайте, как правильно выбрать юридическую фирму для Ваших целей, сэкономить и не
                            рисковать.
                        </p>

                        <div class="row">
                            <div class="col-12 col-sm-6">
                                <div class="promo-box__video-holder">
                                    <div class="promo-box__video-poster">
                                        <div class="promo-box__video-poster-image-wrapper">
                                            <img class="lazy" data-src="images/img07.jpg" alt="">
                                        </div>
                                    </div>
                                    <a href="{{ DynamicContentSheetFacade::getCfs()->get('promoBox_video') }}" class="btn-play" data-fancybox></a>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6">
                                <span class="promo-box__author drop-shadow--dark drop-shadow--dark--only-desktop"><span>Владелец компании</span> <a href="#person-modal" class="fancybox">Шевельков И. В.</a></span>
                                <a href="{!! config('app.settings.social_links.youtube') !!}" class="more-video d-inline-block d-sm-none">Еще видео</a>
                            </div>
                        </div>
                    </div>
                    <div class="promo-box__video-holder d-none d-sm-block">
                        <div class="promo-box__video-poster">
                            <img class="lazy" data-src="images/bg-video.jpg" alt=""></div>
                        <a href="{{ DynamicContentSheetFacade::getCfs()->get('promoBox_video') }}" class="btn-play" data-fancybox></a>
                    </div>
                </div>
                @endif
            @endif
        </div>
    </div>
</div>
