<?php

$elementId = uniqid('form-input-');

?>
<form method="post" class="c-feedback-form js-feedback-form js-feedback-form--with-time"
      data-form-submission-id="deferred_callback_form"
      data-parsley-validate>
    <div class="text-wrap">
        <input type="text" class="js-feedback-form__phone-input text-field phone-mask"
               name="phone" placeholder="Ваш телефон"
               data-parsley-required data-parsley-phone>
    </div>

    <div class="time-row row">
        <div class="col-6">
            <input type="text" class="js-feedback-form__date-input text-field text-date" placeholder="Дата звонка"
                   data-parsley-required>
        </div>
        <div class="col-6">
            <input type="text" class="js-feedback-form__time-input text-field text-time only-time" placeholder="Время"
            data-parsley-time>
        </div>
    </div>

    <input type="submit" class="js-feedback-form__submit submit" value="Перезвоните мне">

    <div class="data-check font-tiny">
        <input type="checkbox" class="checkbox" name="accept" checked="checked"
               id="{!! $elementId !!}-accept" data-parsley-required>
        <label for="{!! $elementId !!}-accept">Согласен на обработку персональных данных</label>
    </div>
</form>
