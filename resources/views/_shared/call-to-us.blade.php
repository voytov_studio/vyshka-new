<?php

/**
 * @var SiteDataProvider $siteDataProvider
 */

use App\Domain\SiteDataProvider;

if (!isset($siteDataProvider))
    $siteDataProvider = app(\App\Domain\SiteDataProvider::class);

if (!isset($skipPhones))
    $skipPhones = [];

?>
<div class="help-box__phone-text">
    <span class="help-box__label">Позвоните нам по телефону</span>

        <a href="tel:{{ $siteDataProvider->getPhoneEntry('main')->value }}" class="help-box__phone js-mango-number">
            {{ $siteDataProvider->getPhoneEntry('main')->display }}
        </a>
</div>
