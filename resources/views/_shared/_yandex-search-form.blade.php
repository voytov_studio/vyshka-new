<form action="https://yandex.ru/search/site/" method="get" target="_blank" accept-charset="utf-8">
	<input type="hidden" name="searchid" value="{!! config('services.yandex.site_search_id') !!}"/>
	<input type="hidden" name="l10n" value="ru"/>
	<input type="hidden" name="reqenc" value=""/>
	<input type="submit" class="submit" value="Найти">
	<div class="keywords-search__text-wrap">
		<input type="search" name="text" class="text js-search-everything-input" placeholder="Поиск по любому слову...">
	</div>
</form>
