<?php

$elementId = uniqid('form-input-');

?>
<form method="post" class="c-feedback-form js-feedback-form" data-parsley-validate>
    <div class="input-group mb-3">
        <input name="phone" type="text"
               class="contact-info__text-field c-phone-input form-control phone-mask js-feedback-form__phone-input" placeholder="Ваш телефон"
               data-parsley-required data-parsley-phone>

        <div class="input-group-append">
            <button class="btn js-feedback-form__submit" type="button">
                <span class="d-none d-sm-block">ЗАКАЗАТЬ ЗВОНОК</span>
                <span class="d-block d-sm-none">ЗАКАЗАТЬ</span>
            </button>
        </div>
    </div>

    <div class="data-check font-tiny">
        <input type="checkbox" class="checkbox" name="accept" checked="checked" id="{!! $elementId !!}-accept" data-parsley-required>
        <label for="{!! $elementId !!}-accept">Согласен на обработку персональных данных</label>
    </div>
</form>
