<?php

$elementId = uniqid('form-input-');

?>
<form method="post" class="c-feedback-form js-feedback-form" data-parsley-validate>
    <div class="contact-info__form-row">
        <div class="contact-info__text-wrap">
            <input type="text" class="js-feedback-form__phone-input contact-info__text-field phone-mask"
                   name="phone" placeholder="Ваш телефон"
                   data-parsley-required data-parsley-phone>
        </div>
        <input type="submit" class="js-feedback-form__submit submit" value="Перезвоните мне">
    </div>
    <div class="data-check font-tiny">
        <input type="checkbox" class="checkbox" name="accept" checked="checked"
               id="{!! $elementId !!}-accept" data-parsley-required>
        <label for="{!! $elementId !!}-accept">Согласен на обработку персональных данных</label>
    </div>
</form>
