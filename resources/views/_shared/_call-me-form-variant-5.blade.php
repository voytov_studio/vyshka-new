<?php

$elementId = uniqid('form-input-');

?>
<form method="post" class="c-feedback-form js-feedback-form" data-parsley-validate>
    <div class="callback-form__row">
        <input type="button" class="js-feedback-form__submit submit" value="Отправить">
        <div class="callback-form__text-wrap">
            <input type="text" class="js-feedback-form__phone-input text phone-mask" placeholder="Ваш телефон"
                   data-parsley-required data-parsley-phone>
        </div>
    </div>
</form>
