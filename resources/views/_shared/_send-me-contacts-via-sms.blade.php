<?php

$elementId = uniqid('form-input-');

?>
<form method="post" class="c-feedback-form js-feedback-form"
      data-url="{!! route('api.web.forms.send-contacts-via-sms') !!}"
      data-form-submission-id="send_contacts_via_sms"
      data-parsley-validate>
    <div class="text-wrap">
        <input type="text" class="js-feedback-form__phone-input text-field phone-mask"
               name="name" placeholder="Ваш телефон"
               data-parsley-required data-parsley-phone>
    </div>

    <input type="submit" class="js-feedback-form__submit submit" value="ПОЛУЧИТЬ КОНТАКТЫ">

    <div class="data-check font-tiny">
        <input type="checkbox" class="checkbox" name="accept" checked="checked"
               id="{!! $elementId !!}-accept" data-parsley-required>
        <label for="{!! $elementId !!}-accept">Согласен на обработку персональных данных</label>
    </div>
</form>
