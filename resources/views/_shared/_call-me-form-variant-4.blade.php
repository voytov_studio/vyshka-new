<?php

$elementId = uniqid('form-input-');

if (!isset($buttonLabel))
    $buttonLabel = 'Перезвоните мне';

if (!isset($wrapperClass))
    $wrapperClass = 'help-form__row';

?>
<form method="post" class="c-feedback-form js-feedback-form" data-parsley-validate>
    <div class="{{ $wrapperClass }}">
        @if(isset($promo))
            {!! $promo !!}
        @else
            <div class="help-box__stick">
                <span class="help-box__stick__label">На {!! \Carbon\Carbon::now()->format('d.m') !!} осталось</span>
                <span class="help-box__stick__amount">1 место</span>
            </div>
        @endif
        <div class="text-wrap">
            <input type="text" class="js-feedback-form__phone-input text-field phone-mask"
                   name="phone" placeholder="Ваш телефон"
                   data-parsley-required data-parsley-phone>
        </div>
    </div>

    <input type="submit" class="js-feedback-form__submit submit" value="{{ $buttonLabel }}">

    <div class="data-check font-tiny">
        <input type="checkbox" class="checkbox" name="accept" checked="checked"
               id="{!! $elementId !!}-accept" data-parsley-required>
        <label for="{!! $elementId !!}-accept">Согласен на обработку персональных данных</label>
    </div>
</form>
