<?php

$elementId = uniqid('form-input-');

if (!isset($buttonLabel))
    $buttonLabel = 'Перезвоните мне';

?>
<form method="post" class="c-feedback-form js-feedback-form" data-parsley-validate>
    <div class="text-wrap">
        <input type="text" class="js-feedback-form__phone-input text-field phone-mask"
               name="phone" placeholder="Ваш телефон"
               data-parsley-required data-parsley-phone>
    </div>

    <button type="submit" class="js-feedback-form__submit submit">
        {{ $buttonLabel }}
    </button>

    <div class="data-check font-tiny">
        <input type="checkbox" class="checkbox" name="accept" checked="checked"
               id="{!! $elementId !!}-accept" data-parsley-required>
        <label for="{!! $elementId !!}-accept">Согласен на обработку персональных данных</label>
    </div>
</form>
