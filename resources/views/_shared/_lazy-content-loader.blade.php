<?php

$classes = $classes ?? null;

?>
<div class="js-lazy-content-loader {!! $classes !!}" data-chunk-id="{{ $contentId }}">
    <div class="c-lazy-content-container">
        <div class="c-spinner">
            <img class="lazy c-spinner__image" data-src="images/loader.svg">
        </div>
    </div>
</div>
