<?php

$siteDataProvider = app(\App\Domain\SiteDataProvider::class);
$cfs = DynamicContentSheetFacade::getCfs();

$offerSection = $cfs->getCollection('new_offer_tabs');

$titles = $offerSection->pluck('offer_title');

if (!empty($titles)) {
    $titles = collect($titles)->filter(function ($value) {
        return !empty($value);
    })->slice(1, 2);
}

$offerItem = \Illuminate\Support\Arr::get($offerSection, 0);

?>
<div class="one" id="one">
    <div class="one__wrapper">
        <div class="container">

            <h1 class="one__title">
                <span>
                    {{ Arr::get($offerItem, 'offer_title') }}
                </span>
            </h1>

            <div class="one__subtitle">
                <span>{{ Arr::get($offerItem, 'offer_subtitle_txt') }}</span>
            </div>

            <div class="consultation">
                <div class="consultation__wrap">
                    <div class="consultation__heading">
                        <span><span>Бесплатная</span></span> консультация
                    </div>

                    @include("v201912._promo-box-form", [
                        'buttonLabel' => DynamicContentSheetFacade::getCfs()->get('offer-button')
                    ])

                    @unless(empty($titles))
                        <div class="consultation__info">
                            @foreach($titles as $t)
                                <div class="consultation__info_item">
                                    {{ $t }}
                                </div>
                            @endforeach
                        </div>
                    @endunless
                </div>
            </div>

            <div class="one__section">
                <div class="one__section_wrap">

                    <div class="progress clearfix">
                        <div class="progress__item progress__item_first">
                            <div class="progress__item_mobile">
                                <img src="images/update__progress_one_xs.png" class="img_fluid" alt="">
                            </div>
                            <div class="progress__item_desktop">
                                <img src="images/update__progress_one.png" class="img_fluid" alt="">
                            </div>
                            <div class="progress__item_link">
                                <a href="#reviews-modal" class="fancybox"><span>Смотреть</span></a>
                            </div>
                        </div>

                        <div class="progress__item progress__item_second">
                            <div class="progress__item_mobile">
                                <img src="images/update__progress_two_xs.png" class="img_fluid" alt="">
                            </div>
                            <div class="progress__item_desktop">
                                <img src="images/update__progress_two.png" class="img_fluid" alt="">
                            </div>
                            <div class="progress__item_link">
                                <a href="https://300.pravo.ru/archive/#2017" class="country-rating__link" target="_blank" rel="noopener nofollow noreferrer">Pravo.ru</a>
                            </div>
                        </div>
                    </div>

                    <div class="warranty">
                        <div class="warranty__logo">
                            <div class="warranty__logo_icon">
                                <i>
                                    <img src="images/update__icon_warranty.svg" class="img_fluid" alt="">
                                </i>
                            </div>
                            <div class="warranty__logo_text">Финансовые<br/>гарантии</div>
                        </div>
                        <div class="warranty__content">Мы гарантируем выполнение Закона о защите прав потребителей, в части возврата денежных средств. Вы платите за полезные услуги.</div>
                    </div>
                </div>
            </div>

            <div class="service">
                <h2 class="service__title">Услуги и цены</h2>
                <div class="service__text">
                    Мы позаботились о наших клиентах и собрали полный перечень всех услуг от профильных юристов!
                    Обратите внимание на эксклюзивные предложения {!! config('site.ot_kogo') !!}
                </div>
                <div class="text_center">
                    <a href="javascript:" class="service__link btn_scroll js-go-to-services-section"><span>Посмотреть все услуги </span></a>
                </div>
            </div>

            <div class="play">
                <div class="play__content">
                    <div class="play__content_title">Узнайте о услугах <br/>за 1 минуту</div>
                    <div class="play__content_text">Рассказывает владелец</div>
                </div>
                <a class="play__button" href="{{ DynamicContentSheetFacade::getCfs()->get('promoBox_video') }}" data-fancybox>
                    <img src="images/update__play_image.png" class="img_fluid" alt="">
                </a>
            </div>

        </div>
    </div>
</div>
