<?php

$elementId = uniqid('form-input-');

?>
<div class="capture">
    <span class="capture__close"></span>
    <div class="capture__wrap">
        <div class="capture__image">
            <img src="images/capture__image.jpg" class="img_fluid" alt="">
        </div>
        <div class="capture__title">Акция: -50% с этой формы</div>
        <div class="capture__subtitle">
            на ВСЕ юридческие услуги до {{ Carbon\Carbon::now()->addDays(1)->format('d.m.Y') }}
        </div>
        <div class="capture__text">
            <span>Просто оставьте номер, звоним уже через 7 секунд</span>
            <img src="images/icon__smile.png" alt="">
        </div>

        <form class="form js-feedback-form" data-parsley-validate>
            <div class="capture__form">
                <div class="capture__form_group">
                    <input name="phone" type="text" placeholder="Телефон"
                           data-parsley-required data-parsley-phone
                           class="js-feedback-form__phone-input phone-mask form_elem form_elem__phone">
                </div>
                <div class="capture__form_group">
                    <button type="submit" class="js-feedback-form__submit btn_base">жду звонка!</button>
                </div>
            </div>

            <label class="form_checkbox" for="{!! $elementId !!}-accept">
                <input type="checkbox" name="accept" checked="checked"
                       id="{!! $elementId !!}-accept" data-parsley-required>

                <span>Вы даете @include('_shared._privacy-policy-link', ['text' => 'согласие на подписку на новости']) компании и тематические рассылки</span>
            </label>
        </form>

    </div>
</div>
