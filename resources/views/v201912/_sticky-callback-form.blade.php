<?php

$elementId = uniqid('form-input-');

?>
<form class="form js-feedback-form" data-parsley-validate>
    <div class="form_phone mb_10">
        <input name="phone" type="text" placeholder="Телефон"
               data-parsley-required data-parsley-phone
               class="form_phone__input">

        <button type="submit" class="js-feedback-form__submit form_phone__button">ПЕРЕЗВОНИТЕ</button>
    </div>

    <label class="form_checkbox" for="{!! $elementId !!}-accept">
        <input type="checkbox" name="accept" checked="checked"
               id="{!! $elementId !!}-accept" data-parsley-required>

        <span>Вы даете @include('_shared._privacy-policy-link', ['text' => 'согласие на подписку на новости']) компании и тематические рассылки</span>
    </label>
</form>
