<?php

$elementId = uniqid('form-input-');

if (!isset($buttonLabel))
    $buttonLabel = 'решить вопрос';

?>
<form class="form js-feedback-form" data-parsley-validate>
    <div class="consultation__row">
        <div class="consultation__form">
            <label class="consultation__form_label" for="consultation__form_input">Ваш телефон</label>
            <input class="consultation__form_input js-feedback-form__phone-input phone-mask form_phone__input"
                   id="consultation__form_input" name="phone"
                   data-parsley-required data-parsley-phone
                   data-parsley-required-message="Поле 'Телефон' обязательно для заполнения."
            >

        </div>
        <div class="consultation__button">
            <button type="submit" class="btn_main btn_pulse js-feedback-form__submit">{!! $buttonLabel !!}</button>
        </div>
    </div>
    <label class="main_checkbox" for="{!! $elementId !!}-accept">
        <input name="accept" type="checkbox" checked="checked"
               id="{!! $elementId !!}-accept" data-parsley-required>
        <span>Я принимаю условия @include('_shared._privacy-policy-link', ['text' => 'политики конфиденциальности'])</span>
    </label>
</form>
