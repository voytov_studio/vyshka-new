<?php

$elementId = uniqid('form-input-');

?>
<div class="recall">
    <div class="recall__wrap">
        <div class="recall__info">
            <div class="recall__title">Вам перезвонить?</div>
            <div class="recall__text">
                Наш лучший юрист по вашему вопросу свяжется с вами в течение 1 минуты и ответит на все ваши вопросы
            </div>
        </div>
        <div class="recall__content">
            <form method="post" class="form c-feedback-form js-feedback-form c-feedback-form--red" data-parsley-validate>
                <div class="form_phone mb_10">

                    <input name="phone" type="text" placeholder="Телефон" value=""
                           data-parsley-required data-parsley-phone
                           class="js-feedback-form__phone-input phone-mask form_phone__input">

                    <button type="submit" class="form_phone__button js-feedback-form__submit">
                        ПЕРЕЗВОНИТЕ
                    </button>

                </div>
                <label class="data-check form_checkbox" for="{!! $elementId !!}-accept">
                    <input
                        type="checkbox" name="accept" checked="checked"
                        id="{!! $elementId !!}-accept" data-parsley-required>

                    <span>Вы даете @include('_shared._privacy-policy-link', ['text' => 'согласие на подписку на новости']) компании и тематические рассылки</span>
                </label>
            </form>
        </div>
    </div>
</div>
