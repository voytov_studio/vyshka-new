<?php

$elementId = uniqid('form-input-');

?>
<div class="form_md">
    <div class="form_md__wrap">
        <div class="form_md__title">Поможем в решении вашей проблемы</div>
        <div class="form_md__text">
            Наш лучший юрист по вашему вопросу свяжется с вами в
            течение 1 минуты и ответит на все ваши вопросы
        </div>
        <div class="form_md__content">

            <form class="form c-feedback-form js-feedback-form c-feedback-form--red" data-parsley-validate>
                <div class="form_phone mb_10">
                    <input name="phone" type="text" placeholder="Телефон" class="js-feedback-form__phone-input phone-mask form_phone__input"
                           data-parsley-required data-parsley-phone>

                    <button type="submit" class="js-feedback-form__submit form_phone__button">ПЕРЕЗВОНИТЕ</button>
                </div>

                <label class="form_checkbox" for="{!! $elementId !!}-accept">
                    <input type="checkbox" name="accept" checked="checked"
                           id="{!! $elementId !!}-accept" data-parsley-required>

                    <span>Вы даете @include('_shared._privacy-policy-link', ['text' => 'согласие на подписку на новости']) компании и тематические рассылки</span>
                </label>
            </form>

        </div>
    </div>
</div>
