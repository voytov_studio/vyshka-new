@extends('errors.layout')

@section('title', __('Server Error'))
@section('code', '500')
@section('message', 'Сервер не смог обработать ваш запрос. Пожалуйста, повторите попытку позже')
