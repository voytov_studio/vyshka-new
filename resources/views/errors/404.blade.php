@extends('errors.layout')

@section('title', 'Не найдено')
@section('code', '404')
@section('message', 'Запрошенная страница не найдена')
