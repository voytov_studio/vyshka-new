<?php

/**
 * @var \App\Domain\WordPressPostAggregate[]|\Illuminate\Support\Collection $posts
 */

?>
@extends('layouts.main')

@section('title', 'Блог')

@section('content')
    <main>
        <div class="blog-promo">
            <div class="container">
                <div class="blog-promo__row">
                    <div class="blog-promo__text-box">
                        <h1>Юридический блог</h1>
                        <p>
                            А чтобы Вы лучше разбирались в тонкостях юридического права, мы подготовили коллекцию
                            полезных статей в нашем блоге.
                        </p>
                        <p>
                            Никакой воды — только полезные советы, которые Вы смело можете применить на практике.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="category-filter">
            <div class="container">
                <span class="category-filter__title">Категории:</span>
                <ul class="tags-list">
                    <li>
                        <a href="{{ route('blog.index') }}" class="@if (Request::url() == route('blog.index')) active @endif">Все</a>
                    </li>

                    @foreach($allCategories as $category)
                        <li>
                            <a href="{{ $category['link'] }}" @if($category['id'] == $currentCategoryId) class="active" @endif>
                                {{ $category['title'] }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="blog-section">
            <div class="container">
                <div class="blog-section__sort">
                    <span class="blog-section__sort-label">Сортировать по:</span>
                    <ul class="blog-section__sort-list">
                        <li>
                            <a href="{{ request()->fullUrlWithQuery(['sortBy' => 'date']) }}"
                               class="@if ($sortBy == 'date') active @endif">
                                по дате добавления
                            </a>
                        </li>
                        <li>
                            <a href="{{ request()->fullUrlWithQuery(['sortBy' => 'rating']) }}"
                               class="@if ($sortBy == 'rating') active @endif">
                                по популярности
                            </a>
                        </li>
                    </ul>
                </div>
                @foreach ($posts->values() as $index => $post)
                    <?php

                    /** @var \App\Domain\WordPressPostAggregate $post */
                    $isOdd = $index % 2 == 0;

                    ?>
                    <div class="blog-row row align-items-start align-items-md-center">
                        @if($isOdd)
                            <div class="col-12 col-sm-5">
                                <div class="blog-row__image">
                                    <a href="{{ $post->getViewLink() }}"><img class="lazy" data-src="{!! $post->getCoverImageUrl() ?? '/images/img24.jpg' !!}" alt="" width="500" height="400"></a>
                                </div>
                            </div>
                        @endif
                        <div class="col-12 col-sm-7 @if(!$isOdd) order-2 order-sm-1 @endif">
                            <div class="blog-row__text-box">
                                <a href="{{ $post->getViewLink() }}" class="blog-row__title">
                                    <span>{{ $post->getTitle() }}</span>
                                </a>
                                <header class="article-header">
                                    @if(false)
                                    <ul class="star-list">
                                        <li><img class="lazy" data-src="images/star01.png" alt=""></li>
                                        <li><img class="lazy" data-src="images/star01.png" alt=""></li>
                                        <li><img class="lazy" data-src="images/star01.png" alt=""></li>
                                        <li><img class="lazy" data-src="images/star02.png" alt=""></li>
                                        <li><img class="lazy" data-src="images/star02.png" alt=""></li>
                                    </ul>
                                    <span class="article-header__rating">Оценка: <b>{{ $post->getRating() }} из 5</b>.</span>
                                    <span class="article-header__rating">На основании: <b>{{ $post->getVote() }}</b> голосов</span>
                                    @endif
                                </header>

                                <p>
                                    {{ $post->getExcerpt() }}
                                </p>

                                <footer class="blog-row__footer">
                                    @foreach($post->getCategories() as $category)
                                        <a href="{{ $category['link'] }}" class="blog-row__category">
                                            {{ $category['name'] }}
                                        </a>
                                    @endforeach

                                    <a href="{{ $post->getViewLink() }}" class="read-more" data-post-id="{{ $post->model->getKey() }}">Читать подробнее</a>
                                </footer>
                            </div>
                        </div>
                        @if(!$isOdd)
                            <div class="col-12 col-sm-5 order-1 order-sm-2">
                                <div class="blog-row__image">
                                    <a href="{{ $post->getViewLink() }}"><img class="lazy" data-src="{!! $post->getCoverImageUrl() ?? '/images/img24.jpg' !!}" alt="" width="500" height="400"></a>
                                </div>
                            </div>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
    </main>
@endsection
