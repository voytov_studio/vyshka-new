<?php

/**
 * @var \App\Domain\WordPressPostAggregate $postAggregate
 * @var \App\Domain\WordPressPostAggregate[]|\Illuminate\Support\Collection $relatedPostAggregates
 */

$siteDataProvider = app(\App\Domain\SiteDataProvider::class);

$returnUrl = $returnUrl ?? route('/');

?>
@extends('layouts.main')

@section('title', $postAggregate->getSeoTitle())

@section('content')
    <main data-post-id="{!! $postAggregate->model->getKey() !!}">
        <div class="c-blog-post-wrapper">
            <div class="container">

                <a href="{{ $returnUrl }}" class="back-to-list mt-3 mb-4">Назад к списку</a>

                <header class=c-blog-post__header>
                    <h2>
                        {{ $postAggregate->getTitle() }}
                    </h2>

                    <div class="c-blog-post__subtitle">
                        <p>
                            {!! $postAggregate->getSubtitle() !!}
                        </p>
                    </div>
                </header>
                <div class="c-blog-post__row">
                    <div class="risks__sidebar d-none d-md-block" id="risks__sidebar1">
                            <span class="risks__sidebar-title">Оглавление:</span>
                            <div class="risks__questions-list js-tabs-x js-blog-post-heading-container" id="post-heading-container">
                                <span class="text-muted">Загрузка оглавления...</span>
                            </div>

                            @include("_shared._text-size-switcher")
                    </div>
                    <div class="risks__content" id="risks__content1">
                        <div class="risks__content-holder">
                            <div class="risks__content-frame frame-type">
                                <div class="tab-content">
                                    @if(false)
                                    <header class="article-header">
                                        <ul class="star-list">
                                            <li><img class="lazy" data-src="images/star01.png" alt=""></li>
                                            <li><img class="lazy" data-src="images/star01.png" alt=""></li>
                                            <li><img class="lazy" data-src="images/star01.png" alt=""></li>
                                            <li><img class="lazy" data-src="images/star02.png" alt=""></li>
                                            <li><img class="lazy" data-src="images/star02.png" alt=""></li>
                                        </ul>
                                        <span class="article-header__rating">Оценка: <b>{{ $postAggregate->getRating() }} из 5</b>.</span>
                                        <span class="article-header__rating">На основании: <b>{{ $postAggregate->getVote() }}</b> голосов</span>
                                    </header>
                                    @endif
                                    <div class="risks__content-image">
                                        <img class="lazy img-thumbnail" data-src="{{ $postAggregate->getCoverImageUrl() }}">
                                    </div>

                                    <div class="c-blog-post__content mb-3">
                                        {!! $postAggregate->getContent() !!}
                                    </div>

                                    <footer class="article-footer">
                                        @if(false)
                                        <div class="select-rating">
                                            <span class="select-rating__label">Оцените статью:</span>
                                            <div class="star-rating">
                                                <div class="star-rating__wrap">
                                                    <input class="star-rating__input" id="star-rating-5" type="radio" name="rating" value="5">
                                                    <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-5" title="5 out of 5 stars"></label>
                                                    <input class="star-rating__input" id="star-rating-4" type="radio" name="rating" value="4">
                                                    <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-4" title="4 out of 5 stars"></label>
                                                    <input class="star-rating__input" id="star-rating-3" type="radio" name="rating" value="3">
                                                    <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-3" title="3 out of 5 stars"></label>
                                                    <input class="star-rating__input" id="star-rating-2" type="radio" name="rating" value="2">
                                                    <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-2" title="2 out of 5 stars"></label>
                                                    <input class="star-rating__input" id="star-rating-1" type="radio" name="rating" value="1">
                                                    <label class="star-rating__ico fa fa-star-o fa-lg" for="star-rating-1" title="1 out of 5 stars"></label>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="share-box">
                                            <span class="share-box__label">Поделитесь:</span>
                                            <ul class="share-box__list">
                                                <li>
                                                    <a href="http://vk.com/share.php?url={{ \Illuminate\Support\Facades\Request::fullUrl() }}">
                                                        <img class="lazy" data-src="images/icon-vk.svg" alt="">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.facebook.com/sharer/sharer.php?u={{ \Illuminate\Support\Facades\Request::fullUrl() }}">
                                                        <img class="lazy" data-src="images/icon-facebook.svg" alt="">
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </footer>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="contact-info d-block d-xl-none contact-info_single h_center">
                    <span class="contact-info__green-heading">Мы УЖЕ знаем, как решить Вашу проблему!</span>
                    <p>
                        Мы более 10 лет занимаемся помощью в банковских делах, и имеем огромный кладезь информации по
                        всем проблемам в этой сфере. <b>Мы Вам точно поможем</b>:
                    </p>

                    <div>
                        <div class="row justify-content-center">
                            <div class="col-10 col-sm-8 mb-4">
                                @include("_shared._call-me-form-variant-2")
                            </div>
                        </div>

                        <div class="help-box__phone-holder">
                            <div class="help-box__phone-icon">
                                <img class="lazy" data-src="images/icon-phone02.png" alt="">
                            </div>

                            @include("_shared.call-to-us", ['skipPhones' => ['primary']])
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if($relatedPostAggregates->isNotEmpty())
            <div class="more-article">
                <div class="container">
                    <div class="more-article__header">
                        <h3>Еще статьи по теме:</h3>
                    </div>
                    <div class="slider-wrap">
                        <div class="container">
                            <div class="articles-slider owl-carousel">
                                @foreach ($relatedPostAggregates as $related)
                                    <div class="item">
                                        <div class="article-item">
                                            <a href="{{ $related->getViewLink() }}">
                                                <div class="article-item__image">
                                                    <img class="lazy" data-src="{{ $related->getCoverImageUrl() }}" alt="">
                                                </div>
                                                <span class="article-item__title">
                                            <span>{{ $related->getTitle() }}</span>
                                        </span>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <a href="javascript:" class="btn-slider-prev articles-prev"></a>
                        <a href="javascript:" class="btn-slider-next articles-next"></a>
                    </div>
                </div>
            </div>
        @endif
    </main>
@endsection

@push('scripts')
    <script>
        var postContentElement = document.querySelector('.c-blog-post__content');
        var headingContainerElement = document.getElementById('post-heading-container');

        window.appModules.blog.initPost(postContentElement, 'blog-post-heading-');
        window.appModules.blog.generateHeading(headingContainerElement, postContentElement);

        $('.js-scroll-to-element').each(function(index, element) {
            initScrollToElementButton($(element));
        });
    </script>
@endpush
