<?php

use Illuminate\Support\Collection;

/**
 * @var \App\Domain\WordPressPostAggregate $postAggregate
 * @var Collection $attachments
 */

$items = $postAggregate->parseMetaCollection('massmedia');

$pressItems = $items->where('publicity_type', '=', 3);

?>
@extends('layouts.main')

@section('title', 'Мы в СМИ: все публикации в прессе')

@section('content')
    <main class="mass-section">
        <div class="container">
            <h1 class="mb-5">
                @yield('title')
            </h1>

            <div class="row">
                @foreach ($pressItems as $item)
                    @php
                        $imageUri = data_get($attachments, $item['source'] . '.thumbnail_url');
                    @endphp
                    <div class="col-md-6 mb-4 item">
                        <div class="tele-item">
                            <div class="tele-item__icon">
                                @if ($imageUri)
                                    <img class="lazy" data-src="{!! $imageUri !!}" alt="">
                                @endif
                            </div>
                            <a href="{!! $item['link'] !!}" class="tele-item__title">
                                {{ $item['header'] }}
                            </a>
                            <div class="tele-item__date">
                                {{ $item['date'] }}
                            </div>
                            <p>
                                {!! $item['text'] !!}
                            </p>

                            <a href="{{ $item['link'] }}" class="to-source">Смотреть в источнике</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </main>
@endsection
