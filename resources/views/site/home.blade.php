<?php

$chunkFactory = app(\App\Domain\ContentChunkFactory::class);

$cfs = DynamicContentSheetFacade::getCfs();

$advantagesItems = $cfs->getCollection('mainPage_advantages');
$positionsItems = $cfs->getCollection('mainPage_positions')
    ->pluck('item');

?>
@extends('layouts.main')

@section('title', $cfs->get('title', ContentPageContextFacade::getCurrentPostAggregate()->getSeoTitle()))

@push('head')
    <meta name="keywords" content="{!! $cfs->get('keywords') !!}">
    <meta name="description" content="{!! $cfs->get('description') !!}">
    <meta name="robots" content="{!! $cfs->get('robots') !!}">
@endpush

@section('content')
    <main>
        @if(isset($postAggregate))
            @include("pages._promo-box")
        @endif

        <div class="d-none d-md-block">
            @include("site._category-navigation")
        </div>

        <div class="guarantee-section guarantee-section--main-page about">
            <div class="container">
                <h2 class="d-block d-sm-none">Наши гарантии</h2>
                <div class="guarantee-section__row">
                    <div class="guarantee-section__video">

                        <div class="guarantee-section__video-holder">
                            <div class="guarantee-section__video-poster">
                                <img src="images/img02.jpg" alt="">
                            </div>
                            <a href="{{ $cfs->get('promoBox_video') }}" class="btn-play" data-fancybox></a>
                        </div>
                        <div class="guarantee-section__video-text">
                            <span class="guarantee-section__video-title">За 2 минуты о компании</span>
                            <p>Узнайте как обеспечиваются Ваши интересы при работе с нашей компанией. <br/></p>
                            <p class="about_dir">
                                Владелец компании <a href="#person-modal" class="fancybox">Шевельков И. В.</a>
                            </p>
                        </div>

                    </div>
                    <div class="guarantee-section__text-box">
                        <h2 class="d-none d-sm-block"><span>О компании</span></h2>
                        <div class="guarantee-section__text-box_about_title">
                            {!! $cfs->get('mainPage_about_title') !!}
                        </div>
                        <div class="guarantee-section__text-box_about_text">
                            {!! $cfs->get('mainPage_about_text') !!}
                        </div>
                    </div>
                </div>

                @include("chunks._guarantees", ['data' => $cfs->getCollection('mainPage_garantees')])
            </div>
        </div>

        <div class="position-section c-certificates-section">
            <div class="container">
                <div class="position_title">
                    Ведущие позиции
                </div>
                <div class="position_subtitle">
                    в российских юридических рейтингах
                </div>
                <div class="position_blocks">
                    <div class="position_block_left">

                        <div class="pos-slide">
                            <div class="pos-slider owl-carousel">
                                <div class="item">
                                    <div class="sert-item">
                                        <a href="/images/certificates/pravo.png" data-fancybox="sert">
                                            <img class="lazy" data-src="images/certificates/pravo.preview.jpg" alt="">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- Вернуть когда будет больше сертификатов
                        <a href="javascript:" class="btn-slider-prev pos-prev"></a>
                        <a href="javascript:" class="btn-slider-next pos-next"></a>
                        --}}
                    </div>
                    <div class="position_block_right">
                        @foreach($positionsItems as $i)
                            <div class="position_block_right_desc">
                                {!! $i !!}
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="priem-section">
            <div class="container">
                <h2 class="d-none d-sm-block">Преимущества</h2>
                <div class="priem_blocks">
                    @for ($index = 0; $index < $advantagesItems->count(); ++$index)
                        <div class="priem_block">
                            <div class="priem_block_img">
                                <img class="lazy" data-src="images/priem{!! $index + 1 !!}.png">
                            </div>
                            <div class="priem_block_title">
                                {!! data_get($advantagesItems, $index . '.head') !!}
                            </div>
                            <div class="priem_block_desc">
                                {!! data_get($advantagesItems, $index . '.text') !!}
                            </div>
                        </div>
                    @endfor
                </div>
            </div>

        </div>

        @include("_shared._lazy-content-loader", ['contentId' => 'how_it_works'])
        @include("_shared._lazy-content-loader", ['contentId' => 'consultation'])

        @php
            $chunk = $chunkFactory->make('we_work_with');
            $chunk->postAggregate = ContentPageContextFacade::getCurrentPostAggregate();

            echo $chunk->render();
        @endphp

        @php
            $block = app(\App\Domain\Content\TermsOfSolution::class);
            $block->postAggregate = ContentPageContextFacade::getCurrentPostAggregate();

            echo $block->render();
        @endphp

        @include("_shared._lazy-content-loader", ['contentId' => 'team'])

        @if(is_ekrual())
            @include("_shared._lazy-content-loader", ['contentId' => 'cases'])
        @endif

        @if(false)
            @include("_shared._lazy-content-loader", ['contentId' => 'mass_media'])
        @endif

        @include("_shared._lazy-content-loader", ['contentId' => 'reviews'])
    </main>
@endsection

@push('scripts')
    <script>
        window.currentPageId = {!! $postAggregate->model->getKey() !!};

        $(document).ready(function () {
            applyGlobalScriptsForElement($('.guarantee-section'));
            initCertificatesSections($('.c-certificates-section'));
        });
    </script>
@endpush
