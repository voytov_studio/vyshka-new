<?php

/**
 * @var \App\Data\SpecializationMenuItemDto[]|\Illuminate\Support\Collection $specializationsMenuItems
 */

?>
<div class="spec-section">
    <div class="container">
        <h2><span>Наши специализации</span></h2>

        <ul class="specialization-list">
            @foreach($specializationsMenuItems as $i)
                <li>
                    @component('chunks._specialization-link', ['route' => $i->url])
                        <span class="icon {{ $i->icon }}"></span>
                        <div class="text-box">
                            <span class="text">{!! str_replace('право', '<span>право</span>', $i->name) !!}</span>
                            <span class="number">({{ $i->counter }})</span>
                        </div>
                    @endcomponent
                </li>
            @endforeach
        </ul>
        <div class="spec-text">
            Если Вы не нашли нужной, воспользуйтесь быстрым поиском по названию услуги или<br/> поиском по ключевым
            словам:
        </div>
        <div class="specialization__header row">
            <div class="col-12 col-md">
                <div class="sort-search">
                    <form action="#">
                        <input name="q" type="text" class="text js-search-service-input" placeholder="Начните набирать название...">
                    </form>
                </div>
            </div>
            <div class="col-12 col-md-4 col-lg-3">
                <div class="keywords-search">
                    <div class="keywords-search__row">
                        @include("_shared._yandex-search-form")
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
