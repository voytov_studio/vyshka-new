<?php

/**
 * @var ContentFieldsStore $cfs
 */

$tabs = $cfs->getArray('howWeWork_tabs');
$whatRequiredItems = collect($cfs->getArray('howWeWork_whatRequired'))->pluck('item');
$requiredDocuments = collect($cfs->getArray('howWeWork_docs'))->pluck('item');

?>
<div class="how-it-works js-tow-it-works-section">
    <div class="container">
        <h2>Как мы работаем?</h2>
        <div class="how-it-works__row">
            <div class="how-it-works__content" id="how-it-works__content">
                <div class="how-it-works__tabs-wrap">
                    <span class="how-it-works__tabs-label">Тип услуг:</span>
                    <ul class="how-it-works__tabs js-tabs">
                        @foreach ($tabs as $index => $i)
                            <li>
                                <a href="#how-it-tab-{!! $index !!}" class="@if ($index == 0) active @endif ">
                                    <span>{{ Arr::get($i, 'title') }}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="how-it-works__tab-content-wrap">
                    @foreach ($tabs as $index => $i)
                        @php

                        $rawStages = $cfs->getCollection($cfs->makePath('howWeWork_tabs.%s.stages', [$index]));
                        $stages = [];

                        foreach ($rawStages as $stageIndex => $data) {
                                $result = $data;
                                $path = $cfs->makePath('howWeWork_tabs.%s.stages.%s.incases', [$index, $stageIndex]);

                                $result['incases'] = $cfs->getCollection($path)->pluck('item');

                                $stages[] = new \Illuminate\Support\Fluent($result);
                        }

                        @endphp
                        <div class="tab-content" id="how-it-tab-{!! $index !!}">
                            <ul class="steps-list">
                                @foreach ($stages as $stageIndex => $si)
                                    <li class="steps-list__item">
                                        <span class="steps-list__step-number">{!! $stageIndex + 1 !!}</span>
                                        <div class="steps-list__heading">
                                            <a href="javascript:" class="steps-list__opener @if ($stageIndex == 0) js-how-it-works__open-at-load @endif">
                                                <span>{{ $si->title }}</span>
                                            </a>
                                            <span class="steps-list__opener-note">
                                                {!! nl2p($si->visible) !!}
                                            </span>
                                        </div>
                                        <div class="steps-list__slide">
                                            <div class="steps-list__slide-holder">
                                                {!! nl2p($si->invisible) !!}

                                                <h4>В этап входит:</h4>

                                                <ul class="tick-list tick-list_bordered">
                                                    @foreach ($si->incases as $i)
                                                        <li>{{ $i }}</li>
                                                    @endforeach
                                                </ul>

                                                <h4>Среднее время на этап:</h4>
                                                <p>
                                                    {!! $si->avarageTime !!}
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="how-it-works__aside" id="how-it-works__aside">
                <h3>Что требуется от Вас:</h3>
                <div class="list-holder">
                    <ul class="numbered-list">
                        @foreach ($whatRequiredItems as $i)
                            <li>{{ $i }}</li>
                        @endforeach
                    </ul>
                    <span class="numbered-end">Всю дальнейшую работу до получения результата мы сделаем сами</span>
                </div>

                @unless($requiredDocuments->isEmpty())
                <h3>Документы от Вас:</h3>
                <div class="list-holder">
                    <div class="box-scroll-210">
                        <ul class="bulleted-list-hole" data-count="{!! $requiredDocuments->count() !!}">
                            @foreach ($requiredDocuments as $i)
                                <li>{{ $i }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>

                <a href="javascript:;"
                   data-fancybox data-type="ajax" data-src="{{ route('site.showDocuments') }}"
                   class="download-link">
                    Скачать список документов
                </a>
                @endunless
            </div>
        </div>
    </div>
</div>
