<?php

/**
 * @var \Illuminate\Support\Collection $data
 */

$make_tab_id = function ($index) {
    return 'faq-tabs-' . $index;
};

$vms = collect($data->get('items'))->map(function ($i) {
    $vm = new \App\ViewModels\LikbezBodyViewModel($i);
    $vm->titleField = 'q';
    $vm->contentField = 'a';
    $vm->parseContent();

    return $vm;
});

?>
@component('chunks._likbez', ['idPrefix' => 'js-faq-likbez__'])
    @slot('title')
        {!! $data->get('title') !!}
    @endslot

    @slot('navigation')
        @foreach($data->get('items') as $index => $item)
            <li>
                <a href="#{!! $make_tab_id($index) !!}" class="js-likbez-anchor">
                    <span>{!! Arr::get($item, 'q') !!}</span>
                </a>
            </li>
        @endforeach
    @endslot

    @slot('content')
        @foreach($vms as $index => $vm)
            @php
                $textAfterExcerpt = $vm->getTextAfterExcerpt()
            @endphp
            <div class="likbez-content" id="{!! $make_tab_id($index) !!}" data-index="{!! $index !!}">
                <h3>
                    {{ $vm->getTitle() }}
                </h3>

                {!! $vm->getExcerpt() !!}

                @if($textAfterExcerpt)
                    <a href="javascript:" class="text-muted"
                       data-expanded-label="Свернуть..."
                       data-toggle="collapse" data-target=".likbez-content__hidden-content.collapse[data-id='{{ $make_tab_id($index) }}']">
                        Читать дальше...
                    </a>

                    <div class="mb-1 likbez-content__hidden-content collapse" data-id="{{ $make_tab_id($index) }}">
                        {!! $textAfterExcerpt !!}
                    </div>
                @endif

            </div>
        @endforeach

        @foreach($data->get('items') as $index => $item)
            <div class="tab-content" id="{!! $make_tab_id($index) !!}">
                <h3>{!! Arr::get($item, 'q') !!}</h3>

                {!! nl2p(Arr::get($item, 'a')) !!}
            </div>
        @endforeach
    @endslot

    @slot('mobileChunk')
        @foreach($data->get('items')->keys() as $index)
            <li class="risks-accordion__item @if($index < 0) active @endif">
                <a href="javascript:" class="risks-accordion__opener">
                    {!! Arr::get($data, 'items.' . $index . '.q') !!}
                </a>
                <div class="slide">
                    <div class="slide-holder">
                        <div class="risks__content-frame">
                            <h3>
                                {!! Arr::get($data, 'items.' . $index . '.q') !!}
                            </h3>

                            {!! nl2p(Arr::get($data, 'items.' . $index . '.a')) !!}
                        </div>
                    </div>
                </div>
            </li>
        @endforeach
    @endslot
@endcomponent
