<?php

/**
 * @var \Illuminate\Support\Collection $data
 */

$tabs = $data;

?>
<div class="c-problems-section mb-5">
    <div class="centered-tabs">
        <div class="how-it-works__tabs-wrap mb-0">
            <ul class="how-it-works__tabs js-tabs">
                @foreach ($tabs as $tabIndex => $tabData)
                    <li>
                        <a href="#problems-tab-{!! $tabIndex + 1 !!}" class="@if ($tabIndex == 0) active @endif">
                            <span>
                                {!! $tabData->title !!}
                            </span>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="tab-content-wrap">
        @foreach ($tabs as $tabIndex => $tabData)
            <div class="tab-content" id="problems-tab-{!! $tabIndex + 1 !!}">
                @php
                    $idPrefix = 'js-problems-likbez__' . $tabIndex . '__';

                    $make_navigation_id = function ($index) use ($idPrefix) {
                        return $idPrefix . 'navigation-item__' . $index;
                    };
                @endphp
                @component('chunks._likbez', ['idPrefix' => $idPrefix])
                    @slot('title')
                        {!! $tabData->title !!}
                    @endslot
                    @slot('navigation')
                        @foreach($tabData->list as $index => $item)
                            <li>
                                <a href="#{!! $make_navigation_id($index) !!}" class="js-likbez-anchor">
                                    <span>{!! data_get($item, 'title') !!}</span>
                                </a>
                            </li>
                        @endforeach
                    @endslot

                    @slot('content')
                        @foreach($tabData->list as $index => $item)
                            <div class="tab-content" id="{!! $make_navigation_id($index) !!}">
                                <h2>{!! data_get($item, 'title') !!}</h2>

                                {!! data_get($item, 'text') !!}
                            </div>
                        @endforeach
                    @endslot

                    @slot('mobileChunk')
                        @foreach($tabData->list->keys() as $index)
                            <li class="risks-accordion__item @if($index < 0) active @endif">
                                <a href="javascript:" class="risks-accordion__opener">
                                    {!! data_get($tabData->list->get($index), 'title') !!}
                                </a>
                                <div class="slide">
                                    <div class="slide-holder">
                                        <div class="risks__content-frame">
                                            {!! data_get($tabData->list->get($index), 'text') !!}
                                        </div>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    @endslot
                @endcomponent
            </div>
        @endforeach
    </div>
</div>

@push('scripts')
    <script>
        $(document).ready(function () {
            $('.how-it-works__tabs.js-tabs a').on("click", function (e) {
                console.debug('Tab triggered: .how-it-works__tabs.js-tabs');

                for (var i = 0; i < document.likbezStickyMenus.length; ++i)
                    document.likbezStickyMenus[i].updateSticky();
            });
        });
    </script>
@endpush
