<?php

/**
 * @var \App\Domain\ContentFieldsStore $cfs
 */

$withRootKey = new \App\Domain\ContentFieldKeyHelper();
$withRootKey->baseKey = 'weWorkWith_';

$sectionOneRootKey = new \App\Domain\ContentFieldKeyHelper();
$sectionOneRootKey->baseKey = 'weWorkWith_sectionOne_';

$sectionTwoRootKey = new \App\Domain\ContentFieldKeyHelper();
$sectionTwoRootKey->baseKey = 'weWorkWith_sectionTwo_';

$cases = $cfs->getArray($withRootKey('cases'));

$yt1 = extract_yt_id($cfs->get($sectionOneRootKey('video_url')));
$yt2 = extract_yt_id($cfs->get($sectionTwoRootKey('video_url')));

?>
<div class="our-clients">
    <div class="container">
        <div class="section-title section-title_left">
            <h2><span>С кем мы работаем</span></h2>
        </div>
        <div class="individuals-row row no-gutters">
            <aside class="individuals-aside col-12 col-sm-6">
                <div class="individuals-video">
                    {{-- Эту картинку не получается сделать lazy, т.к. её контейнер имеет правило overflow: hidden --}}
                    <img src="{!! make_youtube_default_thumbnail_url($yt1) !!}" alt="">
                    <a href="https://www.youtube.com/embed/{{ $yt1 }}" class="btn-play" data-fancybox></a>
                </div>
                <span class="individuals-video-title">
                    {!! $cfs->getBlock($sectionOneRootKey('video_title')) !!}
                </span>
                <span class="individuals-video-text">
                    {!! $cfs->getBlock($sectionOneRootKey('video_text')) !!}
                </span>
            </aside>
            <div class="individuals-content col-12 col-sm">
                <div class="our-clients-heading">
                    <div class="our-clients-heading__icon">
                        <img class="lazy" data-src="images/icon-title01.svg" alt="">
                    </div>
                    <div class="our-clients-heading__text">
                        <h3>
                            {!! $cfs->getBlock($sectionOneRootKey('head')) !!}
                        </h3>
                        <p>
                            {!! $cfs->getBlock($sectionOneRootKey('subhead')) !!}
                        </p>
                    </div>
                </div>

                {!! nl2p($cfs->getBlock($sectionOneRootKey('text'))) !!}

                <div class="row">
                    @php
                        $ours = $cfs->getArray($sectionOneRootKey('ours'));
                        $others = $cfs->getArray($sectionOneRootKey('others'));
                    @endphp
                    <div class="col-12 col-sm-6">
                        <h4>Особенности нашего подхода:</h4>
                        <ul class="tick-list">
                            @foreach ($ours as $i)
                                <li>{{ $i['item'] }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-12 col-sm-6 disabled-col">
                        <h4>Другие юристы:</h4>
                        <ul class="tick-list tick-list_disabled">
                            @foreach ($others as $i)
                                <li>{{ $i['item'] }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="legal-entities-row row no-gutters">
            <div class="legal-entities-content col-12 col-md">
                <div class="our-clients-heading">
                    <div class="our-clients-heading__icon">
                        <img class="lazy" data-src="images/icon-title02.svg" alt="">
                    </div>
                    <div class="our-clients-heading__text">
                        <h3>{!! $cfs->getBlock($sectionTwoRootKey('head')) !!}</h3>
                        <p>{!! $cfs->getBlock($sectionTwoRootKey('subhead')) !!}</p>
                    </div>
                </div>

                {!! nl2p($cfs->getBlock($sectionTwoRootKey('text_1'))) !!}

                <div class="green-box">
                    <div class="holder">
                        {!! nl2p($cfs->getBlock($sectionTwoRootKey('text_2'))) !!}
                    </div>
                </div>

                {!! nl2p($cfs->getBlock($sectionTwoRootKey('text_3'))) !!}

                <h3>
                    <span class="mark-green">
                        {!! nl2p($cfs->getBlock($sectionTwoRootKey('text_4'))) !!}
                    </span>
                </h3>

                {!! nl2p($cfs->getBlock($sectionTwoRootKey('text_5'))) !!}

            </div>
            <aside class="legal-entities-aside col-12 col-md-6">
                <div class="legal-entities__video">
                    <div class="legal-entities__video-poster">
                        <img class="lazy" data-src="{!! make_youtube_default_thumbnail_url($yt2) !!}" alt="">
                        <a href="https://www.youtube.com/embed/{{ $yt2 }}" class="btn-play" data-fancybox></a>
                    </div>
                </div>
                <span class="legal-entities__video-title">{!! $cfs->getBlock($sectionTwoRootKey('video_title')) !!}</span>
                <span class="legal-entities__video-text">
                    {!! $cfs->getBlock($sectionTwoRootKey('video_text')) !!}
                </span>

                @if (false)
                <h3>Наши кейсы</h3>
                <ul class="cases-list">
                    @foreach ($cases as $i)
                        <li>
                            <a href="{!!  $i['link']  !!}" class="cases-list__title">
                                {{ $i['head'] }}
                            </a>
                            <span class="cases-list__sub-title">{{ $i['name'] }}</span>
                        </li>
                    @endforeach
                </ul>
                @endif
            </aside>
        </div>
    </div>
</div>
