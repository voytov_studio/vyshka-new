<?php

/**
 * @var \Illuminate\Support\Collection $data
 */

?>
@component('chunks._likbez', ['blockClass' => 'c-case-development-chunk'])
    @slot('navigation')
        @foreach($data as $index => $item)
            <li>
                <a href="#info-tab{!! $index !!}" class="js-likbez-anchor @if ($index == 0) active @endif">
                    <span>{!! Arr::get($item, 'title') !!}</span>
                </a>
            </li>
        @endforeach
    @endslot

    @slot('content')
        @foreach($data as $index => $item)
            <div class="tab-content" id="info-tab{!! $index !!}">
                <h2>{!! Arr::get($item, 'title') !!}</h2>

                {!! nl2p(Arr::get($item, 'text')) !!}
            </div>
        @endforeach
    @endslot

    @slot('mobileChunk')
        @foreach($data->keys() as $index)
            <li class="risks-accordion__item @if($index < 0) active @endif">
                <a href="javascript:" class="risks-accordion__opener">
                    {!! $data->get($index)->get('title') !!}
                </a>
                <div class="slide">
                    <div class="slide-holder">
                        <div class="risks__content-frame">
                            {!! $data->get($index)->get('text') !!}
                        </div>
                    </div>
                </div>
            </li>
        @endforeach
    @endslot
@endcomponent
