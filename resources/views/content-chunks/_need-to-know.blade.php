<?php

/**
 * @var \Illuminate\Support\Collection $data
 */

$contents = collect($data->get('likbez_content', []));
$likbezHeaders = $contents->pluck('likbez_content_title')->toArray();

$sectionTitle = $data->get('likbez_title');

$sectionTitle = strtr($sectionTitle, [
    'Важно знать:' => '<b>Важно знать:</b><br>'
]);

$cng = new \App\Helpers\ContentElementNameGenerator('js__likbez__need-to-know');

$vms = $contents->map(function ($i) {
    $vm = new \App\ViewModels\LikbezBodyViewModel($i);
    $vm->parseContent();

    return $vm;
});

?>
@component('chunks._likbez')
    @slot('title')
        {!! $sectionTitle !!}
    @endslot

    @slot('navigation')
        @foreach($likbezHeaders as $index => $text)
            <li>
                <a href="#{!! $cng('navigation', $index) !!}" class="js-likbez-anchor">
                    <span>{!! $text !!}</span>
                </a>
            </li>
        @endforeach
    @endslot

    @slot('content')
        @foreach($vms as $index => $vm)
            @php
                $textAfterExcerpt = $vm->getTextAfterExcerpt()
            @endphp
            <div class="likbez-content" id="{!! $cng('navigation', $index) !!}" data-index="{!! $index !!}">
                <h3>
                    {{ $vm->getTitle() }}
                </h3>

                {!! $vm->getExcerpt() !!}

                @if($textAfterExcerpt)
                    <a href="javascript:" class="text-muted"
                       data-expanded-label="Свернуть..."
                       data-toggle="collapse" data-target=".likbez-content__hidden-content.collapse[data-id='{{ $cng('content', $index) }}']">
                        Читать дальше...
                    </a>

                    <div class="mb-1 likbez-content__hidden-content collapse" data-id="{{ $cng('content', $index) }}">
                        {!! $textAfterExcerpt !!}
                    </div>
                @endif

            </div>
        @endforeach
    @endslot

    @slot('contentFooter')
        <div class="contact-info d-none d-md-block">
            @include("chunks._did-not-find-info")
        </div>
    @endslot

    @slot('mobileChunk')
        @foreach($contents->keys() as $index)
            <li class="risks-accordion__item @if($index < 3) active @endif">
                <a href="javascript:" class="risks-accordion__opener">
                    {!! Arr::get($likbezHeaders, $index) !!}
                </a>
                <div class="slide">
                    <div class="slide-holder">
                        <div class="risks__content-frame">
                            <h3>
                                {!! Arr::get($contents, $index . '.likbez_content_title') !!}
                            </h3>

                            {!! Arr::get($contents, $index . '.likbez_content_text') !!}
                        </div>
                    </div>
                </div>
            </li>
        @endforeach
    @endslot
@endcomponent
