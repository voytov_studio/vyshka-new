<div class="consultation-info">
    <div class="container">
        <div class="section-title">
            <h2><span>Как происходит консультация</span></h2>
        </div>
        <div class="consultation-row row no-gutters">
            <div class="consultation-content col-12 col-md">
                <h3>По телефону</h3>

                {!! nl2p($cfs->getBlock('consultation_phone')) !!}

                <div class="row justify-content-center mb-4">
                    <div class="col-lg-6 mb-4">
                        @include("_shared._call-me-form")
                    </div>
                    <div class="col-lg-6">
                        <div class="help-box__phone-holder">
                            <div class="help-box__phone-icon">
                                <img class="lazy" data-src="images/icon-phone02.png" alt="">
                            </div>

                            @include("_shared.call-to-us", ['skipPhones' => ['primary']])
                        </div>
                    </div>
                </div>
                <hr>
                <h3>Онлайн</h3>

                {!! nl2p($cfs->getBlock('consultation_online')) !!}

                <ul class="chat-list">
                    <li>
                        <a href="javascript:" onclick="window.app.widgets.openChatWidget(); return false;">
                            <span class="icon">
                                <img class="lazy" data-src="images/icon-wechat.svg" alt="" width="20" height="20">
                            </span>
                            <span class="text">Написать в чат</span>
                        </a>
                    </li>
                    <li>
                        <a href="{!! config('app.settings.social_links.whatsapp', '#') !!}" target="_blank" rel="noopener nofollow noreferrer">
                            <span class="icon">
                                <img class="lazy" data-src="images/icon-whatsapp.svg" alt="" width="20" height="20">
                            </span>
                            <span class="text">Whatsapp</span>
                        </a>
                    </li>
                    <li>
                        <a href="{!! config('app.settings.social_links.telegram', '#') !!}" target="_blank" rel="noopener nofollow noreferrer">
                            <span class="icon">
                                <img class="lazy" data-src="images/icon-telegram.svg" alt="" width="20" height="20">
                            </span>
                            <span class="text">Telegram</span>
                        </a>
                    </li>
                    <li>
                        <a href="mailto:{!! config('app.settings.contact_email') !!}">
                            <span class="icon">
                                <img class="lazy" data-src="images/icon-email.svg" alt="" width="20" height="20">
                            </span>
                            <span class="text">Отправить письмо</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="consultation-aside col-12 col-md-6">
                <div class="consultation-aside__holder">
                    <h3>В офисе</h3>

                    {!! nl2p($cfs->getBlock('consultation_office')) !!}

                    <h4>Преимущества личной встречи:</h4>
                    <ul class="tick-list tick-list_bordered">
                        <?php

                        $reasonItems = $cfs->getAllOf('consultation_person.');

                        ?>
                        @foreach($reasonItems as $r)
                            <li>{!! $r !!}</li>
                        @endforeach
                    </ul>
                    <br>
                    <br>
                    <p>Мы находимся в центре Санкт-Петербурга, посмотрите
                        <a href="#route-modal" class="fancybox">как добраться</a>.</p>
                </div>
            </div>
        </div>
    </div>
</div>
