<?php

$cfs = DynamicContentSheetFacade::getCfs();

$items = $cfs->getCollection('ivan_listBullets')->pluck('item');

$videoUrl = DynamicContentSheetFacade::getCfs()->get('ivan_video_url');
$videoUrl = make_youtube_embedded_url($videoUrl, '#');

?>
<div class="modal-window" id="person-modal">
	<div class="about-person">
		<div class="about-person__text">
			<h2>Шевельков Иван<br>Вадимович</h2>
			@if(!empty($items))
				<ul class="bulleted-list-yellow">
					@foreach($items as $i)
						<li>{{ $i }}</li>
					@endforeach
				</ul>
			@endif
			<hr>
			<div class="about-person__video">
				<div class="about-person__video-holder d-none d-sm-block">
					<div class="about-person__video-poster">
						<img src="images/img06.jpg" alt="" width="200" height="150">
					</div>
					<a href="{{ $videoUrl }}" class="btn-play" data-fancybox></a>
				</div>
				<div class="about-person__video-text">
					<span class="title">
						{{ DynamicContentSheetFacade::getCfs()->get('ivan_video_title') }}
					</span>
					<p>
						{{ DynamicContentSheetFacade::getCfs()->get('ivan_video_description') }}
					</p>
					<div class="about-person__video-holder d-block d-sm-none">
						<div class="about-person__video-poster">
							<img src="images/img06.jpg" alt="" width="200" height="150">
						</div>
						<a href="{{ $videoUrl }}" class="btn-play"></a>
					</div>
					<a href="{!! config('app.settings.social_links.youtube') !!}" class="more-video">Еще видео</a>
				</div>
			</div>
		</div>
		<div class="about-person__image">
			<img class="lazy" data-src="images/img05.png" alt="" width="404" height="565">
		</div>
	</div>
	<div class="about-person-footer">
		<div class="row">
			<div class="col-12 col-md-6">
				<h2><b>Личная консультация</b><br>у генерального директора</h2>
				<ul class="tick-list">
					<li><span class="tick-list__free">Бесплатно</span>
						<span class="tick-list__old-price">15 000 <span class="price-value">i</span></span></li>
					<li>по банковским вопросам</li>
				</ul>
			</div>
			<div class="col-12 col-md-6">
				<div class="help-form">
					@include("_shared._call-me-form-variant-4")
				</div>
			</div>
		</div>
	</div>
</div>
