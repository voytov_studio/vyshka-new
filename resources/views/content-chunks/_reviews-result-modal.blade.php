<div class="modal-window" id="{!! $modalId !!}" style="display: none;">
	<div class="result-info">
		<h2>Результат по делу №{!! $caseNumber !!}</h2>

		{!! $caseResult !!}
	</div>
</div>
