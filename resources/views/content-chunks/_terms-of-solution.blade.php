<?php

/**
 * @var ContentFieldsStore $cfs
 */

$tabCount = $cfs->get('termsOfSolution_tabs');

$elementRange = range(0, $tabCount - 1);

$baseContentFieldKey = 'termsOfSolution_tabs';

?>
<div class="deadlines">
	<div class="container">
		<div class="section-title section-title_left">
			<h2><span>Сроки решения вопроса</span></h2>
		</div>
		<div class="deadlines__row row no-gutters">
			<div class="deadlines__content col-12 col-md">
				<div class="how-it-works__tabs-wrap">
					<ul class="how-it-works__tabs js-tabs">
						@foreach($elementRange as $index)
							@php
								$key = $baseContentFieldKey . '.' . $index;
							@endphp
							<li>
								<a href="#deadlines-tab-{!! $index + 1 !!}" class="@if ($index == 0) active @endif">
								<span>
									{!! $cfs->getBlock($key . '.title') !!}
								</span>
								</a>
							</li>
						@endforeach
					</ul>
				</div>
				@foreach($elementRange as $index)
					@php
						$key = $baseContentFieldKey . '.' . $index;
						$tabData = $cfs->get($key);
					@endphp
					<div class="tab-content" id="deadlines-tab-{!! $index + 1 !!}">
						{!! $cfs->getTextBlock($key . '.text') !!}
						<br>
						<h3>Какие сроки говорят другие юристы</h3>
						{!! $cfs->getTextBlock($key . '.whatOtherSays') !!}
						<br>
						<div class="message-box">
							<h3>Какие устанавливаем мы</h3>
							{!! $cfs->getTextBlock($key . '.whatWeSay') !!}
						</div>
					</div>
				@endforeach
			</div>
			<div class="deadlines__aside col-12 col-md-6">
				<div class="deadlines__video">
					<div class="deadlines__video-poster">
						<img class="lazy" data-src="images/img11.jpg" alt="">
                        <a href="{!! $cfs->get('termsOfSolution_video') !!}" class="btn-play" data-fancybox></a>
					</div>
					<span class="deadlines__title">Тонкости определения сроков, 2 мин.</span>
					<span class="deadlines__sub-title">Жми и узнай, как правильно установить сроки</span>
				</div>
				<div class="warranty-info">
					<div class="warranty-info__row row no-gutters align-items-center">
						<div class="warranty-info__icon col-6">
							<img class="lazy" data-src="images/icon-warranty.svg" alt="">
						</div>
						<div class="warranty-info__text-box col">
							<h4>Гарантии <b>соблюдения сроков</b></h4>
						</div>
					</div>

                    {!! $cfs->getTextBlock('termsOfSolution_guarantees') !!}
				</div>
			</div>
		</div>
	</div>
</div>
