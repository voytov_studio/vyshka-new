
import jQuery from "jquery";
import LazyLoad from "vanilla-lazyload";

import {EventBus} from "./event-bus";
import LazyContentManager from "./modules/LazyContentManager";

window.app = {};
window.app['EventBus'] = EventBus;
window.lazyContentManager = LazyContentManager;

window.LazyLoad = LazyLoad;

try {
    window.$ = window.jQuery = jQuery;
    window.collapse = require('./modules/Collapse').default;
    window.StickySidebar = require('sticky-sidebar/dist/sticky-sidebar');
    window.Gumshoe = require('gumshoejs/dist/gumshoe');
} catch (e) {
    console.error(e);

    // Do not interrupt script
}

window.axios = require('axios');

window.axios.defaults.headers['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.headers['Content-Type'] = 'application/json';

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
