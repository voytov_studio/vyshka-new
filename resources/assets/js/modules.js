import axios from 'axios';

window.appModules = {
    blog: require('./modules/Blog').default
};

window.app['widgets'] = {
    openChatWidget: function() {
        document.getElementById('callback-widget-summoner').click()
    }
};
