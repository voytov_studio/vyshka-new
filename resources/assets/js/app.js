import Vue from 'vue'

import "./bootstrap"
import "./helpers"

if (process.env.NODE_ENV !== "production") {
    Vue.config.devtools = true;
}

require('./modules');
