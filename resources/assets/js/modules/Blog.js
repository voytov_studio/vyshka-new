/**
 * @author Cyrill Tekord
 */

let headingCounter = 1;

export default {
    initPost(element, idPrefix) {
        let headings = element.querySelectorAll('h2');
        let results = [];

        headings.forEach((element, index) => {
            element.id = idPrefix.concat(headingCounter++);

            results.push({
                id: element.id,
                text: element.innerText
            });
        });

        element.blogPostInfo = {
            headings: results
        };
    },

    generateHeading(headingContainerElement, blogPostElement) {
        let blogPostInfo = blogPostElement.blogPostInfo || {};
        let headings = blogPostInfo.headings || [];

        let results = [];

        for (let index in headings) {
            let i = headings[index];

            let $class = index == 0 ? 'active' : '';

            let template = `
                <div class="c-blog-post-heading__item">
                    <a href="#${i.id}" class="js-scroll-to-element ${$class}">
                        <span>${i.text}</span>
                    </a>
                </div>
                `;

            results.push(template);
        }

        headingContainerElement.innerHTML = results.join("\n");

        console.debug("Сформировано оглавление поста.");
    }
};
