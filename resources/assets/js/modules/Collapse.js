// https://codepen.io/datchley/pen/Jvdryy

// Handler that uses various data-* attributes to trigger
// specific actions, mimicing bootstraps attributes
const triggers = Array.from(document.querySelectorAll('[data-toggle="collapse"]'));

triggers.forEach((i) => {
    i.dataset["collapsedLabel"] = i.innerText;
});

window.addEventListener('click', (ev) => {
    const element = ev.target;

    if (triggers.includes(element)) {
        ev.preventDefault();

        const selector = element.getAttribute('data-target');

        {
            const container = document.querySelector(selector);

            if (container) {
                if (container.classList.contains('show')) {
                    element.innerText = element.dataset["collapsedLabel"];
                }
                else {
                    element.innerText = element.dataset["expandedLabel"];
                }
            }
        }

        collapse(selector, 'toggle');
    }
}, false);

const fnmap = {
    'toggle': 'toggle',
    'show': 'add',
    'hide': 'remove'
};

const collapse = (selector, cmd) => {
    const targets = Array.from(document.querySelectorAll(selector));

    targets.forEach(target => {
        target.classList[fnmap[cmd]]('show');
    });
};

export default collapse;
