/**
 * @author Cyrill Tekord
 */

import axios from "axios";
import {EventBus} from "../event-bus";

const STATE_IDLING = 0;
const STATE_LOADING = 1;
const STATE_ACTIVATED = 2;

export default {
    url: null,

    handlers: {},

    registerHandler(id, callback) {
        this.handlers[id] = callback;
    },

    registerHandlers(idList, callback) {
        for (let i in idList)
            this.registerHandler(idList[i], callback);
    },

    callHandler(id, $element) {
        if (id in this.handlers) {
            console.debug('Calling chunk handler:', id);

            this.handlers[id](id, $element);
        }
    },

    activate($element, params) {
        if ($element.data('state'))
            return;

        $element.data('state', STATE_LOADING);

        let chunkId = $element.data('chunk-id');

        axios.post(this.url, null, {
            params: Object.assign({}, params, {
                chunk_id: chunkId
            })
        }).then((response) => {
            $element.data('state', STATE_ACTIVATED);
            $element.html(response.data);

            this._chunkLoaded(chunkId, $element);
        });
    },
    _chunkLoaded(id, element) {
        console.debug('Chunk has been loaded: ' + id, element);

        this.callHandler(id, element);

        EventBus.$emit('LAZY_CONTENT_MANAGER.CHUNK_LOADED', {
            instance: this,
            chunk_id: id,
            chunk_element: element
        });
    }
};
