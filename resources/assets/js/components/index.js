import Vue from 'vue'

// Common
Vue.component('validation-errors', require('./ValidationErrors.vue'));

// Application-specific
Vue.component('example', require('./ExampleComponent.vue'));
