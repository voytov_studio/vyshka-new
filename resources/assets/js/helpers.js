FormData.prototype.toObject = function() {
    let result = {};

    for (let pair of this.entries()) {
        result[pair[0]] = pair[1];
    }

    return result;
};

FormData.prototype.dumpEntries = function() {
    console.group('FormData dump');

    for(let [key, value] of Object.entries(this.toObject(this))) {
        console.log(key, '=', value);
    }

    console.groupEnd();
};

$.fn.isInViewport = function() {
    let element = $(this);
    let windowElement = $(window);

    let elementTop = element.offset().top;
    let elementBottom = elementTop + element.outerHeight();
    let viewportTop = windowElement.scrollTop();
    let viewportBottom = viewportTop + windowElement.height();

    return elementBottom > viewportTop && elementTop < viewportBottom;
};
