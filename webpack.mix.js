let mix = require('laravel-mix');
let webpack = require('webpack');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

let sassOptions = {
    implementation: require('node-sass')
};

mix
    .js('resources/assets/js/app.js', 'public/build')
    .js('resources/assets/js/admin.js', 'public/build')
    .sass('resources/assets/stylesheets/main.scss', 'public/build', sassOptions)
    .sass('resources/assets/stylesheets/vendor.scss', 'public/build', sassOptions)
    .sass('resources/assets/stylesheets/admin.scss', 'public/build', sassOptions)
    .sourceMaps()
    .extract()
    .options({
        processCssUrls: false
    });

mix.version('public/build/theme.css');
mix.version('public/build/theme.js');

if (mix.inProduction()) {
    mix.version();
}

mix.webpackConfig({
    resolve: {
        alias: {
            'styles': path.resolve(__dirname, './resources/assets/stylesheets/')
        }
    }
});

mix.browserSync({
    open: 'external',
    proxy: process.env.MIX_PROXY,
    host: process.env.MIX_PROXY,
    browser: process.env.MIX_BROWSER,
    files: [
        'public/**/*'
    ]
});
