<?php

// Специфично для проекта
function is_ekrual() {
	return config('app.settings.site_id') == 'ekrual';
}

// Специфично для проекта
function wp_get_image_by_post_id($postId) {
	/** @var \App\Domain\WpPostDataProvider $provider */
	$provider = app('app.shared.wp_post_data_provider');

	$post = $provider->fetchPostAndMakeAggregate($postId);

	if ($post === null)
		return null;

	$urlParts = parse_url($post->model->guid);

	return url(\Illuminate\Support\Str::start($urlParts['path'], '/wordpress'));
}

function wp_get_image_thumbnail_by_post_id($postId, $sizeId = 'thumbnail') {
	/** @var \App\Domain\WpPostDataProvider $provider */
	$provider = app('app.shared.wp_post_data_provider');

	$post = $provider->fetchPostAndMakeAggregate($postId);

	if ($post === null)
		return null;

	return wp_extract_thumbnail_url_from_post($post, $sizeId);
}

// Специфично для проекта
function wp_extract_thumbnail_url_from_post(\App\Domain\WordPressPostAggregate $postAggregate, $sizeId = 'thumbnail') {
	$meta = unserialize($postAggregate->meta->get('_wp_attachment_metadata'));
	$sizes = Arr::get($meta, 'sizes');

	$data = Arr::get($sizes, $sizeId);

	if ($data === null)
		return null;

	$urlParts = parse_url($postAggregate->model->guid);
	$fileUri = dirname($urlParts['path']) . '/' . $data['file'];

	return config('app.settings.base_content_url'). $fileUri;
}

// Специфично для проекта
function wp_get_uri_by_post_id($postId) {
	/** @var \App\Domain\WpPostDataProvider $provider */
	$provider = app('app.shared.wp_post_data_provider');

	$post = $provider->fetchPostAndMakeAggregate($postId);

	if ($post === null)
		return null;

	return config('app.settings.base_content_url') . $post->model->post_uri;
}

function extract_path_from_url($url) {
	$parts = parse_url($url);

	return $parts['path'];
}

function extract_yt_id($youTubeUrl) {
	$count = preg_match('/(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/', $youTubeUrl, $match);

	if ($count == 0)
		return null;

	return $match[1];
}

function make_youtube_default_thumbnail_url($ytId) {
	$result = "//img.youtube.com/vi/$ytId/hqdefault.jpg";

	return $result;
}

function make_youtube_embedded_url($url, $default = null) {
	$ytId = extract_yt_id($url);

	if ($ytId === null)
		return $default;

	$result = "https://www.youtube.com/embed/" . $ytId;

	return $result;
}

function acf_extract_url_from_link($value, $default = null) {
	$data = unserialize($value);

	return \Illuminate\Support\Arr::get($data, 'url', null);
}

// Source: https://stackoverflow.com/questions/7409512/new-line-to-paragraph-function/35907805
function nl2p($string, $line_breaks = true, $xml = true) {
	// Remove current tags to avoid double-wrapping
	$string = str_replace(array('<p>', '</p>', '<br>', '<br />'), '', $string);

	// Default: Use <br> for single line breaks, <p> for multiple line breaks
	if ($line_breaks == true) {
		$string = '<p>' . preg_replace(
				array("/([\n]{2,})/i", "/([\r\n]{3,})/i", "/([^>])\n([^<])/i"),
				array("</p>\n<p>", "</p>\n<p>", '$1<br' . ($xml == true ? ' /' : '') . '>$2'),
				trim($string)) . '</p>'
		;

	// Use <p> for all line breaks if $line_breaks is set to false
	} else {
		$string = '<p>' . preg_replace(
				array("/([\n]{1,})/i", "/([\r]{1,})/i"),
				"</p>\n<p>",
				trim($string)) . '</p>';
	}

	// Remove empty paragraph tags
	$string = str_replace('<p></p>', '', $string);

	return $string;
}
