<?php

namespace App\Providers;

use App\Domain\AcfValueExtractor;
use App\Domain\BlogRepository;
use App\Domain\ContentFieldsStore;
use App\Domain\ContentPageContext;
use App\Domain\DataPostService;
use App\Domain\DynamicContentSheet;
use App\Domain\SiteDataProvider;
use App\Domain\SiteMenuDataProvider;
use App\Domain\WordPressAuthService;
use App\Domain\WpPostDataProvider;
use Illuminate\Config\Repository;
use Illuminate\Container\Container;
use Illuminate\Encryption\Encrypter;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

/**
 * @author Cyrill Tekord
 */
class AppServiceProvider extends ServiceProvider {
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot() {
		View::composer('*', function (\Illuminate\View\View $view) {
			$sharedData = app('presentation.shared-data');

			$view->with($sharedData);
		});

		Blade::if('admin', function () {
			$service = $this->app->make(WordPressAuthService::class);

			return $service->check();
		});

		//
	}

	/**
	 * Register any application services.
	 */
	public function register() {
		$siteSpecificConfigurationFile = config('app.settings.configuration_file');

		if (file_exists($siteSpecificConfigurationFile)) {
			/** @var Repository $config */
			$config = app('config');

			$config->set(require $siteSpecificConfigurationFile);
		}

		if ($this->app->environment() !== 'production') {
			$this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
		}

		//

		$this->app->bind('wp.encryptor', function () {
			$key = base64_decode(config('app.settings.wordpress_integration.key'));
			$encrypter = new Encrypter($key, config('app.settings.wordpress_integration.cipher'));

			return $encrypter;
		});

		$this->app->singleton(WordPressAuthService::class, function (Container $app) {
			$o = new WordPressAuthService($app->make(Request::class), $app->make('wp.encryptor'));

			return $o;
		});

		$this->app->bind(DataPostService::class, function () {
			$o = new DataPostService(app(WpPostDataProvider::class));
			$o->defaultDataPostId = config('app.settings.content_sources.default_data_page');
			$o->categoryDataPostMap = config('app.settings.content_sources.service_prices');

			return $o;
		});

		$this->app->singleton('app.ContentPageContext', ContentPageContext::class);

		$this->app->bind(SiteMenuDataProvider::class, function () {
			$instance = new SiteMenuDataProvider();

			return $instance;
		});

		$this->app->singleton(SiteDataProvider::class, function () {
			$o = new SiteDataProvider(
				$this->app->make(WpPostDataProvider::class),
				$this->app->make(SiteMenuDataProvider::class),
				$this->app->make('app.ContentFieldsStore')
			);

			return $o;
		});

		$this->app->bind(BlogRepository::class, function () {
			$instance = new BlogRepository();
			$instance->blogRootCategoryId = config('app.settings.root_blog_category_id');
			$instance->cachePrefix = config('app.settings.postDataCachePrefix');

			return $instance;
		});

		$this->app->singleton('app.shared.wp_post_data_provider', WpPostDataProvider::class);

		$this->app->bind(ContentFieldsStore::class, function () {
			$o = new ContentFieldsStore();
//			$o->debugMode = config('app.debug');

			return $o;
		});

		$this->app->singleton('app.ContentFieldsStore', function () {
			$o = app(ContentFieldsStore::class);

			return $o;
		});

		$this->app->singleton('app.DynamicContentSheet', function() {
			$o = new DynamicContentSheet(
				app(ContentFieldsStore::class),
				app(AcfValueExtractor::class)
			);

			$o->shouldMergeFieldCallback = function (DynamicContentSheet $self, $key, $value) {
				if (Str::startsWith($key, 'likbez.0')) {
					$alreadyMerged = $self->getMergedFields()
						->contains(function ($item) use ($key) {
							return Str::startsWith($item, 'likbez.0');
						});

					return $alreadyMerged == false;
				}

				if (Str::startsWith($key, 'likbez.1')) {
					$alreadyMerged = $self->getMergedFields()
						->contains(function ($item) use ($key) {
							return Str::startsWith($item, 'likbez.1');
						});

					return $alreadyMerged == false;
				}

				$alreadyMerged = $self->getMergedFields()
					->contains($key);

				return $alreadyMerged == false;
			};

			return $o;
		});

		$this->app->bind(AcfValueExtractor::class, function () {
			$o = new AcfValueExtractor();

			$booleanValueMapper = function ($key, $value) {
				return boolval($value);
			};

			$o->registerMappingRules([
				'promoBox_enabled' => $booleanValueMapper,
				'mainPage_enabled' => $booleanValueMapper,
				'weWorkWith_enabled' => $booleanValueMapper,
				'howWeWork_enabled' => $booleanValueMapper,
				'termsOfSolution_enabled' => $booleanValueMapper,
				'reviews_enabled' => $booleanValueMapper,
				'tatianaInner_enabled' => $booleanValueMapper,
			]);

			$o->registerFilterRules([
				'promoBox_*' => function (Collection $collection, $key, $value) {
					return $collection->get('promoBox_enabled') == 1;
				},
				'mainPage_*' => function (Collection $collection, $key, $value) {
					return $collection->get('mainPage_enabled') == 1;
				},
				'weWorkWith_*' => function (Collection $collection, $key, $value) {
					return $collection->get('weWorkWith_enabled') == 1;
				},
				'howWeWork_*' => function (Collection $collection, $key, $value) {
					return $collection->get('howWeWork_enabled') == 1;
				},
				'consultation_*' => function (Collection $collection, $key, $value) {
					return $collection->get('consultation_enabled') == 1;
				},
				'termsOfSolution_*' => function (Collection $collection, $key, $value) {
					return $collection->get('termsOfSolution_enabled') == 1;
				},
				'reviews_*' => function (Collection $collection, $key, $value) {
					return $collection->get('reviews_enabled') == 1;
				},
				'tatianaInner_*' => function (Collection $collection, $key, $value) {
					return $collection->get('tatianaInner_enabled') == 1;
				},
				'specialists_*' => function (Collection $collection, $key, $value) {
					return $collection->get('specialists_enabled') == 1;
				},
				'likbez_0*' => function (Collection $collection, $key, $value) {
					return !empty($collection->get('likbez_0_likbez_headers'));
				},
				'likbez_1*' => function (Collection $collection, $key, $value) {
					return !empty($collection->get('likbez_1_likbez_headers'));
				},
				'likbez_2*' => function (Collection $collection, $key, $value) {
					return !empty($collection->get('likbez_2_likbez_headers'));
				},
				'likbez_3*' => function (Collection $collection, $key, $value) {
					return !empty($collection->get('likbez_3_likbez_headers'));
				},
			]);

			$o->setGroupPrefixesFromFilter();

			return $o;
		});

		// Static Data

		$this->app->bind('data.specialization_icons', function () {
			return require base_path('data/specialization-icons.php');
		});

		$this->app->bind('data.menu.specializations', function () {
			return collect(require base_path('data/menu.specializations.php'));
		});

		$this->app->bind('data.menu.services', function () {
			$files = [
				base_path('/data/sites/' . config('app.settings.site_id') . '/menu.services.php'),
				base_path('/data/menu.services.php'),
			];

			$existingFile = null;

			foreach ($files as $f) {
				if (file_exists($f)) {
					$existingFile = $f;

					break;
				}
			}

			return collect(require $existingFile);
		});

		$this->app->singleton('data.reviews', function () {
			$externalReviewsFilePath = base_path('data/external-reviews.php');

			$externalReviews = file_exists($externalReviewsFilePath)
				? require $externalReviewsFilePath
				: [];

			return $externalReviews;
		});

		// View Data

		$this->app->singleton('presentation.shared-data', function () {
			$siteDataProvider = app(\App\Domain\SiteDataProvider::class);
			$menuDataProvider = $siteDataProvider->getMenuDataProvider();

			$specializationsMenuItems = $menuDataProvider->getSpecializationMenuItems();
			$servicesMenuItems = $menuDataProvider->getServicesMenuItems();

			return compact(
				'siteDataProvider',
				'menuDataProvider',
				'specializationsMenuItems',
				'servicesMenuItems'
			);
		});

		//
	}
}
