<?php

namespace App\Http\Middleware;

use App\Domain\WordPressAuthService;
use Closure;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @author Cyrill Tekord
 */
class WpAuthenticate {
	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure $next
	 *
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
		$service = app(WordPressAuthService::class);

		if (!$service->check())
			throw new HttpException(Response::HTTP_FORBIDDEN);

		return $next($request);
	}
}
