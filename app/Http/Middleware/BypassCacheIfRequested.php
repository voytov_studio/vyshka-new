<?php

namespace App\Http\Middleware;

use Closure;

/**
 * @author Cyrill Tekord
 */
class BypassCacheIfRequested {
	/**
	 * Handle an incoming request.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Closure $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
		if ($request->query('no-cache') == 1) {
			app('config')->set('cache.default', 'array');
		}

		return $next($request);
	}
}
