<?php

namespace App\Http\Controllers;

use App\Domain\WpPostDataProvider;

/**
 * @author Cyrill Tekord
 */
class DebugController extends Controller {
	protected $postDataProvider;

	public function __construct(WpPostDataProvider $postDataProvider) {
		$this->postDataProvider = $postDataProvider;
	}

	public function dumpPost($postId) {
		$postAggregate = $this->postDataProvider->fetchPostAndMakeAggregate($postId);

		abort_if($postAggregate === null, 404);

		$rawMetaData = $this->postDataProvider->fetchPostMeta($postId)
			->pluck('meta_value', 'meta_key');

		$unwiredMetaData = $this->postDataProvider->unwireMeta($postAggregate->meta);

		dd($postAggregate, $rawMetaData, $unwiredMetaData);
	}
}
