<?php

namespace App\Http\Controllers;

use App\Domain\BlogRepository;
use App\Domain\DataPostService;
use App\Domain\WordPressPostAggregate;
use App\Facades\ContentPageContextFacade;
use App\Facades\DynamicContentSheetFacade;
use App\Models\WordPressTaxonomy;
use App\Models\WordPressTerm;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @author Cyrill Tekord
 */
class BlogController extends Controller {
	protected $blogRepository;

	public function __construct(BlogRepository $blogRepository) {
		$this->blogRepository = $blogRepository;
	}

	protected function initializeContent($currentPostAggregate) {
		$dps = app(DataPostService::class);
		$dps->loadDefaultDataPost();

		if ($currentPostAggregate !== null)
			ContentPageContextFacade::setCurrentPostAggregate($currentPostAggregate);

		DynamicContentSheetFacade::mergeFields($dps->getAllMetas());
	}

	public function indexImplementation(Request $request, $categoryId = null) {
		$allowedSortByValues = ['date', 'rating'];
		$defaultSortBy = $allowedSortByValues[0];

		$sortBy = $request->query('sortBy', $defaultSortBy);

		if ($categoryId === null) {
			$blogRootCategory = config('app.settings.root_blog_category_id');

			$postQuery = $this->blogRepository->queryPostsInCategories([$blogRootCategory]);
		}
		else {
			$postQuery = $this->blogRepository->queryPostsInCategories([$categoryId]);
		}

		$postQuery->where(['post_status' => 'publish']);

		// $posts = $postQuery->forPage($request->query('page', 10));
		$posts = $postQuery->get();

		$postMetas = $this->blogRepository->fetchPostMetaAsKeyValueMapBatch($posts->pluck('ID')->toArray());

		$posts->each(function (&$item) use ($postMetas) {
			$item->mixMeta($postMetas->get($item->ID));

			$terms = $this->filterBlogPostTerms($item->taxonomies);

			$item->categories = $terms->map(function (WordPressTerm $category) {
				$slug = $category->slug;

				return [
					'name' => $category->name,
					'link' => route('blog.indexByCategory', ['slug' => $slug]),
				];
			});
		});

		if ($sortBy !== null) {
			if (!in_array($sortBy, $allowedSortByValues))
				$sortBy = $defaultSortBy;

			if ($sortBy == 'date') {
				$posts = $posts->sortByDesc('post_date_gmt', SORT_STRING);
			}
			else if ($sortBy == 'rating') {
				$posts = $posts->sortByDesc('meta.rating', SORT_STRING);
			}
		}

		$posts = $posts->map(function ($item) {
			$aggregate = new WordPressPostAggregate();
			$aggregate->model = $item;
			$aggregate->meta = $item->meta;

			return $aggregate;
		});

		$blogCategories = $this->blogRepository->fetchAllCategories();

		return view('blog.index', [
			'allCategories' => $blogCategories,
			'currentCategoryId' => $categoryId,
			'sortBy' => $sortBy,
			'posts' => $posts
		]);
	}

	public function index(Request $request) {
		return $this->indexImplementation($request);
	}

	public function indexByCategory(Request $request, $slug) {
		$slug = urlencode($slug);

		$term = WordPressTerm::query()
			->where(['slug' => $slug])
			->firstOrFail();

		$isBlogCategory = WordPressTaxonomy::query()
			->where([
				'term_id' => $term->getKey(),
				'parent' => $this->blogRepository->blogRootCategoryId,
				'taxonomy' => 'category',
			])->exists();

		if (!$isBlogCategory)
			throw new NotFoundHttpException('Запрашиваемый раздел не найден.');

		return $this->indexImplementation($request, $term->getKey());
	}

	public function showPost(Request $request, $slug) {
		$slug = urlencode($slug);

		$postId = Cache::remember('post-permalinks[' . $slug . ']', 1, function () use ($slug) {
			return $this->blogRepository->findPostIdBySlug($slug);
		});

		/** @var \App\Models\WordPressPost $post */
		$post = \App\Models\WordPressPost::query()
			->with(['attachments', 'taxonomies'])
			->where([
				'ID' => $postId,
				'post_status' => 'publish',
				'post_type' => 'post'
			])
			->firstOrFail();

		$postMeta = $this->blogRepository->fetchPostMetaAsKeyValueMap($post->getKey());

		$postAggregate = new WordPressPostAggregate();
		$postAggregate->model = $post;
		$postAggregate->meta = $postMeta;

		$this->initializeContent($postAggregate);

		{
			$terms = $this->filterBlogPostTerms($post->taxonomies);

			if ($terms->isNotEmpty()) {
				$relatedPosts = $this->blogRepository->queryPostsInCategories([$terms->pluck('term_id')])
					->with(['attachments'])
					->whereNotIn('ID', [$post->getKey()])
					->limit(8) // ???
					->get();
			}
			else {
				$relatedPosts = collect();
			}

			$relatedPostMetas = $this->blogRepository->fetchPostMetaAsKeyValueMapBatch($relatedPosts->pluck('ID')->toArray());

			$relatedPostAggregates = $relatedPosts->map(function ($post) use ($relatedPostMetas) {
				$postAggregate = new WordPressPostAggregate();
				$postAggregate->model = $post;
				$postAggregate->meta = $relatedPostMetas->get($post->getKey());

				return $postAggregate;
			});
		}

		$returnUrl = Session::get('blogReturnUrl', route('blog.index'));

		return view('blog.show-post', [
			'postAggregate' => $postAggregate,
			'relatedPostAggregates' => $relatedPostAggregates,
			'returnUrl' => $returnUrl
		]);
	}

	/**
	 * @param WordPressTaxonomy[]|Collection $taxonomies
	 *
	 * @return WordPressTerm[]|Collection
	 */
	protected function filterBlogPostTerms($taxonomies) {
		return $taxonomies->filter(function(WordPressTaxonomy $taxonomy) {
			return $taxonomy->parent == $this->blogRepository->blogRootCategoryId;
		})->pluck('term');
	}
}
