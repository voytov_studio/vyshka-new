<?php

namespace App\Http\Controllers;

use App\Domain\BlogRepository;
use App\Domain\DataPostService;
use App\Domain\WpPostDataProvider;
use App\Facades\ContentPageContextFacade;
use App\Facades\DynamicContentSheetFacade;
use App\Models\WordPressPost;
use Illuminate\Http\Request;

/**
 * @author Cyrill Tekord
 */
class PageController extends Controller {

	protected $blogRepository;
	protected $postDataProvider;

	public function __construct(BlogRepository $blogRepository, WpPostDataProvider $postDataProvider) {
		$this->blogRepository = $blogRepository;
		$this->postDataProvider = $postDataProvider;
	}

	public function show(Request $request, $slug) {
		$postId = $this->blogRepository->findPostIdBySlug($slug, 'page');

		$postAggregate = $this->postDataProvider->fetchPostAndMakeAggregate($postId);

		abort_if($postAggregate === null, 404, 'Запрошенная страница не найдена.');

		{
			ContentPageContextFacade::setCurrentPostAggregate($postAggregate);

			$dps = app(DataPostService::class);
			$dps->loadDefaultDataPost();

			$dps->loadCategoryDataPost($postAggregate->getMeta('page-category'));

			$dps->setCurrentDataPost($postAggregate);

			DynamicContentSheetFacade::mergeFields($dps->getAllMetas());
		}

		if (config('app.debug')) {
			$postId = $postAggregate->model->getKey();

			\Debugbar::info(url("wordpress/wp-admin/post.php?post=$postId&action=edit"));
		}

		return view('pages.show', [
			'postAggregate' => $postAggregate,
		]);
	}
}
