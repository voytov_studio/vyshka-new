<?php

namespace App\Http\Controllers;

use App\Domain\WpPostDataProvider;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Spatie\ResponseCache\Events\ClearedResponseCache;
use Spatie\ResponseCache\Events\ClearingResponseCache;
use Spatie\ResponseCache\Facades\ResponseCache;
use Spatie\ResponseCache\ResponseCacheRepository;

/**
 * @author Cyrill Tekord
 */
class MaintenanceController extends Controller {
	public function __construct() {
		$this->middleware('wpAuthenticate');
	}

	public function showPanel() {
		return view('maintenance.panel');
	}

	public function editPage(Request $request, $pageId) {
		$url = strtr(config('app.settings.wordpress_integration.edit_post_url_template'), [
			'{{pageId}}' => $pageId
		]);

		return redirect($url);
	}

	public function clearCache(ResponseCacheRepository $cache) {
		event(new ClearingResponseCache());

		$cache->clear();

		event(new ClearedResponseCache());

		return back()->with('servicePanels.message', 'Кеш всех страниц очищен');
	}

	public function invalidatePageCache(Request $request, WpPostDataProvider $postDataProvider) {
		$uri = $request->get('uri');

		if ($uri === null)
			return back();

		ResponseCache::forget($uri);

		$httpClient = new Client([
			'base_uri' => config('app.url'),
		]);

		try {
			// Посещаем страницу с чистым клиентом
			$response = $httpClient->get($uri);
		}
		catch (\Exception $e) {
			report($e);
		}

		return back()->with('servicePanels.message', 'Кеш страницы обновлён');
	}

	public function crawler(Request $request) {
		$siteMapPath = public_path('/sitemap.xml');

		$url = config('app.settings.crawler.control_panel_url');

		$url .= '?' . http_build_query([
				'siteMapPath' => $siteMapPath
			]);

		return view('maintenance.crawler', [
			'url' => $url
		]);
	}
}
