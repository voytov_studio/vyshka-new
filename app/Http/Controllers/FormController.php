<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

/**
 * @author Cyrill Tekord
 */
class FormController extends Controller {
	public function sendContactsViaSms(Request $request) {
		$this->validate($request, [
			'phone' => ['required']
		]);

		$phone = $request->input('phone');

		$smsMessage = config('app.settings.contacts_via_sms.message');

		$httpClient = new Client();

		try {
			$response = $httpClient->get('https://lcab.smsint.ru/lcabApi/sendSms.php', [
				'query' => [
					'login' => config('services.smsint.login'),
					'password' => config('services.smsint.password'),
					'txt' => $smsMessage,
					'to' => $phone,
					'source' => config('services.smsint.sender')
				]
			]);

			$response = json_decode($response->getBody(), true);

			Log::info("Отправлено SMS через smsint.ru", [
				'to' => $phone,
				'response' => $response
			]);
		} catch (\Exception $e) {
			throw $e;
		}

		return response(null, Response::HTTP_OK);
	}
}
