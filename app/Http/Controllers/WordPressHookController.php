<?php

namespace App\Http\Controllers;

use App\Domain\PostSavedHookHandler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Cyrill Tekord
 */
class WordPressHookController extends Controller {

	public function onPostSaved(Request $request) {
		$data = (object)$this->validate($request, [
			'post_id' => ['required', 'integer'],
			'url' => ['required', 'url'],
		]);

		Log::info('Web Hook: ' . __METHOD__, [
			'post_id' => $data->post_id,
			'url' => $data->url,
		]);

		$handler = app(PostSavedHookHandler::class);
		$handler->handle($data);

		return response(null, Response::HTTP_NO_CONTENT);
	}
}
