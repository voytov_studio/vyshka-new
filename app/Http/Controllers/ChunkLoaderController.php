<?php

namespace App\Http\Controllers;

use App\Domain\Content\PostChunk;
use App\Domain\ContentChunkFactory;
use App\Domain\WpPostDataProvider;
use App\Facades\ContentPageContextFacade;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @author Cyrill Tekord
 */
class ChunkLoaderController extends Controller {

	/** @var WpPostDataProvider  */
	protected $postDataProvider;

	public function __construct(WpPostDataProvider $postDataProvider) {
		$this->postDataProvider = $postDataProvider;
	}

	public function load(Request $request, ContentChunkFactory $contentChunkFactory) {
		$validator = Validator::make($request->input(), [
			'chunk_id' => ['required'],
			'page_id' => ['integer']
		]);

		if ($validator->fails())
			return response()->json($validator->getMessageBag(), Response::HTTP_UNPROCESSABLE_ENTITY);

		$chunkId = $request->input('chunk_id');
		$pageId = $request->input('page_id');

		if ($pageId !== null) {
			$currentPostAggregate = $this->postDataProvider->fetchPostAndMakeAggregate($pageId);
		}
		else {
			// Некоторые блоки могут существовать вне конкретной страницы
			$currentPostAggregate = null;
		}

		ContentPageContextFacade::setCurrentPostAggregate($currentPostAggregate);

		$chunk = $contentChunkFactory->make($chunkId);

		if ($chunk === null)
			throw new NotFoundHttpException('The requested chunk does not exist.');

		if ($chunk instanceof PostChunk) {
			$chunk->postAggregate = $currentPostAggregate;
		}

		$renderedView = $chunk->render();

		if ($renderedView instanceof View)
			$renderedView = $renderedView->render();

		if ($renderedView === null) {
			return response('');
		}

		if ((bool)$request->query('debug', 0)) {
			return $this->renderWithLayout($renderedView)->render();
		}

		return $renderedView;
	}

	/**
	 * @param string $renderedContentView
	 *
	 * @return \Illuminate\Contracts\View\View
	 */
	protected function renderWithLayout($renderedContentView) {
		/** @var \Illuminate\View\Factory $viewFactory */
		$viewFactory = app(Factory::class);

		$viewFactory->startSection('content');
		echo $renderedContentView;
		$viewFactory->stopSection();

		return $viewFactory->make('layouts.main');
	}
}
