<?php

namespace App\Http\Controllers;

use App\Domain\DataPostService;
use App\Domain\SiteDataProvider;
use App\Domain\WordPressAttachmentExtractor;
use App\Domain\WpPostDataProvider;
use App\Facades\ContentPageContextFacade;
use App\Facades\DynamicContentSheetFacade;
use App\Models\WordPressPost;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

/**
 * @author Cyrill Tekord
 */
class SiteController extends Controller {

	/** @var SiteDataProvider */
	protected $siteDataProvider;

	public function __construct(SiteDataProvider $siteDataProvider) {
		$this->siteDataProvider = $siteDataProvider;
	}

	protected function initializeContent($currentPostAggregate = null) {
		$dps = app(DataPostService::class);
		$dps->loadDefaultDataPost();

		if ($currentPostAggregate !== null)
			ContentPageContextFacade::setCurrentPostAggregate($currentPostAggregate);

		DynamicContentSheetFacade::mergeFields($dps->getAllMetas());
	}

	public function index(WpPostDataProvider $postDataProvider) {
		$postId = config('app.settings.content_sources.main');

		$postAggregate = $postDataProvider->fetchPostAndMakeAggregate($postId);

		abort_if($postAggregate === null, 404, 'Запрашиваемая страница не найдена.');

		{
			ContentPageContextFacade::setCurrentPostAggregate($postAggregate);

			$dps = app(DataPostService::class);
			$dps->loadDefaultDataPost();
			$dps->setCurrentDataPost($postAggregate);

			DynamicContentSheetFacade::mergeFields($dps->getAllMetas());
		}

		return view('site.home', [
			'postAggregate' => $postAggregate
		]);
	}

	public function press(WpPostDataProvider $postDataProvider, WordPressAttachmentExtractor $attachmentExtractor) {
		$postId = config('app.settings.content_sources.mass_media');

		/** @var WordPressPost $model */
		$model = WordPressPost::query()
			->whereKey($postId)
			->firstOrFail();

		$postAggregate = $postDataProvider->makeAggregate($model);

		$this->initializeContent($postAggregate);

		$attachments = $postAggregate->model->attachments;

		$attachments = $attachments->map(function(WordPressPost $item) use ($postDataProvider, $attachmentExtractor) {
			$postAggregate = $postDataProvider->makeAggregate($item);

			return $attachmentExtractor->extract($postAggregate);
		})->keyBy('post_id');

		return view('site.press', [
			'postAggregate' => $postAggregate,
			'attachments' => $attachments
		]);
	}

	public function contacts() {
		$this->initializeContent();

		return view('site.about-us');
	}

	public function showDocuments() {
		/** @var FilesystemAdapter $disk */
		$disk = Storage::disk('public');

		$files = $disk->allFiles('documents');

		$links = collect($files)
			->map(function ($item) use ($disk) {
				$pathInfo = pathinfo($item);
				$fileName = $pathInfo['basename'];

				return [
					'name' => $fileName,
					'path' => $item,
					'url' => $disk->url($item),
				];
			})
			->filter(function ($item) {
				return !Str::startsWith($item['name'], '.');
			})
			->map(function ($item) {
				return [
					'link' => $item['url'],
					'title' => $item['name'],
				];
			});

		$viewData = [
			'links' => $links
		];

		/** @var \Illuminate\View\View $content */
		$content = View::make('chunks._link-list')
			->with($viewData)
			->render();

		return response($content);
	}
}
