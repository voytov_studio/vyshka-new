<?php

namespace App\Http\Controllers;

use App\Domain\WpPostDataProvider;
use App\Models\WordPressPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

/**
 * @author Cyrill Tekord
 */
class SearchController extends Controller {
	/** @var WpPostDataProvider */
	protected $postDataProvider;

	public function __construct(WpPostDataProvider $postDataProvider) {
		$this->postDataProvider = $postDataProvider;
	}

	public function suggestService(Request $request) {
		$q = $request->query('q');

		if ($q === null || strlen($q) < 3)
			return [];

		$q = preg_replace('!\s+!', ' ', $q);
		$q = Str::substr($q, 0, 30);

		$cacheKey = 'search.suggest_service_results.' . $q;

		$result = Cache::remember($cacheKey, 60 * 60, function () use ($q) {
			$posts = WordPressPost::query()
				->select(['ID as id', 'post_title as title', 'earth_postmeta.meta_value as slug'])
				->leftJoin('earth_postmeta', 'earth_postmeta.post_id', '=', 'earth_posts.id')
				->where([
					'post_type' => 'page',
					'post_status' => 'publish'
				])
				->where('post_title', 'like', "%$q%")
				->where('earth_postmeta.meta_key', '=', 'custom_permalink')
				->limit(10)
				->get();

			$result = $posts->map(function (WordPressPost $post) {
				return [
					'name' => $post->title,
					'url' => route('pages.show', ['slug' => $post->slug])
				];
			});

			return $result;
		});

		return response()->json($result);
	}
}
