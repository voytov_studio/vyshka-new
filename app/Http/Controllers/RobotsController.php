<?php

namespace App\Http\Controllers;

use Symfony\Component\HttpFoundation\Response;

/**
 * @author Cyrill Tekord
 */
class RobotsController extends Controller {
	public function __invoke() {
		$fileName = 'robots.' . app()->environment() . '.txt';
		$filePath = public_path($fileName);

		$responseFilePath = public_path('robots.default.txt');

		if (file_exists($filePath))
			$responseFilePath = $filePath;

		return response()->file($responseFilePath);
	}
}
