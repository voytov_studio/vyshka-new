<?php

namespace App\Http\Controllers;

use App\Domain\SiteMenuDataProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\View;

/**
 * @author Cyrill Tekord
 */
class MenuController extends Controller {
	/** @var SiteMenuDataProvider */
	protected $siteMenuDataProvider;

	public function __construct(SiteMenuDataProvider $siteMenuDataProvider) {
		$this->siteMenuDataProvider = $siteMenuDataProvider;
	}

	protected function handleRequest(Request $request, $items) {
		$result = collect($items['links'])
			->unique('link');

		$viewData = [
			'links' => $result
		];

		if ($request->ajax()) {
			/** @var \Illuminate\View\View $content */
			$content = View::make('chunks._link-list')
				->with($viewData)
				->render();

			return response($content);
		}

		return view('debug.links', $viewData);
	}

	public function showPageLinksBySpecialization(Request $request, $id) {
		$name = $id;
		$data = $this->siteMenuDataProvider->getAllSpecializationLinksAsFlatList();

		$items = Arr::first($data, function ($item) use ($name) {
			return $item['name'] == $name;
		}, []);

		abort_if(empty($items), 404, 'Специализация не найдена.');

		return $this->handleRequest($request, $items);
	}

	public function showPageLinksByService(Request $request, $id) {
		$name = $id;
		$data = $this->siteMenuDataProvider->getAllServicesLinksAsFlatList();

		$items = Arr::first($data, function ($item) use ($name) {
			return $item['name'] == $name;
		}, []);

		abort_if(empty($items), 404, 'Услуга не найдена.');

		return $this->handleRequest($request, $items);
	}
}
