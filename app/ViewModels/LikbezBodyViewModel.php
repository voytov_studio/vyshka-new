<?php

namespace App\ViewModels;

use DiDom\Document;
use Illuminate\Support\Arr;

/**
 * @author Cyrill Tekord
 */
class LikbezBodyViewModel {
	/** @var string */
	public $titleField = 'likbez_content_title';

	/** @var string */
	public $contentField = 'likbez_content_text';

	/** @var array */
	protected $content;

	/** @var \DiDom\Element[] */
	protected $contentAsHtmlElements;

	public function __construct($content) {
		$this->content = $content;
	}

	public function parseContent() {
		$this->contentAsHtmlElements = $this->splitContent();
	}

	protected function splitContent() {
		$rawText = Arr::get($this->content, $this->contentField);
		$html = nl2p($rawText);

		if (empty($html)) {
			return [];
		}
		else {
			$document = new Document($html);

			/** @var \DiDom\Element $element */
			return $document->find('body > *');
		}
	}

	/**
	 * @return array
	 */
	public function getContent(): array {
		return $this->content;
	}

	public function getTitle() {
		return Arr::get($this->content, $this->titleField);
	}

	public function getExcerpt() {
		if (empty($this->contentAsHtmlElements))
			return null;

		$result = Arr::first($this->contentAsHtmlElements);

		if ($result !== null)
			$result = $result->html();

		return $result;
	}

	public function getTextAfterExcerpt() {
		if (empty($this->contentAsHtmlElements))
			return null;

		$items = collect($this->contentAsHtmlElements)
			->slice(1)
			->map(function (\DiDom\Element $i) {
				return $i->html();
			});

		$result = $items->join("\n");

		return $result;
	}
}
