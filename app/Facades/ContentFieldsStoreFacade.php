<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @author Cyrill Tekord
 */
class ContentFieldsStoreFacade extends Facade {
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor() {
		return 'app.ContentFieldsStore';
	}
}
