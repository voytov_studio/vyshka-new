<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @author Cyrill Tekord
 */
class ContentPageContextFacade extends Facade {
	/**
	 * @inheritDoc
	 */
	protected static function getFacadeAccessor() {
		return 'app.ContentPageContext';
	}
}
