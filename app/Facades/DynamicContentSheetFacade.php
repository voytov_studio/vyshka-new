<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @author Cyrill Tekord
 */
class DynamicContentSheetFacade extends Facade {
	/**
	 * @inheritDoc
	 */
	protected static function getFacadeAccessor() {
		return 'app.DynamicContentSheet';
	}
}
