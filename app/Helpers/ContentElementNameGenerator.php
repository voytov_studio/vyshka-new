<?php

namespace App\Helpers;

/**
 * @author Cyrill Tekord
 */
class ContentElementNameGenerator {

	/** @var string */
	public $separator;

	/** @var string */
	protected $idPrefix;

	public function __construct($idPrefix = null) {
		$this->idPrefix = $idPrefix ?? uniqid('b-');
		$this->separator = '__';
	}

	/**
	 * @return string
	 */
	public function getIdPrefix() {
		return $this->idPrefix;
	}

	public function __invoke($elementSemanticType, $index = null) {
		$result = $this->idPrefix . $this->separator . $elementSemanticType;

		if ($index !== null)
			$result .= $this->separator . $index;

		return $result;
	}
}
