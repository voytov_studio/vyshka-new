<?php

namespace App\Domain;

use Illuminate\Support\Arr;

/**
 * @author Cyrill Tekord
 */
class DataPostService {
	/** @var array */
	public $categoryDataPostMap = [];

	/** @var integer */
	public $defaultDataPostId;

	/** @var WordPressPostAggregate */
	protected $defaultDataPostAggregate;

	/** @var WordPressPostAggregate */
	protected $categoryDataPostAggregate;

	/** @var WordPressPostAggregate */
	protected $currentDataPostAggregate;

	/** @var WpPostDataProvider */
	protected $postDataProvider;

	public function __construct(WpPostDataProvider $postDataProvider) {
		$this->postDataProvider = $postDataProvider;
	}

	public function loadCategoryDataPost($categoryId) {
		$categoryPageId = Arr::get($this->categoryDataPostMap, $categoryId);

		if ($categoryPageId === null)
			return;

		$this->categoryDataPostAggregate = $this->postDataProvider->fetchPostAndMakeAggregate($categoryPageId);
	}

	public function loadDefaultDataPost() {
		$this->defaultDataPostAggregate = $this->postDataProvider->fetchPostAndMakeAggregate($this->defaultDataPostId);
	}

	public function loadCurrentPage($pageId) {
		$this->currentDataPostAggregate = $this->postDataProvider->fetchPostAndMakeAggregate($pageId);
	}

	public function setDefaultDataPost($postAggregate) {
		$this->defaultDataPostAggregate = $postAggregate;
	}

	public function setCategoryDataPost($postAggregate) {
		$this->categoryDataPostAggregate = $postAggregate;
	}

	public function setCurrentDataPost($postAggregate) {
		$this->currentDataPostAggregate = $postAggregate;
	}

	protected function getAllPostAggregates() {
		return [$this->defaultDataPostAggregate, $this->categoryDataPostAggregate, $this->currentDataPostAggregate];
	}

	public function getAllMetas() {
		$result = array_reduce($this->getAllPostAggregates(), function ($carry, ? WordPressPostAggregate  $item) {
			if ($item !== null) {
				$carry[] = $item->meta;
			}

			return $carry;
		}, []);

		return $result;
	}
}
