<?php

namespace App\Domain;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * @author Cyrill Tekord
 */
class ContentFieldsStore {
	/** @var Collection */
	protected $items;

	/** @var array */
	protected $accessCounter = [];

	/** @var array */
	protected $missingFields = [];

	/** @var bool */
	public $debugMode = false;

	public function __construct() {
		$this->items = collect();
	}

	public function getBlock($key, $default = null) {
		$value = $this->get($key, $default);

		if ($this->debugMode) {
			if (empty($value))
				$value = '(не задано)';

			$html = $this->renderBlock($key, $value);

			$value = $html;
		}

		return $value;
	}

	public function getTextBlock($key, $default = null) {
		$value = $this->get($key, $default);

		$value = nl2p($value);

		if ($this->debugMode) {
			if (empty($value))
				$value = '(не задано)';

			$html = $this->renderBlock($key, $value);

			$value = $html;
		}

		return $value;
	}

	public function renderBlock($key, $value) {
		$html = <<<HTML
<span class="c-content-field-store-wrapper" data-key="$key">
	<span class="c-content-field-store-wrapper__key">$key</span>
	<span class="c-content-field-store-wrapper__value">$value</span>
</span>
HTML;

		return $html;
	}

	public function get($key, $default = null) {
		// Важно НЕ использовать data_get / Arr::get
		$value = $this->items->get($key, $default);

		if (!$this->items->has($key)) {
			if (!in_array($key, $this->missingFields))
				$this->missingFields[] = $key;
		}

		if (!in_array($key, $this->accessCounter))
			$this->accessCounter[$key] = 0;

		$this->accessCounter[$key] += 1;

		return $value;
	}

	public function isMissingOrEmpty($key) {
		$value = $this->items->get($key);

		if (empty($value))
			return true;

		return false;
	}

	public function isNotMissingOrEmpty($key) {
		return !$this->isMissingOrEmpty($key);
	}

	public function getAll() {
		return $this->items;
	}

	public function getAccessCounter() {
		return $this->accessCounter;
	}

	public function getMissingFields() {
		return $this->missingFields;
	}

	public function getAllOf($keyPrefix) {
		return $this->items->filter(function ($value, $key) use ($keyPrefix) {
			return Str::startsWith($key, $keyPrefix);
		});
	}

	public function getArray($keyPrefix) {
		$map = $this->items->filter(function ($value, $key) use ($keyPrefix) {
			return Str::startsWith($key, $keyPrefix . '.');
		});

		$result = [];

		$map->each(function ($value, $key) use (&$result) {
			data_fill($result, $key, $value);
		});

		return Arr::get($result, $keyPrefix, []);
	}

	public function getCollection($keyPrefix) {
		return collect($this->getArray($keyPrefix));
	}

	public function put($key, $value) {
		$this->items[$key] = $value;
	}

	/**
	 * @param array|Collection $items
	 */
	public function merge($items) {
		$this->items = $this->items->merge($items);
	}

	public function makePath($template, $parameters = []) {
		return sprintf($template, ...$parameters);
	}
}
