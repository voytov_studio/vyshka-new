<?php

namespace App\Domain;

use App\Data\SitePhoneDto;

/**
 * @author Cyrill Tekord
 */
class SiteDataProvider {
	/** @var WpPostDataProvider */
	protected $postDataProvider;

	/** @var WordPressPostAggregate */
	public $currentPostAggregate;

	/** @var ContentFieldsStore */
	protected $cfs;

	/** @var SiteMenuDataProvider */
	protected $siteMenuDataProvider;

	/** @var array */
	protected $cachedPhoneEntries = [];

	public function __construct(
		WpPostDataProvider $postDataProvider,
		SiteMenuDataProvider $siteMenuDataProvider,
		ContentFieldsStore $cfs
	) {
		$this->postDataProvider = $postDataProvider;
		$this->siteMenuDataProvider = $siteMenuDataProvider;
		$this->cfs = $cfs;
	}

	public function getCfs() {
		return $this->cfs;
	}

	public function getPhoneEntry($id) {
		if (array_key_exists($id, $this->cachedPhoneEntries)) {
			return $this->cachedPhoneEntries[$id];
		}

		$dto = new SitePhoneDto();

		$dto->value = config("app.settings.phones.$id.value");
		$dto->display = config("app.settings.phones.$id.display");
		$dto->isPresent = in_array(true, [$dto->value, $dto->display]);

		$this->cachedPhoneEntries[$id] = $dto;

		return $dto;
	}

	/**
	 * @return SiteMenuDataProvider
	 */
	public function getMenuDataProvider() {
		return $this->siteMenuDataProvider;
	}
}
