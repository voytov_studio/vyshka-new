<?php

namespace App\Domain;

use App\Models\WordPressPost;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * @author Cyrill Tekord
 */
class WordPressPostAggregate {
	/**
	 * @var WordPressPost
	 */
	public $model;

	/**
	 * @var Collection
	 */
	public $meta;

	public function parseMetaCollection($keyPrefix, $key = null) {
		$collection = collect($this->meta)
			->mapWithKeys(function ($value, $key) {
				return [preg_replace('/\_(\d+)\_/', '.${1}.', $key) => $value];
			});

		$filteredCollection = $collection->filter(function ($metaValue, $metaKey) use ($keyPrefix) {
			return Str::startsWith($metaKey, $keyPrefix);
		});

		$data = [];

		$filteredCollection->each(function ($value, $key) use (&$data) {
			data_fill($data, $key, $value);
		});

		return collect(Arr::get($data, $key ?? $keyPrefix, []));
	}

	public function getMeta($key, $default = null) {
		return $this->meta->get($key, $default);
	}

	public function getLink() {
		$permalink = $this->meta->get('custom_permalink');

		if ($permalink !== null)
			return $permalink;

		return $this->model->post_name;
	}

	public function getViewLink() {
		$link = $this->getLink();

		if (Str::startsWith($link, 'blog/'))
			$link = Str::after($link, 'blog/');

		return route('blog.posts.show', ['slug' => $link]);
	}

	public function getCoverImageUrl() {
		$thumbnailId = $this->meta->get('_thumbnail_id');

		if ($thumbnailId === null)
			return null;

		return wp_get_image_thumbnail_by_post_id($thumbnailId, 'medium_large');
	}

	public function getPostDate() {
		$this->model->post_date_gmt->toDateTimeString();
	}

	public function getCategories() {
		return $this->model->categories;
	}

	public function getExcerpt() {
		return $this->model->post_excerpt;
	}

	public function getContent() {
		return $this->model->post_content;
	}

	public function getPageTitle() {
		return $this->meta->get('title');
	}

	public function getMetaKeywords() {
		return $this->meta->get('keywords');
	}

	public function getMetaDescription() {
		return $this->meta->get('description');
	}

	public function getTitle() {
		return $this->model->post_title;
	}

	public function getSeoTitle() {
		$title = $this->meta->get('seo-h1-title');

		if (empty($title))
			$title = $this->getTitle();

		return $title;
	}

	public function getSubtitle() {
		return $this->meta->get('seo-h1');
	}

	public function getRating() {
		return $this->meta->get('rating');
	}

	public function getVote() {
		return $this->meta->get('vote');
	}
}
