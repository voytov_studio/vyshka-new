<?php

namespace App\Domain;

use App\Models\WordPressUser;
use Illuminate\Contracts\Encryption\Encrypter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * @author Cyrill Tekord
 */
class WordPressAuthService {
	/** @var string */
	public $cookieName = 'wp_logged_in';

	/** @var bool */
	protected $cachedValue;

	/** @var Request */
	protected $request;

	/** @var Encrypter */
	protected $encrypter;

	public function __construct(Request $request, Encrypter $encrypter) {
		$this->request = $request;
		$this->encrypter = $encrypter;
	}

	protected function findIdentityCookie(ParameterBag $bag) {
		foreach ($bag as $key => $value) {
			if ($key == $this->cookieName) {
				return $value;
			}
		}

		return null;
	}

	public function parseCookie($cookie) {
		$data = $this->encrypter->decrypt($cookie);

		$cookieElements = explode('|', $data);

		if (count($cookieElements) !== 2) {
			return false;
		}

		list($user_id, $expiration) = $cookieElements;

		return compact(["user_id", "expiration"]);
	}

	public function removeCookie() {
		Cookie::forget($this->cookieName);
	}

	public function check($useCache = true) {
		if ($useCache && $this->cachedValue !== null)
			return $this->cachedValue;

		$cookie = $this->findIdentityCookie($this->request->cookies);

		if ($cookie === null)
			return false;

		$cookieElements = $this->parseCookie($cookie);

		$userId = $cookieElements['user_id'];
		$expiration = $cookieElements['expiration'];

		if ($expiration < time()) {
			$this->removeCookie();

			if ($useCache)
				$this->cachedValue = false;

			return false;
		}

		$userExists = WordPressUser::query()
			->whereKey($userId)
			->exists();

		if ($useCache)
			$this->cachedValue = $userExists;

		return $userExists;
	}
}
