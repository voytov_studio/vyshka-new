<?php

namespace App\Domain;

use Illuminate\Http\Request;
use Spatie\ResponseCache\CacheProfiles\CacheProfile;
use Spatie\ResponseCache\Hasher\RequestHasher;

/**
 * @author Cyrill Tekord
 */
final class ResponseCacheHasher implements RequestHasher {
	/** @var \Spatie\ResponseCache\CacheProfiles\CacheProfile */
	protected $cacheProfile;

	public function __construct(
		CacheProfile $cacheProfile
	) {
		$this->cacheProfile = $cacheProfile;
	}

	public function getHashFor(Request $request): string {
		$path = parse_url($request->getRequestUri(), PHP_URL_PATH);

		return 'responsecache-' . md5(
				"{$request->getHost()}-{$path}/" . $this->cacheProfile->useCacheNameSuffix($request)
			);
	}
}
