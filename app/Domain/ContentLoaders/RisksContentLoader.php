<?php

namespace App\Domain\ContentLoaders;

use App\Domain\PostAggregate;
use Illuminate\Support\Facades\View;

/**
 * @author Cyrill Tekord
 */
class RisksContentLoader extends BaseContentLoader {
	public function render() {
		$postAggregate = $this->fetchPostAggregate($this->currentPageId);

		if ($postAggregate === null)
			return null;

		return $this->renderWithPost($postAggregate);
	}

	public function renderWithPost(PostAggregate $postAggregate) {
		$risksPostAggregate = $this->fetchPostAggregate(13573);

		return View::make('pages/_risks', [
			'postAggregate' => $postAggregate,
			'risksPostAggregate' => $risksPostAggregate
		]);
	}
}
