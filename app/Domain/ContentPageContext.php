<?php

namespace App\Domain;

/**
 * @author Cyrill Tekord
 */
class ContentPageContext {
	/** @var WordPressPostAggregate */
	protected $currentPostAggregate;

	public function getCurrentPostAggregate() {
		return $this->currentPostAggregate;
	}

	public function setCurrentPostAggregate(? WordPressPostAggregate $currentPostAggregate) {
		$this->currentPostAggregate = $currentPostAggregate;
	}
}
