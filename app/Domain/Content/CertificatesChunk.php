<?php

namespace App\Domain\Content;

use App\Domain\DataPostService;
use App\Facades\DynamicContentSheetFacade;
use Illuminate\Support\Facades\View;

/**
 * @author Cyrill Tekord
 */
class CertificatesChunk extends PostChunk {
	public function render() {
		$dps = app(DataPostService::class);

		$dps->setDefaultDataPost($this->dataPostAggregate);
		$dps->setCurrentDataPost($this->postAggregate);

		DynamicContentSheetFacade::mergeFields($dps->getAllMetas());

		$cfs = DynamicContentSheetFacade::getCfs();

		if (!$this->shouldRender($cfs))
			return null;

		return View::make($this->viewFile ?? 'pages/_certificates', [
			'cfs' => $cfs,
		]);
	}
}
