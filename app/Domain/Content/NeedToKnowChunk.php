<?php

namespace App\Domain\Content;

use App\Domain\DataPostService;
use App\Domain\WpPostDataProvider;
use App\Facades\DynamicContentSheetFacade;
use Illuminate\Support\Facades\View;

/**
 * @author Cyrill Tekord
 */
class NeedToKnowChunk extends PostChunk {
	/** @var array  */
	public $categoryPageMap = [];

	/** @var WpPostDataProvider */
	protected $postDataProvider;

	public function __construct(WpPostDataProvider $postDataProvider) {
		$this->postDataProvider = $postDataProvider;
	}

	public function render() {
		$dps = app(DataPostService::class);

		$dps->loadDefaultDataPost();

		if ($this->postAggregate !== null) {
			$dps->loadCategoryDataPost($this->postAggregate->getMeta('page-category'));
			$dps->setCurrentDataPost($this->postAggregate);
		}

		DynamicContentSheetFacade::mergeFields($dps->getAllMetas());

		$cfs = DynamicContentSheetFacade::getCfs();

		$data = $cfs->getCollection('likbez.0');

		if ($data->isEmpty())
			return null;

		return View::make($this->viewFile ?? 'content-chunks/_need-to-know', [
			'data' => $data,
		]);
	}
}
