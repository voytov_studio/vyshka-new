<?php

namespace App\Domain\Content;

use App\Domain\DataPostService;
use App\Facades\DynamicContentSheetFacade;
use App\Models\WordPressPost;
use Illuminate\Support\Facades\View;

/**
 * @author Cyrill Tekord
 */
class ReviewsChunk extends PostChunk {

	/** @var WordPressPost */
	public $defaultPostAggregate;

	public function render() {
		$dps = app(DataPostService::class);

		$dps->setDefaultDataPost($this->defaultPostAggregate);
		$dps->setCategoryDataPost($this->dataPostAggregate); // TODO: Сделать доп поле в dps
		$dps->setCurrentDataPost($this->postAggregate);

		DynamicContentSheetFacade::mergeFields($dps->getAllMetas());

		$cfs = DynamicContentSheetFacade::getCfs();

		$isBlockEnabled = (bool)$cfs->get('reviews_enabled', false);

		if (!$isBlockEnabled)
			return null;

		return View::make($this->viewFile ?? 'pages/_reviews', [
			'cfs' => $cfs,
		]);
	}
}
