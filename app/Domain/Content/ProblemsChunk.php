<?php

namespace App\Domain\Content;

use App\Domain\DataPostService;
use App\Facades\ContentPageContextFacade;
use App\Facades\DynamicContentSheetFacade;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\View;

/**
 * @author Cyrill Tekord
 */
class ProblemsChunk extends PostChunk {
	public function render() {
		$dps = app(DataPostService::class);

		$dps->loadDefaultDataPost();
		$dps->setCurrentDataPost(ContentPageContextFacade::getCurrentPostAggregate());

		DynamicContentSheetFacade::mergeFields($dps->getAllMetas());

		$cfs = DynamicContentSheetFacade::getCfs();

		$allTabs = $cfs->getCollection('problem-tab');

		if ($allTabs->isEmpty())
			return null;

		/** @var Collection $tabs */
		$tabs = $allTabs->map(function ($item) {
			$nameMap = [
				'problems' => 'Подводные камни',
				'solutions' => 'Решения',
			];

			$rawTitle = Arr::get($item, 'problem-title');

			if (array_key_exists($rawTitle, $nameMap)) {
				$rawTitle = Arr::get($nameMap, $rawTitle, '(Не задано)');
			}
			else {
				$rawTitle = '(Не задано)';
			}

			$data = [
				'title' => $rawTitle,
				'list' => collect($item['problem-list'])->map(function ($item) {
					return (object)[
						'title' => Arr::get($item, 'problem-item'),
						'text' => Arr::get($item, 'problem-text'),
					];
				}),
			];

			return (object)$data;
		});

		$data = $tabs;

		return View::make($this->viewFile ?? 'content-chunks/_problems', [
			'data' => $data,
		])->render();
	}
}
