<?php

namespace App\Domain\Content;

/**
 * @author Cyrill Tekord
 */
abstract class BaseContentChunk {
	/**
	 * @return string|null
	 */
	public function render() {
		return null;
	}
}
