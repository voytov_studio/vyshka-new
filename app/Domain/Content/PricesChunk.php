<?php

namespace App\Domain\Content;

use App\Domain\DataPostService;
use App\Facades\DynamicContentSheetFacade;
use Illuminate\Support\Facades\View;

/**
 * @author Cyrill Tekord
 */
class PricesChunk extends PostChunk {
	public function render() {
		$dps = app(DataPostService::class);

		$dps->setDefaultDataPost($this->dataPostAggregate);
		$dps->setCurrentDataPost($this->postAggregate);

		DynamicContentSheetFacade::mergeFields($dps->getAllMetas());

		$cfs = DynamicContentSheetFacade::getCfs();

		if ($cfs->get('prices', 0) == 0) // !
			return null;

		return View::make($this->viewFile ?? 'pages/_prices', [
			'cfs' => $cfs,
		])->render();
	}
}
