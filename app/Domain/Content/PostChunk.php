<?php

namespace App\Domain\Content;

use App\Domain\DataPostService;
use App\Domain\WordPressPostAggregate;
use App\Facades\DynamicContentSheetFacade;
use Illuminate\Support\Facades\View;

/**
 * TODO: Реализовать загрузку постов произвольного количества.
 *
 * @author Cyrill Tekord
 */
class PostChunk extends BaseContentChunk {
	/** @var WordPressPostAggregate */
	public $postAggregate;

	/** @var WordPressPostAggregate */
	public $dataPostAggregate;

	/** @var string */
	public $viewFile;

	/** @var array */
	public $mergeData = [];

	/** @var boolean|callable(static, ContentFieldsStore) */
	public $enabled = true;

	protected function shouldRender($cfs) {
		if (is_callable($this->enabled)) {
			$shouldRender = call_user_func($this->enabled, $this, $cfs);
		}
		else {
			$shouldRender = (bool)$this->enabled;
		}

		return $shouldRender;
	}

	public function render() {
		$dps = app(DataPostService::class);

		$dps->setDefaultDataPost($this->dataPostAggregate);
		$dps->setCurrentDataPost($this->postAggregate);

		DynamicContentSheetFacade::mergeFields($dps->getAllMetas());

		$cfs = DynamicContentSheetFacade::getCfs();

		if (!$this->shouldRender($cfs))
			return null;

		return View::make($this->viewFile, [
			'postAggregate' => $this->postAggregate,
			'dataPostAggregate' => $this->dataPostAggregate,
			'cfs' => DynamicContentSheetFacade::getCfs(),
		], $this->mergeData)->render();
	}
}
