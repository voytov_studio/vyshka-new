<?php

namespace App\Domain\Content;

use App\Domain\DataPostService;
use App\Facades\DynamicContentSheetFacade;
use Illuminate\Support\Facades\View;

/**
 * @author Cyrill Tekord
 */
class RisksChunk extends PostChunk {
	/** @var array  */
	public $categoryPageMap = [];

	public function render() {
		$dps = app(DataPostService::class);

		$dps->loadDefaultDataPost();

		if ($this->postAggregate !== null) {
			$dps->loadCategoryDataPost($this->postAggregate->getMeta('page-category'));
			$dps->setCurrentDataPost($this->postAggregate);
		}

		DynamicContentSheetFacade::mergeFields($dps->getAllMetas());

		$cfs = DynamicContentSheetFacade::getCfs();

		$data = $cfs->getCollection('likbez.1');

		if ($data->isEmpty())
			return null;

		return View::make($this->viewFile ?? 'pages/_risks', [
			'data' => $data,
		]);
	}
}
