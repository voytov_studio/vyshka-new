<?php

namespace App\Domain\Content;

use App\Domain\DataPostService;
use App\Facades\DynamicContentSheetFacade;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Collection;

/**
 * @author Cyrill Tekord
 */
class FaqChunk extends PostChunk {

	public function render() {
		$dps = app(DataPostService::class);

		$dps->loadDefaultDataPost();

		if ($this->postAggregate !== null) {
			$dps->loadCategoryDataPost($this->postAggregate->getMeta('page-category'));
			$dps->setCurrentDataPost($this->postAggregate);
		}

		DynamicContentSheetFacade::mergeFields($dps->getAllMetas());

		$allTabs = DynamicContentSheetFacade::getCfs()->getCollection('tabs');

		/** @var Collection $faqTabs */
		$faqTabs = $allTabs->reduce(function (Collection $carry, $item) {
			$questionsTab = Arr::get($item, 'tab-questions');

			if (is_array($questionsTab)) {
				$questionsTab = collect($questionsTab)->map(function ($item) {
					return [
						'q' => Arr::get($item, 'tab-question'),
						'a' => Arr::get($item, 'tab-answer'),
					];
				});

				$carry->push(collect([
					'title' => Arr::get($item, 'tab-name'),
					'text' => Arr::get($item, 'tab-text'),
					'items' => $questionsTab
				]));
			}

			return $carry;
		}, collect());

		if ($faqTabs->isEmpty())
			return null;

		$data = $faqTabs->first();

		return View::make($this->viewFile ?? 'content-chunks/_faq', [
			'data' => $data,
		]);
	}
}
