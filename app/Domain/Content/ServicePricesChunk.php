<?php

namespace App\Domain\Content;

use App\Domain\DataPostService;
use App\Domain\WordPressPostAggregate;
use App\Domain\WpPostDataProvider;
use App\Facades\DynamicContentSheetFacade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

/**
 * @author Cyrill Tekord
 */
class ServicePricesChunk extends PostChunk {
	/** @var array  */
	public $categoryPageMap = [];

	/** @var WpPostDataProvider */
	protected $postDataProvider;

	public function __construct(WpPostDataProvider $postDataProvider) {
		$this->postDataProvider = $postDataProvider;
	}

	public function render() {
		if ($this->postAggregate === null)
			return null;

		$internalCategoryId = $this->postAggregate->getMeta('page-category');

		if ($internalCategoryId === null)
			return null;

		$dataPostId = data_get($this->categoryPageMap, $internalCategoryId);

		if ($dataPostId === null)
			return null;

		{
			$dps = app(DataPostService::class);

			$dps->loadDefaultDataPost();

			$dps->loadCategoryDataPost($internalCategoryId);
			$dps->setCurrentDataPost($this->postAggregate);

			DynamicContentSheetFacade::mergeFields($dps->getAllMetas());

			$cfs = DynamicContentSheetFacade::getCfs();
		}

		$dataPostMetaRows = $this->postDataProvider->fetchPostMeta($dataPostId);

		$dataPostAggregate = new WordPressPostAggregate();
		$dataPostAggregate->model = null; // Это костыль из-за нехватки времени
		$dataPostAggregate->meta = collect($dataPostMetaRows)->pluck('meta_value', 'meta_key');

		$data = $dataPostAggregate->meta->filter(function ($value, $key) {
			return Str::startsWith($key, 'servprice_block');
		});

		if ($data->get('servprice_block') == 0)
			return null;

		$dataPostAggregate->meta = $data->mapWithKeys(function ($value, $key) {
			$key = preg_replace('/servprice_block_(\d+)_/', '${1}.', $key);
			$key = preg_replace('/law_subj_(\d+)_(\d+)_/', 'items-${1}.${2}.', $key);
			$key = preg_replace('/law_subj_list_(\d+)_(\d+)_/', 'list.${2}.', $key);
			$key = preg_replace('/law_subj_sublist-(\d+)_(\d+)_/', 'sublist.${2}.', $key);
			$key = preg_replace('/law_subj_incase-(\d+)_(\d+)_/', 'incase.${2}.', $key);
			$key = preg_replace('/law_subj_outcase-(\d+)_(\d+)_/', 'outcase.${2}.', $key);
			$key = preg_replace('/law_subj_sublist_/', '', $key);
			$key = preg_replace('/law_subj_/', '', $key);
			$key = preg_replace('/-(\d+)$/', '', $key);
			$key = preg_replace('/_(\d+)_/', '.${1}.', $key);

			return ['_custom__.' . $key => $value];
		});

		$data = $dataPostAggregate->parseMetaCollection('_custom__.', '_custom__');

		return View::make($this->viewFile ?? 'pages/_service-prices', [
			'cfs' => $cfs,
			'postAggregate' => $this->postAggregate,
			'dataPostAggregate' => $dataPostAggregate,
			'data' => $data,
		]);
	}
}
