<?php

namespace App\Domain\Content;

use App\Domain\DataPostService;
use App\Facades\DynamicContentSheetFacade;
use Illuminate\Support\Facades\View;

/**
 * @author Cyrill Tekord
 */
class WeWorkWithChunk extends PostChunk {

	public function render() {
		$dps = app(DataPostService::class);

		$dps->setDefaultDataPost($this->dataPostAggregate);
		$dps->setCurrentDataPost($this->postAggregate);

		DynamicContentSheetFacade::mergeFields($dps->getAllMetas());

		$cfs = DynamicContentSheetFacade::getCfs();

		if ($cfs->isMissingOrEmpty('weWorkWith_enabled'))
			return null;

		return View::make($this->viewFile, [
			'postAggregate' => $this->postAggregate,
			'dataPostAggregate' => $this->dataPostAggregate,
			'cfs' => $cfs,
		], $this->mergeData)->render();
	}
}
