<?php

namespace App\Domain\Content;

use App\Domain\WpPostDataProvider;
use App\Models\WordPressPost;
use Illuminate\Support\Facades\View;

/**
 * @author Cyrill Tekord
 */
class CasesChunk extends PostChunk {
	/**
	 * @var WpPostDataProvider
	 */
	protected $postDataProvider;

	public function __construct(WpPostDataProvider $postDataProvider) {
		$this->postDataProvider = $postDataProvider;
	}

	public function render() {
		$items = $this->dataPostAggregate->parseMetaCollection('decisions');

		/** @var \Illuminate\Support\Collection $attachments */
		$attachments = $this->dataPostAggregate->model->attachments;

		$attachments = $attachments->mapWithKeys(function(WordPressPost $item) {
			$postAggregate = $this->postDataProvider->makeAggregate($item);

			$isImage = in_array($postAggregate->model->post_mime_type, [
				'image/jpeg',
				'image/pjpeg',
				'image/png',
			]);

			if ($isImage)
				$thumbnail = wp_extract_thumbnail_url_from_post($postAggregate);
			else
				$thumbnail = null;

			return [
				$item->getKey() => [
					'name' => $item->post_title,
					'url' => config('app.settings.base_content_url') . extract_path_from_url($item->guid),
					'thumbnail_url' => $thumbnail,
				]
			];
		});

		return View::make($this->viewFile ?? 'pages/_cases', [
			'items' => $items,
			'attachments' => $attachments,
		])->render();
	}
}
