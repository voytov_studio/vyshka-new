<?php

namespace App\Domain\Content;

use App\Domain\DataPostService;
use App\Facades\ContentPageContextFacade;
use App\Facades\DynamicContentSheetFacade;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Collection;

/**
 * @author Cyrill Tekord
 */
class CaseDevelopmentChunk extends PostChunk {
	public function render() {
		$dps = app(DataPostService::class);

		$dps->loadDefaultDataPost();
		$dps->setCurrentDataPost(ContentPageContextFacade::getCurrentPostAggregate());

		DynamicContentSheetFacade::mergeFields($dps->getAllMetas());

		$cfs = DynamicContentSheetFacade::getCfs();

		$allTabs = $cfs->getCollection('tabs');

		/** @var Collection $tabs */
		$tabs = $allTabs->reduce(function (Collection $carry, $item) {
			$isInterestingTab = Arr::get($item, 'tab-choice');

			if ($isInterestingTab == 1) {
				$carry->push(collect([
					'title' => Arr::get($item, 'tab-name'),
					'text' => Arr::get($item, 'tab-text'),
				]));
			}

			return $carry;
		}, collect());

		if ($tabs->isEmpty())
			return null;

		$data = $tabs;

		return View::make($this->viewFile ?? 'content-chunks/_case-development', [
			'data' => $data,
		]);
	}
}
