<?php

namespace App\Domain\Content;

use App\Domain\DataPostService;
use App\Domain\WordPressPostAggregate;
use App\Facades\ContentPageContextFacade;
use App\Facades\DynamicContentSheetFacade;
use Illuminate\Support\Facades\View;

/**
 * @author Cyrill Tekord
 */
class TermsOfSolution extends PostChunk {
	/** @var WordPressPostAggregate */
	public $defaultPostAggregate;

	public function render() {
		$dps = app(DataPostService::class);

		$dps->loadDefaultDataPost();
		$dps->setCurrentDataPost(ContentPageContextFacade::getCurrentPostAggregate());

		DynamicContentSheetFacade::mergeFields($dps->getAllMetas());

		$cfs = DynamicContentSheetFacade::getCfs();

		if ($cfs->isMissingOrEmpty('termsOfSolution_enabled'))
			return null;

		if ($cfs->isMissingOrEmpty('termsOfSolution_tabs'))
			return null;

		return View::make($this->viewFile ?? 'content-chunks/_terms-of-solution', [
			'postAggregate' => $this->postAggregate,
			'cfs' => $cfs
		], $this->mergeData)->render();
	}
}
