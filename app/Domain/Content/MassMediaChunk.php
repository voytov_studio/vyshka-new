<?php

namespace App\Domain\Content;

use App\Domain\DataPostService;
use App\Domain\WpPostDataProvider;
use App\Facades\DynamicContentSheetFacade;
use App\Models\WordPressPost;
use Illuminate\Support\Facades\View;

/**
 * @author Cyrill Tekord
 */
class MassMediaChunk extends PostChunk {
	/** @var WpPostDataProvider */
	protected $postDataProvider;

	public function __construct(WpPostDataProvider $postDataProvider) {
		$this->postDataProvider = $postDataProvider;
	}

	public function render() {
		$dps = app(DataPostService::class);
		$dps->setCurrentDataPost($this->dataPostAggregate);

		DynamicContentSheetFacade::mergeFields($dps->getAllMetas());

		$cfs = DynamicContentSheetFacade::getCfs();

		if (!$this->shouldRender($cfs))
			return null;

		$items = $cfs->getCollection('massmedia');

		$tvItems = $items->where('publicity_type', '=', 1)
			->map(function ($item) {
				$date = $item["date"];

				if (!empty($date)) {
					// Carbon выбрасывает ошибку Trailing data если в исходном значении есть точка в конце. А она может быть
					// из-за того, что данные в базе не типизированы
					$date = trim($date, ".");

					/** @var \Carbon\Carbon $date */
					$date = \Carbon\Carbon::createFromFormat("d.m.Y", $date);

					if ($date->isValid())
						$item["date"] = $date->format("Y.m.d");
				}

				return $item;
			})
			->sortByDesc('date', SORT_STRING, false);

		$pressItems = $items->where('publicity_type', '=', 3);

		$pressItems = $pressItems
			->take(15);

		$attachments = $this->dataPostAggregate->model->attachments;

		$attachments = $attachments->mapWithKeys(function(WordPressPost $item) {
			$postAggregate = $this->postDataProvider->makeAggregate($item);

			$isImage = in_array($postAggregate->model->post_mime_type, [
				'image/jpeg',
				'image/pjpeg',
				'image/png',
			]);

			if ($isImage)
				$thumbnail = wp_extract_thumbnail_url_from_post($postAggregate);
			else
				$thumbnail = null;

			return [
				$item->getKey() => [
					'name' => $item->post_title,
					'url' => config('app.settings.base_content_url') . extract_path_from_url($item->guid),
					'thumbnail_url' => $thumbnail,
				]
			];
		});

		return View::make($this->viewFile, [
			'cfs' => $cfs,
			'tvItems' => $tvItems,
			'pressItems' => $pressItems,
			'attachments' => $attachments
		], $this->mergeData)->render();
	}
}
