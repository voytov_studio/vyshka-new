<?php

namespace App\Domain\Content;

use App\Domain\DataPostService;
use App\Facades\ContentPageContextFacade;
use App\Facades\DynamicContentSheetFacade;
use Illuminate\Support\Facades\View;

/**
 * @author Cyrill Tekord
 */
class HowWeWorkChunk extends PostChunk {

	public function render() {
		$dps = app(DataPostService::class);

		$dps->loadDefaultDataPost();
		$dps->setCurrentDataPost(ContentPageContextFacade::getCurrentPostAggregate());

		DynamicContentSheetFacade::mergeFields($dps->getAllMetas());

		$cfs = DynamicContentSheetFacade::getCfs();

		if ($cfs->isMissingOrEmpty('howWeWork_enabled'))
			return null;

		return View::make($this->viewFile ?? 'content-chunks._how-it-works', [
			'postAggregate' => $this->postAggregate,
			'dataPostAggregate' => $this->dataPostAggregate,
			'cfs' => $cfs,
		], $this->mergeData)->render();
	}
}
