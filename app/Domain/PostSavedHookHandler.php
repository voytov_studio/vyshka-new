<?php

namespace App\Domain;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Spatie\ResponseCache\ResponseCache;

/**
 * @author Cyrill Tekord
 */
class PostSavedHookHandler {
	/**
	 * @var BlogRepository
	 */
	private $blogRepository;

	public function __construct(BlogRepository $blogRepository) {
		$this->blogRepository = $blogRepository;
	}

	public function handle($data) {
		$url = Arr::get($data, 'url');

		if ($url !== null) {
			$url = Str::after($url, '/wordpress/');

			ResponseCache::forget('/' . $url);
		}

		$this->blogRepository->removeCacheForPost($data->post_id);

		// TODO: В будущем здесь можно запустить посещение страницы кроулером
	}
}
