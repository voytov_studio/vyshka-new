<?php

namespace App\Domain;

use App\Models\WordPressPost;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * @author Cyrill Tekord
 */
class WpPostDataProvider {
	/**
	 * @var callable|null
	 */
	public $metaKeyTransformer;

	/**
	 * @var BlogRepository
	 */
	protected $blogRepository;

	public function __construct(BlogRepository $blogRepository) {
		$this->blogRepository = $blogRepository;
	}

	public function makeAggregate(WordPressPost $post) {
		$postMetaRows = $this->fetchPostMeta($post->getKey());

		$result = new WordPressPostAggregate();
		$result->model = $post;
		$result->meta = $this->extractMetaKeyValueMap($postMetaRows);

		return $result;
	}

	public function fetchPostAndMakeAggregate($postId) {
		$model = $this->queryPost()
			->whereKey($postId)
			->first();

		if ($model === null)
			return null;

		return $this->makeAggregate($model);
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function queryPost() {
		return WordPressPost::query();
	}

	/**
	 * @return \Illuminate\Database\Query\Builder
	 */
	public function queryMetaData() {
		return DB::connection('wordpress')
			->query()
			->from('earth_postmeta')
			->where(function ($query) {
				$query
					->where('meta_key', 'like', DB::raw('"/_wp/_%" ESCAPE "/"'))
					->orwhere('meta_key', 'not like', DB::raw('"/_%" ESCAPE "/"'));
			});
	}

	/**
	 * @param $rows
	 *
	 * @return Collection
	 */
	public function extractMetaKeyValueMap($rows) {
		return collect($rows)->mapWithKeys(function ($row) {
			$key = $this->transformMetaKey($row->meta_key);

			return [
				$key => $row->meta_value
			];
		});
	}

	public function transformMetaKey($key) {
		if (is_callable($this->metaKeyTransformer)) {
			return $this->metaKeyTransformer($key);
		}

		return self::transformUnderscoresToDotsMetaKey($key);
	}

	public static function transformUnderscoresToDotsMetaKey($key) {
		return preg_replace('/\_(\d+)\_/', '.${1}.', $key);
	}

	public function fetchPostMeta($postId) {
		$cacheKey = $this->blogRepository->makeCacheKeyForPostMeta($postId);

		$rows = \Illuminate\Support\Facades\Cache::rememberForever($cacheKey, function () use ($postId) {
			return $this->queryMetaData()
				->where(['post_id' => $postId])
				->get();
		});

		return $rows;
	}

	public function unwireMeta(Collection $metaMap) {
		$data = [];

		$metaMap->each(function ($value, $key) use (&$data) {
			data_fill($data, $key, $value);
		});

		return collect($data);
	}
}
