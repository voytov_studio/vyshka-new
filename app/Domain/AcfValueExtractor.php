<?php

namespace App\Domain;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Str;

/**
 * @author Cyrill Tekord
 */
class AcfValueExtractor {
	/** @var MessageBag */
	protected $errorBag;

	/** @var array */
	protected $filterRules = [];

	/** @var array */
	protected $mappingRules = [];

	/** @var array */
	protected $groupPrefixes = [];

	public function __construct() {
		$this->errorBag = new MessageBag();
	}

	public function getFilterRules() {
		return $this->filterRules;
	}

	public function getMappingRules() {
		return $this->mappingRules;
	}

	public function getGroupPrefixes() {
		return $this->groupPrefixes;
	}

	public function registerFilterRules($rules) {
		$this->filterRules = array_merge($this->filterRules, $rules);
	}

	public function registerMappingRules($rules) {
		$this->mappingRules = array_merge($this->mappingRules, $rules);
	}

	public function setGroupPrefixesFromFilter() {
		foreach ($this->filterRules as $key => $filter) {
			if (Str::contains($key, '*'))
				$groupId = Str::before($key, '*');
			else
				$groupId = $key;

			$this->groupPrefixes[] = $groupId;
		}
	}

	/**
	 * @param Collection $collection
	 *
	 * @return Collection
	 */
	public function extract(Collection $collection) {
		$filterRules = $this->getFilterRules();
		$mappingRules = $this->getMappingRules();

		// Некоторые поля нужно извлекать только при определённых условиях
		$data = $collection->filter(function ($value, $key) use ($filterRules, $collection) {
			$filterCallback = null;

			foreach ($filterRules as $filterRuleKey => $filterRuleCallback) {
				if ($filterRuleKey == $key) {
					$filterCallback = $filterRuleCallback;
				} else if (Str::endsWith($filterRuleKey, '*')) {
					$phrase = Str::before($filterRuleKey, '*');

					if (Str::startsWith($key, $phrase)) {
						$filterCallback = $filterRuleCallback;

						break;
					}
				}
			}

			// Нет особых условий фильтрации
			if ($filterCallback === null)
				return true;

			$result = $filterCallback($collection, $key, $value);

			return $result;
		});

		// Преобразование значений полей
		$result = $data->mapWithKeys(function ($value, $key) use ($mappingRules) {
			$rule = Arr::get($mappingRules, $key);

			if (is_string($rule) || is_callable($rule)) {
				$result = call_user_func($rule, $key, $value);
			} else {
				$this->errorBag->add($key, 'Invalid rule.');

				$result = $value;
			}

			return [$key => $result];
		});

		return $result;
	}
}
