<?php

namespace App\Domain;

/**
 * @author Cyrill Tekord
 */
class WordPressAttachmentExtractor {
	public function extract(WordPressPostAggregate $postAggregate) {
		$isImage = in_array($postAggregate->model->post_mime_type, [
			'image/jpeg',
			'image/pjpeg',
			'image/png',
		]);

		if ($isImage)
			$thumbnail = wp_extract_thumbnail_url_from_post($postAggregate);
		else
			$thumbnail = null;

		return [
			'post_id' => $postAggregate->model->getKey(),
			'name' => $postAggregate->model->post_title,
			'url' => config('app.settings.base_content_url') . extract_path_from_url($postAggregate->model->guid),
			'thumbnail_url' => $thumbnail,
		];
	}
}
