<?php

namespace App\Domain;

use App\Domain\Content\BaseContentChunk;
use App\Domain\Content\CaseDevelopmentChunk;
use App\Domain\Content\CasesChunk;
use App\Domain\Content\CertificatesChunk;
use App\Domain\Content\FaqChunk;
use App\Domain\Content\HowWeWorkChunk;
use App\Domain\Content\MassMediaChunk;
use App\Domain\Content\NeedToKnowChunk;
use App\Domain\Content\PostChunk;
use App\Domain\Content\PricesChunk;
use App\Domain\Content\ProblemsChunk;
use App\Domain\Content\RisksChunk;
use App\Domain\Content\ServicePricesChunk;
use App\Domain\Content\TermsOfSolution;
use App\Domain\Content\ReviewsChunk;
use App\Domain\Content\WeWorkWithChunk;
use App\Facades\ContentPageContextFacade;
use Illuminate\Container\Container;

/**
 * @author Cyrill Tekord
 */
class ContentChunkFactory {
	/** @var Container */
	protected $container;

	/** @var WpPostDataProvider */
	protected $postDataProvider;

	public function __construct(WpPostDataProvider $postDataProvider) {
		$this->container = new Container();
		$this->postDataProvider = $postDataProvider;

		$this->bind();
	}

	protected function bind() {
		$this->container->bind('how_it_works', HowWeWorkChunk::class);

		$this->container->bind('we_work_with', function () {
			$o = new WeWorkWithChunk();
			$o->dataPostAggregate = $this->postDataProvider->fetchPostAndMakeAggregate(config('app.settings.content_sources.default_data_page'));
			$o->postAggregate = ContentPageContextFacade::getCurrentPostAggregate();
			$o->viewFile = 'content-chunks._we-work-with';

			return $o;
		});

		$this->container->bind('terms_of_solution', function () {
			$o = app(TermsOfSolution::class);
			$o->defaultPostAggregate = $this->postDataProvider->fetchPostAndMakeAggregate(config('app.settings.content_sources.default_data_page'));

			return $o;
		});

		$this->container->bind('team', function () {
			$o = new PostChunk();
			$o->dataPostAggregate = $this->postDataProvider->fetchPostAndMakeAggregate(config('app.settings.content_sources.default_data_page'));
			$o->postAggregate = ContentPageContextFacade::getCurrentPostAggregate();
			$o->viewFile = 'pages._team';
			$o->enabled = function (PostChunk $instance, ContentFieldsStore $cfs) {
				return $cfs->isNotMissingOrEmpty('specialists_enabled');
			};

			return $o;
		});

		$this->container->bind('mass_media', function () {
			$o = app(MassMediaChunk::class);
			$o->dataPostAggregate = $this->postDataProvider->fetchPostAndMakeAggregate(config('app.settings.content_sources.mass_media'));
			$o->viewFile = 'chunks/._mass-media';

			return $o;
		});

		$this->container->bind('mass_media_modal', function () {
			$o = $this->make('mass_media');
			$o->viewFile = 'chunks/_mass-media-modal-body';

			return $o;
		});

		$this->container->bind('reviews', function () {
			$o = app(ReviewsChunk::class);
			$o->defaultPostAggregate = $this->postDataProvider->fetchPostAndMakeAggregate(config('app.settings.content_sources.default_data_page'));
			$o->dataPostAggregate = $this->postDataProvider->fetchPostAndMakeAggregate(config('app.settings.content_sources.reviews'));

			return $o;
		});

		$this->container->bind('service_prices', function () {
			$o = app(ServicePricesChunk::class);
			$o->categoryPageMap = config('app.settings.content_sources.service_prices');
			$o->postAggregate = ContentPageContextFacade::getCurrentPostAggregate();

			return $o;
		});

		$this->container->bind('need_to_know', function () {
			$o = app(NeedToKnowChunk::class);
			$o->categoryPageMap = config('app.settings.content_sources.service_prices');

			return $o;
		});

		$this->container->bind('risks', function () {
			$o = app(RisksChunk::class);
			$o->categoryPageMap = config('app.settings.content_sources.service_prices');

			return $o;
		});

		$this->container->bind('case_development', CaseDevelopmentChunk::class);
		$this->container->bind('problems', ProblemsChunk::class);
		$this->container->bind('faq', FaqChunk::class);

		$this->container->bind('cases', function () {
			$o = app(CasesChunk::class);
			$o->dataPostAggregate = $this->postDataProvider->fetchPostAndMakeAggregate(config('app.settings.content_sources.cases'));

			return $o;
		});

		$this->container->bind('certificates', function () {
			$o = app(CertificatesChunk::class);
			$o->dataPostAggregate = $this->postDataProvider->fetchPostAndMakeAggregate(config('app.settings.content_sources.certificates'));
			$o->postAggregate = ContentPageContextFacade::getCurrentPostAggregate();
			$o->enabled = function (PostChunk $instance, ContentFieldsStore $cfs) {
				return $cfs->isNotMissingOrEmpty('certificates_enabled');
			};

			return $o;
		});

		$this->container->bind('company_owner_modal', function () {
			$o = new PostChunk();
			$o->dataPostAggregate = $this->postDataProvider->fetchPostAndMakeAggregate(config('app.settings.content_sources.default_data_page'));
			$o->viewFile = 'content-chunks/_company-owner-info';

			return $o;
		});

		$this->container->bind('consultation', function () {
			$o = new PostChunk();
			$o->dataPostAggregate = $this->postDataProvider->fetchPostAndMakeAggregate(config('app.settings.content_sources.default_data_page'));
			$o->viewFile = 'content-chunks/_consultation';

			$o->enabled = function (PostChunk $instance, ContentFieldsStore $cfs) {
				return $cfs->isNotMissingOrEmpty('consultation_enabled');
			};

			return $o;
		});

		$this->container->bind('prices', function () {
			$o = new PricesChunk();
			$o->dataPostAggregate = $this->postDataProvider->fetchPostAndMakeAggregate('app.settings.content_sources.default_data_page');
			$o->postAggregate = ContentPageContextFacade::getCurrentPostAggregate();

			return $o;
		});

		$chunksToBind = [];

		foreach ($chunksToBind as $id => $class) {
			$this->container->bind($id, $class);
		}
	}

	/**
	 * @param string $id
	 *
	 * @return BaseContentChunk
	 */
	public function make($id) {
		if (!$this->container->has($id))
			return null;

		return $this->container->make($id);
	}
}
