<?php

namespace App\Domain;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * @author Cyrill Tekord
 */
class DynamicContentSheet {

	/** @var callable|null */
	public $shouldMergeFieldCallback;

	/** @var ContentFieldsStore */
	protected $cfs;

	/** @var AcfValueExtractor */
	protected $acfValueExtractor;

	/** @var Collection */
	protected $mergedGroups;

	/** @var Collection */
	protected $mergedFields;

	public function __construct(ContentFieldsStore $cfs, AcfValueExtractor $acfValueExtractor) {
		$this->acfValueExtractor = $acfValueExtractor;
		$this->cfs = $cfs;
		$this->mergedGroups = collect();
		$this->mergedFields = collect();
	}

	public function getCfs(): ContentFieldsStore {
		return $this->cfs;
	}

	public function getAcfValueExtractor(): AcfValueExtractor {
		return $this->acfValueExtractor;
	}

	public function getMergedFields(): Collection {
		return $this->mergedFields;
	}

	public function shouldMergeField($key, $value) {
		if ($this->shouldMergeFieldCallback !== null)
			return call_user_func($this->shouldMergeFieldCallback, $this, $key, $value);

		return true;
	}

	public function mergeFields($inputCollections, $reverseOrder = true) {
		if (!is_array($inputCollections))
			$collections = [$inputCollections];
		else
			$collections = $inputCollections;

		if ($reverseOrder)
			$collections = array_reverse($collections);

		foreach ($collections as $c) {
			$data = $this->acfValueExtractor->extract($c);

			$groupPrefixes = $this->acfValueExtractor->getGroupPrefixes();
			$groupedFields = [];
			$fieldsBelongingToGroups = [];

			foreach ($groupPrefixes as $groupPrefix) {
				foreach ($data as $key => $value) {
					$isPartOfGroup = Str::startsWith($key, $groupPrefix);

					if (!$isPartOfGroup)
						continue;

					$fieldsBelongingToGroups[] = $key;
					$groupedFields[$groupPrefix][$key] = $value;
				}
			}

			$groupedFields = collect($groupedFields);

			$groupKeys = $groupedFields->keys();

			// Получаем идентификаторы групп полей, которые будем мерджить
			$groupsToMerge = $groupKeys->diff($this->mergedGroups);

			// Проходим по интересующим нас группам и извлекаем поля
			foreach ($groupsToMerge as $gId) {
				$fields = $groupedFields->get($gId);

				if ($fields === null)
					continue;

				$this->cfs->merge($fields);
			}

			// Запоминаем группы, которые смерджили чтобы не мерджить их в будущем
			$groupKeys->each(function ($key) {
				$this->mergedGroups->push($key);
			});

			// Теперь мерджим все оставшиеся поля, которые не относятся к группам
			$fieldsToMerge = $data->filter(function ($value, $key) use ($fieldsBelongingToGroups) {
				return !in_array($key, $fieldsBelongingToGroups)
					&& $this->shouldMergeField($key, $value);
			});

			$this->cfs->merge($fieldsToMerge);

			$this->mergedFields = $this->mergedFields->merge($fieldsToMerge->keys());
		}
	}
}
