<?php

namespace App\Domain;

use App\Data\SpecializationMenuItemDto;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

/**
 * @author Cyrill Tekord
 */
class SiteMenuDataProvider {

	protected function extractLinks($array) {
		$extractLink = function ($item) {
			return [
				'link' => $item['link'],
				'title' => $item['name']
			];
		};

		$result = collect();

		collect($array)
			->each(function ($item) use (&$result, $extractLink) {
				if (isset($item['items'])) {
					foreach ($item['items'] as $nestedItem) {
						$result->push($extractLink($nestedItem));
					}
				}
				else {
					$result->push($extractLink($item));
				}
			});

		return $result;
	}

	public function getSpecializationMenuSourceData() {
		return app('data.menu.specializations');
	}

	public function getServicesMenuSourceData() {
		return app('data.menu.services');
	}

	public function getAllSpecializationLinksAsFlatList() {
		$groups = $this->getSpecializationMenuSourceData();

		$result = [];

		foreach ($groups as $g) {
			$groupItems = $g['items'];
			$allItems = array_merge($groupItems['popular'], $groupItems['free'], $groupItems['specialists_1'], $groupItems['specialists_2']);

			$allLinks = $this->extractLinks($allItems);

			$result[] = [
				'name' => $g['name'],
				'links' => $allLinks,
			];
		}

		return $result;
	}

	public function getAllServicesLinksAsFlatList() {
		$groups = $this->getServicesMenuSourceData();

		$result = [];

		foreach ($groups as $g) {
			$groupItems = $g['items'];
			$allItems = $groupItems;

			$allLinks = $this->extractLinks($allItems);

			$result[] = [
				'name' => $g['name'],
				'links' => $allLinks,
			];
		}

		return $result;
	}

	public function getSpecializationsSummary() {
		$allLinks = $this->getAllSpecializationLinksAsFlatList();

		$result = collect($allLinks)->map(function ($item) {
			$counter = $item['links']->count();

			return array_merge($item, [
				'id' => $item['name'],
				'name' => $item['name'],
				'counter' => $counter
			]);
		});

		return $result->toArray();
	}

	/**
	 * @return SpecializationMenuItemDto[]|Collection
	 */
	public function getSpecializationMenuItems() {
		$items = $this->getSpecializationsSummary();
		$iconMap = app('data.specialization_icons');

		$result = collect($items)->map(function ($item) use ($iconMap) {
			$dto = new SpecializationMenuItemDto();

			// Resource
			$dto->id = $item['name'];
			$dto->name = $item['name'];
			$dto->url = route('site.showPageLinksBySpecialization', ['id' => $dto->id]);

			// Presentation
			$dto->elementId = uniqid('specialization-summary-');
			$dto->icon = Arr::get($iconMap, $dto->id);
			$dto->counter = $item['counter'];

			return $dto;
		});

		return $result;
	}

	public function getServicesMenuItems() {
		$menuItems = app('data.menu.services');

		return $menuItems;
	}
}
