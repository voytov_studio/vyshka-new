<?php

namespace App\Domain;

/**
 * @author Cyrill Tekord
 */
class ContentFieldKeyHelper {
	/** @var string */
	public $baseKey;

	public function __invoke($keyPart = null) {
		return $this->baseKey . $keyPart;
	}
}
