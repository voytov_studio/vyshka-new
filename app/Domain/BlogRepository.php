<?php

namespace App\Domain;

use App\Models\WordPressPost;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

/**
 * @author Cyrill Tekord
 */
class BlogRepository {
	/**
	 * @var
	 */
	public $blogRootCategoryId;

	/**
	 * @var
	 */
	public $cachePrefix;

	/**
	 * @var string
	 */
	public $connection = 'wordpress';

	/**
	 * @return \Illuminate\Database\Query\Builder
	 */
	protected function newQuery() {
		return DB::connection($this->connection)
			->query();
	}

	public function fetchAllCategories() {
		$wpCategories = $this->fetchWpCategories();

		$result = $wpCategories->map(function ($item) {
			return [
				'id' => $item->term_id,
				'title' => $item->name,
				'link' => route('blog.indexByCategory', ['slug' => $item->slug])
			];
		});

		return $result;
	}

	protected function fetchWpCategories() {
		if ($this->blogRootCategoryId === null)
			throw new \Exception('Blog Category ID is missing.');

		return DB::connection($this->connection)
			->query()
			->from('earth_terms')
			->leftJoin('earth_term_taxonomy', 'earth_term_taxonomy.term_id', '=', 'earth_terms.term_id')
			->where('earth_term_taxonomy.parent', '=', $this->blogRootCategoryId)
			->where('earth_term_taxonomy.taxonomy', '=', 'category')
			->get();
	}

	public function findPostIdBySlug($slug, $type = 'post') {
		if ($type == 'post')
			$slug = 'blog/' . $slug;

		$postId = DB::connection($this->connection)
			->query()
			->from('earth_posts')
			->select(['ID'])
			->where([
				'post_type' => $type,
				'post_name' => $slug
			])
			->value('ID');

		if ($postId !== null)
			return $postId;

		$customPermalink = DB::connection($this->connection)
			->table('earth_postmeta')
			->where([
				'meta_key' => 'custom_permalink',
				'meta_value' => $slug
			])
			->first();

		return data_get($customPermalink, 'post_id');
	}

	/**
	 * @param $categoryList
	 *
	 * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder
	 */
	public function queryPostsInCategories($categoryList) {
		return WordPressPost::on($this->connection)
			->from('earth_posts')
			->leftJoin('earth_term_relationships', 'earth_posts.ID', '=', 'earth_term_relationships.object_id')
			->leftJoin('earth_term_taxonomy', 'earth_term_relationships.term_taxonomy_id', '=', 'earth_term_taxonomy.term_taxonomy_id')
			->where([
				'earth_posts.post_type' => 'post',
				'earth_term_taxonomy.taxonomy' => 'category'
			])
			->whereIn('earth_term_taxonomy.term_id', $categoryList);
	}

	public function makeCacheKeyForPostMeta($postId) {
		return $this->cachePrefix . '.' . $postId . '.meta';
	}

	public function fetchPostMetaAsKeyValueMap($postId) {
		return Cache::rememberForever($this->makeCacheKeyForPostMeta($postId), function () use ($postId) {
			return $this->newQuery()
				->from('earth_postmeta')
				->select(['meta_key', 'meta_value'])
				->where(['post_id' => $postId])
				->get()
				->pluck('meta_value', 'meta_key');
		});
	}

	public function fetchPostMetaAsKeyValueMapBatch($postIdList) {
		$existingCacheEntries = [];
		$nonCachedPostIds = [];

		foreach ($postIdList as $postId) {
			$cacheEntry = Cache::get($this->makeCacheKeyForPostMeta($postId));

			if ($cacheEntry === null) {
				$nonCachedPostIds[] = $postId;
			}
			else {
				$existingCacheEntries[$postId] = $cacheEntry;
			}
		}

		if (!empty($nonCachedPostIds)) {
			$queryResult = $this->newQuery()
				->from('earth_postmeta')
				->select(['post_id', 'meta_key', 'meta_value'])
				->whereIn('post_id', $nonCachedPostIds)
				->get();

			$parsedQueryResult = $queryResult->groupBy('post_id')
				->map(function($items) {
					return $items->pluck('meta_value', 'meta_key');
				});

			foreach ($parsedQueryResult as $postId => $data) {
				$existingCacheEntries[$postId] = $data;

				Cache::set($this->makeCacheKeyForPostMeta($postId), $data);
			}
		}

		return collect($existingCacheEntries);
	}

	public function removeCacheForPost($postId) {
		Cache::forget($this->makeCacheKeyForPostMeta($postId));
	}
}
