<?php

namespace App\Domain\Redirects;

use Illuminate\Support\Facades\Log;
use Spatie\MissingPageRedirector\Redirector\Redirector;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author Cyrill Tekord
 */
class MissingPageRedirector implements Redirector {
	public function getRedirectsFor(Request $request): array {
		$sourceFilePath = base_path('data/redirects.txt');

		if (!file_exists($sourceFilePath)) {
			Log::error("Не найден файл редиректов", [
				'path' => $sourceFilePath,
			]);

			return [];
		}

		$source = file_get_contents($sourceFilePath);

		$parser = new RedirectFileParser();

		$result = $parser->parse($source);

		return $result;
	}
}
