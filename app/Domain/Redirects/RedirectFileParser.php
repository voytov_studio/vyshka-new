<?php

namespace App\Domain\Redirects;

use Illuminate\Support\Str;

/**
 * @author Cyrill Tekord
 */
final class RedirectFileParser {

	/** @var callable */
	public $invalidDirectiveCallback;

	public function parse(string $input) {
		$rows = preg_split('/\n/', $input);

		$routeMap = [];

		foreach ($rows as $row) {
			if (empty($row) || Str::startsWith($row, "#"))
				continue;

			if (!Str::startsWith($row, "/")) {
				if (is_callable($this->invalidDirectiveCallback))
					call_user_func($this->invalidDirectiveCallback, $row);

				continue;
			}

			$parts = preg_split('/\s+/', $row);

			if (count($parts) != 2) {
				if (is_callable($this->invalidDirectiveCallback))
					call_user_func($this->invalidDirectiveCallback, $row);

				continue;
			};

			$routeMap[$parts[0]] = $parts[1];
		}

		return $routeMap;
	}
}
