<?php

namespace App\Domain;

use Illuminate\Http\Request;
use Spatie\ResponseCache\CacheProfiles\CacheAllSuccessfulGetRequests;

/**
 * @author Cyrill Tekord
 */
class ResponseCacheProfile extends CacheAllSuccessfulGetRequests {

	/** @var WordPressAuthService */
	protected $wpAuthService;

	public function __construct(WordPressAuthService $wpAuthService) {
		$this->wpAuthService = $wpAuthService;
	}

	public function enabled(Request $request): bool {
		// Не кешируем ответ если пользователь залогинен
		if ($this->wpAuthService->check())
			return false;

		return parent::enabled($request);
	}
}
