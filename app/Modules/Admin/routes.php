<?php

Route::group([
	'prefix' => 'admin',
], function () {
	Route::redirect('/', '/admin/dashboard');

	Route::get('dashboard', 'DashboardController@index')
		->name('admin.dashboard');

	Route::get('analytics', 'AnalyticsController@index')
		->name('admin.analytics.index');

	Route::get('reports', 'ReportController@index')
		->name('admin.reports.index');

	//
});
