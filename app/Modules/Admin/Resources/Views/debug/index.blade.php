<?php

/**
 * @var array $data
 */

?>
@extends('admin::layouts.main')

@section('title', 'Debug')

@section('content')
    <main>
        <h1>
            @yield('title')
        </h1>

        @component('admin::_components.card')
            @slot('title')
                <h2>
                    Debug
                </h2>
            @endslot
            @slot('content')
                Write some code here...
            @endslot
        @endcomponent
    </main>
@endsection
