@extends('admin::layouts.main')

@section('content')
    <main>
        <h1>
            Dashboard
        </h1>

        <div class="row">
            <div class="col-xl-4 col-sm-6 mb-3">
                First column
            </div>

            <div class="col-xl-4 col-sm-6 mb-3">
                Second column
            </div>

            <div class="col-xl-4 col-sm-6 mb-3">
                Third column
            </div>
        </div>
    </main>
@endsection
