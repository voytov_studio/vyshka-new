<div class="card">
    <div class="card-body">
        {{ $title }}

        {{ $content }}
    </div>
</div>
