<?php

/**
 * @var array $data
 */

?>
@extends('admin::layouts.main')

@section('title', 'Reports')

@section('content')
    <main>
        @component('admin::_components.card')
            @slot('title')
                <h1 class="c-page-title">
                    @yield('title')
                </h1>
            @endslot
            @slot('content')
                TO BE DONE
            @endslot
        @endcomponent
    </main>
@endsection
