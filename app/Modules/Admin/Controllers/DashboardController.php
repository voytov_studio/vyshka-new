<?php

namespace App\Modules\Admin\Controllers;

/**
 * @author Cyrill Tekord
 */
class DashboardController extends Controller {
	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index() {
		return view('admin::dashboard.index', []);
	}
}
