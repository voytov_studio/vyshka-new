<?php

namespace App\Modules\Admin\Controllers;

use Illuminate\Http\Request;

/**
 * @author Cyrill Tekord
 */
class ReportController extends Controller {
	/**
	 * @return mixed
	 */
	public function index(Request $request) {
		$data = [];

		return view('admin::reports.index', [
			'data' => $data
		]);
	}
}
