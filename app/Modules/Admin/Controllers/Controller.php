<?php

namespace App\Modules\Admin\Controllers;

use App\Http\Controllers\Controller as BaseController;

/**
 * @author Cyrill Tekord
 */
abstract class Controller extends BaseController {
}
