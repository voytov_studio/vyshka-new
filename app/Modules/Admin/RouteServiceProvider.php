<?php

namespace App\Modules\Admin;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider {
	/**
	 * The controller namespace for the module.
	 *
	 * @var string|null
	 */
	protected $namespace = 'App\Modules\Admin\Controllers';

	/**
	 * Define your module's route model bindings, pattern filters, etc.
	 *
	 * @return void
	 */
	public function boot() {
		parent::boot();

		//
	}

	/**
	 * Define the routes for the module.
	 *
	 * @return void
	 */
	public function map() {
		Route::middleware('web')
			->namespace($this->namespace)
			->group(__DIR__ . '/routes.php');
	}
}
