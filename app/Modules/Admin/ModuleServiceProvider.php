<?php

namespace App\Modules\Admin;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

/**
 * @author Cyrill Tekord
 */
class ModuleServiceProvider extends ServiceProvider {
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot() {
		//
	}

	/**
	 * Register any application services.
	 */
	public function register() {
		$this->app->register(RouteServiceProvider::class);

		View::addNamespace('admin', base_path('resources/views/vendor/admin'));
		View::addNamespace('admin', realpath(__DIR__ . '/Resources/Views'));

		//
	}
}
