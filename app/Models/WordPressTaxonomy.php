<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * @property-read WordPressTerm $term
 * @property-read WordPressTaxonomy $parent
 * @property-read WordPressPost[]|Collection $posts
 *
 * @author Cyrill Tekord
 */
class WordPressTaxonomy extends Model {
	/**
	 * @var string
	 */
	protected $connection = 'wordpress';

	/**
	 * @var string
	 */
	protected $table = 'earth_term_taxonomy';

	/**
	 * @var string
	 */
	protected $primaryKey = 'term_taxonomy_id';

	/**
	 * @var array
	 */
	protected $with = ['term'];

	/**
	 * @var bool
	 */
	public $timestamps = false;

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function term() {
		return $this->belongsTo(WordPressTerm::class, 'term_id');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function parent() {
		return $this->belongsTo(WordPressTaxonomy::class, 'parent');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function posts() {
		return $this->belongsToMany(
			WordPressPost::class, 'earth_term_relationships', 'term_taxonomy_id', 'object_id'
		);
	}
}
