<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * @property-read Post[]|Collection $posts
 *
 * @author Cyrill Tekord
 */
class Category extends Model {

	protected $table = 'categories';

	public function posts() {
		return $this->hasManyThrough(Post::class, PostToCategoryLink::class,
			'category_id', 'post_id', 'id', 'post_id');
	}
}
