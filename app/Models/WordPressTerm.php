<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @author Cyrill Tekord
 */
class WordPressTerm extends Model {
	/**
	 * @var string
	 */
	protected $connection = 'wordpress';

	/**
	 * @var string
	 */
	protected $table = 'earth_terms';

	/**
	 * @var string
	 */
	protected $primaryKey = 'term_id';

	/**
	 * @var bool
	 */
	public $timestamps = false;

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasOne
	 */
	public function taxonomy() {
		return $this->hasOne(WordPressTaxonomy::class, 'term_id');
	}
}
