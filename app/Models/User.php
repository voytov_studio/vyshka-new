<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Ramsey\Uuid\Uuid;

/**
 * @property int $id
 * @property string $uuid
 * @property string $email
 * @property string $password
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $status
 * @property \Carbon\Carbon $confirmed_at
 * @property \Carbon\Carbon $last_login_at
 *
 * @mixin \Eloquent
 *
 * @author Cyrill Tekord
 */
class User extends Authenticatable {

	use Notifiable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password'
	];

	/**
	 * @inheritDoc
	 */
	protected static function boot() {
		parent::boot();

		static::creating(function (User $model) {
			$model->uuid = Uuid::uuid4();
		});
	}
}
