<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @author Cyrill Tekord
 */
class WordPressUser extends Model {
	/**
	 * @var string
	 */
	protected $connection = 'wordpress';

	/**
	 * @var string
	 */
	protected $table = 'earth_users';

	/**
	 * @var string
	 */
	protected $primaryKey = 'ID';

	/**
	 * @var bool
	 */
	public $timestamps = false;
}
