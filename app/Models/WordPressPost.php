<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * @property int $ID
 * @property int $post_author
 * @property \Illuminate\Support\Carbon $post_date
 * @property \Illuminate\Support\Carbon $post_date_gmt
 * @property string $post_content
 * @property string $post_title
 * @property string $post_excerpt
 * @property string $post_status
 * @property string $comment_status
 * @property string $ping_status
 * @property string $post_password
 * @property string $post_name
 * @property string $to_ping
 * @property string $pinged
 * @property \Illuminate\Support\Carbon $post_modified
 * @property \Illuminate\Support\Carbon $post_modified_gmt
 * @property string $post_content_filtered
 * @property int $post_parent
 * @property string $guid
 * @property int $menu_order
 * @property string $post_type
 * @property string $post_mime_type
 * @property int $comment_count
 *
 * @property Collection $meta
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WordPressPost[] $attachments
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WordPressTaxonomy[] $taxonomies
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WordPressPost newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WordPressPost newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WordPressPost query()
 *
 * @mixin \Eloquent
 *
 * @author Cyrill Tekord
 */
class WordPressPost extends Model {

	const CREATED_AT = 'post_date';
	const UPDATED_AT = 'post_modified';

	/**
	 * @var string
	 */
	protected $connection = 'wordpress';

	/**
	 * @var string
	 */
	protected $table = 'earth_posts';

	/**
	 * @var string
	 */
	protected $primaryKey = 'ID';

	/**
	 * @var array
	 */
	protected $dates = ['post_date', 'post_date_gmt', 'post_modified', 'post_modified_gmt'];

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function parent() {
		return $this->belongsTo(WordPressPost::class, 'post_parent');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function children() {
		return $this->hasMany(WordPressPost::class, 'post_parent');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function attachments() {
		return $this->hasMany(WordPressPost::class, 'post_parent', 'ID')
			->where('post_type', 'attachment');
	}

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function taxonomies() {
		return $this->belongsToMany(
			WordPressTaxonomy::class, 'earth_term_relationships', 'object_id', 'term_taxonomy_id'
		);
	}

	/**
	 * @param $data
	 */
	public function mixMeta($data) {
		$this->meta = $data;
	}
}
