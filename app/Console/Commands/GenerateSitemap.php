<?php

namespace App\Console\Commands;

use App\Models\WordPressPost;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\Tags\Url;
use Symfony\Component\Console\Helper\ProgressBar;

/**
 * @author Cyrill Tekord
 */
class GenerateSitemap extends Command {
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $signature = 'sitemap:generate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate the sitemap.';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$sitemap = SitemapGenerator::create(config('app.url'))
			->getSitemap();

		// Pages
		{
			$query = WordPressPost::query()
				->whereIn('post_status', ['publish'])
				->where(['post_type' => 'page']);

			$totalCount = $query->count();

			$progress = new ProgressBar($this->output, $totalCount);

			$query = $query
				->chunk(100, function ($results) use (&$sitemap, $progress) {
					/** @var Collection|WordPressPost[] $results */

					$permalinks = DB::connection('wordpress')
						->table('earth_postmeta')
						->select()
						->whereIn('post_id', $results->pluck('ID'))
						->where(['meta_key' => 'custom_permalink'])
						->pluck('meta_value', 'post_id');

					foreach ($results as $i) {
						$currentPostId = $i->ID;
						$permalink = Arr::get($permalinks, $currentPostId);

						if ($permalink === null) {
							$this->output->writeln("Страница #" . $currentPostId . " не имеет постоянной ссылки");

							continue;
						}

						$sitemap->add(Url::create(route('pages.show', ['slug' => $permalink]))
							->setLastModificationDate($i->post_date)
							->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)
							->setPriority(0.7));

						$progress->advance();
					}
				});

			$progress->finish();
		}

		// Blog
		{
			$slugUrlPrefix = 'blog/';

			$query = WordPressPost::query()
				->whereIn('post_status', ['publish'])
				->where(['post_type' => 'post']);

			$totalCount = $query->count();

			$progress = new ProgressBar($this->output, $totalCount);

			$query = $query
				->chunk(100, function ($results) use (&$sitemap, $progress, $slugUrlPrefix) {
					/** @var Collection|WordPressPost[] $results */

					$permalinks = DB::connection('wordpress')
						->table('earth_postmeta')
						->select()
						->whereIn('post_id', $results->pluck('ID'))
						->where(['meta_key' => 'custom_permalink'])
						->pluck('meta_value', 'post_id');

					foreach ($results as $i) {
						$currentPostId = $i->ID;
						$permalink = Arr::get($permalinks, $currentPostId);

						if ($permalink === null) {
							$this->output->writeln("Пост #" . $currentPostId . " не имеет постоянной ссылки");

							continue;
						}

						$isBlogPost = Str::startsWith($permalink, $slugUrlPrefix);

						if (!$isBlogPost) {
							$this->output->writeln("Пост #" . $currentPostId . " не относится к блогу");

							continue;
						}

						$permalink = Str::after($permalink, $slugUrlPrefix);

						$sitemap->add(Url::create(route('blog.posts.show', ['slug' => $permalink]))
							->setLastModificationDate($i->post_date)
							->setChangeFrequency(Url::CHANGE_FREQUENCY_MONTHLY)
							->setPriority(0.7));

						$progress->advance();
					}
				});

			$progress->finish();
		}

		$sitemap
			->writeToFile(public_path('sitemap.xml'));
	}
}
