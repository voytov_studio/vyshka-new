<?php

namespace App\Console\Commands;

use App\Domain\Redirects\RedirectFileParser;
use Illuminate\Console\Command;

/**
 * @author Cyrill Tekord
 */
class InspectRedirects extends Command {
	protected $signature = 'app:inspect-redirects';

	public function __construct() {
		parent::__construct();
	}

	public function handle() {
		$sourceFilePath = base_path('data/redirects.txt');

		if (!file_exists($sourceFilePath)) {
			echo "Не найден файл редиректов";

			exit(1);
		}

		$source = file_get_contents($sourceFilePath);

		$parser = new RedirectFileParser();
		$parser->invalidDirectiveCallback = function ($row) {
			$this->error("Невалидный маршрут: " . $row);
		};

		$result = $parser->parse($source);

		$this->info("Успешно считано маршутров: " . count($result));
	}
}
