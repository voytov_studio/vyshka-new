<?php

namespace App\Data;

/**
 * @author Cyrill Tekord
 */
class SpecializationMenuItemDto {
	// Resource
	public $id;
	public $url;
	public $name;

	// Presentation
	public $elementId;
	public $icon;
	public $counter;
}
