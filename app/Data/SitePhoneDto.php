<?php

namespace App\Data;

/**
 * @author Cyrill Tekord
 */
class SitePhoneDto {
	/** @var string */
	public $value;

	/** @var string */
	public $display;

	/** @var boolean */
	public $isPresent;
}
