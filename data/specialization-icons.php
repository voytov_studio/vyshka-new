<?php

return [
	'Арбитраж' => 'specialization-icon--list-01',
	'Жилищное право' => 'specialization-icon--list-02',
	'Миграционное право' => 'specialization-icon--list-03',
	'Строительство' => 'specialization-icon--list-04',
	'Авторское право' => 'specialization-icon--list-05',
	'Земельное право' => 'specialization-icon--list-06',
	'Наследство' => 'specialization-icon--list-07',
	'Семейное право' => 'specialization-icon--list-08',
	'Автомобильное право' => 'specialization-icon--list-09',
	'Лицензирование' => 'specialization-icon--list-10',
	'Недвижимость' => 'specialization-icon--list-11',
	'Трудовое право' => 'specialization-icon--list-12',
	'Взыскание' => 'specialization-icon--list-13',
	'Межевание' => 'specialization-icon--list-14',
	'Проблемы с кредитами' => 'specialization-icon--list-15',
	'Уголовное право' => 'specialization-icon--list-16',
	'ДДУ и ЖСК' => 'specialization-icon--list-17',
	'Медицинское право' => 'specialization-icon--list-18',
	'Пенсионное право' => 'specialization-icon--list-19',
];
