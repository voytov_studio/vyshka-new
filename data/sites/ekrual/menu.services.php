<?php

return [
	[
		'name' => 'Консультация',
		'items' => [
			[
				'name' => 'Бесплатная юридическая консультация',
				'link' => url('besplatnye-yuridicheskie-konsultacii')
			],
			[
				'name' => 'Юридическая консультация СПб',
				'link' => url('yuridicheskaya-konsultaciya-spb')
			],
			[
				'name' => 'Бесплатный юрист онлайн',
				'link' => url('besplatnyj-yurist-onlajn')
			],
			[
				'name' => 'Задать вопрос юристу онлайн',
				'link' => url('zadat-vopros-yuristu')
			],
			[
				'name' => 'Семейные консультации юриста',
				'link' => url('semejnye-konsultacii-yurista-spb')
			],
			[
				'name' => 'Консультация юриста',
				'link' => url('advokat-konsultaciya-spb-online')
			],
		],
	],
	[
		'name' => 'Суд',
		'items' => [
			[
				'name' => 'Услуги в суде',
				'link' => url('uslugi-sud-pomoshch-yurista-spb')
			],
			[
				'name' => 'Составление судебных документов',
				'link' => url('sostavlenie-sudebnyh-dokumentov')
			],
			[
				'name' => 'Обеспечение иска',
				'link' => url('obespechenie-iska-sudom-pomoshch-yurista')
			],
			[
				'name' => 'Вопросы по исполнительному листу',
				'link' => url('voprosy-po-ispolnitelnomu-listu-pomoshch-yurista')
			],
			[
				'name' => 'Проведение экспертиз',
				'link' => url('provedenie-ehkspertiz-pomoshch-yurista-spb')
			],
			[
				'name' => 'Представительство в судах всех районов СПб и ЛО',
				'items' => [
					[
						'name' => 'Представительство в судах всех районов СПб и ЛО',
						'link' => url('predstavitelstvo-rajonnye-sudy-spb-lo-yurist-advokat')
					],
					[
						'name' => 'Всеволожский городской суд',
						'link' => url('sud/vsevolozhskij-rajonnyj-sud-leningradskoj-oblasti-spb')
					],
					[
						'name' => 'Приморский районный суд СПб',
						'link' => url('sud/primorskij-rajonnyj-sud-sankt-peterburg')
					],
					[
						'name' => 'Петроградский районный суд СПб',
						'link' => url('sud/petrogradskij-rajonnyj-sud-sankt-peterburg')
					],
				],
			],
			[
				'name' => 'Защита в апелляционной инстанции',
				'items' => [
					[
						'name' => 'Апелляция',
						'link' => url('apellyaciya-zhaloba-v-arbitrazhnyj-sud')
					],
					[
						'name' => 'Обжалование в суде',
						'link' => url('obzhalovanie-v-sude-pomoshch-yurista-apellyaciya-kassaciya-nadzornaya-zhaloba')
					],
					[
						'name' => 'Неустойка в апелляции',
						'link' => url('neustojka-apellyaciya-konsultacii-besplatno-spb-moskva')
					],
				],
			],
			[
				'name' => 'Работа в кассационной инстанции',
				'items' => [
					[
						'name' => 'Оспаривание отказов госорганов',
						'link' => url('osporit-otkaz-reshenie-suda-nalogovoj-gosorganov')
					],
					[
						'name' => 'Представительство в арбитражном суде',
						'link' => url('predstavitelstvo-arbitrazhnom-processe-sude')
					],
					[
						'name' => 'Суд с банком по кредиту',
						'link' => url('sud-s-bankom-po-kreditu-ipoteke-yuristy-banki-spb-moskva')
					],
					[
						'name' => 'Суд по членский взносам',
						'link' => url('sud-chlenskim-vznosam-snt-vzyskanie-uplata')
					],
				],
			],
			[
				'name' => 'Семейные дела в суде',
				'link' => url('semejnye-dela-v-sude-pomoshch-yurista')
			],
		],
	],
	[
		'name' => 'Юристы',
		'items' => [
			[
				'name' => 'Юристы "ЭКРУАЛ"',
				'link' => url('besplatnaya-konsultaciya-yurista-kruglosutochno')
			],
			[
				'name' => 'Юрист СПб',
				'link' => url('yurist-sankt-peterburg-spb')
			],
			[
				'name' => 'Районный юрист',
				'link' => url('rajonnyj-yurist-spb')
			],
			[
				'name' => 'Районные юристы',
				'items' => [
					[
						'name' => 'Юрист во Всеволожском районе',
						'link' => url('yurist-vsevolozhskij-rajon-konsultaciya-besplatno')
					],
				],
			],
			[
				'name' => 'Автомобильный юрист',
				'link' => url('avtomobilnyj-yurist-konsultaciya-besplatno')
			],
			[
				'name' => 'Финансовый юрист',
				'items' => [
					[
						'name' => 'Банковский юрист',
						'link' => url('yurist-bankovskim-delam-voprosam-konsultaciya')
					],
					[
						'name' => 'Юрист по ипотеке',
						'link' => url('yurist-ipoteke-konsultaciya-pomoshch')
					],
					[
						'name' => 'Кредитный юрист',
						'link' => url('kreditnyj-yurist-spory-konsultaciya')
					],
					[
						'name' => 'Финансовый юрист',
						'link' => url('finansovyj-yurist')
					],
				],
			],
			[
				'name' => 'Военный юрист',
				'link' => url('yurist-voennye-dela-voprosy-ipoteka-pensiya')
			],
			[
				'name' => 'Гражданский юрист',
				'link' => url('grazhdanskij-yurist-advokat-grazhdanskim-voprosam-sporam-konsultaciya')
			],
			[
				'name' => 'Имущественный юрист',
				'items' => [
					[
						'name' => 'Имущественный юрист',
						'link' => url('yuristy-imushchestvennym-delam-sporam-voprosam')
					],
					[
						'name' => 'Юрист по недвижимости',
						'link' => url('yurist-po-nedvizhimosti-konsultaciya')
					],
					[
						'name' => 'Юрист по сделкам',
						'link' => url('yurist-sdelkam-soprovozhdenie-registraciya-priznanie-nedejstvitelnoj')
					],
				],
			],
			[
				'name' => 'Налоговый юрист',
				'link' => url('yuristy-nalogovym-sporam-voprosam-konsultaciya')
			],
			[
				'name' => 'Миграционный юрист',
				'link' => url('migracionnyj-yurist-besplatnaya-konsultaciya-spb')
			],
			[
				'name' => 'Юрист по административным делам',
				'link' => url('yurist-administrativnym-delam-sporam-pravonarusheniyam')
			],
			[
				'name' => 'Юрист по жилищным вопросам',
				'link' => url('yurist-po-zhilishchnym-voprosam-v-vashem-rajone-advokat')
			],
			[
				'name' => 'Юрист по долгам',
				'link' => url('yurist-dolg-spb')
			],
			[
				'name' => 'Юрист по земельным вопросам',
				'link' => url('zemelnyj-yurist-voprosy-spory-uslugi')
			],
			[
				'name' => 'Юрист по защите прав потребителей',
				'link' => url('yurist-po-zashchite-prav-potrebitelej-pomoshch')
			],
			[
				'name' => 'Юрист по квартирным вопросам',
				'link' => url('yurist-kvartirnym-voprosam-pokupka-prodazha-oformit')
			],
			[
				'name' => 'Юрист по наследству',
				'link' => url('nasledstvennyj-yurist-zaveshchaniya-priznanie-prava-nedostojnym-sud')
			],
			[
				'name' => 'Юрист по пенсионном вопросам',
				'link' => url('pensionnyj-yurist-konsultaciya')
			],
			[
				'name' => 'Юрист по трудовому праву',
				'link' => url('trudovoj-yurist-spory-konsultacii-besplatno')
			],
			[
				'name' => 'Юрист по туризму',
				'link' => url('yurist-turizmu-pomoshch-spory-konsultaciya')
			],
			[
				'name' => 'Юрист с редкой специализацией',
				'link' => url('yuristy-redkoj-specializacii-pomoshch-konsultaciya')
			],
			[
				'name' => 'Бизнес-юрист',
				'link' => url('biznes-yurist-soprovozhdenie-zashchita')
			],
			[
				'name' => 'Юрист по строительству',
				'link' => url('stroitelnyj-yurist-konsultacii')
			],
			[
				'name' => 'Транспортный юрист',
				'link' => url('transportnyj-yurist-spory-pomoshch-konsultaciya-besplatno')
			],
			[
				'name' => 'Таможенный юрист',
				'link' => url('tamozhennyj-yurist-spory-sud-pomoshch-besplatno')
			],
			[
				'name' => 'Юрист по вопросам блокчейн',
				'link' => url('yurist-blokchejn-bitkoin-ico-kriptovalyuta')
			],
		],
	],
	[
		'name' => 'Юристы',
		'items' => [
			[
				'name' => 'Юристы "ЭКРУАЛ"',
				'link' => url('grazhdanskij-advokat-kollegiya-uslugi-konsultaciya')
			],
			[
				'name' => 'Юрист в вашем районе',
				'items' => [
					[
						'name' => 'Юрист в вашем районе',
						'link' => url('rajonnyj-advokat-kollegiya-uslugi-konsultaciya')
					],
					[
						'name' => 'Юрист в Санкт-Петербурге',
						'link' => url('advokat-sankt-peterburga-spb-kollegiya-konsultaciya-palata')
					],
					[
						'name' => 'Юрист в Адмиралтейском районе',
						'link' => url('advokat-admiraltejskij-rajon-uslugi-pomoshch-spb')
					],
					[
						'name' => 'Юрист по Василеостровскому район',
						'link' => url('advokat-vasileostrovskogo-rajona-pomoshch-spb#')
					],
					[
						'name' => 'Юрист Выборгского района',
						'link' => url('advokaty-vyborskij-rajon-kollegiya-konsultaciya-pomoshch')
					],
					[
						'name' => 'Юрист Гатчина',
						'link' => url('advokat-gatchina-sud-konsultacii-besplatno')
					],
					[
						'name' => 'Юрист в Калининском районе',
						'link' => url('advokat-kalininskij-rajon-pomoshch-uslugi-konsultacii')
					],
					[
						'name' => 'Юрист в Кировском районе',
						'link' => url('advokat-v-kirovskom-rajone-besplatno-spb')
					],
					[
						'name' => 'Юрист в Колпино',
						'link' => url('advokat-v-kolpino-konsultacii-besplatno-spb')
					],
					[
						'name' => 'Юрист в Красногвардейском районе',
						'link' => url('advokat-krasnogvardejskij-rajon-sud-konsultacii-besplatno')
					],
					[
						'name' => 'Юрист в Невском районе',
						'link' => url('advokat-nevskij-rajon-konsultacii-besplatno-spb')
					],
					[
						'name' => 'Юрист в Петроградском районе',
						'link' => url('advokat-petrogradskij-rajon-sud-konsultaciya-besplatno')
					],
					[
						'name' => 'Юрист в Петродворцовом районе',
						'link' => url('advokaty-petrodvorcovogo-rajona-sud-konsultaciya-spb')
					],
					[
						'name' => 'Юрист в Приморском районе',
						'link' => url('advokat-v-primorskom-rajone-yuridicheskaya-pomoshch')
					],
					[
						'name' => 'Юрист во Фрунзенском районе',
						'link' => url('advokat-frunzenskij-rajon-pomoshch-uslugi-besplatno')
					],
					[
						'name' => 'Юрист в Центральном районе',
						'link' => url('kollegiya-advokatov-centralnogo-rajona-sud-konsultaciya')
					],
				],
			],
			[
				'name' => 'Юрист в СПб',
				'link' => url('advokat-sankt-peterburg-spb')
			],
			[
				'name' => 'Услуги юриста',
				'link' => url('uslugi-advokata-spb')
			],
			[
				'name' => 'Автомобильный юрист',
				'link' => url('avtomobilnyj-advokat')
			],
			[
				'name' => 'Арбитражный юрист',
				'link' => url('arbitrazhnyj-advokat-arbitrazhnye-dela')
			],
			[
				'name' => 'Юрист по военным вопросам',
				'link' => url('voennyj-advokat-yuridicheskaya-pomoshch-voennosluzhashchim')
			],
			[
				'name' => 'Юрист по ДТП',
				'link' => url('advokat-dtp-pomoshch-avariyah-yurist-konsultacii')
			],
			[
				'name' => 'Кредитный юрист',
				'link' => url('advokat-po-kreditam-dolgam-yurist-po-ipoteke')
			],
			[
				'name' => 'Ипотечный юрист',
				'link' => url('ipotechnyj-advokat-pomoshch-besplatno-spb')
			],
			[
				'name' => 'Юрист по медицинским вопросам',
				'link' => url('medicinskij-advokat-voprosy-spory-konsultaciya')
			],
			[
				'name' => 'Гражданский юрист',
				'link' => url('grazhdanskij-yurist-advokat-grazhdanskim-voprosam-sporam-konsultaciya')
			],
			[
				'name' => 'Юрист по недвижимости',
				'link' => url('advokat-nedvizhimosti-konsultaciya-besplatno')
			],
			[
				'name' => 'Юрист по строительным вопросам',
				'link' => url('advokat-stroitelnye-dela-spory-voprosy-uslugi-sud')
			],
			[
				'name' => 'Юрист по наследственному праву',
				'link' => url('advokat-nasledstvo-prinyatie-oformlenie-vosstanovlenie')
			],
			[
				'name' => 'Юрист по сделках',
				'link' => url('advokat-sdelkam-priznanie-nedejstvitelnoj-soprovozhdenie')
			],
			[
				'name' => 'Юрист по трудовым спорам',
				'link' => url('besplatnyj-trudovoj-advokat-spory-dela')
			],
			[
				'name' => 'Юрист по земельным спорам',
				'link' => url('zemelnyj-advokat-mezhevye-spory-snt-dnp')
			],
			[
				'name' => 'Юрист по имущественным спорам',
				'link' => url('advokat-imushchestvennym-sporam-delam-voprosam-konsultaciya')
			],
			[
				'name' => 'Юрист по страховым вопросам',
				'link' => url('advokat-strahovym-voprosam-kompensaciya-osago-kasko-yuristy')
			],
			[
				'name' => 'Бесплатный юрист',
				'link' => url('besplatnyj-advokat-yuridicheskaya-pomoshch-onlajn-konsultant')
			],
			[
				'name' => 'Уголовный юрист',
				'link' => url('ugolovnyj-advokat-uslugi-pomoshch')
			],
			[
				'name' => 'Юрист суд',
				'link' => url('advokat-v-sude-besplatno')
			],
		],
	],
	[
		'name' => 'Юридическая помощь',
		'items' => [
			[
				'name' => 'Юридическое сопровождение от "ЭКРУАЛ"',
				'link' => url('yuridicheskoe-soprovozhdenie-yuridicheskih-fizicheskih-lic')
			],
			[
				'name' => 'Юридическая фирма "ЭКРУАЛ"',
				'link' => url('yuridicheskaya-firma-n1-v-sankt-peterburge')
			],
			[
				'name' => 'Юридическая оценка',
				'link' => url('yuridicheskaya-ocenka-spb-2018')
			],
			[
				'name' => 'Подбор арбитражного управляющего',
				'link' => url('podbor-arbitrazhnogo-upravlyayushchego')
			],
			[
				'name' => 'Юридические услуги',
				'link' => url('yuridicheskie-uslugi')
			],
			[
				'name' => 'Защита прав туристов',
				'link' => url('zashchita-prav-turistov-yurist-spb')
			],
			[
				'name' => 'Юридические услуги физическим лицам',
				'link' => url('yuridicheskie-uslugi-fizicheskim-licam')
			],
			[
				'name' => 'Юридические услуги юридическим лицам',
				'link' => url('yuridicheskie-uslugi-yuridicheskim-licam')
			],
			[
				'name' => 'Юридическая помощь',
				'link' => url('yuridicheskaya-pomoshch')
			],
			[
				'name' => 'Услуги представителя',
				'link' => url('uslugi-predstavitelya-sud-pomoshch-yursita')
			],
			[
				'name' => 'Ограничение дееспособности гражданина',
				'link' => url('ogranichenie-deesposobnosti-grazhdani')
			],
			[
				'name' => 'Помощь юриста',
				'link' => url('pomoshch-yurista-spb-onlajn')
			],
			[
				'name' => 'Юридическая защита',
				'link' => url('yuridicheskaya-zashchita')
			],
			[
				'name' => 'Удаление негативных отзывов',
				'link' => url('udalenie-negativnyh-otzyvov-kommentariev-stoimost')
			],
			[
				'name' => 'Работа с отказами нотариуса',
				'link' => url('rabota-otkazami-notariusa-zayavlenie-pomoshch-yurist')
			],
			[
				'name' => 'Отказ предоставить землю',
				'link' => url('otkaz-v-zemle-pod-izhs-pomoshch-yuista')
			],
			[
				'name' => 'Услуги по взысканию',
				'link' => url('uslugi-po-vzyskaniyu')
			],
			[
				'name' => 'Розыск имущества',
				'link' => url('rozysk-imushchestva-pomoshch-yurista')
			],
			[
				'name' => 'Взыскание НДС',
				'link' => url('vzyskanie-nds-kontragenta-direktora')
			],
			[
				'name' => 'Юридическое обслуживание юридических лиц',
				'link' => url('yuridicheskoe-obsluzhivanie-yuridicheskih-lic ')
			],
			[
				'name' => 'Оформление собственности',
				'link' => url('Oformlenie-sobstvennosti-doma-zemli-uchastka-nasledstva-sdelki-kvartiry')
			],
			[
				'name' => 'Расторжение договора в одностороннемпорядке',
				'link' => url('rastorzhenie-dogovora-v-odnostoronnem-poryadke-pomoshch-yurista')
			],
			[
				'name' => 'Защита права собственности',
				'link' => url('zashchita-prava-sobstvennosti-pomoshch-yurista')
			],
			[
				'name' => 'Защита от изъятия земельных участков',
				'link' => url('izyatie-zemelnyh-uchastkov-zashchita-pomoshch-yurista')
			],
			[
				'name' => 'Услуги по 44 ФЗ',
				'link' => url('uslugi-44-fz-pomoshch-yurista')
			],
			[
				'name' => 'Сопровождение тендеров и госзакупок',
				'link' => url('yuridicheskoe-soprovozhdenie-tenderov-goszakupok')
			],
			[
				'name' => 'Некачественный ремонт',
				'link' => url('nekachestvennyj-remont')
			],
		],
	],
	[
		'name' => 'Документальное сопровождение',
		'items' => [
			[
				'name' => 'Подготовка любых документов',
				'link' => url('podgotovka-dokumentov-pomoshch-yurista-oformlenie-sud')
			],
			[
				'name' => 'Составление договоров',
				'link' => url('sostavlenie-dogovora-pomoshch-yurista')
			],
			[
				'name' => 'Составление жалоб',
				'link' => url('sostavlenie-zhalob-pomoshch-yurista')
			],
			[
				'name' => 'Иск. Услуги',
				'link' => url('uslugi-isk-pomoshch-yurista')
			],
			[
				'name' => 'Составление искового заявления',
				'link' => url('sostavlenie-podacha-osparivanie-iskovogo-zayavleniya-iska-yurist-advokat')
			],
			[
				'name' => 'Досудебная претензия',
				'link' => url('dosudebnaya-pretenziya-pomoshch-yurista')
			],
			[
				'name' => 'Оформление любых прав',
				'link' => url('registraciya-lyubyh-prav-pomoshch-yurista')
			],
			[
				'name' => 'Помощь вкладчикам семейного капитала',
				'link' => ' /pomoshch-vkladchikam-semejnogo-kapitala',
			],
			[
				'name' => 'Переоформление дома',
				'link' => url('pereoformlenie-doma-pomoshch-yurista')
			],
			[
				'name' => 'Договор дарения (дарственная). Стоимость',
				'link' => url('stoimost-dogovora-dareniya-darstvennoj')
			],
		],
	],
];
