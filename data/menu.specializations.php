<?php

return [
	[
		'name' => 'Земельное право',
		'items' => [
			'popular' => [
				[
					'name' => 'Аренда земли',
					'link' => url('arenda-zemli-uchastka-izhs-snt-stroitelstvo')
				],
				[
					'name' => 'Оформление земли',
					'link' => url('oformlenie-zemli-uchastka-doma-sobstvennost')
				],
				[
					'name' => 'Перевод земли',
					'link' => url('perevod-zemli-selskohozyajstvennaya-izhs-snt-lph')
				],
				[
					'name' => 'Вопросы по СНТ',
					'link' => url('yuridicheskaya-pomoshch-snt-zashchita-sadovodov')
				],
				[
					'name' => 'Вопросы по ДНП ',
					'link' => url('yuridicheskoe-soprovozhdenie-dnp-zashchita-dachnikov')
				],
				[
					'name' => 'Дачная амнистия земли',
					'link' => url('dachnaya-amnistiya-zemelnogo-uchastka-oformit-sobstvennost-registraciya')
				],
				[
					'name' => 'Дарение земельного участка',
					'link' => url('darenie-zemelnogo-uchastka-pomoshch-yurista')
				],
			],
			'free' => [
				[
					'name' => 'Консультация по земельному праву',
					'link' => url('konsultaciya-zemelnym-voprosam-sporam-besplatno')
				],
				[
					'name' => 'Решение земельных споров',
					'link' => url('zemelnye-spory-granicy-sosedi-sud-yurist')
				],
				[
					'name' => 'Юридическая помощь по земельным спорам',
					'link' => url('yuridicheskie-uslugi-zemelnym-voprosam-sporam')
				],
			],
			'specialists_1' => [
				[
					'name' => 'Земельный юрист/',
					'link' => url('zemelnyj-yurist-voprosy-spory-uslugi')
				],
				[
					'name' => 'юрист',
					'link' => url('zemelnyj-advokat-mezhevye-spory-snt-dnp')
				],
				[
					'name' => 'Юрист по СНТ',
					'link' => url('advokat-po-snt-sadovodstvam-konsultaciya-uslugi')
				],
			],
			'specialists_2' => [
				[
					'name' => 'Услуги по ДНП',
					'link' => url('uslugi-po-dnp')
				],
				[
					'name' => 'Сделки с земельными участками',
					'link' => url('sdelki-zemelnymi-uchastkami-oformlenie')
				],
				[
					'name' => 'Купля-продажа земли/участка',
					'link' => url('Kuplya-prodazha-zemli-uchastka-pomoshch-yurista')
				],
				[
					'name' => 'Публичные торги по земле',
					'link' => url('publichnye-torgi-po-zemle-pomoshch-yurista')
				],
				[
					'name' => 'Публичные торги по земле',
					'link' => url('publichnye-torgi-po-zemle-pomoshch-yurista')
				],
				[
					'name' => 'Все по аренде земли',
					'items' => [
						[
							'name' => 'Аренда земли под парковку',
							'link' => url('arenda-zemli-zemelnogo-uchastka-avtostoyanki-parkovki')
						],
						[
							'name' => 'Спор по аренде земли',
							'link' => url('spor-po-arende-zemli-dogovoru-arendy')
						],
						[
							'name' => 'Суд по аренде земли',
							'link' => url('sud-po-arende-zemli-spor-po-zemelnomu-pravu')
						],
					],
				],
				[
					'name' => 'Все по оформлению земли',
					'items' => [
						[
							'name' => 'Оформление земли в собственность',
							'link' => url('zemelnoe-pravo-oformlenie-sobstvennost-spory-yurist-advokat')
						],
						[
							'name' => 'Услуги по землеустройству',
							'link' => url('uslugi-po-zemleustrojstvu-pomoshch-yurista')
						],
						[
							'name' => 'Оформление земельного пая',
							'link' => url('zemelnyj-paj-vydelit-oformit-otmezhevat')
						],
						[
							'name' => 'Оформление дачной амнистии',
							'link' => url('oformlenie-dachnoj-amnistii-na-zemlyu-uchastok-dom#')
						],
						[
							'name' => 'Установление и изменение границ земельных участков',
							'link' => url('ustanovlenie-i-izmenenie-granic-zemelnyh-uchastkov')
						],
						[
							'name' => 'Оформление границ земельного участка',
							'link' => url('oformlenie-granic-zemelnogo-uchastka-yurist-sud-spb')
						],
						[
							'name' => 'Дачная амнистия на строительство',
							'link' => url('dachnaya-amnistiya-poluchenie-razresheniya-na-stroitelstvo#')
						],
						[
							'name' => 'Дачная амнистия на дом',
							'link' => url('dachnaya-amnistiya-konsultacii-besplatno-spb')
						],
						[
							'name' => 'Выкуп, оформление, выдел земельного пая',
							'link' => url('vykup-oformlenie-vydel-zemelnogo-paya-pomoshch-spb#')
						],
						[
							'name' => 'Внеочередное приобретение земельных участков',
							'link' => url('vneocherednoe-priobretenie-zemelnyh-uchastkov')
						],
					],
				],
				[
					'name' => 'Все по переводу земли',
					'items' => [
						[
							'name' => 'Изменение ВРИ',
							'link' => url('izmenenie-vida-razreshennogo-ispolzovaniya-vri')
						],
						[
							'name' => 'Перевод земли в иную категорию',
							'link' => url('perevod-zemli-uchastka-pomoshch-yurist')
						],
					],
				],
				[
					'name' => 'Все для СНТ',
					'items' => [
						[
							'name' => 'Юридическое сопровождение СНТ',
							'link' => url('yuridicheskoe-soprovozhdenie-deyatelnosti-snt')
						],
						[
							'name' => 'Услуги для СНТ',
							'link' => url('uslugi-snt-onsultaciya-yurist-besplatno')
						],
						[
							'name' => 'Суд по членским взносам',
							'link' => url('sud-chlenskim-vznosam-snt-vzyskanie-uplata')
						],
						[
							'name' => 'Споры по членству в СНТ',
							'link' => url('spory-chlenstvo-snt-vstuplenie-isklyuchenie-sud')
						],
					],
				],
				[
					'name' => 'Все для СНТ',
					'items' => [
						[
							'name' => 'Юридическое сопровождение СНТ',
							'link' => url('yuridicheskoe-soprovozhdenie-deyatelnosti-snt')
						],
						[
							'name' => 'Услуги для СНТ',
							'link' => url('uslugi-snt-onsultaciya-yurist-besplatno')
						],
						[
							'name' => 'Суд по членским взносам',
							'link' => url('sud-chlenskim-vznosam-snt-vzyskanie-uplata')
						],
						[
							'name' => 'Споры по членству в СНТ',
							'link' => url('spory-chlenstvo-snt-vstuplenie-isklyuchenie-sud')
						],
					],
				],
				[
					'name' => 'Земельные споры',
					'items' => [
						[
							'name' => 'Земельные споры с соседями',
							'link' => url('zemelnye-spory-s-sosedyami')
						],
						[
							'name' => 'Юридические услуги по земельным спорам',
							'link' => url('yuridicheskie-uslugi-zemelnym-voprosam-sporam')
						],
						[
							'name' => 'Юридическая помощь по земельным спорам',
							'link' => url('yuridicheskie-uslugi-zemelnym-voprosam-sporam')
						],
						[
							'name' => 'Споры со службой по земельному надзору',
							'link' => url('spory-so-sluzhboj-po-zemelnomu-nadzoru-pomoshch-yurista')
						],
						[
							'name' => 'Защита от изъятия земельных участков',
							'link' => url('izyatie-zemelnyh-uchastkov-zashchita-pomoshch-yurista')
						],
						[
							'name' => 'Отказ предоставить землю под ИЖС',
							'link' => url('otkaz-v-zemle-pod-izhs-pomoshch-yuista')
						],
						[
							'name' => 'Оспаривание земельного налога',
							'link' => url('osparivanie-zemelnogo-naloga')
						],
					],
				],
			],
		],
	],
	[
		'name' => 'Межевание',
		'items' => [
			'popular' => [
				[
					'name' => 'Межевание',
					'link' => url('mezhevanie-zemli-uchastka-spb-lo')
				],
				[
					'name' => 'Межевые работы',
					'link' => url('mezhevanie-zemli-uchastka-kadastrovye-raboty')
				],
				[
					'name' => 'Кадастровые работы',
					'link' => url('kadastrovye-raboty-zemelnom-uchastke-provedenie')
				],
				[
					'name' => 'Геодезические работы',
					'link' => url('geodezicheskie-raboty-izyskaniya-semka')
				],
				[
					'name' => 'Перераспределение земель',
					'link' => url('pereraspredelenie-zemelnyh-uchastkov-doley')
				],
				[
					'name' => 'Прирезка земли',
					'link' => url('prirezka-zemli-zemelnogo-uchastka-oformlenie')
				],
				[
					'name' => 'Наложение границ земельного участка',
					'link' => url('nalozhenie-granic-zemelnogo-uchastka-pomoshch-yurista')
				],
			],
			'free' => [
				[
					'name' => 'Решение межевых споров',
					'link' => url('mezhevye-spory-o-granicah-mezhdu-sosedyami')
				],
				[
					'name' => 'Прирезка земли',
					'link' => url('prirezka-zemli-zemelnogo-uchastka-oformlenie')
				],
				[
					'name' => 'Определение порядка пользования ЗУ',
					'link' => url('opredelenie-poryadka-polzovaniya-zemelnym-uchastkom')
				],
			],
			'specialists_1' => [
				[
					'name' => 'Геодезисты в СПб и ЛО',
					'link' => url('uslugi-geodezista-v-spb')
				],
				[
					'name' => 'Услуги кадастрового инженера',
					'link' => url('uslugi-kadastrovogo-inzhenera-pomoshch-stoimost-konsultaciya')
				],
				[
					'name' => 'Предложение для кадастровых инженеров ',
					'link' => url('yuristy-kadastrovye-inzhenery-mezhevanie')
				],
			],
			'specialists_2' => [
				[
					'name' => 'Оспаривание межевания',
					'link' => url('osparivanie-mezhevaniya-pomoshch-zemelnogo-yurista')
				],
				[
					'name' => 'Оспаривание межевания',
					'link' => url('osparivanie-mezhevaniya-pomoshch-zemelnogo-yurista')
				],
				[
					'name' => 'Все по межеванию',
					'items' => [
						[
							'name' => 'Вынос точек в натуру',
							'link' => url('vynos-tochek-v-naturu-pomoshch-yurista')
						],
						[
							'name' => 'Восстановление межевых знаков и документов на землю',
							'link' => url('vosstanovlenie-mezhevyh-znakov-dokumentov-zemlyu#')
						],
						[
							'name' => 'Прирезка земли',
							'link' => url('prirezka-zemli-zemelnogo-uchastka-oformlenie')
						],
						[
							'name' => 'Решение межевых споров',
							'link' => url('mezhevye-spory-o-granicah-mezhdu-sosedyami')
						],
						[
							'name' => 'Споры между соседями по установке забора',
							'link' => url('zemelnyj-dachnyj-spor-zabor-sosedi-sud')
						],
						[
							'name' => 'Стоимость межевания ЗУ в ЛО',
							'link' => url('mezhevanie-zemelnogo-uchastka-leningradskoj-oblasti-zemelnyj-advokat')
						],
						[
							'name' => 'Установление границ земельного участка',
							'link' => url('ustanovlenie-granic-zemelnogo-uchastka-mestnost-sud')
						],
						[
							'name' => 'Уточнение границ земельного участка',
							'link' => url('utochnenie-granic-zemelnogo-uchastka-soglasovanie-mezhevanie')
						],
						[
							'name' => 'Межевание',
							'link' => url('mezhevanie-zemli-uchastka-kadastrovye-raboty')
						],
					],
				],
				[
					'name' => 'Все по межеванию',
					'items' => [
						[
							'name' => 'Вынос точек в натуру',
							'link' => url('vynos-tochek-v-naturu-pomoshch-yurista')
						],
						[
							'name' => 'Восстановление межевых знаков и документов на землю',
							'link' => url('vosstanovlenie-mezhevyh-znakov-dokumentov-zemlyu#')
						],
						[
							'name' => 'Прирезка земли',
							'link' => url('prirezka-zemli-zemelnogo-uchastka-oformlenie')
						],
						[
							'name' => 'Решение межевых споров',
							'link' => url('mezhevye-spory-o-granicah-mezhdu-sosedyami')
						],
						[
							'name' => 'Споры между соседями по установке забора',
							'link' => url('zemelnyj-dachnyj-spor-zabor-sosedi-sud')
						],
						[
							'name' => 'Стоимость межевания ЗУ в ЛО',
							'link' => url('mezhevanie-zemelnogo-uchastka-leningradskoj-oblasti-zemelnyj-advokat')
						],
						[
							'name' => 'Установление границ земельного участка',
							'link' => url('ustanovlenie-granic-zemelnogo-uchastka-mestnost-sud')
						],
						[
							'name' => 'Уточнение границ земельного участка',
							'link' => url('utochnenie-granic-zemelnogo-uchastka-soglasovanie-mezhevanie')
						],
						[
							'name' => 'Межевание',
							'link' => url('mezhevanie-zemli-uchastka-kadastrovye-raboty')
						],
					],
				],
				[
					'name' => 'Кадастровые работы',
					'items' => [
						[
							'name' => 'Выполнение кадастровых работ',
							'link' => url('kadastrovye-raboty-zemelnom-uchastke-provedenie')
						],
						[
							'name' => 'Исправление реестровой ошибки',
							'link' => url('ispravlenie-tekhnicheskoj-kadastrovoj-oshibki-reestra-pomoshch-yurista')
						],
						[
							'name' => 'Постановка на кадастровый учёт',
							'link' => url('postanovka-kadastrovyj-uchet-uchastkov-nedvizhimosti')
						],
						[
							'name' => 'Исправление кадастрового паспорта',
							'link' => url('ispravlenie-kadastrovogo-pasporta-pomoshch-yurista')
						],
						[
							'name' => 'Публичная кадастровая карта',
							'link' => url('publichnaya-kadastrovaya-karta-pomoshch-konsultaciya')
						],
						[
							'name' => 'Самозахват земли',
							'link' => url('samozahvat-zemli-uzakonit-oformit')
						],
						[
							'name' => 'Уменьшение кадастровой стоимости',
							'link' => url('snizhenie-osparivanie-kadastrovoj-stoimosti#')
						],
					],
				],
				[
					'name' => 'Геодезические работы',
					'items' => [
						[
							'name' => 'Геодезическая съемка земельного участка',
							'link' => url('geodezicheskaya-semka-zemelnogo-uchastka-mestnosti-zdanij')
						],
						[
							'name' => 'Исполнительная съёмка',
							'link' => url('kontrolno-ispolnitelnaya-semka-kommunikacij-setej-obekta')
						],
					],
				],
				[
					'name' => 'Операции с землей',
					'items' => [
						[
							'name' => 'Выдел земли',
							'link' => url('vydel-zemli-nature-doli-arenda')
						],
						[
							'name' => 'Схема расположения земельного участка',
							'link' => url('skhema-raspolozheniya-zemelnogo-uchastka-pomoshch-yurista')
						],
						[
							'name' => 'Объединение земельных участков',
							'link' => url('obedinenie-zemelnyh-uchastkov-pereraspredelenie')
						],
						[
							'name' => 'Раздел земли',
							'link' => url('razdel-zemli-doma-uchastka-doli-granicy-sud')
						],
					],
				],
			],
		],
	],
	[
		'name' => 'Проблемы с кредитами',
		'items' => [
			'popular' => [
				[
					'name' => 'Банкротство физических лиц',
					'link' => url('bankrotstvo-fizicheskih-lic-ip-konsultaciya')
				],
				[
					'name' => 'Проблемы с кредитами',
					'link' => url('problemy-s-kreditami-pomoshch-yurista')
				],
				[
					'name' => 'Реструктуризация кредитов',
					'link' => url('restrukturizaciya-kredita-dolga-valyutnoj-ipoteki')
				],
				[
					'name' => 'Рефинансирование кредитов',
					'link' => url('refinansirovanie-kreditov-bankov-ipoteki-dolgov')
				],
				[
					'name' => 'Спор с банком по кредиту',
					'link' => url('sud-s-bankom-po-kreditu-ipoteke-yuristy-banki-spb-moskva')
				],
				[
					'name' => 'Расторжение кредитного договора',
					'link' => url('rastorzhenie-kreditnogo-dogovora-bankom-sud')
				],
				[
					'name' => 'Получение кредита',
					'link' => url('pomoshch-kreditnogo-brokera-prosrochki')
				],
				[
					'name' => 'Помощь вкладчикам КПК "Семейный капитал"',
					'link' => url('pomoshch-vkladchikam-semejnogo-kapitala')
				],
			],
			'free' => [
				[
					'name' => 'Консультация по кредитным вопросам',
					'link' => url('konsultaciya-kreditnym-voprosam-yuridicheskaya-besplatnaya')
				],
				[
					'name' => 'Помощь по вопросам кредитования',
					'link' => url('kreditnyj-finansovyj-ipotechnyj-konsultant')
				],
				[
					'name' => 'Помощь по реестру кредиторов',
					'link' => url('vklyuchenie-isklyuchenie-reestr-kreditorov')
				],
			],
			'specialists_1' => [
				[
					'name' => 'Антиколлекторы N1 ',
					'link' => url('antikollektory-uslugi-pomoshch-besplatno')
				],
				[
					'name' => 'Банковский юрист',
					'link' => url('yurist-bankovskim-delam-voprosam-konsultaciya')
				],
				[
					'name' => 'Кредитный юрист/',
					'link' => url('kreditnyj-yurist-spory-konsultaciya')
				],
				[
					'name' => 'юрист',
					'link' => url('advokat-po-kreditam-dolgam-yurist-po-ipoteke')
				],
				[
					'name' => 'Юрист/',
					'link' => url('advokat-dolg-spb')
				],
				[
					'name' => 'юрист по долгам',
					'link' => url('yurist-dolg-spb')
				],
				[
					'name' => 'Юрист по банкротству физ.лиц',
					'link' => url('yurist-bankrotstvu-fizicheskih-lic-konsultaciya')
				],
				[
					'name' => 'Юрист/',
					'link' => url('ipotechnyj-advokat-pomoshch-besplatno-spb')
				],
				[
					'name' => 'юрист по ипотеке',
					'link' => url('ipotechnyj-yurist-pomoshch')
				],
				[
					'name' => 'Финансовый, кредитный, ипотечный консультант',
					'link' => url('kreditnyj-finansovyj-ipotechnyj-konsultant')
				],
			],
			'specialists_2' => [
				[
					'name' => 'Расторжение договора автокредита',
					'link' => url('rastorzhenie-dogovora-avtokredita')
				],
				[
					'name' => 'Расторжение договора автокредита',
					'link' => url('rastorzhenie-dogovora-avtokredita')
				],
				[
					'name' => 'Все по банкротству',
					'items' => [
						[
							'name' => 'Сохранить имущество при банкротстве',
							'link' => url('sohranit-imushchestvo-pri-bankrotstve-pomoshch-yurista-spb')
						],
						[
							'name' => 'Подбор финансового управляющего',
							'link' => url('podbor-SRO-finansovogo-upravlyayushchego-spb-lo')
						],
						[
							'name' => 'Услуги по банкротству',
							'link' => url('uslugi-bankrotstvo-pomoshch-yurista')
						],
						[
							'name' => 'Банкротство физических лиц',
							'link' => url('bankrotstvo-fizicheskih-lic-ip-konsultaciya')
						],
						[
							'name' => 'Стоимость банкротства физических лиц',
							'link' => url('bankrotstvo-fizicheskih-lic-stoimost-advokat-yurist-spb-moskva')
						],
						[
							'name' => 'Банкротство юридических лиц',
							'link' => url('bankrotstvo-yur-lic-organizacii-kompanii-yurist')
						],
						[
							'name' => 'Помощь заемщикам',
							'link' => url('pomoshch-zaemshchikam-poluchenie-kredita-ipoteki-podderzhka-pri-prosrochkah')
						],
					],
				],
				[
					'name' => 'Все по банкротству',
					'items' => [
						[
							'name' => 'Сохранить имущество при банкротстве',
							'link' => url('sohranit-imushchestvo-pri-bankrotstve-pomoshch-yurista-spb')
						],
						[
							'name' => 'Подбор финансового управляющего',
							'link' => url('podbor-SRO-finansovogo-upravlyayushchego-spb-lo')
						],
						[
							'name' => 'Услуги по банкротству',
							'link' => url('uslugi-bankrotstvo-pomoshch-yurista')
						],
						[
							'name' => 'Банкротство физических лиц',
							'link' => url('bankrotstvo-fizicheskih-lic-ip-konsultaciya')
						],
						[
							'name' => 'Стоимость банкротства физических лиц',
							'link' => url('bankrotstvo-fizicheskih-lic-stoimost-advokat-yurist-spb-moskva')
						],
						[
							'name' => 'Банкротство юридических лиц',
							'link' => url('bankrotstvo-yur-lic-organizacii-kompanii-yurist')
						],
						[
							'name' => 'Помощь заемщикам',
							'link' => url('pomoshch-zaemshchikam-poluchenie-kredita-ipoteki-podderzhka-pri-prosrochkah')
						],
					],
				],
				[
					'name' => 'Споры с банками',
					'items' => [
						[
							'name' => 'Споры с банками',
							'link' => url('spory-s-bankami')
						],
						[
							'name' => 'Споры с микрокредитными организациями',
							'link' => url('spory-mikrokreditnymi-mikrofinansovymi-organizaciyami')
						],
						[
							'name' => 'Суд по ипотеке',
							'link' => url('sud-po-ipoteke-pomoshch-yurista-2018')
						],
						[
							'name' => 'Споры с Альфа-банком',
							'link' => url('pomoshch-v-sporah-s-alfa-bankom')
						],
						[
							'name' => 'Возврат страховки',
							'link' => url('vozvrat-strahovki')
						],
						[
							'name' => 'Помощь с банками',
							'link' => url('pomoshch-banki-yurist-spb')
						],
						[
							'name' => 'Помощь при просрочке',
							'link' => url('pomoshch-pri-prosrochke')
						],
					],
				],
				[
					'name' => 'Споры с банками',
					'items' => [
						[
							'name' => 'Споры с банками',
							'link' => url('spory-s-bankami')
						],
						[
							'name' => 'Споры с микрокредитными организациями',
							'link' => url('spory-mikrokreditnymi-mikrofinansovymi-organizaciyami')
						],
						[
							'name' => 'Суд по ипотеке',
							'link' => url('sud-po-ipoteke-pomoshch-yurista-2018')
						],
						[
							'name' => 'Споры с Альфа-банком',
							'link' => url('pomoshch-v-sporah-s-alfa-bankom')
						],
						[
							'name' => 'Возврат страховки',
							'link' => url('vozvrat-strahovki')
						],
						[
							'name' => 'Помощь с банками',
							'link' => url('pomoshch-banki-yurist-spb')
						],
						[
							'name' => 'Помощь при просрочке',
							'link' => url('pomoshch-pri-prosrochke')
						],
					],
				],
				[
					'name' => 'Получение кредита',
					'items' => [
						[
							'name' => 'Получение ипотеки с гос поддержкой',
							'link' => url('gospodderzhka-ipoteke-sberbank-vtb24-yurist')
						],
						[
							'name' => 'Решение вопроса валютной ипотеки',
							'link' => url('perevod-valyutnoj-ipoteki-rubli-pomoshch')
						],
						[
							'name' => 'Помощь в ипотеке',
							'link' => url('pomoshch-ipoteka-yurist')
						],
						[
							'name' => 'Помощь по кредитам',
							'link' => url('pomoshch-po-voprosam-kreditovaniya-spb')
						],
					],
				],
			],
		],
	],
	[
		'name' => 'Недвижимость',
		'items' => [
			'popular' => [
				[
					'name' => 'Приватизация имущества',
					'link' => url('privatizaciya-zhilya-nedvizhimosti-spb-moskva')
				],
				[
					'name' => 'Оформление в собственность',
					'link' => url('Oformlenie-sobstvennosti-doma-zemli-uchastka-nasledstva-sdelki-kvartiry')
				],
				[
					'name' => 'Консультация при покупке недвижимости',
					'link' => url('konsultaciya-pri-pokupke-nedvizhimos')
				],
				[
					'name' => 'Перевод недвижимости',
					'link' => url('perevod-nedvizhimosti-spb-moskva')
				],
				[
					'name' => 'Сделки с недвижимостью',
					'link' => url('yuridicheskoe-soprovozhdenie-sdelki-nedvizhimost')
				],
				[
					'name' => 'Перепланировка по лучшей стоимости',
					'link' => url('luchshaya-stoimost-pereplanirovki-spb-pomoshch-yurista')
				],
				[
					'name' => 'Реконструкция дома',
					'link' => url('rekonstrukciya-doma-pomoshch-yurista')
				],
				[
					'name' => 'Оспаривание кадастровой стоимости',
					'link' => url('osparivanie-kadastrovoj-stoimosti-nedvizhimosti')
				],
			],
			'free' => [
				[
					'name' => 'Консультация юриста по недвижимости',
					'link' => url('yurist-po-nedvizhimosti-konsultaciya')
				],
				[
					'name' => 'Помощь по приватизации жилья',
					'link' => url('yurist-po-privatizacii-kvartiry-komnaty-zemli-doma')
				],
				[
					'name' => 'Экспертиза и проверка договора',
					'link' => url('ehkspertiza-proverka-dogovorov-podryada-kupli-prodazhi')
				],
			],
			'specialists_1' => [
				[
					'name' => 'Юрист/',
					'link' => url('advokat-nedvizhimosti-konsultaciya-besplatno')
				],
				[
					'name' => 'юрист по недвижимости',
					'link' => url('yurist-po-nedvizhimosti-konsultaciya')
				],
				[
					'name' => 'Юрист/',
					'link' => url('advokat-perevod-nedvizhimosti-spb-moskva')
				],
				[
					'name' => 'юрист по переводу недвижимости',
					'link' => url('yurist-perevod-nedvizhimosti-spb-moskva')
				],
				[
					'name' => 'Квартирный юрист',
					'link' => url('kvartirnyj-advokat-zhilishchnyj-vopros-konsultacii-onlain-besplatno#')
				],
				[
					'name' => 'Юрист по квартирным вопросам',
					'link' => url('yurist-kvartirnym-voprosam-pokupka-prodazha-oformit')
				],
				[
					'name' => 'Юрист по приватизации',
					'link' => url('yurist-po-privatizacii-kvartiry-komnaty-zemli-doma')
				],
			],
			'specialists_2' => [
				[
					'name' => 'Расторжение договора',
					'link' => url('rastorzhenie-dogovora-arendy-kupli-prodazhi-postavki')
				],
				[
					'name' => 'Суд по недвижимости',
					'link' => url('sud-po-nedvizhimosti-pomoshch-yurista-spb')
				],
				[
					'name' => 'Суд по недвижимости',
					'link' => url('sud-po-nedvizhimosti-pomoshch-yurista-spb')
				],
				[
					'name' => 'Все о приватизации',
					'items' => [
						[
							'name' => 'Отказ от приватизации',
							'link' => url('otkaz-ot-privatizacii-oformim-osporim-besplatno')
						],
						[
							'name' => 'Приватизация 2017, 2018',
							'link' => url('privatizaciya-zhilya-kvartiry-2017-2018-prodlenie')
						],
						[
							'name' => 'Приватизация 2017, 2018',
							'link' => url('privatizaciya-zhilya-kvartiry-2017-2018-prodlenie')
						],
						[
							'name' => 'Приватизация гаража',
							'link' => url('privatizaciya-garazha-zemli-v-garazhnom-kooperative')
						],
						[
							'name' => 'Приватизация дачного участка',
							'link' => url('privatizaciya-dachnogo-uchastka-zemel-postroek')
						],
						[
							'name' => 'Приватизация доли',
							'link' => url('privatizaciya-doli-kvartiry-doma-zhilya-besplatno')
						],
						[
							'name' => 'Приватизация дома',
							'link' => url('privatizaciya-doma-dachnogo-zhilogo-s-zemlej')
						],
						[
							'name' => 'Приватизация жилья',
							'link' => url('privatizaciya-zhilya-kvartiry-doma-komnaty-2017')
						],
						[
							'name' => 'Приватизация ИЖС',
							'link' => url('privatizaciya-zemelnogo-uchastka-pod-izhs')
						],
						[
							'name' => 'Приватизация квартир',
							'link' => url('privatizaciya-kvartiry-doli-komnaty')
						],
						[
							'name' => 'Приватизация комнаты',
							'link' => url('privatizaciya-komnaty-oformlenie-sobstvennost-yurist')
						],
						[
							'name' => 'Приватизация комнаты в общежитии',
							'link' => url('privatizaciya-komnaty-obshchezhitii-konsultaciya')
						],
						[
							'name' => 'Приватизация муниципальной комнаты',
							'link' => url('privatizaciya-municipalnoj-nedvizhimosti-kommunalki-v-sobstvennost')
						],
						[
							'name' => 'Приватизация садового участка',
							'link' => url('privatizaciya-sadovogo-dachnogo-uchastka-zemli')
						],
						[
							'name' => 'Суд по приватизации',
							'link' => url('privatizaciya-zhilya-cherez-sud-besplatno')
						],
					],
				],
				[
					'name' => 'Все о приватизации',
					'items' => [
						[
							'name' => 'Отказ от приватизации',
							'link' => url('otkaz-ot-privatizacii-oformim-osporim-besplatno')
						],
						[
							'name' => 'Приватизация 2017, 2018',
							'link' => url('privatizaciya-zhilya-kvartiry-2017-2018-prodlenie')
						],
						[
							'name' => 'Приватизация 2017, 2018',
							'link' => url('privatizaciya-zhilya-kvartiry-2017-2018-prodlenie')
						],
						[
							'name' => 'Приватизация гаража',
							'link' => url('privatizaciya-garazha-zemli-v-garazhnom-kooperative')
						],
						[
							'name' => 'Приватизация дачного участка',
							'link' => url('privatizaciya-dachnogo-uchastka-zemel-postroek')
						],
						[
							'name' => 'Приватизация доли',
							'link' => url('privatizaciya-doli-kvartiry-doma-zhilya-besplatno')
						],
						[
							'name' => 'Приватизация дома',
							'link' => url('privatizaciya-doma-dachnogo-zhilogo-s-zemlej')
						],
						[
							'name' => 'Приватизация жилья',
							'link' => url('privatizaciya-zhilya-kvartiry-doma-komnaty-2017')
						],
						[
							'name' => 'Приватизация ИЖС',
							'link' => url('privatizaciya-zemelnogo-uchastka-pod-izhs')
						],
						[
							'name' => 'Приватизация квартир',
							'link' => url('privatizaciya-kvartiry-doli-komnaty')
						],
						[
							'name' => 'Приватизация комнаты',
							'link' => url('privatizaciya-komnaty-oformlenie-sobstvennost-yurist')
						],
						[
							'name' => 'Приватизация комнаты в общежитии',
							'link' => url('privatizaciya-komnaty-obshchezhitii-konsultaciya')
						],
						[
							'name' => 'Приватизация муниципальной комнаты',
							'link' => url('privatizaciya-municipalnoj-nedvizhimosti-kommunalki-v-sobstvennost')
						],
						[
							'name' => 'Приватизация садового участка',
							'link' => url('privatizaciya-sadovogo-dachnogo-uchastka-zemli')
						],
						[
							'name' => 'Суд по приватизации',
							'link' => url('privatizaciya-zhilya-cherez-sud-besplatno')
						],
					],
				],
				[
					'name' => 'Все по оформлению недвижимости',
					'items' => [
						[
							'name' => 'Оформление прав собственности',
							'link' => url('oformlenie-prav')
						],
						[
							'name' => 'Тех. план на объект недвижимости',
							'link' => url('tekhnicheskij-plan-na-obekt-nedvizhimosti')
						],
						[
							'name' => 'Легализация самовольных построек',
							'link' => url('oformlenie-uzakonenie-samovolnyh-postroek')
						],
						[
							'name' => 'Оформление дома',
							'link' => url('oformlenie-doma-na-uchastke-v-sobstvennost-dokumenty')
						],
						[
							'name' => 'Оформление гаража',
							'link' => url('oformlenie-garazha-v-sobstvennost')
						],
						[
							'name' => 'Определение порядка пользования',
							'link' => url('poryadok-polzovaniya-zhilym-pomeshcheniem-kvartiroj-zemlej-imushchestvom')
						],
						[
							'name' => 'Оформление квартиры',
							'link' => url('oformlenie-kvartiry')
						],
						[
							'name' => 'Переоформление прав',
							'link' => url('pereoformlenie-prav')
						],
						[
							'name' => 'Признание права собственности',
							'link' => url('priznanie-prava-sobstvennosti-dom-nedvizhimost-zemelnyj-uchastok')
						],
						[
							'name' => 'Регистрация дома',
							'link' => url('registraciya-doma')
						],
						[
							'name' => 'Дарение недвижимости',
							'link' => url('darenie-nedvizhimosti-pomoshch-yurista')
						],
						[
							'name' => 'Согласование перепланировки под ключ',
							'link' => url('soglasovanie-pereplanirovki-pod-klyuch')
						],
					],
				],
				[
					'name' => 'Все по оформлению недвижимости',
					'items' => [
						[
							'name' => 'Оформление прав собственности',
							'link' => url('oformlenie-prav')
						],
						[
							'name' => 'Тех. план на объект недвижимости',
							'link' => url('tekhnicheskij-plan-na-obekt-nedvizhimosti')
						],
						[
							'name' => 'Легализация самовольных построек',
							'link' => url('oformlenie-uzakonenie-samovolnyh-postroek')
						],
						[
							'name' => 'Оформление дома',
							'link' => url('oformlenie-doma-na-uchastke-v-sobstvennost-dokumenty')
						],
						[
							'name' => 'Оформление гаража',
							'link' => url('oformlenie-garazha-v-sobstvennost')
						],
						[
							'name' => 'Определение порядка пользования',
							'link' => url('poryadok-polzovaniya-zhilym-pomeshcheniem-kvartiroj-zemlej-imushchestvom')
						],
						[
							'name' => 'Оформление квартиры',
							'link' => url('oformlenie-kvartiry')
						],
						[
							'name' => 'Переоформление прав',
							'link' => url('pereoformlenie-prav')
						],
						[
							'name' => 'Признание права собственности',
							'link' => url('priznanie-prava-sobstvennosti-dom-nedvizhimost-zemelnyj-uchastok')
						],
						[
							'name' => 'Регистрация дома',
							'link' => url('registraciya-doma')
						],
						[
							'name' => 'Дарение недвижимости',
							'link' => url('darenie-nedvizhimosti-pomoshch-yurista')
						],
						[
							'name' => 'Согласование перепланировки под ключ',
							'link' => url('soglasovanie-pereplanirovki-pod-klyuch')
						],
					],
				],
				[
					'name' => 'Перевод недвижимости',
					'items' => [
						[
							'name' => 'Перевод из долевой собственности',
							'link' => url('perevod-iz-dolevoj-sobstvennosti-nedvizhimosti-v-sovmestnuyu')
						],
						[
							'name' => 'Перевод недвижимости в ипотеке',
							'link' => url('perevod-pomeshcheniya-yurist-advokat-spb-moskva')
						],
						[
							'name' => 'Перевод недвижимости цена, стоимость',
							'link' => url('perevod-nedvizhimosti-spb-moskva')
						],
						[
							'name' => 'Признание помещения нежилым',
							'link' => url('priznanie-zhilya-pomeshcheniya-doma-nezhilym-konsultacii-yurist')
						],
						[
							'name' => 'Консультация по переводу в нежилой фонд',
							'link' => url('konsultaciya-yurista-advokata-po-perevodu-v-nezhiloj-fond-spb-moskva')
						],
					],
				],
			],
		],
	],
	[
		'name' => 'Жилищное право',
		'items' => [
			'popular' => [
				[
					'name' => 'Жилищные споры',
					'link' => url('zhilishchnye-spory-s-sosedyami-zhsk-tszh-uk-advokaty-yuristy')
				],
				[
					'name' => 'Выселение и защита от выселения',
					'link' => url('vyselit-iz-kvartiry-zhilogo-pomeshcheniya')
				],
				[
					'name' => 'Улучшение жилищных условий',
					'link' => url('uluchshenie-zhilishchnyh-uslovij-uchet-ochered-yuridicheskaya-pomoshch')
				],
				[
					'name' => 'Затопление квартиры',
					'link' => url('zaliv-zatoplenie-kvartiry-ocenka-ushcherba-pomoshch-yurist')
				],
			],
			'free' => [
				[
					'name' => 'Консультация по жилищным вопросам',
					'link' => url('yuridicheskaya-konsultaciya-po-zhilishchnym-voprosam')
				],
				[
					'name' => 'Разрешение жилищных споров',
					'link' => url('zhilishchnye-spory-s-sosedyami-zhsk-tszh-uk-advokaty-yuristy')
				],
				[
					'name' => 'Решение жилищного вопроса',
					'link' => url('zhilishchnyj-vopros-bystroe-reshenie-konsultacii-besplatno-yuristy-advokaty')
				],
			],
			'specialists_1' => [
				[
					'name' => 'Жилищный юрист/',
					'link' => url('zhilishchnyj-advokat-spory-konsultaciya-besplatno')
				],
				[
					'name' => 'юрист',
					'link' => url('yurist-po-zhilishchnym-voprosam-v-vashem-rajone-advokat')
				],
				[
					'name' => 'Военный жилищный юрист',
					'link' => url('voennyj-zhilishchnyj-yurist-konsultacii-besplatno-ipoteka-yurist-advokat')
				],
				[
					'name' => 'Жилищный юрист в вашем районе',
					'link' => url('yurist-po-zhilishchnym-voprosam-v-vashem-rajone-advokat')
				],
			],
			'specialists_2' => [
				[
					'name' => 'Перевод недвижимости',
					'items' => [
						[
							'name' => 'Перевод из долевой собственности',
							'link' => url('perevod-iz-dolevoj-sobstvennosti-nedvizhimosti-v-sovmestnuyu')
						],
						[
							'name' => 'Перевод недвижимости в ипотеке',
							'link' => url('perevod-pomeshcheniya-yurist-advokat-spb-moskva')
						],
						[
							'name' => 'Перевод недвижимости цена, стоимость',
							'link' => url('perevod-nedvizhimosti-spb-moskva')
						],
						[
							'name' => 'Признание помещения нежилым',
							'link' => url('priznanie-zhilya-pomeshcheniya-doma-nezhilym-konsultacii-yurist')
						],
						[
							'name' => 'Консультация по переводу в нежилой фонд',
							'link' => url('konsultaciya-yurista-advokata-po-perevodu-v-nezhiloj-fond-spb-moskva')
						],
					],
				],
				[
					'name' => 'Жилищные споры',
					'items' => [
						[
							'name' => 'Ведение дела в суде по жилищным вопросам',
							'link' => url('vedenie-dela-v-sude-po-zhilishchnym-voprosam-pomoshch-yurista')
						],
						[
							'name' => 'Жилищный вопрос',
							'link' => url('zhilishchnyj-vopros-bystroe-reshenie-konsultacii-besplatno-yuristy-advokaty')
						],
						[
							'name' => 'Признание права собственности',
							'link' => url('priznanie-prava-sobstvennosti-dom-nedvizhimost-zemelnyj-uchastok')
						],
						[
							'name' => 'Расторжение договора ЖСК',
							'link' => url('rastorzhenie-dogovora-zhsk-prekrashchenie-chlenstva-yurist-advokat')
						],
						[
							'name' => 'Споры с управляющей компанией',
							'link' => url('spory-upravlyayushchej-kompaniej-tszh-zhilishchnym-voprosam-yurist')
						],
						[
							'name' => 'Судебная практика по жилищным спорам',
							'link' => url('sudebnaya-praktika-po-zhilishchnym-sporam-zhilishchnyj-advokat-yurist')
						],
						[
							'name' => 'Споры с администрацией',
							'link' => url('spory-s-administraciej-pomoshch-yurista-zemelnye-zhilishchnye')
						],
						[
							'name' => 'Вселение и выселение граждан',
							'link' => url('vselenie-grazhdan-sobstvennika-kvartiru-sud')
						],
						[
							'name' => 'Оформление жилищных прав',
							'link' => url('oformlenie-zhilishchnyh-prav-pomoshch-yurista')
						],
					],
				],
				[
					'name' => 'Жилищные споры',
					'items' => [
						[
							'name' => 'Ведение дела в суде по жилищным вопросам',
							'link' => url('vedenie-dela-v-sude-po-zhilishchnym-voprosam-pomoshch-yurista')
						],
						[
							'name' => 'Жилищный вопрос',
							'link' => url('zhilishchnyj-vopros-bystroe-reshenie-konsultacii-besplatno-yuristy-advokaty')
						],
						[
							'name' => 'Признание права собственности',
							'link' => url('priznanie-prava-sobstvennosti-dom-nedvizhimost-zemelnyj-uchastok')
						],
						[
							'name' => 'Расторжение договора ЖСК',
							'link' => url('rastorzhenie-dogovora-zhsk-prekrashchenie-chlenstva-yurist-advokat')
						],
						[
							'name' => 'Споры с управляющей компанией',
							'link' => url('spory-upravlyayushchej-kompaniej-tszh-zhilishchnym-voprosam-yurist')
						],
						[
							'name' => 'Судебная практика по жилищным спорам',
							'link' => url('sudebnaya-praktika-po-zhilishchnym-sporam-zhilishchnyj-advokat-yurist')
						],
						[
							'name' => 'Споры с администрацией',
							'link' => url('spory-s-administraciej-pomoshch-yurista-zemelnye-zhilishchnye')
						],
						[
							'name' => 'Вселение и выселение граждан',
							'link' => url('vselenie-grazhdan-sobstvennika-kvartiru-sud')
						],
						[
							'name' => 'Оформление жилищных прав',
							'link' => url('oformlenie-zhilishchnyh-prav-pomoshch-yurista')
						],
					],
				],
				[
					'name' => 'Улучшение жилищных условий',
					'items' => [
						[
							'name' => 'Восстановление в очереди на жильё',
							'link' => url('vosstanovlenie-ocheredi-zhilyo-uluchshenie-zhilishchnyh-uslovij-konsultaciya')
						],
						[
							'name' => 'Постановка на учет нуждающихся в улучшении жилья',
							'link' => url('postanovka-na-uchet-nuzhdayushchihsya-v-uluchshenii-zhilya-yurist-spb')
						],
					],
				],
			],
		],
	],
	[
		'name' => 'ДДУ и ЖСК',
		'items' => [
			'popular' => [
				[
					'name' => 'Проверка ДДУ',
					'link' => url('proverka-ddu-dogovor-dolevogo-uchastiya-zastrojshchika-dolevka')
				],
				[
					'name' => 'Взыскание неустойки по ДДУ',
					'link' => url('vzyskanie-neustojki-ddu-peni-dolevka-214-fz-zastrojshchik-zhsk')
				],
				[
					'name' => 'Задержка сдачи дома',
					'link' => url('zaderzhka-sdachi-doma-pomoshch-yurista')
				],
				[
					'name' => 'Споры по ДДУ',
					'link' => url('spory-ddu-dolevka-214-fz-prosrochka-dolevoe-stroitelstvo-sud')
				],
				[
					'name' => 'Сопровождение сделок с ДДУ',
					'link' => url('sdelka-dolevka-ddu-214-fz-dolevoe-zastrojshchik-prosrochka-sroka-zhsk')
				],
				[
					'name' => 'Расторжение договора ЖСК',
					'link' => url('rastorzhenie-dogovora-zhsk-prekrashchenie-chlenstva-yurist-advokat')
				],
				[
					'name' => 'Банкротство застройщиков по ДДУ',
					'link' => url('bankrotstvo-zastrojshchika-pri-dolevom-stroitelstve')
				],
			],
			'free' => [
				[
					'name' => 'Бесплатные консультации по ДДУ',
					'link' => url('konsultaciya-yurista-advokata-besplatno-ddu-dolyovka-214-fz#')
				],
				[
					'name' => 'Юридическая помощь по ДДУ',
					'link' => url('yuridicheskaya-pomoshch-dolevka-ddu-214-fz-zastrojshchik-zhsk')
				],
				[
					'name' => 'Юридическая помощь обманутым дольщикам',
					'link' => url('yuridicheskaya-pomoshch-dolshchikam-yurist')
				],
			],
			'specialists_1' => [
				[
					'name' => 'Юрист по ДДУ',
					'link' => url('yurist-po-ddu-pomoshch-vzyskanie-neustojki')
				],
				[
					'name' => 'Помощь пайщикам ЖК "Силы природы"',
					'link' => url('pomoshch-pajshchikam-zhk-sily-prirody')
				],
				[
					'name' => 'Банкротство О2 Development ',
					'link' => url('bankrotstvo-zastrojshchika-o2-development-zhk-sily-prirody')
				],
				[
					'name' => 'Банкротство «Романтика» ',
					'link' => url('bankrotstvo-zastrojshchika-ooo-romantika')
				],
				[
					'name' => 'Полис Групп',
					'link' => url('vzyskanie-neustojki-sud-ooo-polis-grupp')
				],
				[
					'name' => 'Норманн',
					'link' => url('spor-s-zastrojshchikom-normann-dolevoe-uchastie')
				],
				[
					'name' => 'ЛенСпецСтрой',
					'link' => url('spory-s-ooo-lenspecstroj-vzyskanie-neustojki-sud')
				],
				[
					'name' => 'Дальпитерстрой',
					'link' => url('spory-s-zastrojshchikom-ooo-stroitelnaya-kompaniya-dalpiterstroj')
				],
				[
					'name' => 'СПб Реновация',
					'link' => url('spory-s-zastrojshchikom-ooo-spb-renovaciya')
				],
				[
					'name' => 'Главстрой - СПб',
					'link' => url('spory-s-zastrojshchikom-ooo-glavstroj-spb')
				],
				[
					'name' => 'Лидер Групп',
					'link' => url('spory-po-ddu-s-gk-lider-grupp')
				],
				[
					'name' => 'ПИК',
					'link' => url('spory-s-zastrojshchikom-po-ddu-gk-pik')
				],
				[
					'name' => 'ИПС',
					'link' => url('spory-po-ddu-s-zastrojshchikom-ao-sk-ips')
				],
				[
					'name' => 'Петрострой',
					'link' => url('spory-po-ddu-s-zastrojshchikom-ooo-petrostroj')
				],
				[
					'name' => 'Нева Строй Инвест',
					'link' => url('spory-po-ddu-s-zastrojshchikom-ooo-neva-stroj-invest')
				],
				[
					'name' => 'Л1-4 (ЛЭК)',
					'link' => url('spory-po-ddu-s-zastrojshchikom-ooo-l1-4-lehk')
				],
				[
					'name' => 'Петротрест',
					'link' => url('spory-po-ddu-s-zastrojshchikom-petrotrest')
				],
				[
					'name' => 'Центр Долевого Строительства (ЦДС)',
					'link' => url('spory-po-ddu-s-zastrojshchikom-centr-dolevogo-stroitelstva-cds')
				],
				[
					'name' => 'ГК "Легенда"',
					'link' => url('spory-po-ddu-s-zastrojshchikom-legenda')
				],
				[
					'name' => 'ЛенСпецСМУ',
					'link' => url('spory-po-ddu-s-zastrojshchikom-lenspecsmu')
				],
				[
					'name' => 'СУ-155',
					'link' => url('spory-po-ddu-s-zastrojshchikom-su-155')
				],
				[
					'name' => 'Россторой',
					'link' => url('spory-po-ddu-s-zastrojshchikom-rosstroj')
				],
				[
					'name' => 'ООО "Титан"',
					'link' => url('spory-po-ddu-s-zastrojshchikom-titan')
				],
				[
					'name' => 'ГК "Город"',
					'link' => url('spory-po-ddu-s-zastrojshchikom-gk-gorod')
				],
				[
					'name' => 'ЛСР',
					'link' => url('spory-po-ddu-s-zastrojshchikom-lsr')
				],
				[
					'name' => 'Сэтл Сити (Setl City)',
					'link' => url('spory-po-ddu-s-zastrojshchikom-sehtl-siti')
				],
				[
					'name' => 'Северный город',
					'link' => url('spory-po-ddu-s-zastrojshchikom-severnyj-gorod')
				],
				[
					'name' => 'Арсенал-недвижимость',
					'link' => url('spory-po-ddu-s-zastrojshchikom-arsenal-nedvizhimost')
				],
				[
					'name' => 'RBI',
					'link' => url('spory-po-ddu-s-zastrojshchikom-rbi')
				],
				[
					'name' => 'РосСтройИнвест',
					'link' => url('spory-po-ddu-s-zastrojshchikom-rosstrojinvest')
				],
				[
					'name' => 'Лидер',
					'link' => url('spory-po-ddu-s-zastrojshchikom-lider')
				],
				[
					'name' => 'ГК "Эталон"',
					'link' => url('spory-po-ddu-s-zastrojshchikom-gk-ehtalon')
				],
				[
					'name' => 'Строительное управление',
					'link' => url('spory-po-ddu-s-zastrojshchikom-stroitelnoe-upravlenie')
				],
				[
					'name' => 'Импульс',
					'link' => url('spory-po-ddu-s-zastrojshchikom-impuls')
				],
				[
					'name' => 'Возрождение Санкт-Петербурга',
					'link' => url('spory-po-ddu-s-zastrojshchikom-vozrozhdenie-sankt-peterburga')
				],
				[
					'name' => 'Петрополь',
					'link' => url('spory-po-ddu-s-zastrojshchikom-petropol')
				],
				[
					'name' => '47 трест',
					'link' => url('spory-po-ddu-s-47-trest')
				],
				[
					'name' => 'AAG',
					'link' => url('spory-s-zastrojshchikom-aag')
				],
				[
					'name' => 'Aiber Group',
					'link' => url('spory-s-zastrojshchikom-aiber-group')
				],
				[
					'name' => 'Bonava (NCC)',
					'link' => url('spory-s-zastrojshchikom-bonava-ncc')
				],
				[
					'name' => 'ГК "Астра"',
					'link' => url('spory-s-zastrojshchikom-po-ddu-s-astra')
				],
				[
					'name' => 'Glorax Development',
					'link' => url('spory-po-ddu-s-zastrojshchikom-glorax-development')
				],
				[
					'name' => 'ЮИТ',
					'link' => url('spory-s-zastrojshchikom-yuit-sankt-peterburg')
				],
				[
					'name' => 'EKE',
					'link' => url('spory-s-zastrojshchikom-po-ddu-s-eke')
				],
				[
					'name' => 'IMD Group',
					'link' => url('spory-s-zastrojshchikom-imd-group')
				],
				[
					'name' => 'Северный Город',
					'link' => url('spory-s-zastrojshchikom-severnyj-gorod')
				],
			],
			'specialists_2' => [
				[
					'name' => 'Уступка и переуступка прав по ДДУ',
					'link' => url('ustupka-pereustupka-prav-trebovaniya-ddu-214-fz')
				],
				[
					'name' => 'Уступка и переуступка прав по ДДУ',
					'link' => url('ustupka-pereustupka-prav-trebovaniya-ddu-214-fz')
				],
				[
					'name' => 'Все услуги по ДДУ',
					'items' => [
						[
							'name' => 'Операции с ДДУ',
							'link' => url('dogovor-ddu-pomoshch-yurista')
						],
						[
							'name' => 'Оформление ДДУ',
							'link' => url('oformlenie-ddu-pomoshch-yurista')
						],
						[
							'name' => 'Претензии по ДДУ',
							'link' => url('pretenzii-ddu-214-fz-neustojka-zastrojshchik-prosrochka')
						],
						[
							'name' => 'Признание ДДУ недействительным',
							'link' => url('priznanie-ddu-nedejstvitelnym-nezaklyuchennym-214-fz-sud')
						],
						[
							'name' => 'Расторжение договора долевого участия',
							'link' => url('rastorzhenie-dogovora-dolevogo-uchastiya-ddu')
						],
						[
							'name' => 'Регистрация ДДУ',
							'link' => url('registraciya-ddu-dogovor-dolevogo-uchastiya-dolyovka-otkaz-yurist')
						],
						[
							'name' => 'Сопровождение заключения договора ДУ',
							'link' => url('soprovozhdenie-zaklyucheniya-dogovora-dolevogo-uchastiya')
						],
						[
							'name' => 'Иск по ДДУ',
							'link' => url('isk-ddu-pomoshch-yurista')
						],
					],
				],
				[
					'name' => 'Все услуги по ДДУ',
					'items' => [
						[
							'name' => 'Операции с ДДУ',
							'link' => url('dogovor-ddu-pomoshch-yurista')
						],
						[
							'name' => 'Оформление ДДУ',
							'link' => url('oformlenie-ddu-pomoshch-yurista')
						],
						[
							'name' => 'Претензии по ДДУ',
							'link' => url('pretenzii-ddu-214-fz-neustojka-zastrojshchik-prosrochka')
						],
						[
							'name' => 'Признание ДДУ недействительным',
							'link' => url('priznanie-ddu-nedejstvitelnym-nezaklyuchennym-214-fz-sud')
						],
						[
							'name' => 'Расторжение договора долевого участия',
							'link' => url('rastorzhenie-dogovora-dolevogo-uchastiya-ddu')
						],
						[
							'name' => 'Регистрация ДДУ',
							'link' => url('registraciya-ddu-dogovor-dolevogo-uchastiya-dolyovka-otkaz-yurist')
						],
						[
							'name' => 'Сопровождение заключения договора ДУ',
							'link' => url('soprovozhdenie-zaklyucheniya-dogovora-dolevogo-uchastiya')
						],
						[
							'name' => 'Иск по ДДУ',
							'link' => url('isk-ddu-pomoshch-yurista')
						],
					],
				],
				[
					'name' => 'Взыскание неустойки по ДДУ',
					'items' => [
						[
							'name' => 'Взыскание неустойки за квартиру',
							'link' => url('vzyskanie-neustojki-za-kvartiru-po-dogovoru-du-spb-moskva')
						],
						[
							'name' => 'Спец. центр по вопросам взыскания неустойки ',
							'link' => url('vzyskanie-neustojki-214-sudebnaya-praktika-yurist-advokat-ddu-spb-moskva')
						],
						[
							'name' => 'Взыскание неустойки с застройщика',
							'link' => url('ddu-dolevoe-stroitelstvo-sud-yurist')
						],
						[
							'name' => 'Досудебное взыскание неустойки по ДДУ',
							'link' => url('pretenziya-neustojka-po-ddu-pomoshch-spb-moskva')
						],
						[
							'name' => 'Неустойка за просрочку сдачи дома',
							'link' => url('vzyskanie-neustojki-yurist-advokat-spb-moskva')
						],
						[
							'name' => 'Неустойка по договору подряда',
							'link' => url('neustojka-po-dogovoru-podryada-pomoshch-konsultacii-besplatno-yurist')
						],
						[
							'name' => 'Неустойка по долевому строительству',
							'link' => url('neustojka-po-dolevomu-stroitelstvu-yuristy-advokaty-spb')
						],
						[
							'name' => 'Неустойка по просрочке ДДУ',
							'link' => url('neustojka-prosrochka-dogovora-dolevogo-uchastiya-yurist-advokat-spb')
						],
						[
							'name' => 'Неустойка, штраф по ДДУ',
							'link' => url('neustojka-shtraf-po-ddu-konsultaciya-besplatno-yuristy-spb-moskva')
						],
						[
							'name' => 'Расчет выплат по ДДУ',
							'link' => url('ddu-raschet')
						],
					],
				],
				[
					'name' => 'Взыскание неустойки по ДДУ',
					'items' => [
						[
							'name' => 'Взыскание неустойки за квартиру',
							'link' => url('vzyskanie-neustojki-za-kvartiru-po-dogovoru-du-spb-moskva')
						],
						[
							'name' => 'Спец. центр по вопросам взыскания неустойки ',
							'link' => url('vzyskanie-neustojki-214-sudebnaya-praktika-yurist-advokat-ddu-spb-moskva')
						],
						[
							'name' => 'Взыскание неустойки с застройщика',
							'link' => url('ddu-dolevoe-stroitelstvo-sud-yurist')
						],
						[
							'name' => 'Досудебное взыскание неустойки по ДДУ',
							'link' => url('pretenziya-neustojka-po-ddu-pomoshch-spb-moskva')
						],
						[
							'name' => 'Неустойка за просрочку сдачи дома',
							'link' => url('vzyskanie-neustojki-yurist-advokat-spb-moskva')
						],
						[
							'name' => 'Неустойка по договору подряда',
							'link' => url('neustojka-po-dogovoru-podryada-pomoshch-konsultacii-besplatno-yurist')
						],
						[
							'name' => 'Неустойка по долевому строительству',
							'link' => url('neustojka-po-dolevomu-stroitelstvu-yuristy-advokaty-spb')
						],
						[
							'name' => 'Неустойка по просрочке ДДУ',
							'link' => url('neustojka-prosrochka-dogovora-dolevogo-uchastiya-yurist-advokat-spb')
						],
						[
							'name' => 'Неустойка, штраф по ДДУ',
							'link' => url('neustojka-shtraf-po-ddu-konsultaciya-besplatno-yuristy-spb-moskva')
						],
						[
							'name' => 'Расчет выплат по ДДУ',
							'link' => url('ddu-raschet')
						],
					],
				],
				[
					'name' => 'Споры по ДДУ',
					'items' => [
						[
							'name' => 'Споры в строительстве',
							'link' => url('spory-v-stroitelstve-dolevoe-uchastie-zastrojshchik-konsultaciya')
						],
						[
							'name' => 'Споры по ДДУ в суде',
							'link' => url('ddu-sud-rastorzhenie-neustojka')
						],
						[
							'name' => 'Расторжение договора ЖСК',
							'link' => url('rastorzhenie-dogovora-zhsk-prekrashchenie-chlenstva-yurist-advokat')
						],
					],
				],
			],
		],
	],
	[
		'name' => 'Наследство',
		'items' => [
			'popular' => [
				[
					'name' => 'Восстановление сроков наследства',
					'link' => url('vosstanovlenie-srokov-nasledstva-sud-yurist-advokat')
				],
				[
					'name' => 'Оформление наследства',
					'link' => url('oformlenie-nasledstva-zaveshchanie-dokumenty')
				],
				[
					'name' => 'Получение наследства',
					'link' => url('poluchenie-nasledstva-spb-moskva')
				],
				[
					'name' => 'Споры по наследству',
					'link' => url('spory-po-nasledstvu-konsultacii-besplatno-spb-moskva')
				],
				[
					'name' => 'Консультация по наследственным вопросам',
					'link' => url('yuridicheskaya-konsultaciya-nasledstvennye-voprosy-spory-yurist-advokat')
				],
				[
					'name' => 'Юридические услуги по наследству',
					'link' => url('advokat-nasledstvo-prinyatie-oformlenie-vstuplenie')
				],
			],
			'free' => [
				[
					'name' => 'Бесплатная консультация по наследству',
					'link' => url('konsultaciya-po-nasledstvennomu-voprosu-spb-moskva')
				],
				[
					'name' => 'Разрешение споров по наследству',
					'link' => url('spory-po-nasledstvu-konsultacii-besplatno-spb-moskva')
				],
				[
					'name' => 'Юридическая помощь по оформлению наследства',
					'link' => url('oformlenie-nasledstva-zaveshchanie-dokumenty')
				],
			],
			'specialists_1' => [
				[
					'name' => 'Юрист по завещаниям',
					'link' => url('advokat-po-zaveshchaniyam')
				],
				[
					'name' => 'Юрист',
					'link' => url('yurist-po-zaveshchaniyam-spb')
				],
				[
					'name' => 'Наследственный юрист/',
					'link' => url('advokat-nasledstvo-prinyatie-oformlenie-vosstanovlenie')
				],
				[
					'name' => 'юрист',
					'link' => url('nasledstvennyj-yurist-zaveshchaniya-priznanie-prava-nedostojnym-sud')
				],
			],
			'specialists_2' => [
				[
					'name' => 'Споры по ДДУ',
					'items' => [
						[
							'name' => 'Споры в строительстве',
							'link' => url('spory-v-stroitelstve-dolevoe-uchastie-zastrojshchik-konsultaciya')
						],
						[
							'name' => 'Споры по ДДУ в суде',
							'link' => url('ddu-sud-rastorzhenie-neustojka')
						],
						[
							'name' => 'Расторжение договора ЖСК',
							'link' => url('rastorzhenie-dogovora-zhsk-prekrashchenie-chlenstva-yurist-advokat')
						],
					],
				],
				[
					'name' => 'Восстановление наследства',
					'items' => [
						[
							'name' => 'Восстановление принятия наследства',
							'link' => url('vosstanovlenie-v-pravah-na-prinyatie-nasledstva-spb-moskva')
						],
						[
							'name' => 'Восстановление пропущенного срока принятия наследства',
							'link' => url('vosstanovlenie-propushchennogo-nasledstva-srokov-spb-moskva')
						],
					],
				],
				[
					'name' => 'Восстановление наследства',
					'items' => [
						[
							'name' => 'Восстановление принятия наследства',
							'link' => url('vosstanovlenie-v-pravah-na-prinyatie-nasledstva-spb-moskva')
						],
						[
							'name' => 'Восстановление пропущенного срока принятия наследства',
							'link' => url('vosstanovlenie-propushchennogo-nasledstva-srokov-spb-moskva')
						],
					],
				],
				[
					'name' => 'Оформление наследства',
					'items' => [
						[
							'name' => 'Оформление наследства за границей',
							'link' => url('oformlenie-nasledstva-za-granicej-yuristy-advokaty-spb-moskva')
						],
						[
							'name' => 'Оформление наследства в собственность',
							'link' => url('oformit-nasledstvo-v-sobstvennost-konsultacii-besplatno')
						],
						[
							'name' => 'Вступление в наследство',
							'link' => url('vstuplenie-v-nasledstvo-po-zaveshchaniyu-konsultacii-besplatno#')
						],
						[
							'name' => 'Открытие наследства',
							'link' => url('otkrytie-nasledstva-spb-moskva-pomoshch-yurista')
						],
						[
							'name' => 'Наследование бизнеса',
							'link' => url('nasledovanie-biznesa-yuridicheskoe-soprovozhdenie-spb-moskva')
						],
						[
							'name' => 'Наследование имущества',
							'link' => url('nasledstvo-imushchestva-konsultacii-besplatno-spb-moskva')
						],
						[
							'name' => 'Наследование квартиры',
							'link' => url('nasledstvo-kvartiry-oformlenie-vstuplenie-spb-moskva')
						],
						[
							'name' => 'Услуги по оформлению наследства',
							'link' => url('stoimost-uslug-po-oformleniyu-nasledstva-spb-moskva')
						],
					],
				],
				[
					'name' => 'Оформление наследства',
					'items' => [
						[
							'name' => 'Оформление наследства за границей',
							'link' => url('oformlenie-nasledstva-za-granicej-yuristy-advokaty-spb-moskva')
						],
						[
							'name' => 'Оформление наследства в собственность',
							'link' => url('oformit-nasledstvo-v-sobstvennost-konsultacii-besplatno')
						],
						[
							'name' => 'Вступление в наследство',
							'link' => url('vstuplenie-v-nasledstvo-po-zaveshchaniyu-konsultacii-besplatno#')
						],
						[
							'name' => 'Открытие наследства',
							'link' => url('otkrytie-nasledstva-spb-moskva-pomoshch-yurista')
						],
						[
							'name' => 'Наследование бизнеса',
							'link' => url('nasledovanie-biznesa-yuridicheskoe-soprovozhdenie-spb-moskva')
						],
						[
							'name' => 'Наследование имущества',
							'link' => url('nasledstvo-imushchestva-konsultacii-besplatno-spb-moskva')
						],
						[
							'name' => 'Наследование квартиры',
							'link' => url('nasledstvo-kvartiry-oformlenie-vstuplenie-spb-moskva')
						],
						[
							'name' => 'Услуги по оформлению наследства',
							'link' => url('stoimost-uslug-po-oformleniyu-nasledstva-spb-moskva')
						],
					],
				],
				[
					'name' => 'Все по принятию наследства',
					'items' => [
						[
							'name' => 'Признание права собственности на наследство',
							'link' => url('priznanie-prava-sobstvennosti-na-nasledstvo-spb-moskva')
						],
						[
							'name' => 'Фактическое принятие наследства',
							'link' => url('fakticheskoe-prinyatie-nasledstva-dokazat-osporit-yurist-sud')
						],
						[
							'name' => 'Принятие наследства по завещанию',
							'link' => url('nasledstvo-po-zaveshchaniyu-konsultacii-besplatno-spb-moskva')
						],
					],
				],
				[
					'name' => 'Споры по наследству',
					'items' => [
						[
							'name' => 'Оспаривание завещания',
							'link' => url('osparivanie-zaveshchaniya-pomoshch-yurista-spb')
						],
						[
							'name' => 'Оспаривание долгов по наследству',
							'link' => url('dolgi-nasledstvu-osparivanie-konsultaciya-besplatno-yurist')
						],
						[
							'name' => 'Раздел наследства',
							'link' => url('razdel-nasledstva-imushchestva-doli-doma-sud')
						],
						[
							'name' => 'Получение наследства через суд',
							'link' => url('osparivanie-zaveshchaniya-yuristy-advokaty-spb-moskva')
						],
						[
							'name' => 'Отказ от наследства',
							'link' => url('otkaz-ot-nasledstva-sostavlenie-zayavleniya-spb-moskva')
						],
					],
				],
			],
		],
	],
	[
		'name' => 'Трудовое право',
		'items' => [
			'popular' => [
				[
					'name' => 'Трудовые споры',
					'link' => url('reshenie-trudovyh-sporov-sud-yurist-advokat')
				],
				[
					'name' => 'Восстановление на работе',
					'link' => url('vosstanovlenie-rabote-uvolennogo-sotrudnika-isk-zayavlenie-sud')
				],
				[
					'name' => 'Незаконное увольнение',
					'link' => url('nezakonnoe-uvolnenie-konsultacii-onlain-besplatno-spb-moskva')
				],
				[
					'name' => 'Задержка заработной платы',
					'link' => url('zaderzhka-zarabotnoj-platy-rabotodatelem-kompensaciya')
				],
				[
					'name' => 'Проверка трудовой инспекции',
					'link' => url('vneplanovaya-proverka-organizacii-trudovoj-inspekcii')
				],
			],
			'free' => [
				[
					'name' => 'Консультация по трудовому праву',
					'link' => url('konsultaciya-yurista-advokata-po-trudovomu-pravu-besplatno')
				],
				[
					'name' => 'Услуги по трудовому праву',
					'link' => url('uslugi-yurista-advokata-po-trudovym-voprosam')
				],
				[
					'name' => 'Помощь по трудовым вопросам',
					'link' => url('yuridicheskaya-pomoshch-po-trudovym-voprosam')
				],
			],
			'specialists_1' => [
				[
					'name' => 'Трудовой юрист/',
					'link' => url('trudovoj-advokat-spory-prava-voprosy')
				],
				[
					'name' => 'юрист',
					'link' => url('trudovoj-yurist-spory-konsultacii-besplatno')
				],
			],
			'specialists_2' => [
				[
					'name' => 'Споры по наследству',
					'items' => [
						[
							'name' => 'Оспаривание завещания',
							'link' => url('osparivanie-zaveshchaniya-pomoshch-yurista-spb')
						],
						[
							'name' => 'Оспаривание долгов по наследству',
							'link' => url('dolgi-nasledstvu-osparivanie-konsultaciya-besplatno-yurist')
						],
						[
							'name' => 'Раздел наследства',
							'link' => url('razdel-nasledstva-imushchestva-doli-doma-sud')
						],
						[
							'name' => 'Получение наследства через суд',
							'link' => url('osparivanie-zaveshchaniya-yuristy-advokaty-spb-moskva')
						],
						[
							'name' => 'Отказ от наследства',
							'link' => url('otkaz-ot-nasledstva-sostavlenie-zayavleniya-spb-moskva')
						],
					],
				],
				[
					'name' => 'Трудовые споры',
					'items' => [
						[
							'name' => 'Споры с работодателем',
							'link' => url('spory-s-rabotodatelem-konsultacii-besplatno-spb-moskva')
						],
						[
							'name' => 'Суд по трудовому спору',
							'link' => url('sud-po-trudovomu-sporu-s-rabotodatelem-spb-moskva')
						],
						[
							'name' => 'Защита прав работодателя ',
							'link' => url('zashchita-prav-interesov-rabotodatelya')
						],
						[
							'name' => 'Вынужденное увольнение',
							'link' => url('vynuzhdenie-uvolneniyu-raboty-beremennoj-pensionera')
						],
						[
							'name' => 'Здержка заработной платы',
							'link' => url('zaderzhka-zarabotnoj-platy-rabotodatelem-kompensaciya')
						],
					],
				],
			],
		],
	],
	[
		'name' => 'Строительство',
		'items' => [
			'popular' => [
				[
					'name' => 'Споры в строительстве',
					'link' => url('spory-v-stroitelstve-dolevoe-uchastie-zastrojshchik-konsultaciya')
				],
				[
					'name' => 'Получение разрешения на строительство',
					'link' => url('poluchenie-razreshenie-na-stroitelstvo')
				],
				[
					'name' => 'Получение технических условий на объект строительства',
					'link' => url('tekhnicheskie-usloviya-podklyuchenie-poluchenie')
				],
				[
					'name' => 'Комплексное освоение территории строительства',
					'link' => url('kompleksnoe-osvoenie-territorii-stroitelstva')
				],
			],
			'free' => [
				[
					'name' => 'Консультация юриста по строительству',
					'link' => url('stroitelnyj-yurist-konsultacii')
				],
				[
					'name' => 'Консультация юриста по долевому строительству',
					'link' => url('advokat-dolevomu-stroitelstvu-dolevka-ddu-214-fz')
				],
				[
					'name' => 'Юридическая консультация онлайн',
					'link' => url('besplatnyj-yurist-onlajn')
				],
			],
			'specialists_1' => [
				[
					'name' => 'Строительный юрист/',
					'link' => url('stroitelnyj-yurist-konsultacii')
				],
				[
					'name' => 'юрист',
					'link' => url('advokat-stroitelnye-dela-spory-voprosy-uslugi-sud')
				],
				[
					'name' => 'Юрист/',
					'link' => url('advokat-dolevomu-stroitelstvu-dolevka-ddu-214-fz')
				],
				[
					'name' => 'юрист по долевому строительству',
					'link' => url('yurist-dolevomu-stroitelstvu-ddu-214-fz')
				],
			],
			'specialists_2' => [
				[
					'name' => 'Трудовые споры',
					'items' => [
						[
							'name' => 'Споры с работодателем',
							'link' => url('spory-s-rabotodatelem-konsultacii-besplatno-spb-moskva')
						],
						[
							'name' => 'Суд по трудовому спору',
							'link' => url('sud-po-trudovomu-sporu-s-rabotodatelem-spb-moskva')
						],
						[
							'name' => 'Защита прав работодателя ',
							'link' => url('zashchita-prav-interesov-rabotodatelya')
						],
						[
							'name' => 'Вынужденное увольнение',
							'link' => url('vynuzhdenie-uvolneniyu-raboty-beremennoj-pensionera')
						],
						[
							'name' => 'Здержка заработной платы',
							'link' => url('zaderzhka-zarabotnoj-platy-rabotodatelem-kompensaciya')
						],
					],
				],
				[
					'name' => 'Споры в строительстве',
					'items' => [
						[
							'name' => 'Взыскание по ДДУ',
							'link' => url('ddu-dolevoe-stroitelstvo-sud-yurist#')
						],
						[
							'name' => 'Взыскание неустойки в строительстве',
							'link' => url('vzyskanie-neustojki-v-stroitelstve-advokat-spb-moskva')
						],
					],
				],
			],
		],
	],
	[
		'name' => 'Арбитраж',
		'items' => [
			'popular' => [
				[
					'name' => 'Арбитражные споры',
					'link' => url('arbitrazhnye-spory')
				],
				[
					'name' => 'Арбитражная защита',
					'link' => url('arbitrazh-zashchita-sud')
				],
				[
					'name' => 'Экспертиза в арбитражном процессе',
					'link' => url('ehkspertiza-arbitrazhnyj-process-naznachenie-vozrazheniya')
				],
				[
					'name' => 'Ходатайство в арбитражный суд',
					'link' => url('hodatajstvo-arbitrazhnyj-sud-sostavlenie-podacha-yurist')
				],
				[
					'name' => 'Ведение арбитражных дел',
					'link' => url('vedenie-arbitrazhnyh-del-predstavitelstvo-yurista')
				],
				[
					'name' => 'Арбитражный суд',
					'link' => url('arbitrazhnyj-sud')
				],
			],
			'free' => [
				[
					'name' => 'Консультация по арбитражным вопросам',
					'link' => url('konsultaciya-arbitrazhnye-dela-besplatno-onlajn-yurist-advokat-sud')
				],
				[
					'name' => 'Консультация по взысканию неустоек, алиментов',
					'link' => url('konsultaciya-yurista-po-vzyskaniyu-dolgov')
				],
			],
			'specialists_1' => [
				[
					'name' => 'Арбитражный юрист/',
					'link' => url('arbitrazhnyj-advokat-arbitrazhnye-dela')
				],
				[
					'name' => 'юрист',
					'link' => url('arbitrazhnyj-yurist-arbitrazhnye-dela-spory-voprosy-konsultaciya-sud')
				],
			],
			'specialists_2' => [
				[
					'name' => 'Споры в строительстве',
					'items' => [
						[
							'name' => 'Взыскание по ДДУ',
							'link' => url('ddu-dolevoe-stroitelstvo-sud-yurist#')
						],
						[
							'name' => 'Взыскание неустойки в строительстве',
							'link' => url('vzyskanie-neustojki-v-stroitelstve-advokat-spb-moskva')
						],
					],
				],
				[
					'name' => 'Арбитражные споры',
					'items' => [
						[
							'name' => 'Представительство в арбитражном суде',
							'link' => url('predstavitelstvo-arbitrazhnom-processe-sude')
						],
						[
							'name' => 'Услуги по арбитражным спорам',
							'link' => url('pomoshch-uslugi-arbitrazhnye-dela-spory-voprosy-konsultaciya-sud')
						],
						[
							'name' => 'Работа с отказами ',
							'link' => url('arbitrazh-rabota-otkazami-trebovanij-arbitrazhnyj-upravlyayushchij-sud')
						],
						[
							'name' => 'Составление арбитражного соглашения',
							'link' => url('sostavlenie-arbitrazhnogo-tretejskogo-soglasheniya-sud')
						],
						[
							'name' => 'Жалоба в арбитражный суд',
							'link' => url('arbitrazhnaya-zhaloba-sud-apellyacionnaya-kassacionnaya-nadzornaya')
						],
						[
							'name' => 'Защита в арбитражном суде',
							'link' => url('arbitrazhnyj-sud')
						],
						[
							'name' => 'Надежная помощь в арбитраже',
							'link' => url('arbitrazhnyj-spb-yurist-advokat-sud')
						],
						[
							'name' => 'Помощь в арбитраже СПб',
							'link' => url('arbitrazhnyj-spb-yurist-advokat-sud')
						],
						[
							'name' => 'Любые услуги в арбитраже',
							'link' => url('uslugi-arbitrazh')
						],
						[
							'name' => 'Стоимость арбитражных услуг',
							'link' => url('stoimost-arbitrazhnyh-uslug-yurist-spb-2017')
						],
					],
				],
			],
		],
	],
	[
		'name' => 'Взыскание',
		'items' => [
			'popular' => [
				[
					'name' => 'Взыскание залога',
					'link' => url('vzyskanie-zaloga')
				],
				[
					'name' => 'Взыскание всех видов неустоек',
					'link' => url('vzyskanie-neustoek-shtrafov-peni-cherez-sud')
				],
				[
					'name' => 'Взыскание убытков и упущенной выгоде',
					'link' => url('vzyskanie-ubytkov-upushchennoj-vygody-arbitrazh')
				],
				[
					'name' => 'Взыскание ущерба',
					'link' => url('vzyskanie-ushcherba-materialnogo-moralnogo-yurist')
				],
				[
					'name' => 'Возврат вкладов из БМ "Инвест"',
					'link' => url('pomoshch-vkladchikam-bm-invest')
				],
				[
					'name' => 'Взыскание компенсации за некачественный ремонт',
					'link' => url('nekachestvennyj-remont-kvartiry-pomoshch-yurista')
				],
			],
			'free' => [],
			'specialists_1' => [
				[
					'name' => 'Юрист по взысканию',
					'link' => url('advokat-vzyskaniyu-dolgov-alimentov-neustojki-zadolzhennosti')
				],
			],
			'specialists_2' => [
				[
					'name' => 'Взыскание неустойки в Арбитражном суде',
					'link' => url('vzyskanie-neustojki-arbitrazhnyj-sud-praktika')
				],
				[
					'name' => 'Взыскание неустойки в Арбитражном суде',
					'link' => url('vzyskanie-neustojki-arbitrazhnyj-sud-praktika')
				],
				[
					'name' => 'Взыскание неустоек',
					'items' => [
						[
							'name' => 'Взыскание неустойки в арбитражном суде',
							'link' => url('vzyskanie-neustojki-arbitrazhnyj-sud-praktika')
						],
						[
							'name' => 'Взыскание неустойки со страховой компании',
							'link' => url('vzyskanie-neustojki-strahovoj-kompanii-osago-kasko')
						],
						[
							'name' => 'Срок взыскания неустойки',
							'link' => url('srok-vzyskaniya-neustojki-yurist-spb-moskva')
						],
					],
				],
				[
					'name' => 'Взыскание убытков',
					'items' => [
						[
							'name' => 'Взыскание убытков в арбитражном суде',
							'link' => url('vzyskanie-ubytkov-arbitrazhnom-sude-obshchej-yurisdikcii')
						],
						[
							'name' => 'Взыскание по договору подряда',
							'link' => url('vzyskanie-ubytkov-dogovoru-podryada-postavki-arendy')
						],
						[
							'name' => 'Взыскание убытков при банкротстве',
							'link' => url('vzyskanie-ubytkov-bankrotstv%D0%BE-rukovoditelya-banka')
						],
						[
							'name' => 'Взыскание убытков с генерального директора',
							'link' => url('vzyskanie-ubytkov-generalnogo-byvshego-direktora')
						],
						[
							'name' => 'Взыскание убытков с застройщика',
							'link' => url('vzyskanie-zastrojshchika-ubytkov-neustojki-spb')
						],
						[
							'name' => 'Взыскание с заказчика',
							'link' => url('vzyskanie-ubytkov-s-zakazchika-po-dogovoru-podryada-konsultacii-yurist')
						],
						[
							'name' => 'Юридические услуги по взысканию',
							'link' => url('yuridicheskie-uslugi-po-vzyskaniyu-pomoshch-sud-advokat')
						],
						[
							'name' => 'Взыскание за некачественный ремонт',
							'link' => url('nekachestvennyj-remont')
						],
					],
				],
				[
					'name' => 'Взыскание ущерба',
					'items' => [
						[
							'name' => 'Взыскание материального ущерба',
							'link' => url('vzyskanie-materialnogo-ushcherba-zashchita-v-sude')
						],
						[
							'name' => 'Взыскание морального ущерба',
							'link' => url('vzyskanie-ushcherba-za-moralnyj-vred-polnaya-kompensaciya-konsultacii-besplatno-yurist')
						],
						[
							'name' => 'Взыскание ущерба от преступления',
							'link' => url('vzyskanie-ushcherba-ot-prestupleniya-kompensaciya-spb')
						],
						[
							'name' => 'Взыскание ущерба с работника',
							'link' => url('vzyskanie-ushcherba-s-rabotnika-sotrudnika-konsultacii-onlain-besplatno-yurist')
						],
						[
							'name' => 'Взыскание ущерба через суд',
							'link' => url('vzyskanie-ushcherba-sud-sankt-peterburg')
						],
					],
				],
			],
		],
	],
	[
		'name' => 'Автомобильное право',
		'items' => [
			'popular' => [
				[
					'name' => 'Взыскание ущерба после ДТП',
					'link' => url('vzyskanie-ushcherba-po-dtp-moralnaya-kompensaciya')
				],
				[
					'name' => 'Расторжение договора автокредита',
					'link' => url('rastorzhenie-dogovora-avtokredita')
				],
				[
					'name' => 'Автокредит. Защита в суде',
					'link' => url('sud-neuplata-avtokredita-bank-vozvrat-strahovki-pomoshch-yurist-advokat')
				],
				[
					'name' => 'Вождение в нетрезвом виде',
					'link' => url('vozhdenie-v-netrezvom-vide-pomoshch-yurista')
				],
				[
					'name' => 'Возврат водительских прав',
					'link' => url('dosrochnyj-vozvrat-voditel%27skih-prav-pomoshch-yurista')
				],
				[
					'name' => 'Возврат водительских прав в сложных случаях',
					'link' => url('vozvrashchenie-voditelskih-prav')
				],
			],
			'free' => [
				[
					'name' => 'Консультация автоюриста',
					'link' => url('avtomobilnyj-yurist-konsultaciya-besplatno')
				],
				[
					'name' => 'Консультация автомобильного юриста',
					'link' => url('avtomobilnyj-advokat')
				],
				[
					'name' => 'Юридическая помощь по ДТП',
					'link' => url('yuridicheskaya-pomoshch-dtp-spoy-gibdd-vozmeshchenie-ushcherba-sud')
				],
				[
					'name' => 'Консультация по автокредиту',
					'link' => url('konsultaciya-po-avtokreditu')
				],
			],
			'specialists_1' => [
				[
					'name' => 'Автомобильный юрист/',
					'link' => url('avtomobilnyj-advokat')
				],
				[
					'name' => 'юрист',
					'link' => url('avtomobilnyj-yurist-konsultaciya-besplatno')
				],
				[
					'name' => 'Юрист/',
					'link' => url('advokat-dtp-pomoshch-avariyah-yurist-konsultacii')
				],
				[
					'name' => 'юрист по ДТП',
					'link' => url('yurist-dtp-besplatno-pomoshch-avariya-konsultacii')
				],
				[
					'name' => 'Юрист по ПДД ',
					'link' => url('yurist-pdd-konsultaciya-spb')
				],
			],
			'specialists_2' => [
				[
					'name' => 'Консультация по ДТП',
					'link' => url('konsultaciya-po-dtp-pomoshch-yurista')
				],
				[
					'name' => 'Юридическая помощь по ДТП',
					'link' => url('yuridicheskaya-pomoshch-dtp-spoy-gibdd-vozmeshchenie-ushcherba-sud')
				],
				[
					'name' => 'Защита прав ДТП',
					'link' => url('dtp-zashchita-prav-vinovnika')
				],
			],
		],
	],
	[
		'name' => 'Пенсионное право',
		'items' => [
			'popular' => [
				[
					'name' => 'Пенсионные споры',
					'link' => url('pensionnye-spory')
				],
			],
			'free' => [],
			'specialists_1' => [
				[
					'name' => 'Пенсионный юрист',
					'link' => url('pensionnyj-yurist-konsultaciya')
				],
			],
			'specialists_2' => [
				[
					'name' => 'Консультация пенсионного юриста',
					'link' => url('pensionnyj-yurist-konsultaciya')
				],
			],
		],
	],
	[
		'name' => 'Авторское право',
		'items' => [
			'popular' => [
				[
					'name' => 'Нарушение авторских прав',
					'link' => url('narushenie-avtorskih-prav-st-146-advokaty-yuristy-spb-moskva')
				],
				[
					'name' => 'Защита патентного права',
					'link' => url('zashchita-patentnogo-prava-pomoshch-yurista')
				],
				[
					'name' => 'Защита интеллектуальной собственности',
					'link' => url('zashchita-intellektualnoj-sobstvennosti-pomoshch-yurista')
				],
			],
			'free' => [],
			'specialists_1' => [
				[
					'name' => 'Юрист/',
					'link' => url('yurist-avtorskomu-pravu-zashchita-pomoshch-konsultaciya')
				],
				[
					'name' => 'юрист по авторскому праву',
					'link' => url('avtorskoe-pravo-intellektualnaya-sobstvennost')
				],
			],
			'specialists_2' => [
				[
					'name' => 'Консультация по авторскому праву',
					'link' => url('besplatnye-yuridicheskie-konsultacii')
				],
			],
		],
	],
	[
		'name' => 'Семейное право',
		'items' => [
			'popular' => [
				[
					'name' => 'Расторжение брака',
					'link' => url('rastorzhenie-braka-pomoshch-yurista-2017')
				],
				[
					'name' => 'Раздел имущества',
					'link' => url('razdel-imushchestva-suprugov-pomoshch-yurista')
				],
				[
					'name' => 'Признание брака недействительным',
					'link' => url('priznanie-braka-nedejstvitelnym-pomoshch-yurista')
				],
				[
					'name' => 'Услуги по алиментам',
					'link' => url('uslugi-po-alimentam-pomoshch-yurista')
				],
				[
					'name' => 'Споры по алиментам',
					'link' => url('alimenty-spory-pomoshch-yurista')
				],
				[
					'name' => 'Установление отцовства',
					'link' => url('ustanovlenie-otcovstva-pomoshch-yurista')
				],
				[
					'name' => 'Оспаривание отцовства',
					'link' => url('osparivanie-otcovstva-pomoshch-yurista')
				],
				[
					'name' => 'Помощь по алиментам',
					'link' => url('pomoshch-po-alimentam-konsultaciya-yurist-spb')
				],
			],
			'free' => [
				[
					'name' => 'Консультация юриста по алиментам',
					'link' => url('konsultacija-yurista-po-alimentam-besplatno-onlain-spb')
				],
				[
					'name' => 'Семейные консультации юриста',
					'link' => url('semejnye-konsultacii-yurista-spb')
				],
			],
			'specialists_1' => [
				[
					'name' => 'Семейный юрист/',
					'link' => url('semejnyj-yurist-besplatnaya-konsultaciya')
				],
				[
					'name' => 'юрист',
					'link' => url('semejnyj-advokat-pomoshch-spb-moskva')
				],
				[
					'name' => 'Бракоразводный юрист/',
					'link' => url('brakorazvodnyj-yurist-besplatnaya-konsultaciya-spb')
				],
				[
					'name' => 'юрист',
					'link' => url('brakorazvodnyj-advokat-besplatnaya-konsultaciya-spb')
				],
				[
					'name' => 'Юрист по разводу/',
					'link' => url('yurist-po-razvodam-konsultaciya-besplatno-spb')
				],
				[
					'name' => 'юрист по разводу',
					'link' => url('brakorazvodnyj-advokat-besplatnaya-konsultaciya-spb')
				],
				[
					'name' => 'Юрист/',
					'link' => url('yurist-po-razdelu-imushchestva')
				],
				[
					'name' => 'юрист по разделу имущества',
					'link' => url('advokat-po-razdelu-imushchestva')
				],
				[
					'name' => 'Юрист/',
					'link' => url('yurist-po-alimentam-pomoshch')
				],
				[
					'name' => 'юрист по алиментам',
					'link' => url('advokat-po-alimentam-pomoshch')
				],
				[
					'name' => 'Юрист/',
					'link' => url('poryadok-obshcheniya-s-detmi-yurist-spb')
				],
				[
					'name' => 'юрист по определению порядка общения с детьми',
					'link' => url('poryadok-obshcheniya-s-detmi-advokat-spb')
				],
				[
					'name' => 'Юрист по лишению/восстановлению родительских прав',
					'link' => url('lishenie-vosstanovlenie-roditelskih-prav-sud-yurist')
				],
				[
					'name' => 'Юрист по по лишению/восстановлению родительских прав',
					'link' => url('lishenie-vosstanovlenie-roditelskih-prav-sud-advokat')
				],
			],
			'specialists_2' => [
				[
					'name' => ' ',
					'link' => url('')
				],
				[
					'name' => ' ',
					'link' => url('')
				],
				[
					'name' => 'Дополнительно по разводу и алиментам',
					'items' => [
						[
							'name' => 'Суд по алиментам',
							'link' => url('sud-po-alimentam-pomoshch-yurista')
						],
						[
							'name' => 'Сколько стоит развод?',
							'link' => url('skolko-stoit-razvod-pomoshch-yurista')
						],
						[
							'name' => 'Развод через суд',
							'link' => url('razvod-cherez-sud')
						],
						[
							'name' => 'Помощь юриста при разводе',
							'link' => url('razvod-yurist-pomoshch-spb')
						],
						[
							'name' => 'Оспаривание алиментов',
							'link' => url('osparivanie-alimentov-pomoshch-yurista')
						],
						[
							'name' => 'Консультация семейного юриста',
							'link' => url('konsultaciya-semejnogo-yurista-pomoshch')
						],
						[
							'name' => 'Развод с иностранным гражданином',
							'link' => url('razvod-s-inostrannym-grazhdaninom-pomoshch-yurista')
						],
					],
				],
			],
		],
	],
	[
		'name' => 'Миграционное право',
		'items' => [
			'popular' => [],
			'free' => [],
			'specialists_1' => [
				[
					'name' => 'Паспорт гражданина России',
					'link' => url('yurist-pasport-grazhdanina-rossii')
				],
			],
			'specialists_2' => [
				[
					'name' => 'Защита от депортации',
					'link' => url('advokat-po-delam-bezhencev')
				],
			],
		],
	],
	[
		'name' => 'Медицинское право',
		'items' => [
			'popular' => [
				[
					'name' => 'Врачебная ошибка',
					'link' => url('vrachebnaya-oshibka-pomoshch-yurist-advokat')
				],
			],
			'free' => [],
			'specialists_1' => [
				[
					'name' => 'Медицинский юрист/',
					'link' => url('medicinskij-yurist-konsultaciya-pomoshch-spb')
				],
				[
					'name' => 'юрист',
					'link' => url('medicinskij-advokat-voprosy-spory-konsultaciya')
				],
			],
			'specialists_2' => [
				[
					'name' => 'Дела в отношении психически нездоровых ',
					'link' => url('ugolovnye-dela-v-otnoshenii-psihicheski-nezdorovyh-yurist-advokat')
				],
			],
		],
	],
	[
		'name' => 'Лицензирование',
		'items' => [
			'popular' => [
				[
					'name' => 'Лицензирование медицинской деятельности',
					'link' => url('licenzirovanie-medicinskoj-deyatelnosti-spb')
				],
				[
					'name' => 'Лицензирование работы с опасными отходами',
					'link' => url('licenzirovanie-raboty-s-othodami-2017-spb')
				],
				[
					'name' => 'Лицензия на алкоголь',
					'link' => url('licenziya-na-alkogol-pomoshch-v-poluchenii')
				],
				[
					'name' => 'Получение лицензии МЧС',
					'link' => url('oformlenie-licenzii-mchs-yurist-spb')
				],
			],
			'free' => [],
			'specialists_1' => [
				[
					'name' => 'Помощь в получении лицензии',
					'link' => url('pomoshch-v-poluchenii-licenzii-na-osushchestvlenie')
				],
			],
			'specialists_2' => [],
		],
	],
	[
		'name' => 'Уголовное право',
		'items' => [
			'popular' => [
				[
					'name' => 'Уклонение от уплаты налогов',
					'link' => url('uklonenie-ot-uplaty-nalogov-sborov-organizacii-yurist')
				],
				[
					'name' => 'Незаконное предпренимательство',
					'link' => url('nezakonnoe-predprinimatelstvo-deyatelnost-biznes-shtraf-nakazanie')
				],
				[
					'name' => 'Хищение чужого имущества',
					'link' => url('advokat-po-delam-o-hishchenii-statya-158-159-pomoshch-sud-spb-moskva')
				],
				[
					'name' => 'Причинение вреда здоровью',
					'link' => url('umyshlennoe-prichinenie-vreda-zdorovyu-statya-111-112-113-114-115')
				],
				[
					'name' => 'Клевета',
					'link' => url('kleveta-oskorblenie-statya-128-1-ugolovnyj-advokat-spb-moskva')
				],
				[
					'name' => 'Завладение чужим имуществом',
					'link' => url('nezakonnoe-zavladenie-chuzhim-imushchestvom-ugolovnyj-yurist')
				],
			],
			'free' => [
				[
					'name' => 'Консультация уголовного юриста',
					'link' => url('ugolovnyj-advokat-uslugi-pomoshch')
				],
				[
					'name' => 'Консультация по уголовным вопросам',
					'link' => url('besplatnaya-yuridicheskaya-konsultaciya-advokata-po-ugolovnym-delam')
				],
				[
					'name' => 'Юридическая помощь по уголовному праву',
					'link' => url('yuridicheskie-uslugi-po-ugolovnomu-pravu-advokat-yurist')
				],
			],
			'specialists_1' => [
				[
					'name' => 'Уголовный/',
					'link' => url('ugolovnyj-advokat-uslugi-pomoshch')
				],
				[
					'name' => 'криминальный юрист',
					'link' => url('kriminalnyj-advokat-pomoshch-po-ugolovnomu-delu-yurist')
				],
				[
					'name' => 'Юрист по экономическим преступлениям',
					'link' => url('advokat-po-ehkonomicheskim-prestupleniyam-pomoshch-zashchita-sud')
				],
			],
			'specialists_2' => [
				[
					'name' => ' ',
					'link' => url('')
				],
				[
					'name' => ' ',
					'link' => url('')
				],
				[
					'name' => ' ',
					'link' => url('')
				],
				[
					'name' => 'Оборот наркотиков',
					'link' => url('ugolovnyj-advokat-po-narkotikam-statya-228-234')
				],
				[
					'name' => 'Оборот наркотиков',
					'link' => url('ugolovnyj-advokat-po-narkotikam-statya-228-234')
				],
				[
					'name' => 'Экономические преступления',
					'items' => [
						[
							'name' => 'Уклонение от уплаты налогов',
							'link' => url('uklonenie-ot-uplaty-nalogov-sborov-fizicheskimi-licami')
						],
						[
							'name' => 'Получениедача взятки',
							'link' => url('poluchenie-dacha-vzyatki-oborotni-v-pogonah-konsultaciya-advokata#')
						],
						[
							'name' => 'Незаконное получение кредита',
							'link' => url('nezakonnoe-poluchenie-kredita-statya-176-yurist-advokat-spb-moskva')
						],
						[
							'name' => 'Отмывание денег',
							'link' => url('advokat-po-otmyvaniyu-denezhnyh-sredstv-spb-moskva')
						],
						[
							'name' => 'Нецелевое расходование бюджетных средств',
							'link' => url('necelevoe-raskhodovanie-byudzhetnyh-sredstv-statya-285')
						],
						[
							'name' => 'Незаконное завладение чужим имуществом',
							'link' => url('nezakonnoe-zavladenie-chuzhim-imushchestvom-ugolovnyj-yurist')
						],
						[
							'name' => 'Злоупотребление полномочиями',
							'link' => url('zloupotreblenie-polnomochiyami-advokat-sud-spb-moskva')
						],
						[
							'name' => 'Растрата вверенного имущества',
							'link' => url('rastrata-chuzhogo-vverennogo-imushchestva-st-160-ugolovnyj-advokat#')
						],
						[
							'name' => 'Фиктивное банкротство',
							'link' => url('fiktivnoe-prednamerennoe-bankrotstvo-otvetstvennost-statya-197')
						],
					],
				],
				[
					'name' => 'Незаконное предпринимательство ',
					'items' => [
						[
							'name' => 'Незаконный экспорт',
							'link' => url('advokat-po-delam-nezakonnogo-ehksporta-iz-rossii-statya-189')
						],
						[
							'name' => 'Незаконная реализация табачных изделий',
							'link' => url('advokat-realizaciya-tabachnyh-izdelij-spb-moskva')
						],
						[
							'name' => 'Незаконный оборот древесины',
							'link' => url('hranenie-sbyt-nezakonno-priobretennoj-drevesiny-yurist-advokat')
						],
						[
							'name' => 'Незаконная банковская деятельность',
							'link' => url('nezakonnnaya-bankovskaya-deyatelnost-statya-172-ugolovnyj-advokat')
						],
						[
							'name' => 'Незаконное проведение азартных игр',
							'link' => url('nezakonnye-organizaciya-provedenie-azartnyh-igr-yurist')
						],
						[
							'name' => 'Контрабанда',
							'link' => url('advokat-po-kontrabande-statya-188-229-1-226-1-zashchita-v-sude-yuristy')
						],
					],
				],
				[
					'name' => 'Чужое имущество',
					'items' => [
						[
							'name' => 'Кража',
							'link' => url('krazha-imushchestva-statya-158-ugolovnyj-advokat-spb-moskva#')
						],
						[
							'name' => 'Грабеж',
							'link' => url('grabezh-statya-161-yuristy-spb-moskva')
						],
						[
							'name' => 'Разбой',
							'link' => url('razboj-statya-162-spb-moskva#')
						],
						[
							'name' => 'Уничтожение чужого имущества',
							'link' => url('advokat-po-delam-ob-unichtozhenii-chuzhogo-imushchestva-statya-167')
						],
					],
				],
				[
					'name' => 'Тяжкие преступления',
					'items' => [
						[
							'name' => 'Принуждение к действиям сексуального характера',
							'link' => url('prinuzhdenie-k-dejstviyam-seksualnogo-haraktera-statya-133-ugolovnyj-advokat')
						],
						[
							'name' => 'Изнасилование',
							'link' => url('iznasilovanie-statya-131-ugolovnyj-advokat-spb-moskva')
						],
						[
							'name' => 'Ст.132',
							'link' => url('nasilstvennye-dejstviya-seksualnogo-haraktera-statya-132-ugolovny-advokat')
						],
						[
							'name' => 'Ст.134',
							'link' => url('polovoe-snoshenie-licom-ne-dostigshim-shestnadcatiletnego-vozrasta-statya-134')
						],
						[
							'name' => 'Побои, истязания',
							'link' => url('poboi-istyazanie-statya-116-117-ugolovnyj-advokat-spb-moskva#')
						],
						[
							'name' => 'Развратные действия',
							'link' => url('razvratnye-dejstviya-statya-135-yuristy-advokaty')
						],
					],
				],
				[
					'name' => 'Должностные преступления',
					'items' => [
						[
							'name' => 'Халатность',
							'link' => url('halatnost-vrachej-policii-uchastkovogo-uchitelej-statya-293')
						],
						[
							'name' => 'Фальсификация финансовых документов	',
							'link' => url('falsifikaciya-dokumentov-finansovoj-otchetnosti-organizacii-statya-327')
						],
						[
							'name' => 'Оскорбление представителя власти',
							'link' => url('oskorblenie-predstavitelya-vlasti-statya-319-zashchita-sud-advokat')
						],
					],
				],
			],
		],
	],
];
