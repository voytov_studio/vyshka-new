<?php

use Illuminate\Database\Seeder;

/**
 * @author Cyrill Tekord
 */
class UsersTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		factory(\App\Models\User::class, 1)->create();
	}
}
