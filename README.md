# ВЫСШАЯ ИНСТАНЦИЯ

## Деплоймент

Устанавливаем пакеты:

```
npm i
```

Делаем сборку ассетов для продакшена:

```
npm run production
```

Замечу, что это собирает не все ассеты. Есть ассеты, которые были в исходных макетах, их собираем через gulp:

```
./node_modules/.bin/gulp css:production
./node_modules/.bin/gulp js:production
```

или одной командой:

```
./node_modules/.bin/gulp all:production
```

Теперь нужно еще раз вызвать `npm run production` чтобы обновить таймстемпы ассетов в файле `mix-manifest.json`, как собранных через mix, так и через gulp.
