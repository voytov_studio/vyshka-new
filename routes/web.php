<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SiteController@index')
	->name('site.home');

Route::get('/robots.txt', 'RobotsController');

Route::group([
	'prefix' => 'debug'
], function () {
	Route::get('posts/{postId}/dump', 'DebugController@dumpPost');
});

Route::group([
	'prefix' => 'api/web',
], function () {
	Route::match(['get', 'post'], '/content-loader', 'ChunkLoaderController@load')
		->name('api.contentLoader');

	Route::get('/search/services', 'SearchController@suggestService')
		->name('api.suggestService');

	route::post('/forms/contacts-via-sms', 'FormController@sendContactsViaSms')
		->name('api.web.forms.send-contacts-via-sms');

	//
});

Route::group([
	'prefix' => 'site'
], function () {
	Route::get('press', 'SiteController@press');

	Route::get('documents', 'SiteController@showDocuments')
		->name('site.showDocuments');

	Route::get('page-links/specializations/{id}', 'MenuController@showPageLinksBySpecialization')
		->name('site.showPageLinksBySpecialization');

	Route::get('page-links/services/{id}', 'MenuController@showPageLinksByService')
		->name('site.showPageLinksByService');
});

Route::get('/contacts', 'SiteController@contacts')
	->name('site.contacts');

Route::group([
	'prefix' => 'blog'
], function () {
	Route::get('/', 'BlogController@index')
		->name('blog.index');

	Route::get('/categories/{slug}', 'BlogController@indexByCategory')
		->name('blog.indexByCategory');

	Route::get('/{slug}', 'BlogController@showPost')
		->name('blog.posts.show');
});

Route::group([
	'prefix' => 'maintenance',
	'middleware' => ['doNotCacheResponse']
], function () {
	Route::get('/panel', 'MaintenanceController@showPanel');

	Route::get('/pages/{pageId}/edit', 'MaintenanceController@editPage')
		->name('servicePanels.pages.edit');

	Route::get('/pages/clear-cache', 'MaintenanceController@clearCache')
		->name('servicePanels.pages.clearCache');

	Route::get('/pages/invalidate-cache', 'MaintenanceController@invalidatePageCache')
		->name('servicePanels.pages.invalidateCache');

	Route::get('/crawler', 'MaintenanceController@crawler')
		->name('servicePanels.crawler');
});

\Illuminate\Support\Facades\Auth::routes();

Route::get('/{slug}', 'PageController@show')
	->name('pages.show');
