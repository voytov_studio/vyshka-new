const gulp = require('gulp');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const pipeline = require('readable-stream').pipeline;
const cleanCSS = require('gulp-clean-css');

let jsFilesToConcatenate = [
    './public/js/jquery.easy-autocomplete.js',
    './public/js/owl.carousel.min.js',
    './public/js/datepicker.min.js',
    './public/js/jquery.fancybox.js',
    './public/js/jquery.tabs.js',
    './public/js/jquery.maskedinput.js',
    './public/js/jquery.accordion.js',
    './public/js/jquery.openclose.js',
    './public/js/parsley.js',
    './public/js/main.js',
    './public/js/App.Forms.js',
    './public/js/App.LazyContent.js',
    './public/js/App.Goals.js',
    './public/js/App.Widgets.js',
];

let cssFilesToConcatenate = [
    './public/css/jquery.fancybox.css',
    './public/css/easy-autocomplete.css',
    './public/css/base.css',
    './public/css/all.css',
];

function css_concatenate() {
    return gulp.src(cssFilesToConcatenate)
        .pipe(concat('theme.css'))
        .pipe(gulp.dest('./public/build/'));
}

gulp.task('css:concatenate', css_concatenate);

gulp.task('css:watch', gulp.series([css_concatenate, () => {
    gulp.watch('./public/css/*.css', css_concatenate);
}]));

gulp.task('css:minify', () => {
    return gulp.src('./public/build/theme.css')
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('./public/build/'));
});

gulp.task('css:production', gulp.series(['css:concatenate', 'css:minify']));

gulp.task('js:concatenate', function() {
    return gulp.src(jsFilesToConcatenate)
        .pipe(concat('theme.js'))
        .pipe(gulp.dest('./public/build/'));
});

gulp.task('js:compress', function () {
    return pipeline(
        gulp.src('./public/build/theme.js'),
        uglify(),
        gulp.dest('./public/build/')
    );
});

gulp.task('js:production', gulp.series(['js:concatenate', 'js:compress']));

gulp.task('all:production', gulp.series([
    'css:production',
    'js:production',
]));
